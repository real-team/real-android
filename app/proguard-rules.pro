# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/britt/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}



# Gson

# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature
# Gson deserialization uses introspection to get the json entry names for the field names,
# so we do not want to change them.
-keep class com.joinreal.model.** { *; }

-keep class com.crashlytics.** { *; }

-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewInjector { *; }


-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

-keepclassmembers class ** {
    @com.squareup.otto.Subscribe public *;
    @com.squareup.otto.Produce public *;
}

-dontwarn com.crashlytics.**
-dontwarn okio.**
-dontwarn org.xbill.DNS.spi.**

#QUICKBLOX
-keep class org.jivesoftware.smack.** { *; }
-keep class org.jivesoftware.smackx.** { *; }
-keep class com.quickblox.** { *; }
-keep class * extends org.jivesoftware.smack { *; }
-keep class * implements org.jivesoftware.smack.debugger.SmackDebugger { *; }


-keep class se.emilsjolander.** { *; }
-dontwarn se.emilsjolander.**

-keep class android.support.design.widget.** { *; }
-keep interface android.support.design.widget.** { *; }
-dontwarn android.support.design.**

-keep class com.android.support.design.widget.** { *; }
-keep interface com.android.support.design.widget.** { *; }
-dontwarn com.android.support.design.**

-keepattributes JavascriptInterface