package com.joinreal.app.interfaces;

import android.view.View;

import com.joinreal.app.Application;
import com.joinreal.app.adapters.SearchResultsAdapter;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.Helper;
import com.joinreal.events.ShouldOpenFavoritesEvent;
import com.joinreal.model.PropertyIds;
import com.joinreal.tasks.UpdateFavotiresTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/21/15.
 */
public class UpdateFavoritesHelper implements SearchResultsAdapter.UpdateFavoritesListener {

    private static UpdateFavoritesHelper instance;

    private List<PropertyIds> propertiesToBeAddedToFavorites;
    private List<PropertyIds> propertiesToBeRemovedFromFavorites;

    public static UpdateFavoritesHelper getInstance() {
        if (instance == null) {
            instance = new UpdateFavoritesHelper();
        }
        return instance;
    }

    private UpdateFavoritesHelper() {
        this.propertiesToBeAddedToFavorites = new ArrayList<>();
        this.propertiesToBeRemovedFromFavorites = new ArrayList<>();
    }


    @Override
    public void removeFromFavorites(String mlsId, String mlsNum, final View parentView, String screenName) {
        PropertyIds property = new PropertyIds(mlsId, mlsNum);
        if (!propertiesToBeAddedToFavorites.remove(property)) {
            propertiesToBeRemovedFromFavorites.add(property);
        }
        Helper.getSnackbar(Application.getAppContext(), parentView, "Item removed from Favorites", getOnClickListener(screenName)).show();

    }

    @Override
    public void addToFavorites(String mlsId, String mlsNum, final View parentView, String screenName) {
        final View.OnClickListener listener = getOnClickListener(screenName);
        PropertyIds property = new PropertyIds(mlsId, mlsNum);
        if (!propertiesToBeRemovedFromFavorites.remove(property)) {
            propertiesToBeAddedToFavorites.add(property);
        }
        Helper.getSnackbar(Application.getAppContext(), parentView, "Item added to Favorites", listener).show();

    }

    private View.OnClickListener getOnClickListener(final String screenName) {
        return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BusProvider.getBus().post(new ShouldOpenFavoritesEvent());
                    Analytics.getInstance().trackSnackbarNavigation(screenName, "Favorites");
                }
            };
    }

    public void updateChanges() {
        System.out.println("updateChanges");
        new UpdateFavotiresTask(new ArrayList(propertiesToBeAddedToFavorites), new ArrayList(propertiesToBeRemovedFromFavorites))
                .execute();
        propertiesToBeAddedToFavorites.clear();
        propertiesToBeRemovedFromFavorites.clear();
    }

}
