package com.joinreal.app.interfaces;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.joinreal.app.common.TypefaceManager;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.ViewModelItem;

/**
 * Created by britt on 6/3/15.
 */
public abstract class ViewHolderAbstract extends RecyclerView.ViewHolder{

    public ViewHolderAbstract(View itemView) {
        super(itemView);
        findViewsByIds(itemView);
        TypefaceManager.getInstance(itemView.getContext()).assignTypeface(itemView);
    }

    protected abstract void findViewsByIds(View itemView);

    public abstract void onBindView(ViewModelItem viewModelItem);
}
