package com.joinreal.app.interfaces;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brittbarak on 6/30/15.
 */
public interface PropertySummaryLayoutSetter {

    public String getMlsId();
    public String getMlsNum();
    public String getFullAddress();
    public String getFormattedPrice();
    public Double getBeds();
    public Double getBaths();
    public ArrayList<String> getPhotosMeduim();
    public Double getLatitude();
    public Double getLongitude();
    public boolean isFavorite();
    public void setFavorite(boolean favorite);
    public String getFirstPhoto();
    public String getStaticMapUrl();


}
