package com.joinreal.app.interfaces;

import android.os.Bundle;

import com.joinreal.model.data.SearchFilter;

/**
 * Created by britt on 6/16/15.
 */
public interface OpenSearchFiltersListener {

    void openMapViewFragment();

    void openPropertyListViewFragment();

    void openFiltersActivityForContact(String customerId, String displayName, SearchFilter filter);

    //TODO: is it the right listener?
    void openProperyListFragment(Bundle bd);

    void openFiltersActivity(boolean shouldResetFilter);

    void openFavoritesFragment(Bundle bd);
}
