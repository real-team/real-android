package com.joinreal.app.interfaces;

import com.joinreal.model.view.ContactVM;

import java.util.ArrayList;

/**
 * Created by britt on 6/16/15.
 */
public interface AgentActionsListener extends INavigationListener {
    void openChatDialogsFragment();

    void openContactInfoFragment(ContactVM contactVM);

//    void openNotesFragment(String notes, String login);

    void openInviteContactsFragment();

    void openInviteContactsFragment(String entryTagToReturnTo);

    void openInviteNewContactFragment(String entryTagToReturnTo);

//    void openContactsFragment(String entryTagToReturnTo);

    void openSendInvitesFragment(ArrayList<String> contactsToInvite, String entryTagToReturnTo);

    void openSendInvitesFragment(ArrayList<String> contactsToInvite, boolean shouldOnlySendSms, String entryTagToReturnTo);

    void openInvitationSentFragment(int size, String entryTagToReturnTo);

    void openInvitationSentFragment(String displayName, String entryTagToReturnTo);

    void openEditAgentProfileFragment();

    void lockDrawer(String title);

    void lockDrawer();

    void unlockDrawer(String title);

    void unlockDrawer();

}
