package com.joinreal.app.interfaces;

import com.joinreal.model.data.User;

/**
 * Created by britt on 6/1/15.
 */
public interface ApiCallbackInterface {
    public void onSuccess(String json);
    public void onFailure(Throwable throwable);
    public void onWrongPassword(Throwable throwable);
}
