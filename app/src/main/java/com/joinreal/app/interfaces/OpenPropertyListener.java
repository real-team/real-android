package com.joinreal.app.interfaces;

import com.joinreal.events.OpenPropertyEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/24/15.
 */
public interface OpenPropertyListener {
    public void openPropertyPage(String mlsid, String mlsnum, ArrayList<String> photos);

    }
