package com.joinreal.app.interfaces;

import android.text.TextWatcher;

import com.joinreal.model.data.SearchFilter;
import com.joinreal.model.view.ContactVM;

import java.util.ArrayList;

/**
 * Created by britt on 6/16/15.
 */
public interface ISearchToolbarListener{

    void onSearchActionClick();

    void onCancelSearchClick();

    void addTextChangedListener(TextWatcher textWatcher);

    void removeTextChangedListener(TextWatcher textWatcher);
}
