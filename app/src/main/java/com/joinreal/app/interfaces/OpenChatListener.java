package com.joinreal.app.interfaces;

import android.content.Intent;

import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.PropertyViaChatVM;

import java.util.ArrayList;

/**
 * Created by britt on 6/23/15.
 */
public interface OpenChatListener {
    void sendPropertyViaChat(PropertyViaChatVM property);

    void openPrivateChat(Intent intent);

    void openPrivateChat(ContactVM contactVM);

    void getDialogsFromNetwork();
}
