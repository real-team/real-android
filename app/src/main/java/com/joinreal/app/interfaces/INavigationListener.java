package com.joinreal.app.interfaces;

import android.text.TextWatcher;

/**
 * Created by britt on 6/16/15.
 */
public interface INavigationListener {

    void setToolbarTitle(String title);

    void setHomeIcon(int drawableId);

    void alignContainerAndToolbarTop();

    void containerBelowToolbar();

    void sendSupportEmail();

    void openWebviewFragment(String url);

    void openCropImageFragment();
}
