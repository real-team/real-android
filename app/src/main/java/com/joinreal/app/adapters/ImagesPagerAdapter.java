package com.joinreal.app.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.joinreal.R;
import com.joinreal.app.common.BusProvider;
import com.joinreal.events.OpenPropertyEvent;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/17/15.
 */
public class ImagesPagerAdapter extends PagerAdapter {
    Context context;
    LayoutInflater mLayoutInflater;
    ArrayList<String> photos;
    private String mlsId;
    private String mlsnum;
    private boolean isPagerClickable;

    public ImagesPagerAdapter(Context context, ArrayList<String> photos, String mlsId, String mlsnum, boolean isPagerClickable) {
        this.context = context;
        this.photos = photos;
        this.mlsId = mlsId;
        this.mlsnum = mlsnum;
        this.isPagerClickable = isPagerClickable || ((mlsId != null) && (mlsnum != null));
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ImagesPagerAdapter(Context context, ArrayList<String> photos, String mlsId, String mlsnum) {
        this(context, photos, mlsId, mlsnum, true);
    }

    public ImagesPagerAdapter(Context context, ArrayList<String> photos) {
        this(context, photos, null, null, false);
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View v = mLayoutInflater.inflate(R.layout.image_for_pager, container, false);
        ImageView imageView = (ImageView) v.findViewById(R.id.imageview_for_pager);
        Picasso.with(context).load(photos.get(position)).into(imageView);
        container.addView(v);
        if(isPagerClickable) {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BusProvider.getBus().post(new OpenPropertyEvent(mlsId, mlsnum, photos));
                }
            });
        }
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }

    public ArrayList<String> getItemList() {
        return photos;
    }
}
