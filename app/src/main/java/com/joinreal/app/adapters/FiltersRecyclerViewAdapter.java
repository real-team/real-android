package com.joinreal.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinreal.R;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.viewholders.GridViewItemHolderAbstract;
import com.joinreal.model.view.FilterViewType;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.RecyclerFilterItemVM;
import com.joinreal.model.view.RecyclerFiltersVM;
import com.squareup.otto.Subscribe;

import java.util.List;

/**
 * Created by britt on 6/2/15.
 */
public class FiltersRecyclerViewAdapter extends RecyclerView.Adapter<GridViewItemHolderAbstract>{
    private final Context context;
    private final List<RecyclerFilterItemVM> items;
    private final int layoutId;

    public FiltersRecyclerViewAdapter(Context context, FiltersViewModelItem viewModelItem) {
        this.context = context;
        this.items = ((RecyclerFiltersVM)viewModelItem).getItems();
        this.layoutId = viewModelItem.getViewType() == FilterViewType.GRID ? R.layout.item_grid_filter : R.layout.item_checklist_filter;
        BusProvider.getBus().register(this);

    }

    @Subscribe
    public void test(String s) {
        // TODO: React to the event somehow!
        System.out.println(getClass().getSimpleName() + " otto test : " + s);
    }

    @Override
    public GridViewItemHolderAbstract onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new GridViewItemHolderAbstract(context, view);
    }

    @Override
    public void onBindViewHolder(final GridViewItemHolderAbstract holder, int position) {
        holder.onBindView(items.get(position));
    }

    @Override
    public int getItemCount() {
        return (items == null) ? 0 : items.size();
    }

}
