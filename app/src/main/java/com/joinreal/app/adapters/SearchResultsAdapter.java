package com.joinreal.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.app.viewholders.SearchResultsViewHolder;
import com.joinreal.app.viewholders.SearchResultsViewHolderAgent;
import com.joinreal.app.viewholders.SearchResultsViewHolderCustomer;
import com.joinreal.model.data.PropertyThin;

import java.util.List;

/**
 * Created by britt on 6/15/15.
 */
public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsViewHolder> {

    private final Context context;
    private final String screenName;
    private List<PropertyThin> properties;
    private final OpenChatListener openChatListener;
    private UpdateFavoritesListener favoritesListener;

    public interface UpdateFavoritesListener{
        void removeFromFavorites(String mlsId, String mlsNum, final View parentView, String screenName);

        void addToFavorites(String mlsId, String mlsNum, final View parentView, String screenName);
    }

    public SearchResultsAdapter(Context context, UpdateFavoritesListener favoritesListener, List<PropertyThin> properties, OpenChatListener openChatListener, String screenName) {
        this.context = context;
        this.properties = properties;
        this.favoritesListener = favoritesListener;
        this.openChatListener = openChatListener;
        this.screenName = screenName;
    }

    @Override
    public SearchResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_property_list, parent, false);
        View parentView = ((Activity)context).findViewById(android.R.id.content);
        if (Application.getClient().isAgent()) {
            return new SearchResultsViewHolderAgent(view, favoritesListener, parentView, openChatListener, screenName);
        } else {
            return new SearchResultsViewHolderCustomer(view, favoritesListener, parentView, openChatListener, screenName);
        }
    }


    @Override
    public void onBindViewHolder(SearchResultsViewHolder holder, int position) {
        PropertyThin property = properties.get(position);
        holder.onBindView(property);
    }

    @Override
    public int getItemCount() {
        return properties.size();
    }

}
