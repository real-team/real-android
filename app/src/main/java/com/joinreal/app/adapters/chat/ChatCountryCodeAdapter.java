package com.joinreal.app.adapters.chat;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.joinreal.R;
import com.joinreal.model.data.chat.ChatCountryCode;

import java.util.ArrayList;
import java.util.Collections;


public class ChatCountryCodeAdapter extends BaseAdapter {

    private ArrayList<ChatCountryCode> mData = new ArrayList<ChatCountryCode>();
    private EventListener event;
    private LayoutInflater inflater;
    private int selected = -1;

    public ChatCountryCodeAdapter(Context context, ArrayList<ChatCountryCode> list) {
        super();
        mData = list;
        Collections.sort(mData);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public void setListener(EventListener listener){
        event = listener;
    }

    public interface EventListener {

        void click(ChatCountryCode chatCountryCode);

    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public ChatCountryCode getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.chat_country_code, viewGroup, false);
            countryCode = (TextView) convertView;
            convertView.setTag(countryCode);
        }
        else {
            countryCode = (TextView) convertView.getTag();
        }

        countryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (event!=null){
                    event.click(mData.get(position));
                }
                selected = position;
                notifyDataSetChanged();
            }
        });

        if (position == selected){
            SpannableString spanString = new SpannableString("(" + mData.get(position).getCode()+") "+mData.get(position).getCountry());
            spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
            spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
            countryCode.setText(spanString);
        }
        else {
            countryCode.setText("(" +mData.get(position).getCode()+") "+mData.get(position).getCountry());
        }

        return convertView;
    }

    private static TextView countryCode;

}
