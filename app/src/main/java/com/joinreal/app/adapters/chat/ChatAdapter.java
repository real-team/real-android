package com.joinreal.app.adapters.chat;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.chat.PrivateChatActivity;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.OpenPropertyListener;
import com.joinreal.model.data.chat.ChatMessageData;
import com.joinreal.utils.ChatDisplayUtil;
import com.joinreal.utils.ChatImagesUtils;
import com.joinreal.utils.TimeStringUtils;
import com.joinreal.utils.RoundedTransformation;
import com.quickblox.chat.model.QBChatMessage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

import static com.joinreal.utils.ChatDisplayUtil.dp;


public class ChatAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private final static boolean HIDE_AVATARS = true;

    private static final int OUTGOING_MESSAGE = 0;
    private static final int INCOMING_MESSAGE = 1;
    private static final int PROPERTY_MESSAGE = 2;
    private static final int SYSTEM_MESSAGE = 3;

    private static final String TYPE_LEAD = "lead";
    private static final String TYPE_PROPERTY = "property";
    private static final String TYPE_DEFAULT = "null";
    private static final String VISIBLE = "visible";

    private final OpenPropertyListener openPropertyListener;

    private List<ChatMessageData> chatMessages;
    private Calendar calendar = Calendar.getInstance();
    private LayoutInflater vi;
    private ChatImagesUtils.IImageObject contact;
    private boolean userInTheApp = true;

    public void setContact(ChatImagesUtils.IImageObject contact) {
        this.contact = contact;
    }

    public void clearLocalMessages(long lastNewMsgDateSent) {
        if (chatMessages != null && chatMessages.size() > 0) {
            int i = 0;
            while (i < chatMessages.size() && chatMessages.get(i).getDateSent() < lastNewMsgDateSent) {
                i++;
            }
            if (i < chatMessages.size()) {
                for (int j = 1; j <= chatMessages.size() - i; j++) {
                    chatMessages.remove(chatMessages.size() - 1);
                }
            }
            //a patch for bug fix of duplicated first message.
            // problem is the date sent received when actually sending the first message (and adding it to the adapter)
            // is different then the date sent received on callback QBEntityCallbackImpl (on QBChatService.getDialogMessages)
            if ((chatMessages.size() == 1) && (chatMessages.get(0).getDateSent() == lastNewMsgDateSent - 1)) {
                chatMessages.remove(0);
            }
        }
        notifyDataSetChanged();
    }

    public void markMessagesRead() {
        if (chatMessages != null) {
            for (ChatMessageData chatMessageData : chatMessages) {
                chatMessageData.setIsRead(true);
            }
            notifyDataSetChanged();
        }
    }

    public ChatAdapter(List<ChatMessageData> chatMessages, OpenPropertyListener openPropertyListener) {
        this.chatMessages = chatMessages;
        this.openPropertyListener = openPropertyListener;
        vi = (LayoutInflater) Application.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setuserInTheApp(boolean intheapp) {
        userInTheApp = intheapp;
    }

    public List<ChatMessageData> getMessages() {
        return chatMessages;
    }

    public void setMessages(List<ChatMessageData> messages) {
        chatMessages = messages;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (chatMessages != null) {
            return chatMessages.size();
        } else {
            return 0;
        }
    }

    @Override
    public ChatMessageData getItem(int position) {
        if (chatMessages != null) {
            return chatMessages.get(position);
        } else {
            return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        String msgType = getItem(position).getMsgType();
        boolean isOutgoing = getItem(position).isOutgoing();
        if ((msgType != null) && (msgType.equals(TYPE_LEAD))) {
            return SYSTEM_MESSAGE;
        } else if ((getItem(position).getImageUrl() != null) ||
                ((msgType != null) && (msgType.equals(TYPE_PROPERTY)))) {
            return PROPERTY_MESSAGE;
        } else if ((msgType == null) || (msgType.equals(TYPE_DEFAULT))) {
            if (isOutgoing) {
                return OUTGOING_MESSAGE;
            } else {
                return INCOMING_MESSAGE;
            }
        }
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ChatMessageData chatMessage = getItem(position);
        switch (getItemViewType(position)) {
            case OUTGOING_MESSAGE:
                convertView = outgoingMessageView(convertView, chatMessage);
                break;
            case PROPERTY_MESSAGE:
                convertView = imageMessageView(convertView, chatMessage);
                break;
            case INCOMING_MESSAGE:
                convertView = incomingMessageView(convertView, chatMessage);
                break;
            case SYSTEM_MESSAGE:
                convertView = getSystemMessageView(convertView, chatMessage);
        }
        TypefaceManager.getInstance(parent.getContext()).assignTypeface(convertView);
        return convertView;
    }

    private View getSystemMessageView(View convertView, final ChatMessageData chatMessage) {
        convertView = vi.inflate(R.layout.chat_listview_header, null);
        TextView tv = (TextView) convertView.findViewById(R.id.text);
        tv.setText(chatMessage.getBody());
        return convertView;
    }


    private View imageMessageView(View convertView, final ChatMessageData chatMessage) {
        ImageViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(R.layout.chat_image_layout, null);
            holder = createImageViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ImageViewHolder) convertView.getTag();
        }

        String url = chatMessage.getImageUrl();
//        ChatImage.getinstance().displayImage(url, holder.image, ChatImage.getOptions());
        Picasso.with(convertView.getContext()).load(url).transform(new RoundedTransformation(14, 0)).into(holder.image);
        int resId = chatMessage.isOutgoing() ? R.drawable.outgoing_message_background : R.drawable.incoming_message_background;
        holder.bg.setBackground(Helper.getDrawable(convertView.getContext(), resId));
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: pass all photos
                ArrayList<String> photos = new ArrayList<String>();
                photos.add(chatMessage.getImageUrl());
                openPropertyListener.openPropertyPage(chatMessage.getProperty(StaticParams.KEY_MLS_ID), chatMessage.getProperty(StaticParams.KEY_MLS_NUM), photos);
                Analytics.getInstance().trackEvent("Private Chat Activity", "Clicked on property message");

            }
        });

        return convertView;
    }


    private View incomingMessageView(View convertView, ChatMessageData chatMessage) {
        IncomingViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(R.layout.chat_incoming_message, null);
            holder = createIncomingViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (IncomingViewHolder) convertView.getTag();
        }

        if (!chatMessage.isShowAvatar() && HIDE_AVATARS) {
            holder.photo.setVisibility(View.GONE);
        } else {
            holder.photo.setVisibility(View.VISIBLE);
        }

        if (contact != null && contact.getBitmap() != null) {
            holder.photo.setImageBitmap(contact.getBitmap());
        } else {
            holder.photo.setImageDrawable(Application.getAppContext().getResources().getDrawable(R.drawable.contact_icon));
        }

        holder.textMessage.setText(chatMessage.getBody());
        holder.time.setText(TimeStringUtils.secondsToTimeMessage(chatMessage.getDateSent()));
        return convertView;
    }

    private View outgoingMessageView(View convertView, ChatMessageData chatMessage) {
        OutgoingViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(R.layout.chat_outgoing_message, null);
            holder = createOutgoingViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (OutgoingViewHolder) convertView.getTag();
        }

        if (!chatMessage.isShowAvatar() && HIDE_AVATARS) {
            holder.photo.setVisibility(View.GONE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.textLayout.setLayoutParams(params);
        } else {
            holder.photo.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.LEFT_OF, R.id.outgoing_photo);
            holder.textLayout.setLayoutParams(params);
        }

        holder.textMessage.setText(chatMessage.getBody());
        holder.time.setText(TimeStringUtils.secondsToTimeMessage(chatMessage.getDateSent()));
        if (chatMessage.isRead()) {
            holder.seenV.setImageDrawable(Application.getAppContext().getResources().getDrawable(R.drawable.check_double_green));
        } else {
            holder.seenV.setImageDrawable(Application.getAppContext().getResources().getDrawable(R.drawable.check_double_gray));
        }
        if (!userInTheApp) {
            holder.seenV.setImageDrawable(Application.getAppContext().getResources().getDrawable(R.drawable.sent_email));
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.seenV.getLayoutParams();
            params.setMargins((int) (4 * dp()), (int) (8 * dp()), (int) (4 * dp()), (int) (1 * dp()));
        }
        return convertView;
    }

    public void add(QBChatMessage message) {
        if (chatMessages == null) chatMessages = new ArrayList<>();
        String visibility = message.getProperty(VISIBLE);
        if ((visibility == null) || (visibility.equals(Application.getCurrentChatUser().getId().toString()))) {
            chatMessages.add(new ChatMessageData(message));
            notifyDataSetChanged();
        }
    }

    public void add(List<QBChatMessage> messages) {
        for (QBChatMessage message : messages) {
            add(message);
        }
    }

    public void update(List<QBChatMessage> messages) {
        chatMessages = new ArrayList<>();
        add(messages);
    }

    private IncomingViewHolder createIncomingViewHolder(View v) {
        IncomingViewHolder holder = new IncomingViewHolder();
        holder.textMessage = (TextView) v.findViewById(R.id.incoming_text);
        holder.textMessage.setTypeface(Typeface.createFromAsset(Application.getAppContext().getAssets(), Application.GOTHAM_NARROW_BOOK));
        holder.time = (TextView) v.findViewById(R.id.incoming_time);
        holder.time.setTypeface(Typeface.createFromAsset(Application.getAppContext().getAssets(), Application.GOTHAM_NARROW_LIGHT));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (ChatDisplayUtil.getScreenWidth() / 2 - 20), ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.BELOW, R.id.incoming_text);
        layoutParams.setMargins(0, (int) (6 * dp()), 0, 0);
        holder.time.setLayoutParams(layoutParams);
        holder.photo = (ImageView) v.findViewById(R.id.incoming_photo);
        //   holder.textLayout = v.findViewById(R.id.incoming_text_layout);
        return holder;
    }

    private OutgoingViewHolder createOutgoingViewHolder(View v) {
        OutgoingViewHolder holder = new OutgoingViewHolder();
        holder.textMessage = (TextView) v.findViewById(R.id.outgoing_text);
        holder.textMessage.setTypeface(Typeface.createFromAsset(Application.getAppContext().getAssets(), Application.GOTHAM_NARROW_BOOK));
        holder.time = (TextView) v.findViewById(R.id.outgoing_time);
        holder.time.setTypeface(Typeface.createFromAsset(Application.getAppContext().getAssets(), Application.GOTHAM_NARROW_LIGHT));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (ChatDisplayUtil.getScreenWidth() / 2 - 30), ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.BELOW, R.id.outgoing_text);
        layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.is_checked);
        layoutParams.setMargins(0, (int) (6 * dp()), 0, 0);
        holder.time.setLayoutParams(layoutParams);
        holder.seenV = (ImageView) v.findViewById(R.id.is_checked);
        holder.photo = (ImageView) v.findViewById(R.id.outgoing_photo);
        holder.textLayout = v.findViewById(R.id.outgoing_text_layout);
        return holder;
    }

    private ImageViewHolder createImageViewHolder(View v) {
        ImageViewHolder holder = new ImageViewHolder();
//        holder.timeTV = (TextView) v.findViewById(R.id.image_message_time);
//        holder.seenV = (ImageView) v.findViewById(R.id.image_seen_v);
//        holder.photo = (ImageView) v.findViewById(R.id.senderAnonymousImage);
        holder.image = (ImageView) v.findViewById(R.id.chat_image);
        holder.bg = v.findViewById(R.id.chat_image_bg);
        return holder;
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater vi = (LayoutInflater) Application.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = vi.inflate(R.layout.chat_listview_header, null);
        }
        TextView textView = (TextView) view.findViewById(R.id.text);
        textView.setText(TimeStringUtils.secondsToDateHeader(getItem(i).getDateSent()));
        textView.setTypeface(Typeface.createFromAsset(Application.getAppContext().getAssets(), Application.GOTHAM_NARROW_LIGHT));
        return view;
    }

    @Override
    public long getHeaderId(int i) {
        calendar.setTimeInMillis(getItem(i).getDateSent() * 1000);
        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    private static class IncomingViewHolder {
        public TextView textMessage;
        public TextView time;
        public ImageView photo;
        public ImageView image;
        public View textLayout;
    }

    private static class OutgoingViewHolder {
        public TextView textMessage;
        public TextView time;
        public ImageView seenV;
        public ImageView photo;
        public ImageView image;
        public View textLayout;
    }

    private static class ImageViewHolder {
        public TextView textMessage;
        public TextView time;
        public ImageView seenV;
        public ImageView photo;
        public ImageView image;
        public View bg;
    }
}
