package com.joinreal.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.common.ViewTypes;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.app.presenters.FiltersButtonsPresenter;
import com.joinreal.app.viewholders.PropertyFieldViewHolder;
import com.joinreal.app.viewholders.PropertySubcategoryViewHolder;
import com.joinreal.model.view.OverviewVM;
import com.joinreal.model.view.PropertyFieldVM;
import com.joinreal.model.view.PropertySubcategoryVM;
import com.joinreal.model.view.ViewModelItem;

import java.io.IOException;
import java.util.List;

/**
 * Created by brittbarak on 6/30/15.
 */
public class PropertyFeaturesAdapter extends RecyclerView.Adapter<ViewHolderAbstract> {
    private final Context context;
    private final List<ViewModelItem> innerItems;

    public PropertyFeaturesAdapter(Context context, List<ViewModelItem> subcategories) {
        this.context = context;
        this.innerItems = subcategories;
    }

    @Override
    public int getItemViewType(int position) {
        return innerItems.get(position).getTypeIntCode();
    }

    @Override
    public ViewHolderAbstract onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ViewTypes.PROPERTY_SUBCATEGORY_VIEW_TYPE:
                return PropertySubcategoryViewHolder.getNewViewHolder(parent);
            case ViewTypes.PROPERTY_FIELD_VIEW_TYPE:
                return PropertyFieldViewHolder.getNewViewHolder(parent);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolderAbstract holder, int position) {
        holder.onBindView(innerItems.get(position));
    }

    @Override
    public int getItemCount() {
        return (innerItems == null) ? 0 : innerItems.size();
    }






}
