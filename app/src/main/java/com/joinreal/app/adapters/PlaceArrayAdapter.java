package com.joinreal.app.adapters;

/**
 * Created by britt on 6/8/15.
 */
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceTypes;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.LocationTracker;
import com.joinreal.app.common.TypefaceManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class PlaceArrayAdapter
        extends BaseAdapter implements Filterable {
    private final Context context;
    private GoogleApiClient mGoogleApiClient;
    private AutocompleteFilter mPlaceFilter;
    private LatLngBounds mBounds;
    private ArrayList<PlaceAutocomplete> mResultList;

    private static String[] locationTypes = Application.getAppContext().getResources().getStringArray(R.array.location_types);
    private static String[] locationTypesExt = Application.getAppContext().getResources().getStringArray(R.array.location_types_ext);

    /**
     * Constructor
     *  @param context  Context
     * @param bounds   Used to specify the search bounds
     * @param filter   Used to specify place types
     */
    public PlaceArrayAdapter(Context context, LatLngBounds bounds,
                             AutocompleteFilter filter) {
        this.context = context;
        mBounds = bounds;
        mPlaceFilter = filter;
    }



    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            mGoogleApiClient = null;
        } else {
            mGoogleApiClient = googleApiClient;
        }
    }

    @Override
    public int getCount() {
        return (mResultList == null) ?  0 : mResultList.size();
    }

    @Override
    public PlaceAutocomplete getItem(int position) {
        return mResultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.location_list_item, parent, false);
        TextView t = (TextView)rowView.findViewById(R.id.textview);
        t.setText(mResultList.get(position).description);
        int colorId = (position == 0) ? R.color.blue_dark : R.color.black;
        t.setTextColor(context.getResources().getColor(colorId));
        TypefaceManager.getInstance(context).assignTypeface(t);
        return rowView;
    }

    private ArrayList<PlaceAutocomplete> getPredictions(CharSequence constraint) {
        if (mGoogleApiClient != null) {
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                    mBounds, mPlaceFilter);
            // Wait for predictions, set the timeout.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);
            final Status status = autocompletePredictions.getStatus();
            if (! status.isSuccess()) {
                Crashlytics.log("Error getting place predictions: " + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());
            while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
                int typeIndex = prediction.getPlaceTypes().get(0);
                resultList.add(new PlaceAutocomplete(prediction.getPlaceId(), prediction.getDescription(), getLocationTypeString(typeIndex)));
            }
            // Buffer release
            autocompletePredictions.release();
            return resultList;
        }
        Crashlytics.logException(new Throwable("Error getting place predictions: Google API client is not connected."));
        return null;
    }

    private String getLocationTypeString(int typeIndex) {
        if (typeIndex < 1000) {
            if (typeIndex < locationTypes.length) {
                return locationTypes[typeIndex];
            }
        } else {
            typeIndex -= 1001;
            if (typeIndex < locationTypesExt.length) {
                return locationTypesExt[typeIndex];
            }
        }
        return "";
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null) {
                    // Query the autocomplete API for the entered constraint
                    mResultList = getPredictions(constraint);
                    if ((mResultList != null) && (mResultList.size() > 0)) {

                        //add header:
                        mResultList.add(0, new PlaceAutocomplete(null, "CURRENT LOCATION", null));

                        // Results
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    notifyDataSetChanged();
                } else {
                    // The API did not return any results, invalidate the data set.
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    public void clear() {
        if (mResultList != null) {
            mResultList.clear();
        }
    }


    public class PlaceAutocomplete {

        public CharSequence placeId;
        public CharSequence description;
        private String placeType;

        PlaceAutocomplete(CharSequence placeId, CharSequence description, String placeType) {
            this.placeId = placeId;
            this.description = description;
            this.placeType = placeType;
        }

        public String getPlaceType() {
            return placeType;
        }

        @Override
        public String toString() {
            return description.toString();
        }
    }
}
