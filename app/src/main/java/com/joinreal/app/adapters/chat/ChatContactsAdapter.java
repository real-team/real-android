package com.joinreal.app.adapters.chat;


import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.ViewTypes;
import com.joinreal.model.view.ContactsActionVM;
import com.joinreal.model.view.ContactsSubtitleVM;
import com.joinreal.utils.ChatImagesUtils;
import com.joinreal.utils.CircleTransform;
import com.joinreal.utils.ContactManager;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.ViewModelItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ChatContactsAdapter extends RecyclerView.Adapter<ViewHolderAbstract> { //implements MultipleSelectionListener {
    private ArrayList<ViewModelItem> listItems;
    private ArrayList<ViewModelItem> filteredListItems;
    private onChatUserClickListener event;
    protected static onChatUserClickListener listener;
    static boolean isSelectableContactList;
    ArrayList<ContactVM> contactsToInvite;
    private ContactsSubtitleVM actionSubtitile;
    private int actionSubtitileIndex;

    public ChatContactsAdapter(onChatUserClickListener listener, ArrayList<ViewModelItem> items, boolean isSelectableContactList) {
        this.listener = listener;
        this.isSelectableContactList = isSelectableContactList;
        this.listItems = items;
        this.contactsToInvite = new ArrayList<>();
        if (isSelectableContactList) {
            findSubtitleWithAction();
        }
//        if (!ContactManager.isContactsLoaded()) {
//            getModifiedContactsFromDevice();
//        }
//        else {
        resetFilter();
//        }

    }

    private void findSubtitleWithAction() {
        for (int i = 0; i < listItems.size(); i++) {
            if (listItems.get(i).getTypeIntCode() == ViewTypes.CONTACTS_SUBTITLE_WITH_ACTION) {
                actionSubtitile = (ContactsSubtitleVM) listItems.get(i);
                actionSubtitileIndex = i;
                break;
            }
        }
    }

    @Override
    public ViewHolderAbstract onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ViewTypes.CHAT_USER:
                return ContactViewHolder.getNewViewHolder(parent, this);
            case ViewTypes.CONTACTS_SUBTITLE:
                return SubtitleViewHolder.getNewViewHolder(parent);
            case ViewTypes.CONTACTS_SUBTITLE_WITH_ACTION:
                return SubtitleWithActionViewHolder.getNewViewHolder(parent, this);
            case ViewTypes.CONTACTS_ACTION:
                return ActionRowViewHolder.getNewViewHolder(parent);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolderAbstract holder, int position) {
        holder.onBindView(filteredListItems.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return filteredListItems.get(position).getTypeIntCode();
    }

    public void resetFilter() {
        filteredListItems = new ArrayList<>(listItems);
        setActionTextIfNeeded(0);
    }

    public void setFilter(String str) {
        int selectedItemsInFilterCount = 0;
        filteredListItems.clear();
        for (ViewModelItem viewModelItem : listItems) {
            if (viewModelItem instanceof ContactVM) {
                if (((ContactVM) viewModelItem).getContact().getDisplayName().toLowerCase().contains(str.toLowerCase())) {
                    filteredListItems.add(viewModelItem);
                    if (((ContactVM) viewModelItem).isSelected()) {
                        selectedItemsInFilterCount++;
                    }
                }
            } else if (isSelectableContactList && (viewModelItem.getTypeIntCode() == ViewTypes.CONTACTS_SUBTITLE_WITH_ACTION)) {
                filteredListItems.add(viewModelItem);
            }
        }

        notifyDataSetChanged();
        setActionTextIfNeeded(selectedItemsInFilterCount);
    }

    private void setActionTextIfNeeded(int selectedItemsInFilterCount) {
        if (isSelectableContactList) {
            if (selectedItemsInFilterCount < filteredListItems.size() - 1) {
                actionSubtitile.setIsSelectionTextShown(true);
            } else if (selectedItemsInFilterCount == filteredListItems.size() - 1) {
                actionSubtitile.setIsSelectionTextShown(false);
            }
            notifyItemChanged(actionSubtitileIndex - 1);
        }
    }


    private void setActionTextIfNeeded() {
        setActionTextIfNeeded(filteredListItems.size() - 1);
    }

    public ChatContactsAdapter setListener(onChatUserClickListener listener) {
        event = listener;
        return this;
    }

    public void setItems(ArrayList<ViewModelItem> viewModelItems) {
        this.listItems = viewModelItems;
        notifyDataSetChanged();
        resetFilter();
    }

    public interface onChatUserClickListener {
        void onContactRowClick(ContactVM contactVM);
    }

    @Override
    public int getItemCount() {
        return (filteredListItems == null) ? 0 : filteredListItems.size();
    }

    void deselectAll() {
        for (ViewModelItem vm : filteredListItems) {
            if (vm instanceof ContactVM) {
                ((ContactVM) vm).setSelected(false);
                listener.onContactRowClick((ContactVM) vm);
            } else if (vm.getTypeIntCode() == ViewTypes.CONTACTS_SUBTITLE_WITH_ACTION) {
                ((ContactsSubtitleVM) vm).setIsSelectionTextShown(true);
            }
        }
        notifyDataSetChanged();
    }

    void selectAll() {
        for (ViewModelItem vm : filteredListItems) {
            if (vm instanceof ContactVM) {
                ((ContactVM) vm).setSelected(true);
                listener.onContactRowClick((ContactVM) vm);
            } else if (vm.getTypeIntCode() == ViewTypes.CONTACTS_SUBTITLE_WITH_ACTION) {
                ((ContactsSubtitleVM) vm).setIsSelectionTextShown(false);
            }
        }
        notifyDataSetChanged();
    }

    public static class ContactViewHolder extends ViewHolderAbstract {
        ImageView photo;
        TextView name;
        TextView subtitle;
        TextView initials;
        ImageView check;
        ChatContactsAdapter adapter;

        public static ContactViewHolder getNewViewHolder(ViewGroup parent, ChatContactsAdapter adapter) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_contact_item, parent, false);
            return new ContactViewHolder(view, adapter);
        }

        public ContactViewHolder(View itemView, ChatContactsAdapter adapter) {
            super(itemView);
            this.adapter = adapter;
        }

        @Override
        protected void findViewsByIds(View itemView) {
            photo = (ImageView) itemView.findViewById(R.id.thumb);
            name = (TextView) itemView.findViewById(R.id.title);
            initials = (TextView) itemView.findViewById(R.id.contact_initials);
            check = (ImageView) itemView.findViewById(R.id.check);
            subtitle = (TextView) itemView.findViewById(R.id.subtitle);
        }

        @Override
        public void onBindView(final ViewModelItem viewModelItem) {
            final ContactVM viewModel = (ContactVM) viewModelItem;
            name.setText(viewModel.getContact().getDisplayName());

            ChatImagesUtils.setContactProfileThumb(viewModel, photo, initials);

            if (isSelectableContactList) {
                check.setVisibility(View.VISIBLE);
                check.setImageResource(viewModel.isSelected() ? R.drawable.check_blue : R.drawable.check_gray);
            } else {
                check.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isSelectableContactList) {

                        viewModel.setSelected(!viewModel.isSelected());
                        check.setImageResource(viewModel.isSelected() ? R.drawable.check_blue : R.drawable.check_gray);
                        if (viewModel.isSelected()) {
                            adapter.contactsToInvite.add(viewModel);
                            adapter.setActionTextIfNeeded();
                        } else {
                            adapter.contactsToInvite.remove(viewModel);
                            adapter.setActionTextIfNeeded(0);
                        }

                    }
                    listener.onContactRowClick(viewModel);
                }
            });
            subtitle.setVisibility(View.VISIBLE);
            String label = (viewModel.getContact().getLabel() == null) ? ContactManager.LABEL_OTHER : viewModel.getContact().getLabel();
            subtitle.setText(label + ": " + viewModel.getContact().getPhone());
        }
    }


    public static class ActionRowViewHolder extends ViewHolderAbstract {
        ImageView icon;
        TextView title;

        public static ViewHolderAbstract getNewViewHolder(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_contact_item, parent, false);
            return new ActionRowViewHolder(view);
        }

        public ActionRowViewHolder(View itemView) {
            super(itemView);
            itemView.findViewById(R.id.contact_initials).setVisibility(View.GONE);
        }

        @Override
        protected void findViewsByIds(View itemView) {
            icon = (ImageView) itemView.findViewById(R.id.thumb);
            title = (TextView) itemView.findViewById(R.id.title);
        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
            ContactsActionVM viewModel = (ContactsActionVM) viewModelItem;
            icon.setImageResource(viewModel.getIconId());
            title.setText(viewModel.getTitle());
            itemView.setOnClickListener(viewModel.getOnClickListener());
        }

    }

    public static class SubtitleViewHolder extends ViewHolderAbstract {
        TextView title;

        public static ViewHolderAbstract getNewViewHolder(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contacts_subtitle, parent, false);
            return new SubtitleViewHolder(view);
        }

        public SubtitleViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void findViewsByIds(View itemView) {
            title = (TextView) itemView.findViewById(R.id.title);
        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
            title.setText(((ContactsSubtitleVM) viewModelItem).getTitle());
        }

    }

    public static class SubtitleWithActionViewHolder extends SubtitleViewHolder {
        private TextView action;
        private ContactsSubtitleVM subtitleVM;
        ChatContactsAdapter multipleSelectionListener;

        interface MultipleSelectionListener {
            void selectAll();

            void deselectAll();
        }

        public static ViewHolderAbstract getNewViewHolder(ViewGroup parent, ChatContactsAdapter multipleSelectionListener) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contacts_subtitle, parent, false);
            return new SubtitleWithActionViewHolder(view, multipleSelectionListener);
        }

        public SubtitleWithActionViewHolder(View itemView, ChatContactsAdapter multipleSelectionListener) {
            super(itemView);
            this.multipleSelectionListener = multipleSelectionListener;
        }

        @Override
        protected void findViewsByIds(View itemView) {
            super.findViewsByIds(itemView);
            action = (TextView) itemView.findViewById(R.id.action);

        }

        @Override
        public void onBindView(final ViewModelItem viewModelItem) {
            super.onBindView(viewModelItem);
            subtitleVM = (ContactsSubtitleVM) viewModelItem;
            action.setVisibility(View.VISIBLE);
            setActionNeededText();
            action.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    subtitleVM.setIsSelectionTextShown(!subtitleVM.isSelectionTextShown());
                    setActionNeededText();
                    if (subtitleVM.isSelectionTextShown()) {
                        multipleSelectionListener.deselectAll();
                    } else {
                        multipleSelectionListener.selectAll();
                    }
                }
            });
        }

        private void setActionNeededText() {
            action.setText(subtitleVM.isSelectionTextShown() ? subtitleVM.getSelectionText() : subtitleVM.getDeselectionText());
        }

    }

}
