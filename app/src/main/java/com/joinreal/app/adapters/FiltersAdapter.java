package com.joinreal.app.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.ViewGroup;

import com.joinreal.app.common.BusProvider;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.app.presenters.FiltersButtonsPresenter;
import com.joinreal.app.presenters.FiltersChecklistPresenter;
import com.joinreal.app.presenters.FiltersSpinnerPresenter;
import com.joinreal.app.presenters.FiltersExpandablePresenter;
import com.joinreal.app.presenters.FiltersGridPresenter;
import com.joinreal.app.presenters.FiltersPlusMinusBarPresenter;
import com.joinreal.app.presenters.FiltersRangeBarPresenter;
import com.joinreal.app.presenters.FiltersRecylerItemPresenter;
import com.joinreal.app.presenters.FiltersResetButtonPresenter;
import com.joinreal.model.view.FilterViewType;
import com.joinreal.model.view.FiltersViewModelItem;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import static android.support.v7.widget.RecyclerView.*;
import static com.joinreal.app.presenters.FiltersButtonsPresenter.*;

/**
 * Created by britt on 6/3/15.
 */
public class FiltersAdapter extends Adapter<ViewHolderAbstract> implements FiltersResetButtonPresenter.OnResetClickListener {
    private final Context context;
    private final List<FiltersViewModelItem> viewModelList;
    private boolean isMainFiltersScreenAdapter;
    private FiltersButtonsPresenter.BuyRentToggleListener buyRentToggleListener;

    Bus bus = BusProvider.getBus();

    public FiltersAdapter(Fragment fragment, List<FiltersViewModelItem> viewModelList, boolean isMainFiltersScreenAdapter) {
        this(fragment.getActivity().getApplicationContext(), viewModelList, isMainFiltersScreenAdapter, (BuyRentToggleListener) fragment);
    }

    public FiltersAdapter(Context context, List<FiltersViewModelItem> viewModelList, boolean isMainFiltersScreenAdapter) {
        this(context, viewModelList, isMainFiltersScreenAdapter, null);
    }

    public FiltersAdapter(Context context, List<FiltersViewModelItem> viewModelList, boolean isMainFiltersScreenAdapter, BuyRentToggleListener listener) {
        bus.register(this);
        this.context = context;
        this.viewModelList = viewModelList;
        this.isMainFiltersScreenAdapter = isMainFiltersScreenAdapter;
        this.buyRentToggleListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return viewModelList.get(position).getTypeIntCode();
    }

    @Override
    public ViewHolderAbstract onCreateViewHolder(ViewGroup parent, int viewTypeCode) {
        FilterViewType viewType = FilterViewType.getViewTypeByCode(viewTypeCode);
        switch(viewType){
            case BUY_RENT_BUTTONS:
                return new FiltersButtonsPresenter(buyRentToggleListener).getNewViewHolder(parent);
            case PLUS_MINUS_BAR:
                return new FiltersPlusMinusBarPresenter().getNewViewHolder(parent, isMainFiltersScreenAdapter);
            case RANGE:
                return new FiltersRangeBarPresenter().getNewViewHolder(parent, isMainFiltersScreenAdapter);
            case GRID:
                return new FiltersGridPresenter().getNewViewHolder(parent, isMainFiltersScreenAdapter);
            case CHECK_LIST:
                return new FiltersChecklistPresenter().getNewViewHolder(parent, isMainFiltersScreenAdapter);
            case EXPANDABLE:
                return new FiltersExpandablePresenter().getNewViewHolder(parent);
            case CHECKLIST_ITEM:
                return new FiltersRecylerItemPresenter().getNewViewHolder(parent, context);
            case RESET_BUTTON:
                return new FiltersResetButtonPresenter().getNewViewHolder(parent, this);
            case SPINNER:
                return new FiltersSpinnerPresenter(context).getNewViewHolder(parent, isMainFiltersScreenAdapter);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolderAbstract holder, int position) {
        holder.onBindView(viewModelList.get(position));
    }

    @Override
    public int getItemCount() {
        return (viewModelList == null) ? 0 : viewModelList.size();
    }

    @Override
    public void onReset() {
        for (FiltersViewModelItem item : viewModelList) {
            item.resetValues();
        }
        notifyDataSetChanged();
    }
}
