package com.joinreal.app.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.ViewTypes;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.app.viewholders.AdditionalFeatureViewHolder;
import com.joinreal.app.viewholders.AddressAndMapViewHolder;
import com.joinreal.app.viewholders.BasicInfoViewHolder;
import com.joinreal.app.viewholders.OverviewViewHolder;
import com.joinreal.app.viewholders.PropertyPageSummaryViewHolder;
import com.joinreal.model.view.ViewModelItem;

import java.util.ArrayList;
import java.util.List;

import static android.support.v7.widget.RecyclerView.Adapter;

/**
 * Created by britt on 6/3/15.
 */
public class PropertyAdapter extends Adapter<ViewHolderAbstract> {
    private final Context context;
    private final List<ViewModelItem> viewModelList;
    private String screenName;

    public PropertyAdapter(Context context, ArrayList<ViewModelItem> viewModelList, String screenName) {
        BusProvider.getBus().register(this);
        this.context = context;
        this.viewModelList = viewModelList;
        this.screenName = screenName;
    }

    @Override
    public int getItemViewType(int position) {
        return viewModelList.get(position).getTypeIntCode();
    }

    @Override
    public ViewHolderAbstract onCreateViewHolder(ViewGroup parent, int viewType) {
        switch(viewType){
            case  ViewTypes.SUMMARY_INFO_VIEW_TYPE:
                return PropertyPageSummaryViewHolder.getNewViewHolder(parent, screenName);
            case ViewTypes.BASIC_INFO_VIEW_TYPE:
                return BasicInfoViewHolder.getNewViewHolder(parent);
            case ViewTypes.OVERVIEW_VIEW_TYPE:
                return OverviewViewHolder.getNewViewHolder(parent);
            case ViewTypes.ADDITIONAL_FRATURE_VIEW_TYPE:
                return AdditionalFeatureViewHolder.getNewViewHolder(parent);
            case ViewTypes.ADDRESS_MAP_VIEW_TYPE:
                return AddressAndMapViewHolder.getNewViewHolder(parent);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolderAbstract holder, int position) {
        holder.onBindView(viewModelList.get(position));
    }

    @Override
    public int getItemCount() {
        return (viewModelList == null) ? 0 : viewModelList.size();
    }

}
