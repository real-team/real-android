package com.joinreal.app.adapters.chat;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.joinreal.R;
import com.joinreal.app.Application;

import com.joinreal.app.common.Helper;
import com.joinreal.app.common.Prefs;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.common.ViewTypes;
import com.joinreal.events.GetContactFromLoginEvent;
import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.ContactsActionVM;
import com.joinreal.tasks.GetContactFromLoginTask;
import com.joinreal.utils.ChatImagesUtils;
import com.joinreal.utils.ContactManager;
import com.joinreal.model.data.Contact;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.data.chat.ChatDialogDataVM;
import com.joinreal.model.data.chat.ChatDialogInfo;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.utils.ChatConnect;
import com.joinreal.utils.TimeStringUtils;
import com.quickblox.chat.model.QBDialog;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class ChatDialogsAdapter extends RecyclerView.Adapter<ViewHolderAbstract> {
    private List<ChatDialogDataVM> dataSource;
    private List<ViewModelItem> filteredData = new ArrayList<>();
    private String filteredString = "";
    protected DialogsEventListener listener;

    public interface DialogsEventListener {
        void onDialogClick(QBDialog dialog);

        void onDialogDelete(ChatDialogDataVM chatDialogDataVM);

        void onUserFound(final Contact contact, ChatDialogDataVM data);

        void inviteContacts();
    }

    public ChatDialogsAdapter(DialogsEventListener listener, List<ChatDialogDataVM> dialogs) {
        this.listener = listener;
        updateDialogs(dialogs);
    }

    public void updateDialogs() {
        updateDialogs(dataSource);
    }

    public void updateDialogs(List<ChatDialogDataVM> dialogs) {
        System.out.println("updateDialogs : " + dialogs.size());
        dataSource = (dialogs == null) ? new ArrayList<ChatDialogDataVM>() : dialogs;
        for (int i = 0; i < dataSource.size(); i++) {
            final int index = i;
            final ChatDialogDataVM data = dataSource.get(i);
            ChatConnect.getNameAndPhotoFromDialog(data.getQBDialog(), new ChatConnect.GetContactListener() {
                @Override
                public void onContactFound(final Contact contact) {
                    listener.onUserFound(contact, data);
                }

                @Override
                public void onContactNotFound() {
                    System.out.println("updateDialogs.onContactNotFound, getNameAndPhotoFromDialog, dialog id : " + data.getQBDialog().getDialogId());
                    ChatConnect.getUserById(data.getOccupantId(), new ChatConnect.GetUserListener() {
                        @Override
                        public void onUserFound(String qbUserLogin) {
                            new GetContactFromLoginTask(qbUserLogin, index).execute();

//                            Contact contact = null;
//                            try {
//                                contact = Application.getClient().getContactFromQBId(qbUserLogin);
//                            } catch (NetworkException e) {
//                                e.printStackTrace();
////                                listener.onuserFound(contact, data);
//                            }
//                            if (contact != null) {
//                                listener.onUserFound(contact, data);
//                            }
                        }

                        @Override
                        public void onUserNotFound() {
                            Crashlytics.log("onUserNotFound, dialog id : " + data.getQBDialog().getDialogId());
                        }
                    });
                }
            });
        }
        notifyDataSetChanged();
        resetFilter();
    }


    public void setDialogs(List<ChatDialogDataVM> dialogs) {
        if (dialogs != null) {
            dataSource = dialogs;
            Log.d(Application.CHAT_TAG, "setDialogs reset filter");
            resetFilter();
        }
    }

    public List<ChatDialogDataVM> getDataSource() {
        return dataSource;
    }

    public void resetFilter() {
        setFilter(filteredString);
    }

    private int i = 0;

    public void setFilter(String str) {
        i++;
        filteredString = str;

        filteredData.clear();
        if (dataSource != null) {
            for (ChatDialogDataVM dialog : dataSource) {
                if (dialog.getChatNameFromContacts() != null) {
                    if (dialog.getChatNameFromContacts().toLowerCase().contains(str))
                        filteredData.add(dialog);
                }
            }
            if (filteredData.isEmpty()) {
                filteredData.add(new ContactsActionVM("Invite contacts to download the app in order to chat with them", R.drawable.contact_invite, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.inviteContacts();
                    }
                }));
            }
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolderAbstract onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ViewTypes.CHAT_DIALOG:
                return ViewHolder.getNewViewHolder(parent, listener);
            case ViewTypes.CONTACTS_ACTION:
                return ChatContactsAdapter.ActionRowViewHolder.getNewViewHolder(parent);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolderAbstract holder, int position) {
        holder.onBindView(filteredData.get(position));

    }

    @Override
    public int getItemCount() {
        return (filteredData == null) ? 0 : filteredData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return filteredData.get(position).getTypeIntCode();
    }

    public static class ViewHolder extends ViewHolderAbstract {
        private final DialogsEventListener listener;
        ImageView photo;
        TextView name;
        TextView timeTV;
        TextView text;
        View property;
        TextView messagesCount;
        ImageView checkSent;
        ImageView sentEmail;
        View delete;
        private TextView initials;

        public static ViewHolderAbstract getNewViewHolder(ViewGroup parent, DialogsEventListener listener) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_layout, parent, false);
            return new ViewHolder(view, listener);
        }

        public ViewHolder(View view, DialogsEventListener listener) {
            super(view);
            this.listener = listener;
            TypefaceManager.getInstance(view.getContext()).assignTypeface(itemView);
        }

        @Override
        protected void findViewsByIds(View itemView) {
            photo = (ImageView) itemView.findViewById(R.id.thumb);
            initials = (TextView) itemView.findViewById(R.id.contact_initials);
            name = (TextView) itemView.findViewById(R.id.title);
            text = (TextView) itemView.findViewById(R.id.chats_text);
            timeTV = (TextView) itemView.findViewById(R.id.chats_time);
            messagesCount = (TextView) itemView.findViewById(R.id.unread_messages_count);
            property = itemView.findViewById(R.id.property);
            checkSent = (ImageView) itemView.findViewById(R.id.check_sent);
            sentEmail = (ImageView) itemView.findViewById(R.id.sent_email);
            delete = itemView.findViewById(R.id.delete);
        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
            final ChatDialogDataVM dialogData = ((ChatDialogDataVM) viewModelItem);
            text.setTextColor(Application.getAppContext().getResources().getColor(R.color.black));
            if (dialogData.getLastMessage() == null) {
                if (dialogData.getLastMessageUserId() != null) {
                    text.setText(Application.getAppContext().getResources().getString(R.string.property));
                    property.setVisibility(View.VISIBLE);
                } else {
                    text.setText("");
                    property.setVisibility(View.GONE);
                }
            } else {
                if (dialogData.getLastMessage().contains(Application.GENERATED_MESSAGE)
                        && Application.getCurrentChatUser() != null && dialogData.getLastMessageUserId().equals(Application.getCurrentChatUser().getId())) {
                    text.setTextColor(Application.getAppContext().getResources().getColor(R.color.red));
                    text.setText(Application.getAppContext().getResources().getString(R.string.downloaded_the_app));
                } else {
                    text.setText(dialogData.getLastMessage());
                }
                property.setVisibility(View.GONE);
            }
            long time = (System.currentTimeMillis() / 1000);
            if (dialogData.getLastMessageUserId() != null) {
                int lastMessageUserId = dialogData.getLastMessageUserId();
                long lastMessageTime = dialogData.getLastMessageDateSent();

                if (Application.getCurrentChatUser() == null || !dialogData.getLastMessageUserId().equals(Application.getCurrentChatUser().getId())) {
                    checkSent.setVisibility(View.GONE);
                    sentEmail.setVisibility(View.GONE);
                } else {
                    ChatDialogInfo chatDialogInfo = Prefs.getInstance().getDialogInfo(dialogData.getDialogId());
                    if (chatDialogInfo != null) {
                        checkSent.setVisibility(View.VISIBLE);
                        if (chatDialogInfo.isMessageReadBool()) {
                            checkSent.setImageDrawable(Helper.getDrawable(Application.getAppContext(), R.drawable.check_double_green));
                        } else {
                            checkSent.setImageDrawable(Helper.getDrawable(Application.getAppContext(), R.drawable.check_double_gray));
                        }
                        if (chatDialogInfo.isSentByEmailBool()) {
                            sentEmail.setVisibility(View.VISIBLE);
                            checkSent.setVisibility(View.GONE);
                        } else {
                            sentEmail.setVisibility(View.GONE);
                        }
                    } else {
                        checkSent.setVisibility(View.GONE);
                        sentEmail.setVisibility(View.GONE);
                    }
                }

                timeTV.setText(TimeStringUtils.secondsToTime(lastMessageTime));
            } else {
                timeTV.setText("");
                checkSent.setVisibility(View.GONE);
            }

            final ChatDialogDataVM chatDialogDataVM = dialogData;
            Contact contact = ContactManager.getContactByLogin(chatDialogDataVM.getChatLogin());
            setNameTextView(chatDialogDataVM);
            //TODO: temp: new contact vm
            if (contact != null) {
                ChatImagesUtils.setContactProfileThumb(new ContactVM(contact), photo, initials);
            }
            setUnreadMsgCount(chatDialogDataVM);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDialogClick(dialogData.getQBDialog());
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDialogDelete(dialogData);
                }
            });
        }

        private void setNameTextView(ChatDialogDataVM chatDialogDataVM) {
            if (chatDialogDataVM.getChatNameFromContacts() == null) {
                name.setText("");
            } else {
                name.setText(chatDialogDataVM.getChatNameFromContacts());
            }
        }

        private void setUnreadMsgCount(ChatDialogDataVM chatDialogDataVM) {
            if (chatDialogDataVM.getUnreadMessageCount() > 0) {
                messagesCount.setVisibility(View.VISIBLE);
                messagesCount.setText(chatDialogDataVM.getUnreadMessageCount() + "");
            } else {
                messagesCount.setVisibility(View.GONE);
            }
        }
    }

    @Subscribe
    public void onReceivedContactByLogin(GetContactFromLoginEvent event) {
        listener.onUserFound(event.getContact(), dataSource.get(event.getDialogVMIndex()));
    }
}
