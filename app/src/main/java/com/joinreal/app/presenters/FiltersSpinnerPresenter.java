package com.joinreal.app.presenters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.SpinnerFilterItemVM;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.ViewModelItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/3/15.
 */
public class FiltersSpinnerPresenter {

    private final Context context;

    public FiltersSpinnerPresenter(Context context){
        this.context = context;
    }

    public ViewHolderAbstract getNewViewHolder(ViewGroup parent, boolean isMainFilter) {
        View contentView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_filters_spinner, parent, false);
        if (isMainFilter) {
            CardView card = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_filters, parent, false);
            card.addView(contentView);
            return new SpinnerViewHolderAbstract(card);
        } else {
            return new SpinnerViewHolderAbstract(contentView);
        }
    }

    private class SpinnerViewHolderAbstract extends ViewHolderAbstract implements AdapterView.OnItemSelectedListener {
        private SpinnerFilterItemVM viewModel;
        private Spinner spinner;

        public SpinnerViewHolderAbstract(View itemView) {
            super(itemView);
        }

        @Override
        protected void findViewsByIds(View itemView) {
            spinner = (Spinner)itemView.findViewById(R.id.filters_spinner);
            spinner.setOnItemSelectedListener(this);
        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
            viewModel = (SpinnerFilterItemVM)viewModelItem;
            ArrayAdapter adapter = new FilterSpinnerAdapter(
                    context, R.layout.dropdrown_list_item ,viewModel.getOptions());
            spinner.setAdapter(adapter);
            spinner.setSelection(viewModel.getSelectedValueIndex());
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            viewModel.setSelectedIndex(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

        private class FilterSpinnerAdapter extends ArrayAdapter<String> {
            LayoutInflater inflater;
            List<SpinnerFilterItemVM.SpinnerOption> options;
            
            public FilterSpinnerAdapter(Context context, int resource) {
                super(context, resource);
            }

            public FilterSpinnerAdapter(Context context, int layoutId, List<SpinnerFilterItemVM.SpinnerOption> options) {
                super(context, layoutId, getLabels(options));
                this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                this.options = options;
            }


            @Override
            public View getDropDownView(int position, View convertView,ViewGroup parent) {
                return getCustomView(position, convertView, parent);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return getCustomView(position, convertView, parent);

            }

            private View getCustomView(int position, View convertView, ViewGroup parent) {
                View row = inflater.inflate(R.layout.dropdrown_list_item, parent, false);
                TextView label = (TextView)row.findViewById(R.id.spinner_item_textview);
                label.setText(options.get(position).getLabel());
                TypefaceManager.getInstance(context).assignTypeface(label);
                return row;
            }

        }
        public List<String> getLabels(List<SpinnerFilterItemVM.SpinnerOption> options) {
            List<String> labels = new ArrayList<>();
            for (SpinnerFilterItemVM.SpinnerOption option : options) {
                labels.add(option.getLabel());
            }
            return labels;
        }

    }
}
