package com.joinreal.app.presenters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinreal.R;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.ViewModelItem;

/**
 * Created by britt on 6/7/15.
 */
public class FiltersResetButtonPresenter {
    OnResetClickListener listener;

    public interface OnResetClickListener{
         public void onReset();
     }

    public ViewHolderAbstract getNewViewHolder(ViewGroup parent, OnResetClickListener listener) {
        this.listener = listener;
        View contentView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filters_reset_button, parent, false);
            return new ResetViewHolderAbstract(contentView);
        }
    private class ResetViewHolderAbstract extends ViewHolderAbstract {

        public ResetViewHolderAbstract(View itemView) {
            super(itemView);
        }

        @Override
        protected void findViewsByIds(View itemView) {
            itemView.findViewById(R.id.reset_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onReset();
                }
            });
        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
        }
    }
}
