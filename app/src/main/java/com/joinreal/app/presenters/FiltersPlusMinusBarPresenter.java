package com.joinreal.app.presenters;

import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.common.Helper;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.PlusMinusFiltersVM;
import com.joinreal.model.view.ViewModelItem;

/**
 * Created by britt on 6/3/15.
 */
public class FiltersPlusMinusBarPresenter {

    public ViewHolderAbstract getNewViewHolder(ViewGroup parent, boolean isMainFilter) {
        View contentView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_plus_minus_bar, parent, false);
        if (isMainFilter) {
            CardView card = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_filters, parent, false);
            card.addView(contentView);
            return new PMViewHolderAbstract(card);
        } else {
            return new PMViewHolderAbstract(contentView);
        }
    }

    private class PMViewHolderAbstract extends ViewHolderAbstract implements View.OnClickListener {
        //        PlusMinusBar plusMinusBar;
        TextView label;
        TextView value;
        private PlusMinusFiltersVM viewModel;
        private ImageView minusImage;
        private ImageView plusImage;

        public PMViewHolderAbstract(View itemView) {
            super(itemView);
        }

        @Override
        protected void findViewsByIds(View itemView) {
            label = (TextView) itemView.findViewById(R.id.label_textview);
            value = (TextView) itemView.findViewById(R.id.value_textview);
            minusImage = (ImageView)itemView.findViewById(R.id.minus);
            minusImage.setOnClickListener(this);
            plusImage = (ImageView) itemView.findViewById(R.id.plus);
            plusImage.setOnClickListener(this);

        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
            viewModel = (PlusMinusFiltersVM) viewModelItem;
            label.setText(viewModel.getLabel());
            setValueTextView();
            enableOrDisableButtons();
        }

        private void setValueTextView() {
            Resources res = itemView.getContext().getResources();
            if (viewModel.isMinValueSelected()) {
                value.setText("ANY ");
                value.setTextSize(Helper.pxlsToDp(itemView.getContext(), label.getTextSize()));
            } else {
                String text = Double.toString(viewModel.getSelectedValue()) + "+ ";
                text = text.replace(".0", "");
                value.setText(text);
                value.setTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.font_size_medium));
            }

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.minus:
                    viewModel.decrementSelectedValue();
                    break;
                case R.id.plus:
                    viewModel.incrementSelectedValue();
                    break;
            }
            setValueTextView();
            enableOrDisableButtons();
        }

        private void enableOrDisableButtons() {
            boolean shouldEnable = (! viewModel.isMinValueSelected());
            minusImage.setEnabled(shouldEnable);
            itemView.findViewById(R.id.minus_divider).setEnabled(shouldEnable);

            shouldEnable = (! viewModel.isMaxValueSelected());
            plusImage.setEnabled(shouldEnable);
            itemView.findViewById(R.id.plus_divider).setEnabled(shouldEnable);

        }
    }
}
