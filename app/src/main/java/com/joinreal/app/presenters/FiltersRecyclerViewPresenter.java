package com.joinreal.app.presenters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinreal.R;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.ViewModelItem;
import com.squareup.otto.Bus;

/**
 * Created by britt on 6/3/15.
 */
public abstract class FiltersRecyclerViewPresenter {

    Bus bus = BusProvider.getBus();
    public FiltersRecyclerViewPresenter() {
        bus.register(this);

    }

    public ViewHolderAbstract getNewViewHolder(ViewGroup parent, boolean isMainFilter) {
        View contentView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filters_recyclerview, parent, false);
        if (isMainFilter) {
            CardView card = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_filters, parent, false);
            card.addView(contentView);
            return new RecyclerViewHolderAbstract(card);
        } else {
            return new RecyclerViewHolderAbstract(contentView);
        }
    }

    protected class RecyclerViewHolderAbstract extends ViewHolderAbstract {
        RecyclerView recyclerView;

        public RecyclerViewHolderAbstract(View itemView) {
            super(itemView);
            findViewsByIds(itemView);
        }

        @Override
        protected void findViewsByIds(View itemView) {
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerview);
        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
            bindFilterView(viewModelItem, recyclerView, itemView.getContext());
        }

    }

    protected abstract void bindFilterView(ViewModelItem viewModelItem, RecyclerView recyclerView, Context context);
}
