package com.joinreal.app.presenters;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.joinreal.app.adapters.FiltersRecyclerViewAdapter;
import com.joinreal.app.common.BusProvider;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.GridFiltersVM;
import com.joinreal.model.view.ViewModelItem;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

/**
 * Created by britt on 6/3/15.
 */
public class FiltersGridPresenter extends FiltersRecyclerViewPresenter{


    @Override
    protected void bindFilterView(ViewModelItem viewModelItem, RecyclerView recyclerView, Context context) {
        recyclerView.setLayoutManager(new GridLayoutManager(context, 3));
        recyclerView.setAdapter(new FiltersRecyclerViewAdapter(context, (FiltersViewModelItem) viewModelItem));
    }

//    Bus bus = BusProvider.getBus();
//
//
//    public FiltersGridPresenter() {
//        bus.register(this);
//
//    }

    @Subscribe
    public void test(String s) {
        // TODO: React to the event somehow!
        System.out.println(getClass().getSimpleName() + " otto test : " + s);
    }
}
