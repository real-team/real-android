package com.joinreal.app.presenters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinreal.R;
import com.joinreal.app.adapters.FiltersAdapter;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.ExpandableItemFiltersVM;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.views.ExpandableFilterLayout;

/**
 * Created by britt on 6/3/15.
 */
public class FiltersExpandablePresenter {

    public ViewHolderAbstract getNewViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filters_expandable, parent, false);
        return new ExpandableViewHolderAbstract(view);
    }

        private class ExpandableViewHolderAbstract extends ViewHolderAbstract {
            ExpandableFilterLayout expandableItem;

        public ExpandableViewHolderAbstract(View itemView) {
            super(itemView);
            findViewsByIds(itemView);
        }

        @Override
        protected void findViewsByIds(View itemView) {
            expandableItem = (ExpandableFilterLayout) itemView.findViewById(R.id.expandlable_filter);
        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
            ExpandableItemFiltersVM viewModel = (ExpandableItemFiltersVM) viewModelItem;
            expandableItem.setTitle(viewModel.getTitle());
            expandableItem.setInnerItems(new FiltersAdapter(itemView.getContext(), viewModel.getItems(), false));
        }

    }
}
