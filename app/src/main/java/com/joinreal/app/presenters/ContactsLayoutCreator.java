package com.joinreal.app.presenters;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.adapters.chat.ChatContactsAdapter;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.ISearchToolbarListener;
import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.utils.ContactManager;

import java.util.ArrayList;

/**
 * Created by brittbarak on 7/6/15.
 */
public class ContactsLayoutCreator {

    private ISearchToolbarListener agentActionsListener;
    private final boolean isSelectableContactList;
    private ContactsLayoutCreatorListener listener;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View rootView;
    private int homeIcon;
    private TextWatcher textWatcher;

    public interface ContactsLayoutCreatorListener extends ChatContactsAdapter.onChatUserClickListener {
        ActionBar getActivityActionBar();

        Menu getActionMenu();
    }

    public void onCancelClick() {
        ((ChatContactsAdapter) recyclerView.getAdapter()).resetFilter();

    }

    public void updateDataList(ArrayList<ViewModelItem> viewModelItems) {
        swipeRefreshLayout.setRefreshing(false);
        ((ChatContactsAdapter) recyclerView.getAdapter()).setItems(viewModelItems);
    }

    public ContactsLayoutCreator(ContactsLayoutCreatorListener listener, ISearchToolbarListener agentActionsListener) {
        this(listener, agentActionsListener, false);
    }

    public ContactsLayoutCreator(ContactsLayoutCreatorListener listener, ISearchToolbarListener agentActionsListener, boolean isSelectableContactList) {
        this.listener = listener;
        this.agentActionsListener = agentActionsListener;
        this.isSelectableContactList = isSelectableContactList;
        textWatcher = getSearchViewTextWatcher();

    }

    public void onResume() {
//        setupToolbar();
        agentActionsListener.addTextChangedListener(textWatcher);
    }

    public void onPause() {
        agentActionsListener.removeTextChangedListener(textWatcher);
    }

    public void createLayout(View rootView, int homeIcon, ArrayList<ViewModelItem> listItems) {
        this.rootView = rootView;
        this.homeIcon = homeIcon;
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        setListView(listItems);
        listener.getActivityActionBar().setHomeAsUpIndicator(homeIcon);
        setToolbarViews();
        TypefaceManager.getInstance(Application.getAppContext()).assignTypeface(rootView);
    }

    private void setToolbarViews() {
        rootView.findViewById(R.id.cancel_search).
                setOnClickListener(getCancelClickListener());
    }

    private TextWatcher getSearchViewTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ((ChatContactsAdapter) recyclerView.getAdapter()).setFilter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private View.OnClickListener getCancelClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                agentActionsListener.onCancelSearchClick();
                ((ChatContactsAdapter) recyclerView.getAdapter()).resetFilter();

                listener.getActionMenu().findItem(R.id.action_add).setVisible(true);
                listener.getActionMenu().findItem(R.id.action_search).setVisible(true);
            }
        };
    }

    private void setListView(ArrayList<ViewModelItem> listItems) {
        recyclerView.setLayoutManager(new LinearLayoutManager(Application.getAppContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new ChatContactsAdapter(listener, listItems, isSelectableContactList));
        if (! ContactManager.didMergeContactsOnce()) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }
    }

    public void enableSwipe(boolean shouldEnable) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setEnabled(shouldEnable);
        }
    }

    public void stopRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }


}