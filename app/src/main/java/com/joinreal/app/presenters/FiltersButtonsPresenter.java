package com.joinreal.app.presenters;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.events.ToggleBuyRentEvent;
import com.joinreal.model.view.ButtonsFiltersVM;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.ViewModelItem;
import com.squareup.otto.Bus;

/**
 * Created by britt on 6/3/15.
 */
public class FiltersButtonsPresenter {

    private final Bus bus;
    private BuyRentToggleListener listener;

    public interface BuyRentToggleListener{
        public void onBuyRentToggle(boolean isBuySelected);
        public boolean getIsBuy();
    }

    public FiltersButtonsPresenter(BuyRentToggleListener listener) {
        this.listener = listener;
        bus = BusProvider.getBus();
    }

    public ViewHolderAbstract getNewViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filters_buy_rent_buttons, parent, false);
        return new ButtonsViewHolderAbstract(view);
    }

    private class ButtonsViewHolderAbstract extends ViewHolderAbstract implements View.OnClickListener {
        TextView buyTextview;
        TextView rentTextview;
        View buyIndicator;
        View rentIndicator;
        private ButtonsFiltersVM viewModel;

        public ButtonsViewHolderAbstract(View itemView) {
            super(itemView);
            itemView.findViewById(R.id.buy_button).setOnClickListener(this);
            itemView.findViewById(R.id.rent_button).setOnClickListener(this);
            bus.register(this);

        }

        @Override
        protected void findViewsByIds(View itemView) {
            buyTextview = (TextView) itemView.findViewById(R.id.buy_text);
            rentTextview = (TextView) itemView.findViewById(R.id.rent_text);
            buyIndicator = itemView.findViewById(R.id.buy_indicator);
            rentIndicator = itemView.findViewById(R.id.rent_indicator);
        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
            viewModel = (ButtonsFiltersVM) viewModelItem;
            setButtonSelectedView();
        }

        private void setButtonSelectedView() {
            if (viewModel.isBuyChecked()) {
                setViewSelected(buyTextview, buyIndicator);
                setViewNotSelected(rentTextview, rentIndicator);
            } else {
                setViewSelected(rentTextview, rentIndicator);
                setViewNotSelected(buyTextview, buyIndicator);
            }

        }

        private void setViewSelected(TextView textView, View indicatoreView) {
            Resources res = itemView.getContext().getApplicationContext().getResources();
            textView.setTextColor(res.getColor(R.color.blue_dark));
            indicatoreView.setBackgroundColor(res.getColor(R.color.blue_dark));
        }

        private void setViewNotSelected(TextView textView, View indicatoreView) {
            Resources res = itemView.getContext().getApplicationContext().getResources();
            textView.setTextColor(res.getColor(R.color.blue));
            indicatoreView.setBackgroundColor(res.getColor(R.color.transparent));
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.buy_button:
                    if (! viewModel.isBuyChecked()) {
                        setButtonSelected(true);
                    }
                    break;
                case R.id.rent_button:
                    if (viewModel.isBuyChecked()) {
                        setButtonSelected(false);
                    }
                    break;
            }
        }
    private void setButtonSelected(boolean isBuySelected) {
        viewModel.setIsBuyChecked(isBuySelected);
        setButtonSelectedView();

        //updateFavoritesListener.onBuyRentToggle(isBuySelected);
        bus.post(new ToggleBuyRentEvent(isBuySelected));

    }

    }


}
