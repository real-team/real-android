package com.joinreal.app.presenters;

import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.events.ToggleBuyRentEvent;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.RangeSeekBarFiltersVM;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.views.RangeSeekBar;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

/**
 * Created by britt on 6/3/15.
 */
public class FiltersRangeBarPresenter {
    private RangeSeekBarFiltersVM viewModel;
    Bus bus = BusProvider.getBus();
    RangeSeekBar rangeSeekBar;
    TextView label;



    public FiltersRangeBarPresenter() {
        bus.register(this);
    }

    //TODO: maybe subscribe the view holder ?
    @Subscribe
    public void onToggleBuyRent(ToggleBuyRentEvent event) {
        viewModel.toggleButRent(event.isBuySelected());
        setRangeBarValues();
        setLabelText();
        setRangeSteps();
    }

    public ViewHolderAbstract getNewViewHolder(ViewGroup parent, boolean isMainFilter) {
        View contentView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filters_range_bar, parent, false);
        if (isMainFilter) {
            CardView card = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_filters, parent, false);
            card.addView(contentView);
            return new RangeViewHolderAbstract(card);
        } else {
            return new RangeViewHolderAbstract(contentView);
        }
    }

    private class RangeViewHolderAbstract extends ViewHolderAbstract implements RangeSeekBar.OnRangeSeekBarChangeListener {
        public RangeViewHolderAbstract(View itemView) {
            super(itemView);
        }

        @Override
        protected void findViewsByIds(View itemView) {
            rangeSeekBar = (RangeSeekBar) itemView.findViewById(R.id.range_bar);
            label = (TextView) itemView.findViewById((R.id.range_bar_label));
        }

        @Override
        public void onBindView(ViewModelItem viewModelItem) {
            viewModel = (RangeSeekBarFiltersVM) viewModelItem;
            rangeSeekBar.setNotifyWhileDragging(true);
            rangeSeekBar.setOnRangeSeekBarChangeListener(this);
            setLabelText();
            setRangeBarValues();
        }

        @Override
        public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
            viewModel.setSelectedMinMaxValues((Integer) minValue, (Integer) maxValue);
            setRangeSteps();
            setLabelText();
        }

    }
        protected void setRangeSteps() {
            rangeSeekBar.setSelectedMinValue(viewModel.getSelectedMinValueForRange());
            rangeSeekBar.setSelectedMaxValue(viewModel.getSelectedMaxValueForRange());
        }

    protected void setRangeBarValues() {
        rangeSeekBar.setRangeValues(viewModel.getAbsoluteMin(), viewModel.getAbsoluteMax());
        rangeSeekBar.setSelectedMinValue(viewModel.getSelectedMinValueForRange());
        rangeSeekBar.setSelectedMaxValue(viewModel.getSelectedMaxValueForRange());
    }

    protected void setLabelText() {
        String labelString;
        if (viewModel.isAllRangeSelected()){
            labelString = viewModel.getFullRangeLabel();
        } else {
            labelString = viewModel.getSelectedMinLabel() + " - " + viewModel.getSelectedMaxLabel() + " " + viewModel.getLabel();
        }
        label.setText(labelString);
    }

}
