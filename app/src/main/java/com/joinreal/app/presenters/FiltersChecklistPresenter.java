package com.joinreal.app.presenters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.joinreal.app.adapters.FiltersRecyclerViewAdapter;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.views.DynamicLinearLayoutManager;

/**
 * Created by britt on 6/4/15.
 */
public class FiltersChecklistPresenter extends FiltersRecyclerViewPresenter{


    @Override
    protected void bindFilterView(ViewModelItem viewModelItem, RecyclerView recyclerView, Context context) {
        recyclerView.setLayoutManager(new DynamicLinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new FiltersRecyclerViewAdapter(context, (FiltersViewModelItem) viewModelItem));
    }
}
