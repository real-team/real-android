package com.joinreal.app.presenters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinreal.R;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.app.viewholders.GridViewItemHolderAbstract;
import com.squareup.otto.Subscribe;

/**
 * Created by britt on 6/7/15.
 */
public class FiltersRecylerItemPresenter {
//    private final int layoutId;
    private GridViewItemHolderAbstract viewHolder;


    public FiltersRecylerItemPresenter() {
        BusProvider.getBus().register(this);

    }


    public ViewHolderAbstract getNewViewHolder(ViewGroup parent, Context context) {
//        this.layoutId = viewModelItem.getViewType() == FilterViewType.GRID ? R.layout.item_grid_filter : R.layout.item_checklist_filter;
        int layoutId = R.layout.item_checklist_filter;
        View contentView = LayoutInflater.from(parent.getContext())
                .inflate(layoutId, parent, false);
        viewHolder = new GridViewItemHolderAbstract(context, contentView);
        return viewHolder;

    }

    @Subscribe
    public void test(String s) {
        // TODO: React to the event somehow!
        System.out.println(getClass().getSimpleName() + " otto test : " + s);
        viewHolder.title.setText(s);
    }

//    private class FiltersListRowViewHolder extends ViewHolderAbstract implements View.OnClickListener {
//
//        private CheckListRowFilterItemVM viewModel;
//
//        public FiltersListRowViewHolder(View itemView) {
//            super(itemView);
//        }
//
//    @Override
//    public void onBindView(FiltersViewModelItem viewModelItem) {
//        viewModel = (CheckListRowFilterItemVM) viewModelItem;
//
//
//        holder.onBindView(items.get(position));
//    }
//        }
}
