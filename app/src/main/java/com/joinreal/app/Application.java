package com.joinreal.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.joinreal.R;
import com.joinreal.app.api.RealClient;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.Prefs;
import com.joinreal.model.data.chat.ChatUsersPhonesMap;
import com.quickblox.chat.QBPrivateChatManager;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.messages.QBMessages;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBSubscription;
import com.quickblox.users.model.QBUser;

import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.support.multidex.MultiDexApplication;

/**
 * Created by britt on 6/22/15.
 */
public class Application extends MultiDexApplication {
    //For Chat :
    public static final String CHAT_TAG = "chattagg";
    public static final String AGENT_PHONE = "972544685929";
    public static final int AGENT_ID = 3247886;
    public static final boolean FILTER_AGENT_DIALOG = false;

    //push
    public static final String PUSH_PROJECT_NUMBER = "78840496812";

    public static final String TYPE_MESSAGE = "push_message";
    public static final int NOTIFICATION_ID = 45344535;

//    public static final String USER_PASSWORD = "JoinReal2015";
    public static final String USER_PASSWORD = "Apple1sRed";
//    public static final String APP_ID = "23102";
    public static final String APP_ID = "3";
    public static final String AUTH_KEY = "PTfZUtBhJPaFsVb";
//    public static final String AUTH_KEY = "8ZAAehDOkDvNZOX";
    public static final String AUTH_SECRET = "yZHDhvtU5SSVOXh";
//    public static final String AUTH_SECRET = "xYM4wFV-ekqNrm-";
    public static final String SMS = "sms";

    public static final String GOTHAM_NARROW_LIGHT = "typeface/GothamNarrow-Light.otf";
    public static final String GOTHAM_NARROW_BOOK = "typeface/GothamNarrow-Book.otf";
    public static final String GOTHAM_NARROW_MEDIUM = "typeface/GothamNarrow-Medium.otf";

    private static final String SUPPORT_PHONE = "";
    public static final String GENERATED_MESSAGE = "I want to invite you";
    public static QBUser AGENT_FIRST_MESSAGE;
    public static QBUser SUPPORT_USER;

    private static String countryPhoneCode = "972";
    private static String userPhone;
    private static QBUser currentChatUser;
    private static QBPrivateChatManager qBPrivateChatManager;
    private static boolean activityVisible;
    private static ChatUsersPhonesMap usersPhones;
    private static boolean didCreateMainActivity;
    private static QBEnvironment qbEnv;
    private static boolean isOriginallyQAVersion;
    private GoogleCloudMessaging gcm;
    private String regid;





    private static Context context;
    private static RealClient client;
    private static String devLevel;
    private static Prefs prefs;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private static final String PROPERTY_ID = "UA-63435661-2";
    //Logging TAG
    private static final String TAG = "MyApp";

    public static int GENERAL_TRACKER = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        Application.context = getApplicationContext();
        devLevel = getString(R.string.dev_level);
        qbEnv = devLevel.equals(context.getString(R.string.prod)) ? QBEnvironment.PRODUCTION : QBEnvironment.DEVELOPMENT;
        isOriginallyQAVersion = (! isProductionEnv());
        Analytics.getInstance().setDefaultTracker(this);
        initCrashlytics();
        chatAppOnCreate();
        prefs = Prefs.getInstance();
        initClient();
    }

    private void initCrashlytics() {
        Fabric.with(this, new Crashlytics());
        Crashlytics.setBool("is_qa_device", !devLevel.equals(R.string.prod));
        Crashlytics.setString("environment", devLevel);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        Crashlytics.setString("version_name", pInfo.versionName);
        Crashlytics.setInt("version_code", pInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void chatAppOnCreate() {
        AGENT_FIRST_MESSAGE = new QBUser();
        AGENT_FIRST_MESSAGE.setLogin(AGENT_PHONE);
        AGENT_FIRST_MESSAGE.setPassword(USER_PASSWORD);
        AGENT_FIRST_MESSAGE.setId(AGENT_ID);

        SUPPORT_USER = new QBUser();
        SUPPORT_USER.setLogin(SUPPORT_PHONE);
        SUPPORT_USER.setPassword(USER_PASSWORD);

        if (Prefs.getInstance().getPushRegid().equals("")){

            getRegId();
        }
        countryPhoneCode = Prefs.loadCountryCode();
    }

    public void getRegId(){
        final Context context = this;
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(com.joinreal.app.Application.getAppContext());
                    }
                    InstanceID instanceID = InstanceID.getInstance(context);
                    regid = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    msg = "Device registered, registration ID=" + regid;
                    Log.i("GCM", msg);

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (regid != null && !regid.equals("")) {
                    Prefs.saveRegid(regid);
                    //Comment out on purpose. This function needs
                    //(and do) get calls after the chat is connected (ChatService initialize)
                    //subscribeToPushNotifications();

                }
            }
        }.execute(null, null, null);
    }

    public static void subscribeToPushNotifications() {
        Crashlytics.log("Application.subscribeToPushNotifications()");
        String pushRegid = prefs.getPushRegid();
        if (!pushRegid.equals("")) {
            String deviceId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
            QBMessages.subscribeToPushNotificationsTask(pushRegid, deviceId, getQbEnv(), new QBEntityCallbackImpl<ArrayList<QBSubscription>>() {
                @Override
                public void onSuccess(ArrayList<QBSubscription> subscriptions, Bundle args) {
                    Log.d(CHAT_TAG, "subscribed");
                    prefs.setIsSubscribed();
                }

                @Override
                public void onError(List<String> errors) {
                    Crashlytics.log("subscribeToPushNotificationsTask error : "  + ((errors == null) ? "null" : errors.get(0)));
                }
            });
        }
    }

    public static String getUserPhone() {
        return client.getUser().getUsername();
    }

    public static QBUser getCurrentChatUser() {
        if (currentChatUser == null){
            currentChatUser = Prefs.getCurrentUser();
            if (currentChatUser == null) {
                client.getUser();
            }
        }
        return currentChatUser;
    }

    public static void setCurrentChatUser(QBUser currentUser) {
        Application.currentChatUser = currentUser;
        prefs.saveCurrentUser(currentUser);
    }

    static public String getStr(int id) {
        return context.getResources().getString(id);
    }

    public static ChatUsersPhonesMap getUsersPhones(){ //todo check this map?????
        if (usersPhones == null){
            usersPhones = Prefs.getUsersPhones();
            if (usersPhones == null){
                usersPhones = new ChatUsersPhonesMap();
                usersPhones.setMap(new HashMap<Integer, String>());
                prefs.saveUsersPhones(usersPhones);
            }
        }
        return usersPhones;
    }

    public static void addUserPhone(Integer id,String phone){
        usersPhones = getUsersPhones();
        usersPhones.getMap().put(id, phone);
        saveUsersPhones(usersPhones);
    }

    public static void saveUsersPhones(ChatUsersPhonesMap chatUsersPhonesMap){
        usersPhones = chatUsersPhonesMap;
        prefs.saveUsersPhones(chatUsersPhonesMap);
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }




    private void initClient() {
        if (client == null) {
            client = new RealClient();
            client.initSession(this);
        }
    }

    public static RealClient getRealClient() {
        return client;
    }

    public static String getDevLevel() {
        return devLevel;
    }

    public static void setDevLevel(String devLevel) {
        Application.devLevel = devLevel;
        Application.qbEnv = devLevel.equals(context.getString(R.string.prod)) ?
                QBEnvironment.PRODUCTION : QBEnvironment.DEVELOPMENT;
    }

    public static RealClient getClient() {
        return client;
    }

    public static Context getAppContext() {
        return context;
    }

    public static Prefs getPrefs() {
        return prefs;
    }

    public static void setCreatedMainActivity() {
        didCreateMainActivity = true;
    }

    public static QBEnvironment getQbEnv() {
        return qbEnv;
        //TODO: for test!!!
//        return QBEnvironment.DEVELOPMENT;
    }

    public static boolean isProductionEnv() {
        return devLevel.equals(context.getString(R.string.prod));
    }

    public static boolean isOriginallyQAVersion() {
        return isOriginallyQAVersion;
    }
}
