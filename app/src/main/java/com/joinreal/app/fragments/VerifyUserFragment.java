package com.joinreal.app.fragments;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.CustomerWelcomeActivity;
import com.joinreal.app.activities.MainActivityAgent;
import com.joinreal.app.api.RealClient;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.ApiCallbackInterface;
import com.joinreal.events.TaskBackFromNetworkEvent;
import com.joinreal.exceptions.NetworkException;
import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;

/**
 * Created by britt on 6/1/15.
 */
public class VerifyUserFragment extends RealBaseFragment {

    public static String TAG = "VERIFY_USER_FRAGMENT";
    @InjectView(R.id.phone_number_title)
    TextView phoneNumberTitle;
    @InjectView(R.id.pin_digit_1)
    EditText pinDigit1;
    @InjectView(R.id.pin_digit_2)
    EditText pinDigit2;
    @InjectView(R.id.pin_digit_3)
    EditText pinDigit3;
    @InjectView(R.id.pin_digit_4)
    EditText pinDigit4;
    @InjectView(R.id.verify_button)
    Button verifyButton;
    @InjectView(R.id.verification_msg)
    TextView verificationMsg;

    @InjectViews({R.id.pin_digit_1, R.id.pin_digit_2, R.id.pin_digit_3, R.id.pin_digit_4})
    List<EditText> pinDigits;

    private RealClient client;
    private boolean isWaitingForAssaignedAgentToReturn;

    public static VerifyUserFragment getNewInstance(Bundle bd) {
        VerifyUserFragment instance = new VerifyUserFragment();
        instance.setArguments(bd);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = Application.getClient();
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getRestBus().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login_verification, container, false);
        ButterKnife.inject(this, rootView);
        setViewsContent();
        ((AppCompatActivity) getActivity()).setSupportActionBar((Toolbar) rootView.findViewById(R.id.toolbar));
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        ab.setHomeAsUpIndicator(Helper.getDrawable(getActivity(), R.drawable.back_arrow_blue));
        ab.setDisplayHomeAsUpEnabled(true);

        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }

    @OnClick(R.id.didnt_get_code_text)
    public void onTopBackButtonClick() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.verify_button)
    public void onVerifyButtonPress() {
        setSuccessVerifyingMsg();
        AsyncTask signInTask = new AsyncTask<Object, Void, Void>() {
            @Override
            protected Void doInBackground(Object... params) {
                sendSignInRequest();
                return null;
            }
        };
        signInTask.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setViewsContent() {
        String phoneNumber = getArguments().getString(StaticParams.KEY_PHONE_NUM, "");
        phoneNumberTitle.setText(phoneNumber);
        pinDigit1.requestFocus();
        setTextChangedListeners();

        ButterKnife.apply(pinDigits, TEXT_COLOR, getResources().getColor(R.color.blue));
        ButterKnife.apply(pinDigits, BACKGROUND_DRAWABLE, getPinDigitBgDrawable(R.color.blue));
    }

    private void setTextChangedListeners() {
        pinDigit1.addTextChangedListener(getOnTextChangedListener(pinDigit2));
        pinDigit2.addTextChangedListener(getOnTextChangedListener(pinDigit3));
        pinDigit3.addTextChangedListener(getOnTextChangedListener(pinDigit4));
        pinDigit4.addTextChangedListener(getOnTextChangedListener(verifyButton));
    }

    private TextWatcher getOnTextChangedListener(final View nexViewToFocus) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    nexViewToFocus.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
    }

    private void sendSignInRequest() {
        System.out.println("in sendSignInRequest");
        ApiCallbackInterface callback = getApiCallback(client);
        String username = getArguments().getString(StaticParams.KEY_PHONE_NUM, "");
        client.signIn(username, getPassword(), callback);
    }

    private ApiCallbackInterface getApiCallback(final RealClient client) {
        return new ApiCallbackInterface() {
            @Override
            public void onSuccess(String json) {
                setSuccessVerificationMsg();

                //TODO: add aimation
                ButterKnife.apply(pinDigits, TEXT_COLOR, getResources().getColor(R.color.green));
                ButterKnife.apply(pinDigits, BACKGROUND_DRAWABLE, getPinDigitBgDrawable(R.color.green));

                client.getUserFromNetwork(json);
                client.setAssignedAgent();
//                client.syncContacts(getActivity());

                openNextActivity();

            }

            @Override
            public void onFailure(Throwable throwable) {
                setUnknownUserMsg();
                ButterKnife.apply(pinDigits, TEXT_COLOR, getResources().getColor(R.color.red));
                ButterKnife.apply(pinDigits, BACKGROUND_DRAWABLE, getPinDigitBgDrawable(R.color.red));

                String msg = "User does not extist. Please contact us : info@joinreal.com";
                NetworkException apiException = new NetworkException(Helper.getExceptionCodeForResponse(NetworkCodes.EXCEPTION_OTHER), msg, "");
                Helper.handleApiException(apiException, NetworkCodes.NO_SUCH_USER_ON_SIGNIN);
            }

            @Override
            public void onWrongPassword(Throwable throwable) {
                setIncorrectVerificationMsg();

                //TODO: add aimation
                ButterKnife.apply(pinDigits, TEXT_COLOR, getResources().getColor(R.color.red));
                ButterKnife.apply(pinDigits, BACKGROUND_DRAWABLE, getPinDigitBgDrawable(R.color.red));

            }
        };
    }

    private void openNextActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = null;
                if (Application.getClient().isAgent()) {
                    intent = new Intent(getActivity(), MainActivityAgent.class);
                } else {
                    if (Application.getClient().getAssignedAgent() != null) {
                        intent = new Intent(getActivity(), CustomerWelcomeActivity.class);
                    } else {
                        isWaitingForAssaignedAgentToReturn = true;
                    }
                }
                if (intent != null) {
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        }, 700);
    }

    @Subscribe
    public void onRecievedAssignedAgentEvent(TaskBackFromNetworkEvent e) {
        if ((isWaitingForAssaignedAgentToReturn) && (e.getTaskCode() == NetworkCodes.TASK_ASSIAGNED_AGENT)) {
            if (e.getException() != null) {
                isWaitingForAssaignedAgentToReturn = false;
                Intent intent = new Intent(getActivity(), CustomerWelcomeActivity.class);
                startActivity(intent);
                getActivity().finish();
            } else {
                Helper.handleApiException(e.getException(), NetworkCodes.TASK_ASSIAGNED_AGENT);
            }
        }
    }

    private void setSuccessVerifyingMsg() {
        verificationMsg.setVisibility(View.VISIBLE);
        verificationMsg.setText(getResources().getString(R.string.checking_password_msg));
        verificationMsg.setTextColor(getResources().getColor(R.color.blue));
    }

    private void setIncorrectVerificationMsg() {
        verificationMsg.setVisibility(View.VISIBLE);
        verificationMsg.setText(getResources().getString(R.string.incorrect_password_msg));
        verificationMsg.setTextColor(getResources().getColor(R.color.red));
    }

    private void setUnknownUserMsg() {
        verificationMsg.setVisibility(View.VISIBLE);
        verificationMsg.setText(getResources().getString(R.string.incorrect_user));
        verificationMsg.setTextColor(getResources().getColor(R.color.red));
    }

    private void setSuccessVerificationMsg() {
        verificationMsg.setVisibility(View.VISIBLE);
        verificationMsg.setText(getResources().getString(R.string.correct_password_msg));
        verificationMsg.setTextColor(getResources().getColor(R.color.green));
    }

    private GradientDrawable getPinDigitBgDrawable(int colorId) {
        GradientDrawable drawable = (GradientDrawable) pinDigit1.getBackground();
        drawable.setStroke(Helper.dpToPxl(getActivity(), 1.5f), getResources().getColor(colorId));
        return drawable;
    }

    private static final ButterKnife.Setter<View, Integer> TEXT_COLOR = new ButterKnife.Setter<View, Integer>() {
        @Override
        public void set(View view, Integer color, int index) {
            ((EditText) view).setTextColor(color);
        }
    };

    private static final ButterKnife.Setter<View, GradientDrawable> BACKGROUND_DRAWABLE = new ButterKnife.Setter<View, GradientDrawable>() {
        @Override
        public void set(View view, GradientDrawable drawable, int index) {
            view.setBackground(drawable);
        }
    };

    private String getPassword() {
        return pinDigit1.getText().toString() + pinDigit2.getText().toString() +
                pinDigit3.getText().toString() + pinDigit4.getText().toString();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onPause() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onStop() {
        BusProvider.getRestBus().unregister(this);
        super.onStop();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
