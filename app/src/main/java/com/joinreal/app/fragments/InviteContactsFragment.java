package com.joinreal.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.app.interfaces.ISearchToolbarListener;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.app.presenters.ContactsLayoutCreator;
import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.ContactsActionVM;
import com.joinreal.model.view.ContactsSubtitleVM;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.utils.ChatKeyboardUtils;
import com.joinreal.utils.ContactManager;

import java.util.ArrayList;


/**
 * Created by brittbarak on 7/6/15.
 */
public class InviteContactsFragment extends RealBaseFragment implements ContactsLayoutCreator.ContactsLayoutCreatorListener {
    public static final String TAG = "InviteContactsFragment";
    private ContactsLayoutCreator contactsLayoutCreator;
    OpenChatListener listener;
    AgentActionsListener agentActionsListener;
    ISearchToolbarListener searchToolbarListener;
    ArrayList<String> contactPhonesToInvite;
    private Menu menu;
    private TextView contactsSelectedTV;
    private boolean didClickInvite;

    public static InviteContactsFragment getNewInstance(String entryTagBeforeInviteFragment) {
        InviteContactsFragment fragment = new InviteContactsFragment();
        Bundle bd = new Bundle(1);
        bd.putString(StaticParams.ENTRY_TAG_TO_RETURN_TO, entryTagBeforeInviteFragment);
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        agentActionsListener = (AgentActionsListener) activity;
        searchToolbarListener = (ISearchToolbarListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listener = (OpenChatListener) getActivity();
        contactPhonesToInvite = new ArrayList<>();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.contacts_and_chats_layout, container, false);
        rootView.findViewById(R.id.included_toolbar_search).setVisibility(View.GONE);

        contactsLayoutCreator = new ContactsLayoutCreator(this, searchToolbarListener, true);
        ArrayList<ViewModelItem> viewModelItems = getViewModelItems();
        contactsLayoutCreator.createLayout(rootView, R.drawable.back_arrow_blue, viewModelItems);
        contactsLayoutCreator.enableSwipe(false);

        rootView.findViewById(R.id.invite_contacts_bottom_layout).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contactPhonesToInvite.size() > 0) {
                    onCancelClick();
                    agentActionsListener.openSendInvitesFragment(contactPhonesToInvite, getArguments().getString(StaticParams.ENTRY_TAG_TO_RETURN_TO));
                    didClickInvite = true;
                }
            }
        });

        contactsSelectedTV = (TextView) rootView.findViewById(R.id.contacts_selected);

//TODO: temp
        rootView.findViewById(R.id.progressBar).setVisibility(View.GONE);

        return rootView;
    }

    private ArrayList<ViewModelItem> getViewModelItems() {
        ArrayList<ViewModelItem> viewModelItems = new ArrayList<>();
        viewModelItems.add(new ContactsActionVM("Invite New Contact", R.drawable.contact_invite_new, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agentActionsListener.openInviteNewContactFragment(getArguments().getString(StaticParams.ENTRY_TAG_TO_RETURN_TO));
            }
        }));
        viewModelItems.add(new ContactsSubtitleVM("All Contacts", "INVITE ALL", "DESELECT ALL"));
        viewModelItems.addAll(ContactManager.getViewModelsList());
        return viewModelItems;
    }

    @Override
    public void onResume() {
        super.onResume();
        contactsLayoutCreator.onResume();
        agentActionsListener.lockDrawer("Invite");
        if (didClickInvite) {
            contactPhonesToInvite.clear();
            didClickInvite = false;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //TODO: same as contactsFragment
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_contacts, menu);
        this.menu = menu;
        menu.removeItem(R.id.action_compose);
        MainActivityAbstract.addCancelActionButton(menu, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelClick();
            }
        });

        menu.removeItem(R.id.action_add);
    }

    public void onCancelClick() {
        menu.findItem(MainActivityAbstract.ACTION_CANCEL_ID).setVisible(true);
        menu.findItem(R.id.action_search).setVisible(true);
        searchToolbarListener.onCancelSearchClick();
        contactsLayoutCreator.onCancelClick();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                searchToolbarListener.onSearchActionClick();
                menu.findItem(MainActivityAbstract.ACTION_CANCEL_ID).setVisible(true);
                item.setVisible(false);
                Analytics.getInstance().trackToolbarNavigtionEvent(getScreenName(), "Search");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public ActionBar getActivityActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public Menu getActionMenu() {
        return getActionMenu();
    }

    @Override
    public void onContactRowClick(ContactVM contactVM) {
        if (contactVM.isSelected()) {
            contactPhonesToInvite.add(contactVM.getContact().getPhone());
        } else {
            contactPhonesToInvite.remove(contactVM.getContact().getPhone());
        }
        contactsSelectedTV.setText(getNumContactSelectedText());
        ChatKeyboardUtils.hideKeyboard(getView());
    }

    private String getNumContactSelectedText() {
        if (contactPhonesToInvite.isEmpty()) {
            return "No Contacts Selected";
        } else if (contactPhonesToInvite.size() == 1) {
            return "1 Contact Selected";
        } else {
            return contactPhonesToInvite.size() + " Contacts Selected";
        }
    }

    @Override
    public void onPause() {
        contactsLayoutCreator.onPause();
        onCancelClick();
        super.onPause();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
