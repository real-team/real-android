package com.joinreal.app.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.LocationTracker;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.app.viewholders.SearchResultsViewHolder;
import com.joinreal.app.viewholders.SearchResultsViewHolderAgent;
import com.joinreal.app.viewholders.SearchResultsViewHolderCustomer;
import com.joinreal.model.data.PropertyThin;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by britt on 6/18/15.
 */
public class MapViewPropertiesFragment extends PropertyListFragmentAbstract {

    public static final String TAG = "MapViewPropertiesFragment";
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private View propertyView;
    HashMap <String, Integer> markerIdToPropertyIndex = new HashMap<String, Integer>();
    private Marker currentClickedMarker;
    private String currentClickedMarkerPrice;
    private Geocoder geocoder;
    private double deafultLong;
    private double deafultLat;
    protected int propetiesViewdCount;


    public static final MapViewPropertiesFragment getNewInstance(ArrayList<PropertyThin> properties, double latitude, double longitude) {
        MapViewPropertiesFragment fragment = new MapViewPropertiesFragment();
        Bundle bd = new Bundle(3);
        bd.putSerializable(StaticParams.KEY_PROPERTY_LIST, properties);
        bd.putDouble(StaticParams.KEY_LATITUDE, latitude);
        bd.putDouble(StaticParams.KEY_LONGITUDE, longitude);
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        android.support.v4.app.FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, mapFragment).commit();
        }


/***at this time google play services are not initialize so get map and add what ever you want to it in onResume() or onStart() **/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deafultLat = getArguments().getDouble(StaticParams.KEY_LATITUDE);
        deafultLong = getArguments().getDouble(StaticParams.KEY_LONGITUDE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_mapview, container, false);
        this.propertyView = rootview.findViewById(R.id.included_item_property);
        super.createView();
        return rootview;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_recentyl_posted);
        menu.removeItem(R.id.action_price_lth);
        menu.removeItem(R.id.action_price_htl);
        menu.removeItem(R.id.action_map_view);
        if (! Application.getClient().isAgent()) {
            menu.removeItem(R.id.action_search);
        }
    }

    @Override
    protected void onItemsAddedToList(int itemsCount) {
        for (int i = properties.size() - itemsCount; i < properties.size(); i++) {
            addMarkerForIndexInList(i);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (map == null) { //TODO: can move to create view?
            map = mapFragment.getMap();
            geocoder = new Geocoder(getActivity());
            if (geocoder.isPresent()) {
                for (int i = 0; i < properties.size(); i++) {
                    addMarkerForIndexInList(i);
                }
            } else {
                System.out.println("GEOCODER NOT PRESENT");
            }
            map.getUiSettings().setRotateGesturesEnabled(true);
            setMapFocus();

        }
    }

    private void setMapFocus() {
        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds bounds = builder.build();
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 40);
                map.animateCamera(cu);
                setOnMapClickListener();
                setOnMarkersClickListener();
            }
        });
    }

    private void addMarkerForIndexInList(int i) {
        Marker marker;
        PropertyThin p = properties.get(i);
        if ((p.getLatitude() != null) && (p.getLongitude() != null)) {
            marker = map.addMarker(getMarker(new LatLng(p.getLatitude(), p.getLongitude()), p));
            builder.include(marker.getPosition());
        } else {
            marker = addMarkerByAddress(p.getAddressForLocation(), p);
        }
        markerIdToPropertyIndex.put(marker.getId(), i);
    }

    private void setOnMapClickListener() {
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                propertyView.setVisibility(View.GONE);
                if (currentClickedMarker != null) {
                    currentClickedMarker.setIcon(getMarkerCostumeIcon(currentClickedMarkerPrice, false));
                }
                currentClickedMarker = null;
                currentClickedMarkerPrice = null;
            }
        });
    }

    private void setOnMarkersClickListener() {
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                unselectCurrentMarker();

                int propertyIndex = markerIdToPropertyIndex.get(marker.getId());
                PropertyThin p = properties.get(propertyIndex);

                displayPropertyCard(p);

                selectNewMarker(marker, p);
                propetiesViewdCount++;
                return false;
            }
        });
    }

    private void selectNewMarker(Marker marker, PropertyThin p) {
        marker.setIcon(getMarkerCostumeIcon(p.getShortPrice(), true));
        currentClickedMarker = marker;
        currentClickedMarkerPrice = p.getShortPrice();
    }

    private void displayPropertyCard(PropertyThin p) {
        propertyView.setVisibility(View.VISIBLE);
        //TODO: to factory
        SearchResultsViewHolder holder;
        if (Application.getClient().isAgent()) {
            holder = new SearchResultsViewHolderAgent(propertyView, updateFavoritesHelper, getView(), (OpenChatListener) getActivity(), getScreenName());
        } else {
            holder = new SearchResultsViewHolderCustomer(propertyView, updateFavoritesHelper, getView(), (OpenChatListener) getActivity(), getScreenName());
        }

        holder.onBindView(p);
    }

    private void unselectCurrentMarker() {
        if (currentClickedMarker != null) {
            currentClickedMarker.setIcon(getMarkerCostumeIcon(currentClickedMarkerPrice, false));
            currentClickedMarker = null;
            currentClickedMarkerPrice = null;
        }
    }

    LatLngBounds.Builder builder = new LatLngBounds.Builder();


    private Marker addMarkerByAddress(String location, PropertyThin property) {

        LatLng latLng = LocationTracker.getLatLngFromAddress(location);
        if (latLng != null) {
            MarkerOptions markerOptions = getMarker(latLng, property);
            builder.include(markerOptions.getPosition());
            return map.addMarker(markerOptions);
        } else {
           return map.addMarker(getMarker(new LatLng(deafultLat, deafultLong), property));

        }
    }

    private MarkerOptions getMarker(LatLng latLng, final PropertyThin property) {
        return new MarkerOptions().position(latLng)
                .icon(getMarkerCostumeIcon(property.getShortPrice(), false));
    }

    private BitmapDescriptor getMarkerCostumeIcon(String price, boolean isSelected) {
        View markerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.text_bubble, null);
        TextView numTxt = (TextView) markerView.findViewById(R.id.num_txt);
        numTxt.setText(price);
        TypefaceManager.getInstance(getActivity()).assignTypeface(numTxt);

        ImageView bubbleImg = (ImageView) markerView.findViewById(R.id.bubble_img);
        bubbleImg.setImageDrawable(Helper.getDrawable(getActivity(), isSelected ? R.drawable.price_bubble_pink : R.drawable.price_bubble_blue));

        return BitmapDescriptorFactory.fromBitmap(createDrawableFromView(markerView));
    }

    // Convert a view to bitmap
    public Bitmap createDrawableFromView(View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public void setScreenName() {
        super.setScreenName();
        screenName += "Map";
    }

    @Override
    protected int getPropetiesViewedCount() {
        return propetiesViewdCount;
    }
}
