package com.joinreal.app.fragments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.AgentActionsListener;

/**
 * Created by brittbarak on 7/14/15.
 */
public class InviteSentFragment extends RealBaseFragment {
    private static final String KEY_INVITES_COUNT = "KEY_INVITES_COUNT";
    private static final String KEY_INVITE_NAME = "KEY_INVITE_NAME";
    public static final String TAG = "InviteSentFragment";
    AgentActionsListener agentActionsListener;

    public static InviteSentFragment getNewInstance(int count, String displayName, String entryTagToReturnTo) {
        InviteSentFragment fragment = new InviteSentFragment();
        Bundle bd = new Bundle(3);
        bd.putInt(KEY_INVITES_COUNT, count);
        bd.putString(KEY_INVITE_NAME, displayName);
        bd.putString(StaticParams.ENTRY_TAG_TO_RETURN_TO, entryTagToReturnTo);
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        agentActionsListener = (AgentActionsListener) getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_invitation_sent, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        rootView.findViewById(R.id.not_in_contacts_note).setVisibility(View.INVISIBLE);

        int invitesCount = getArguments().getInt(KEY_INVITES_COUNT);
        if (invitesCount == 1) {
            String name = getArguments().getString(KEY_INVITE_NAME, null);
            if (name != null) {
                ((TextView) rootView.findViewById(R.id.sent_invitations_text)).setText("invitations sent to");
                ((TextView)rootView.findViewById(R.id.phone_number_sent_to)).setText(name);
            } else {
                ((TextView) rootView.findViewById(R.id.sent_invitations_text)).setText("invitations sent");
                rootView.findViewById(R.id.phone_number_sent_to).setVisibility(View.GONE);
            }
        } else {
            ((TextView) rootView.findViewById(R.id.sent_invitations_text)).setText(invitesCount + " invitations sent");
            rootView.findViewById(R.id.phone_number_sent_to).setVisibility(View.GONE);
        }

        rootView.findViewById(R.id.invitations_sent_layout).setVisibility(View.VISIBLE);
        String title = getArguments().getInt(KEY_INVITES_COUNT) == 1 ? "Inviation sent" : getArguments().getInt(KEY_INVITES_COUNT) + " invitations sent";
        ((TextView) rootView.findViewById(R.id.sent_invitations_text)).setText(title);
        rootView.findViewById(R.id.phone_number_sent_to).setVisibility(View.GONE);

        TextView upperButton = ((TextView) rootView.findViewById(R.id.upper_button));
        upperButton.setText("Send More");
        upperButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack(getArguments().getString(StaticParams.ENTRY_TAG_TO_RETURN_TO), 0);
                agentActionsListener.openInviteContactsFragment(getArguments().getString(StaticParams.ENTRY_TAG_TO_RETURN_TO));
            }
        });

        TextView lowerButton = ((TextView) rootView.findViewById(R.id.lower_button));
        lowerButton.setText("Done");
        lowerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack(getArguments().getString(StaticParams.ENTRY_TAG_TO_RETURN_TO), 0);
            }
        });


        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        agentActionsListener.setToolbarTitle("Invitations Sent");
        agentActionsListener.setHomeIcon(R.drawable.hamburger);

    }

//    @Override
//    public void onStop() {
//        getActivity().getSupportFragmentManager().popBackStack(getArguments().getString(StaticParams.ENTRY_TAG_TO_RETURN_TO), 0);
//        super.onStop();
//    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
