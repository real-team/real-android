package com.joinreal.app.fragments;

import android.os.Bundle;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.events.ReceivedPropertiesEvent;
import com.joinreal.model.data.PropertyThin;
import com.joinreal.tasks.GetCustomerFavoritesTask;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;


/**
 * Created by britt on 6/16/15.
 */
public class FavoritesContainerFragment extends PropertyListContainerFragment {

    public static final String TAG = "Favorites Container";

    public static FavoritesContainerFragment getNewInstance(Bundle bd) {
        FavoritesContainerFragment fragment = new FavoritesContainerFragment();
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(StaticParams.KEY_PROPERTY_LIST) || (properties != null)) {
            properties = (ArrayList<PropertyThin>) getArguments().get(StaticParams.KEY_PROPERTY_LIST);
            openInitialPropertyListFragment();
        }
        if (!Application.getClient().isAgent()) {
            new GetCustomerFavoritesTask().execute();
        }
        setHasOptionsMenu(false);
    }

    @Override
    public void setToolbar() {
        String title = Application.getClient().isAgent() ? getArguments().getString(StaticParams.KEY_CONTACT_NAME) + "'s Favorites" : "My Favorites";
        navigationListener.setToolbarTitle(title);
        if (navigationListener instanceof AgentActionsListener) {
            ((AgentActionsListener) navigationListener).lockDrawer(title);
        }
    }


    @Subscribe
    public void getPropertyList(ReceivedPropertiesEvent event) {
        properties = event.getProperties();
        if (properties == null) {
            properties = new ArrayList<>();
            Helper.showNetworkErrorToast(getActivity(), NetworkCodes.CANT_GET_FAVORITES);
        }
        openInitialPropertyListFragment();

    }


    //TODO: into interface or something
    public void openMapView() {
        MapViewPropertiesFragment mapFragment = MapViewPropertiesFragment.getNewInstance(properties, json.get("latitude").getAsDouble(), json.get("longitude").getAsDouble());
        //TODO: for test, until server bug is fixed:
//        MapViewPropertiesFragment mapFragment = MapViewPropertiesFragment.getInstance(properties, 29.7604, 95.3698);

        getChildFragmentManager().beginTransaction().replace(R.id.search_results_container, mapFragment).commit();
    }

    public void openListView() {
        SearchResultsListFragment listViewFragment = SearchResultsListFragment.getNewInstance(getArguments(), properties);
        getChildFragmentManager().beginTransaction().replace(R.id.search_results_container, listViewFragment).commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (navigationListener instanceof AgentActionsListener) {
            ((AgentActionsListener) navigationListener).unlockDrawer();
        }
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
