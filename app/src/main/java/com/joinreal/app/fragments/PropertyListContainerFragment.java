package com.joinreal.app.fragments;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.joinreal.R;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.app.interfaces.INavigationListener;
import com.joinreal.events.LoadMorePropertiesEvent;
import com.joinreal.events.MorePropertiessAddedEvent;
import com.joinreal.events.ReceivedPropertiesEvent;
import com.joinreal.events.SortPropertiesEvent;
import com.joinreal.model.data.PropertyThin;
import com.joinreal.tasks.FindPropertiesTask;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;


/**
 * Created by britt on 6/16/15.
 */
public class PropertyListContainerFragment extends RealBaseFragment {
    public static final String TAG = "Search Results Container";
    private static final Double DEFAULT_LAT = 29.7604;
    private static final Double DEFAULT_LONG = 95.3698;
    protected FindPropertiesTask task;
    protected ArrayList<PropertyThin> properties;
    JsonObject json;
    protected int resultsPageIndex = 0;
    INavigationListener navigationListener;

    public static PropertyListContainerFragment getInstance(Bundle bd) {
        PropertyListContainerFragment fragment = new PropertyListContainerFragment();
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        navigationListener = (INavigationListener) activity;

    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getBus().register(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        json = new Gson().fromJson(getArguments().getString(StaticParams.KEY_FILTERS_JSON), JsonObject.class);
        if (getArguments().getBoolean(StaticParams.KEY_OPEN_NO_RESULTS_PAGE, false)) {
            openInitialPropertyListFragment();
        } else if (! getArguments().containsKey(StaticParams.KEY_IS_FAVORITES_LIST)) {
            executeNewFindPropertiesTask();
            setHasOptionsMenu(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_property_list_container, container, false);
        setProgressbar(rootView);
        return rootView;
    }

    private void setProgressbar(View rootView) {
        ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.blue_dark),
                PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbar();
    }

    public void setToolbar() {
        String title = getTitle();
        if (navigationListener instanceof AgentActionsListener) {
            if (getArguments().getBoolean(StaticParams.KEY_SHOULD_LOCK_DRAWER, false)) {
                ((AgentActionsListener) navigationListener).lockDrawer(title);
            } else {
                ((AgentActionsListener) navigationListener).unlockDrawer(title);
            }
        }
        navigationListener.setToolbarTitle(title);
    }

    protected String getTitle() {
        if (getArguments().containsKey(StaticParams.KEY_CONTACT_NAME)) {
            return "Contact " + getArguments().getString(StaticParams.KEY_CONTACT_NAME);
        } else {
            return getArguments().getString(StaticParams.KEY_TITLE_PROPERTY_LIST, null);
        }
    }


    @Subscribe
    public void getPropertyList(ReceivedPropertiesEvent event) {
        if (event.getProperties() != null) {

            if (resultsPageIndex == 0) {
                properties = event.getProperties();
                openInitialPropertyListFragment();
            } else {
                properties.addAll(event.getProperties());
                BusProvider.getBus().post(new MorePropertiessAddedEvent(properties.size()));
            }
        } else {
            Helper.showNetworkErrorToast(getActivity(), NetworkCodes.CANT_GET_PROPERTIES);

        }
    }

    public void openInitialPropertyListFragment() {
        Fragment fragment;
        if ((properties == null) || (properties.size() == 0)) {
            fragment = NoResultsFragment.getNewInstance(getArguments().getString(StaticParams.KEY_STATE), getArguments().getBoolean(StaticParams.KEY_IS_FAVORITES_LIST));
            getChildFragmentManager().beginTransaction().replace(R.id.search_results_container, fragment, NoResultsFragment.TAG).commit();
        } else {
            fragment = SearchResultsListFragment.getNewInstance(getArguments(), properties);
            getChildFragmentManager().beginTransaction().replace(R.id.search_results_container, fragment, SearchResultsListFragment.TAG).commit();
        }
    }


    @Subscribe
    public void onShouldLoadMoreProperties(LoadMorePropertiesEvent event) {
        resultsPageIndex++;
        executeNewFindPropertiesTask();
    }


    @Subscribe
    public void onShouldLSortProperties(SortPropertiesEvent event) {
        resultsPageIndex = 0;
        json.addProperty("sort_by", event.getSortByValue());
        executeNewFindPropertiesTask();

        Fragment fragment = getChildFragmentManager().findFragmentByTag(SearchResultsListFragment.TAG);
        if (fragment != null) {
            getChildFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    private void executeNewFindPropertiesTask() {
        task = new FindPropertiesTask(getActivity(), json, resultsPageIndex, getArguments().getString(StaticParams.KEY_CONTACT_ID, null));
        task.execute();
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getBus().unregister(this);

    }

    //TODO: into interface or something
    public void openMapView() {
        Double defaultLat = (json.get("latitude") != null) ? json.get("latitude").getAsDouble() : DEFAULT_LAT;
        Double defaultLng = (json.get("longitude") != null) ? json.get("longitude").getAsDouble() : DEFAULT_LONG;
        MapViewPropertiesFragment mapFragment = MapViewPropertiesFragment.getNewInstance(properties, defaultLat, defaultLng);
        getChildFragmentManager().beginTransaction().replace(R.id.search_results_container, mapFragment).addToBackStack(mapFragment.TAG).commit();
        setToolbar();
    }

    public void openListView() {
        SearchResultsListFragment listViewFragment = SearchResultsListFragment.getNewInstance(getArguments(), properties);
        getChildFragmentManager().beginTransaction().replace(R.id.search_results_container, listViewFragment).addToBackStack(listViewFragment.TAG).commit();
        setToolbar();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
