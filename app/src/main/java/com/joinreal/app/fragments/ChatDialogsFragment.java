package com.joinreal.app.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.app.activities.chat.CreateNewChatActivity;
import com.joinreal.app.activities.chat.PrivateChatActivity;
import com.joinreal.app.adapters.chat.ChatContactsAdapter;
import com.joinreal.app.adapters.chat.ChatDialogsAdapter;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.Prefs;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.app.interfaces.ISearchToolbarListener;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.events.ChatDialogsReceivedEvent;
import com.joinreal.events.ContactsUpdatedEvent;
import com.joinreal.model.data.Contact;
import com.joinreal.model.data.chat.ChatDialogDataVM;
import com.joinreal.utils.ChatDialogsManager;
import com.joinreal.utils.ContactManager;
import com.quickblox.chat.model.QBDialog;
import com.squareup.otto.Subscribe;

/**
 * Created by brittbarak on 7/14/15.
 */
public class ChatDialogsFragment extends RealBaseFragment implements ChatDialogsAdapter.DialogsEventListener {

    public static final String TAG = "ContactsFragment";
    OpenChatListener openChatListener;
    AgentActionsListener agentActionsListener;
    ISearchToolbarListener searchToolbarListener;
    private Menu menu;

    private ChatDialogsAdapter dialogAdapter;

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private TextWatcher textWatcher;

    private boolean didSendMessage;

    public static ChatDialogsFragment getInstance(Bundle bd) {
        ChatDialogsFragment fragment = new ChatDialogsFragment();
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        agentActionsListener = (AgentActionsListener) activity;
        searchToolbarListener = (ISearchToolbarListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openChatListener = (OpenChatListener) getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.contacts_and_chats_layout, container, false);
        rootView.findViewById(R.id.included_toolbar_search).setVisibility(View.GONE);
        setListView(rootView);
        textWatcher = getSearchViewTextWatcher();
        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }

    private void setListView(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        dialogAdapter = new ChatDialogsAdapter(this, ChatDialogsManager.getInstance().produceAnswer().getDialogs());
        recyclerView.setAdapter(dialogAdapter);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);

    }

    private void saveDialogsToPrefs() {
        //TODO move to prefs, and dont commit every time.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (dialogAdapter != null) {
                    Prefs.getInstance().saveDialogs(dialogAdapter.getDataSource());
                }
            }
        }, 1000);
    }

    private TextWatcher getSearchViewTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dialogAdapter.setFilter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }


    private View.OnClickListener getSearchIconClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchToolbarListener.onSearchActionClick();

            }
        };
    }

    private View.OnClickListener getCancelClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchToolbarListener.onCancelSearchClick();

                ((ChatContactsAdapter) recyclerView.getAdapter()).resetFilter();
                menu.findItem(R.id.action_add).setVisible(true);
                menu.findItem(R.id.action_search).setVisible(true);
            }
        };
    }

    public void setActionButton(ImageView imageView) {
        imageView.setImageDrawable(Helper.getDrawable(getActivity(), R.drawable.plus));
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
                intent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                startActivity(intent);
            }
        });
        imageView.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        searchToolbarListener.addTextChangedListener(textWatcher);
        ((MainActivityAbstract) getActivity()).setToolbarTitle("Chats");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburger);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_contacts, menu);
        this.menu = menu;
        menu.removeItem(R.id.action_add);
        MainActivityAbstract.addCancelActionButton(menu, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("fragment on click openChatListener");
                onCancelClick();
            }
        });
    }

    public void onCancelClick() {
        menu.findItem(MainActivityAbstract.ACTION_CANCEL_ID).setVisible(true);
//        menu.findItem(R.id.action_search).setVisible(true);
        menu.findItem(R.id.action_compose).setVisible(true);

        searchToolbarListener.onCancelSearchClick();
        dialogAdapter.resetFilter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                searchToolbarListener.onSearchActionClick();
                menu.findItem(R.id.action_compose).setVisible(false);
                menu.findItem(MainActivityAbstract.ACTION_CANCEL_ID).setVisible(true);
                item.setVisible(false);
                Analytics.getInstance().trackToolbarNavigtionEvent(getScreenName(), "Search");
                return true;
            case R.id.action_compose:
                startActivity(new Intent(Application.getAppContext(), CreateNewChatActivity.class));
                Analytics.getInstance().trackToolbarNavigtionEvent(getScreenName(), "New Chat");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public void onDialogsRecievedEvent(final ChatDialogsReceivedEvent e) {
        System.out.println("ChatDialogsFragment ChatDialogsReceivedEvent : " + e.getDialogs().size());
        dialogAdapter.updateDialogs(e.getDialogs());
        recyclerView.scrollToPosition(0);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
//                swipeRefreshLayout.setRefreshing(e.areDialogsFromPrefs() || (! ContactManager.didMergeContactsOnce()));
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        saveDialogsToPrefs();
    }

    @Subscribe
    public void onContactsUpdated(ContactsUpdatedEvent e) {
        if (e.isUpdateComplete()) {
            dialogAdapter.updateDialogs();
        }
    }

    @Override
    public void onPause() {
        searchToolbarListener.removeTextChangedListener(textWatcher);
        onCancelClick();
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("ChatDialogsFragment register (ChatDialogsReceivedEvent)");
        BusProvider.getBus().register(this);
        BusProvider.getBus().register(dialogAdapter);
    }

    @Override
    public void onStop() {
        BusProvider.getBus().unregister(this);
        BusProvider.getBus().unregister(dialogAdapter);
        super.onStop();
    }

    @Override
    public void onDialogClick(QBDialog dialog) {
        //TODO: Through main activity
        Intent chat = new Intent(Application.getAppContext(), PrivateChatActivity.class);
        chat.putExtra(PrivateChatActivity.DIALOG, dialog);
        if ((!didSendMessage) && (getArguments() != null)) {
            chat.putExtras(getArguments());
            didSendMessage = true; //so a message won't be sent again when going back from PrivateChat
        }
        startActivity(chat);
        dialog.setUnreadMessageCount(0);
        dialogAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDialogDelete(final ChatDialogDataVM chatDialogDataVM) {
//        ChatDialogs.chooseDialog(getActivity(), getResources().getString(R.string.do_you_want_to_delete_dialog),
//                getResources().getString(R.string.yes),
//                getOnShouldDeleteClickListener(chatDialogDataVM), getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
    }

    @Override
    public void onUserFound(final Contact contact, final ChatDialogDataVM data) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                System.out.println("in activity onContactFound, run(), contact = null ? " + (contact == null));
                if (contact != null) {
                    if (contact.getDisplayName() != null && !contact.getDisplayName().equals("")) {
                        data.setChatNameFromContacts(contact.getDisplayName());
                    } else {
                        data.setChatNameFromContacts(contact.getPhone());
                    }
                    data.setChatLogin(contact.getChatLogin());
                    if (dialogAdapter != null) {
                        dialogAdapter.resetFilter();
                    }
                }
            }
        });
    }

    @Override
    public void inviteContacts() {
        agentActionsListener.openInviteContactsFragment();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
