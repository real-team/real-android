package com.joinreal.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.INavigationListener;
import com.joinreal.model.data.Agent;
import com.joinreal.utils.ChatImagesUtils;
import com.squareup.picasso.Picasso;

/**
 * Created by brittbarak on 7/26/15.
 */
public class AgentProfileReadOnlyFragment extends RealBaseFragment {

    public static final String TAG = "AgentProfileReadOnlyFragment";
    protected Agent agent;
    protected View rootView;
    protected TextView buyersCheckbox;
    protected TextView sellersCheckbox;
    protected TextView rentersCheckbox;
    protected TextView birthday;
    protected ImageView thumb;
    protected TextView agentTitle;
    protected TextView about;
    protected TextView phone;
    protected TextView email;
    protected TextView address;
    protected TextView licenseStates;
    protected TextView zipCodes;
    protected TextView languages;
    protected TextView website;
    protected TextView linkedin;
    protected TextView facebook;
    protected TextView twitter;
    protected TextView name;

    protected INavigationListener navigationListener;

    public static AgentProfileReadOnlyFragment getNewInstance() {
        return new AgentProfileReadOnlyFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
            this.navigationListener = (INavigationListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigationListener.setHomeIcon(R.drawable.ic_close);
        }

    @Override
    public void onResume() {
        super.onResume();
        navigationListener.setToolbarTitle("My Agent");
        if (! Application.getClient().isAgent()) {
            navigationListener.containerBelowToolbar();
        }
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        agent = Application.getClient().isAgent() ? Application.getClient().getUser().getAgent() : Application.getClient().getAssignedAgent();
        rootView = inflater.inflate(R.layout.fragment_agent_profile, container, false);
        setContentView();
        return rootView;
    }


    protected void setContentView() {
        thumb = (ImageView) rootView.findViewById(R.id.thumb);
        if (agent.getNewProfileImage() != null) {
            thumb.setImageBitmap(agent.getNewProfileImage());
        } else {
            Picasso.with(getActivity()).load(agent.getImage()).transform(ChatImagesUtils.getCircleTransform()).into(thumb);
        }

        name = (TextView) rootView.findViewById(R.id.agent_name);
        name.setText(agent.getFullName());
        agentTitle = (TextView) rootView.findViewById(R.id.agent_title);
        agentTitle.setText(agent.getTitle());
        about = (TextView) rootView.findViewById(R.id.agent_about);
        about.setText(agent.getAbout());
        phone = (TextView) rootView.findViewById(R.id.phone);
        phone.setText(agent.getPhone());
        email = (TextView) rootView.findViewById(R.id.email);
        email.setText(agent.getEmail());
        address = (TextView) rootView.findViewById(R.id.address);
        setAddressTextView();

        birthday = (TextView) rootView.findViewById(R.id.birthday);
        setBirthdayTextView();

        setRealtorInfoExpandableView(rootView);

        setSocialMediaExpandableView(rootView);

        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
    }

    protected void setAddressTextView() {
        if (agent.getFullAddress() == null) {
            address.setVisibility(View.GONE);
        } else {
            address.setText(agent.getFullAddress());
        }
    }

    protected void setBirthdayTextView() {
        if (agent.getBirthdayToDisplay() == null) {
            birthday.setVisibility(View.GONE);
        } else {
            birthday.setText(agent.getBirthdayToDisplay());
        }
    }

    private void setRealtorInfoExpandableView(View rootView) {
        setWorkWithCheckBoxes();

        licenseStates = (TextView) rootView.findViewById(R.id.states);
        licenseStates.setText(agent.getLicenseStates());
        zipCodes = (TextView) rootView.findViewById(R.id.zip_codes);
        zipCodes.setText(agent.getZipcodes());
        languages = (TextView) rootView.findViewById(R.id.languages);
        languages.setText(agent.getLanguages());
    }

    protected void setWorkWithCheckBoxes() {
        buyersCheckbox = (TextView) rootView.findViewById(R.id.buyers_checkbox);
        setCheckbox(buyersCheckbox, agent.isWorkWithBuyers());
        sellersCheckbox = (TextView) rootView.findViewById(R.id.sellers_checkbox);
        setCheckbox(sellersCheckbox, agent.isWorkWithSellers());
        rentersCheckbox = (TextView) rootView.findViewById(R.id.renters_checkbox);
        setCheckbox(rentersCheckbox, agent.isWorkWithRenters());
    }

    protected void setCheckbox(TextView buyersCheckbox, boolean isChecked) {
        buyersCheckbox.setCompoundDrawablesWithIntrinsicBounds(0, (isChecked ? R.drawable.check_box : R.drawable.check_box_empty), 0, 0);
    }

    protected void setSocialMediaExpandableView(View rootView) {
        website = (TextView) rootView.findViewById(R.id.website);
        setSocialMediaTextView(website, agent.getWebsiteUrl());

        linkedin = (TextView) rootView.findViewById(R.id.linkedin);
        setSocialMediaTextView(linkedin, agent.getLinkedinProfileUrl());

        facebook = (TextView) rootView.findViewById(R.id.facebook);
        setSocialMediaTextView(facebook, agent.getFacebookProfileUrl());

        twitter = (TextView) rootView.findViewById(R.id.twitter);
        setSocialMediaTextView(twitter, agent.getTwitterProfileUrl());


        hideSocialExpandableIfNeeded();
    }

    protected void hideSocialExpandableIfNeeded() {
        boolean areNoSocialUrls = (agent.getWebsiteUrl() == null) && (agent.getLinkedinProfileUrl() == null)
                && (agent.getFacebookProfileUrl() == null) && (agent.getTwitterProfileUrl() == null);

        if (areNoSocialUrls) {
            rootView.findViewById(R.id.social_media_expandable_card).setVisibility(View.GONE);
        }
    }

    protected void setSocialMediaTextView(TextView textView, String url) {
        if ((url == null) || (url.length() == 0)) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setText(url);
        }
    }

    @Override
    public void onPause() {
        if (! Application.getClient().isAgent()) {
            navigationListener.alignContainerAndToolbarTop();
            navigationListener.setHomeIcon(R.drawable.user_profile);
        }
        super.onPause();

    }
}
