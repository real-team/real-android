package com.joinreal.app.fragments;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.joinreal.R;
import com.joinreal.app.interfaces.INavigationListener;

/**
 * Created by brittbarak on 8/9/15.
 */
public class WebviewFragment extends RealBaseFragment {
    public static final String TAG = "WebviewFragment";
    private static final String KEY_URL = "KEY_URL";
    private WebView webview;
    private ProgressBar proggressbar;
    private INavigationListener listener;

    public static WebviewFragment getNewInstance(String url) {
        WebviewFragment fragment = new WebviewFragment();
        Bundle bd = new Bundle(1);
        bd.putString(KEY_URL, url);
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (INavigationListener)activity;
    }

    @Override
    public void onStart() {
        super.onStart();
        listener.containerBelowToolbar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_webview, container, false);
        webview = (WebView) rootView.findViewById(R.id.webview);
        proggressbar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        proggressbar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.blue_dark),
                PorterDuff.Mode.SRC_ATOP);
        configureWebview();
        return rootView;
    }

    private void configureWebview() {
        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.setWebViewClient(new CustomeBrowser());
        webview.loadUrl(getArguments().getString(KEY_URL));
    }

    @Override
    public void onStop() {
        super.onStop();
        listener.alignContainerAndToolbarTop();
    }

    private class CustomeBrowser extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished(WebView view, String url) {
            proggressbar.setVisibility(View.GONE);
        }
    }

    @Override
    protected String getScreenName() {
        return getArguments().getString(KEY_URL);
    }
}
