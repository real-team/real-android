package com.joinreal.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.FiltersActivity;
import com.joinreal.app.adapters.PlaceArrayAdapter;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.LocationTracker;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.SearchButtonVisibilityEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.LocationObj;
import com.joinreal.model.data.User;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by britt on 6/8/15.
 */
public class SearchLocationFragment extends RealBaseFragment implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationTracker.LocationRecievedListener, FiltersActivity.BuildJsonListener {

    private static final String LOG_TAG = "SearchLocationFragment";
    @InjectView(R.id.auto_complete_text_view)
    AutoCompleteTextView mAutocompleteTextView;
    @InjectView(R.id.current_location_img)
    ImageView currLocation;
    @InjectView(R.id.clear)
    ImageView clear;
    @InjectView(R.id.search_icon)
    ImageView searchIcon;
    @InjectView(R.id.cancel)
    TextView cancel;
    @InjectView(R.id.places_listview)
    ListView placeListView;

    private LatLng latLng;
    private String locationShortString;
    private String locationDescription = "";

    public static final String TAG = SearchLocationFragment.class.getSimpleName();
    private LocationTracker locationTracker;
    private LocationObj locationObj;
    private SearchButtonVisibilityEvent searchButtonVisibilityEvent;

    public static SearchLocationFragment getNewInstance(LocationObj locationObj, String locationString, String locationDesc) {
        SearchLocationFragment fragment = new SearchLocationFragment();
        if (locationObj != null) {
            Bundle bd = new Bundle(3);
            bd.putString(StaticParams.LOCATION_OBJ_JSON, new Gson().toJson(locationObj));
            bd.putString(StaticParams.KEY_LOCATION_STR, locationString);
            bd.putString(StaticParams.KEY_LOCATION_DESC, locationDesc);
            fragment.setArguments(bd);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationTracker = new LocationTracker();
        setHasOptionsMenu(true);
        searchButtonVisibilityEvent = new SearchButtonVisibilityEvent(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_location_fragment, container, false);
        ButterKnife.inject(this, rootView);
        //TODO: move this to application or client?
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .build();

        setupToolBar(rootView);

        setAutoComplete();
        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }

    ActionBar ab;

    private void setupToolBar(View rootView) {
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        ab.setHomeAsUpIndicator(Helper.getDrawable(getActivity(), R.drawable.ic_close));
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;


    //TODO: chec????
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));

    private void setAutoComplete() {
        mAutocompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus || v.hasFocus()) {
                    onAutocompleteViewClick();
                }
            }
        });
        mAutocompleteTextView.setThreshold(3);

        mAutocompleteTextView.setOnItemClickListener(mAutocompleteClickListener);

        mPlaceArrayAdapter = new PlaceArrayAdapter(getActivity(), BOUNDS_MOUNTAIN_VIEW, null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);

        placeListView.setAdapter(mPlaceArrayAdapter);
        placeListView.setOnItemClickListener(mAutocompleteClickListener);

        if (getArguments() != null) {
            locationShortString = getArguments().getString(StaticParams.KEY_LOCATION_STR);
            locationDescription = getArguments().getString(StaticParams.KEY_LOCATION_DESC);
            locationObj = new Gson().fromJson(getArguments().getString(StaticParams.LOCATION_OBJ_JSON), LocationObj.class);
            if (locationShortString == null) {
                locationShortString = locationObj.getCity() + ", " + locationObj.getState();
                locationDescription = locationObj.getLocationDesctiption();
            }
            mAutocompleteTextView.setText(locationDescription);
        }


    }

    private String locationType;
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            searchButtonVisibilityEvent.setShouldShowButton(true);
            BusProvider.getBus().post(searchButtonVisibilityEvent);

            if (position == 0) {
                onGetCurrenLocationClick();
            } else {
                final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
                final String placeId = String.valueOf(item.placeId);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
                onCancelClick();
                locationShortString = (String) item.description;
                locationDescription = (String) item.description;
                if (locationObj != null) {
                    locationObj.setDescription(locationDescription);
                }
                locationType = item.getPlaceType();
                mAutocompleteTextView.setText(item.description);
            }
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Crashlytics.log(getClass().getSimpleName() + " Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Selecting the first object buffer.
            if (places != null) {
                final Place place = places.get(0);
                latLng = place.getLatLng();
                parseLocationByLatLng();
            }
            places.release();
        }
    };

    private void parseLocationByLatLng() {
        locationTracker.parseLocation(latLng.latitude, latLng.longitude);
        locationObj = locationTracker.getLocationObj();
        locationShortString = locationTracker.getLocationString();
        locationTracker.stopTrackingLocationFromNetwork();
    }


    @Override
    public void onConnected(Bundle bundle) {
        System.out.println("onConnected");
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        System.out.println("onConnectionFailed");
        User user = Application.getClient().getUser();
        Crashlytics.log(getClass().getSimpleName() + " Google Places API connection failed with error code: " + connectionResult.getErrorCode());
        BusProvider.getRestBus().post(new NetworkExceptionEvent(new NetworkException("On SearchLocationFragment Google Places API connection failed with error code: " + connectionResult.getErrorCode()), NetworkCodes.ERROR_GOOGLE_PLACES));
//        Helper.getExceptionSnackbar(getActivity(), getActivity().findViewById(android.R.id.content), "onConnectionFailed to find location, error code : " + connectionResult.getErrorCode(), user.getUsername(), user.getToken(), "").show();
    }


    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("onConnectionSuspended");
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

//    @OnClick(R.id.auto_complete_text_view)
    @OnClick(R.id.autocomplete_layout)
    public void onAutocompleteViewClick() {
        searchButtonVisibilityEvent.setShouldShowButton(false);
        BusProvider.getRestBus().post(searchButtonVisibilityEvent);

        ab.setDisplayHomeAsUpEnabled(false);
        currLocation.setVisibility(View.GONE);
        clear.setVisibility(View.VISIBLE);
        cancel.setVisibility(View.VISIBLE);
        placeListView.setVisibility(View.VISIBLE);
        placeListView.setAdapter(mPlaceArrayAdapter);
        searchIcon.setImageDrawable(Helper.getDrawable(getActivity(), R.drawable.ic_search_small));

    }

    @OnClick(R.id.clear)
    public void onClearClick() {
        locationDescription = "";
        onLocationRecieved("", null);
        mPlaceArrayAdapter.clear();
    }

    @OnClick(R.id.cancel)
    public void onCancelClick() {
        searchButtonVisibilityEvent.setShouldShowButton(true);
        BusProvider.getRestBus().post(searchButtonVisibilityEvent);

        onLocationRecieved("", null);
        mPlaceArrayAdapter.clear();
        ab.setDisplayHomeAsUpEnabled(true);

        currLocation.setVisibility(View.VISIBLE);
        clear.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);
        placeListView.setVisibility(View.GONE);
        searchIcon.setImageDrawable(Helper.getDrawable(getActivity(), R.drawable.search_small_gray));

        mAutocompleteTextView.clearFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mAutocompleteTextView.getWindowToken(), 0);
        //TODO: animation

    }


    @OnClick(R.id.current_location_img)
            public void onGetCurrenLocationClick() {
        locationDescription = "CURRENT LOCATION";
        mAutocompleteTextView.setText(locationDescription);
        locationTracker.getUserLocation(getActivity(), this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onLocationRecieved(String locationString, LocationObj location) {
        this.locationShortString = locationString;
        locationObj = location;
        if (location != null) {
            locationDescription = location.getLocationDesctiption();
            latLng = new LatLng(location.getLat(), location.getLng());
        } else {
            locationDescription = locationString;
            latLng = null;
        }
        mAutocompleteTextView.setText(locationDescription);
        locationTracker.stopTrackingLocationFromNetwork();
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
        jsonObject.addProperty("latitude", locationObj.getLat());
        jsonObject.addProperty("longitude", locationObj.getLng());
        jsonObject.addProperty("city", locationObj.getCity());
        jsonObject.addProperty("state", locationObj.getState());
        jsonObject.addProperty("street_name", locationObj.getStreet());
        jsonObject.addProperty("zip", locationObj.getZip());
        jsonObject.addProperty("location_type", locationType);
    }

    @Override
    public boolean isValidated(Context context, View parentView) {
        boolean isValidated = (locationObj != null);
        if (!isValidated) {
            Helper.getSnackbar(context, parentView, "Please enter Address, ZIP or Place")
                    .show();
        }
        return isValidated;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    public String getState() {
        return locationObj.getState();
    }

    public String getLocationShortString() {
        return locationShortString;
    }

    public String getLocationDesc() {
        return locationDescription;
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
