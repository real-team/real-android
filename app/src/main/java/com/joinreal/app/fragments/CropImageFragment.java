package com.joinreal.app.fragments;

/**
 * Created by brittbarak on 8/30/15.
 */

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.utils.ChatKeyboardUtils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class CropImageFragment extends RealBaseFragment{
    public static final String TAG = "Crop Image Fragment";
    private CropImageView mCropImageView;
    private ImageView imageview;

    public static CropImageFragment getInstance() {
        return new CropImageFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.activity_crop_image, container, false);
        mCropImageView = (CropImageView) rootView.findViewById(R.id.crop_imageview);
        imageview = (ImageView) rootView.findViewById(R.id.imageview);
        if (Application.getClient().hasNewProfileImage()) {
            mCropImageView.setImageBitmap(Application.getClient().getNewProfileImage());
            imageview.setVisibility(View.GONE);
            mCropImageView.setVisibility(View.VISIBLE);
        } else {
            Picasso.with(getActivity()).load(Application.getClient().getProfileImage()).into(imageview);
            imageview.setVisibility(View.VISIBLE);
            mCropImageView.setVisibility(View.GONE);
//            mCropImageView.setImageUri(Uri.parse("file://"+Application.getClient().getProfileImage()));
        }
        startActivityForResult(getPickImageChooserIntent(), 200);
        setOnClickListeners(rootView);
        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }

    private void setOnClickListeners(View rootView) {
        rootView.findViewById(R.id.save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveCroppedImageClick();


            }
        });
        rootView.findViewById(R.id.upload_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(getPickImageChooserIntent(), 200);
            }
        });

        rootView.findViewById(R.id.rotate_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropImageView.rotateImage(90);
            }
        });
    }

    private void onSaveCroppedImageClick() {
        Bitmap cropped = mCropImageView.getCroppedImage(500, 500);
        if (cropped != null) {
            mCropImageView.setImageBitmap(cropped);
        }
        if (mCropImageView.getCroppedOvalImage() != null) {
            BusProvider.getBus().post(mCropImageView.getCroppedOvalImage());
            Helper.getSnackbar(getActivity(), getView(), "Saving profile image").show();
        }
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ChatKeyboardUtils.hideKeyboard(getView());
//                getActivity().getSupportFragmentManager().beginTransaction().remove(CropImageFragment.this).commit();
                getActivity().onBackPressed();

            }
        }, 700);
    }


    /**
     * Crop the image and set it back to the cropping view.
     */
//    public void onSaveCroppedImageClick() {
//        Bitmap cropped = mCropImageView.getCroppedImage(500, 500);
//        Application.getClient().setNewProfileImage(mCropImageView.getCroppedOvalImage());
//        if (cropped != null) {
//            mCropImageView.setImageBitmap(cropped);
//        }
//
//        Intent data = new Intent();
//        data.putExtra(StaticParams.KEY_PROFILE_IMAGE, mCropImageView.getCroppedOvalImage());
//        setResult(RESULT_OK, data);
//        this.finish();
//    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);
            mCropImageView.setImageUri(imageUri);
            imageview.setVisibility(View.GONE);
            mCropImageView.setVisibility(View.VISIBLE);
        }
    }


    /**
     * Create a chooser intent to select the source to get image from.<br/>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the intent chooser.
     */
    public Intent getPickImageChooserIntent() {


        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();


        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();


        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }


        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }


        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);


        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");


        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));


        return chooserIntent;
    }


    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }


    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}