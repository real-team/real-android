package com.joinreal.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.StaticParams;
import com.joinreal.events.StartActivityEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.Agent;
import com.joinreal.utils.ChatImagesUtils;
import com.joinreal.utils.ChatKeyboardUtils;
import com.joinreal.views.ExpandableLayout;
import com.joinreal.views.StartDatePicker;
import com.squareup.otto.Subscribe;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by brittbarak on 7/27/15.
 */
public class AgentProfileEditFragment extends AgentProfileFragment implements ChatImagesUtils.ISelectImageListener, StartDatePicker.ISetDateListener {
    public static final String TAG = "AgentProfileEditFragment";
    private static final int STATES_ARRAY_ID = R.array.states_array;
    private Agent originalAgent;
    private boolean isValid;
    private TextView uploadImageOverlay;
    private Bitmap originalImage;
    private EditText city;
    private EditText zip;
    private Spinner statesSpinner;
    private UpdatedAgentDetailsEvent updatedAgentEvent = new UpdatedAgentDetailsEvent();
    private List<String> statesList;

    public static AgentProfileEditFragment getNewInstance() {
        return new AgentProfileEditFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        originalAgent = Application.getClient().getUser().getAgent();
        agent = new Agent(originalAgent);
        rootView = inflater.inflate(R.layout.fragment_edit_agent_profile, container, false);
        statesList = Arrays.asList(getActivity().getResources().getStringArray(STATES_ARRAY_ID));
        uploadImageOverlay = (TextView) rootView.findViewById(R.id.upload_photo);
        uploadImageOverlay.setBackground(Helper.getTintedDrawable(R.drawable.circle, getResources().getColor(R.color.gray_light_shadow)));
        city = (EditText) rootView.findViewById(R.id.city);
        zip = (EditText) rootView.findViewById(R.id.zip);
        statesSpinner = (Spinner) rootView.findViewById(R.id.state_spinner);

        setContentView();

        return rootView;
    }

    private void setStatesSpinner() {
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                STATES_ARRAY_ID, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statesSpinner.setPromptId(R.string.state_prompt);
        statesSpinner.setAdapter(spinnerAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        agentActionsListener.lockDrawer("My Profile");
    }

    @Override
    protected void setTopActionButton(Menu menu) {
        TextView tv = MainActivityAbstract.getActionButtonTextView("SAVE", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClick();
            }
        });
        menu.add(0, 0, 1, "SAVE").setActionView(tv).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    }

    @Override
    protected void setContentView() {
        super.setContentView();

        rootView.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClick();
            }
        });

        rootView.findViewById(R.id.img_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ChatImagesUtils.selectImage(AgentProfileEditFragment.this);
//                startActivityForResult(new Intent(getActivity(), CropImageActivity.class), StaticParams.REQUEST_PICK_IMAGE);
                navigationListener.openCropImageFragment();

            }
        });

    }

    @Override
    protected void setWorkWithCheckBoxes() {
        super.setWorkWithCheckBoxes();
        buyersCheckbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                agent.setIsWorkWithBuyers(!agent.isWorkWithBuyers());
                setCheckbox(buyersCheckbox, agent.isWorkWithBuyers());

            }
        });

        sellersCheckbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                agent.setIsWorkWithSellers(!agent.isWorkWithSellers());
                setCheckbox(sellersCheckbox, agent.isWorkWithSellers());
            }
        });

        rentersCheckbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                agent.setIsWorkWithRenters(!agent.isWorkWithRenters());
                setCheckbox(rentersCheckbox, agent.isWorkWithRenters());

            }
        });
    }

    @Override
    protected void setSocialMediaTextView(TextView textView, String url) {
        if (url != null) {
            textView.setText(url);
        }
    }

    @Override
    protected void hideSocialExpandableIfNeeded() {

    }

    @Override
    protected void setAddressTextView() {
        if(agent.getAddress1() != null) {
            address.setText(agent.getAddress1());
        }
        if(agent.getCity() != null) {
            city.setText(agent.getCity());
        }
        if ((agent.getZip() != null) && (agent.getZip().length() > 0)) {
            zip.setText(agent.getZip());
        }

        setStatesSpinner();
        if(agent.getState() != null) {
            int index = statesList.indexOf(agent.getState());
            statesSpinner.setSelection(index);
        } else {
            statesSpinner.setPrompt(statesSpinner.getPrompt());
        }
    }

    @Override
    protected void setBirthdayTextView() {
        if (agent.getBirthdayToDisplay() != null) {
            birthday.setText(agent.getBirthdayToDisplay());
        }
        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartDatePicker dialogFragment = new StartDatePicker();
                dialogFragment.setListener(AgentProfileEditFragment.this);

                //TODO: if there's birthday, set birthady
                Calendar c = Calendar.getInstance();
                dialogFragment.setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dialogFragment.show(getChildFragmentManager(), "start_date_picker");
            }
        });
    }

//    int startYear = c.get(Calendar.YEAR);
//    int startMonth = c.get(Calendar.MONTH);
//    int startDay = c.get(Calendar.DAY_OF_MONTH);

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public void onStartActivityForResult(StartActivityEvent event) {
        startActivityForResult(event.getIntent(), event.getRequestCode());

    }


    private void onSaveButtonClick() {
        saveAgentState();
        validateAllFields();
        if (isValid) {
            Application.getClient().setUserAgent(agent);
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        Application.getClient().updateAgentDetails(agent);
                    } catch (NetworkException e) {
                        Helper.handleApiException(e, NetworkCodes.CANT_UPDATE_AGENT_DETAILS);
                    }
                }
            };
            new Thread(runnable).start();
            BusProvider.getBus().post(updatedAgentEvent);

            Helper.getSnackbar(getActivity(), getView(), "Saving profile").show();
            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ChatKeyboardUtils.hideKeyboard(getView());
//                    getActivity().getSupportFragmentManager().beginTransaction().remove(AgentProfileEditFragment.this).commit();
                    getActivity().onBackPressed();
                }
            }, 500);
        }
    }

    private void validateAllFields() {
        isValid = false;
        boolean isValidField;

        isValidField = (agent.getProfileImage() != null) || (agent.getNewProfileImage() != null);
        validateField(isValidField, "Please add a profile image", R.color.white, uploadImageOverlay);
        if (! isValidField) {
            return;
        }

        isValidField = isMatchingPatterns(Patterns.PHONE, agent.getPhone());
        validateField(isValidField, "Please enter a valid phone number", R.color.dark_blue, phone);
        if (! isValidField) {
            return;
        }

        isValidField = isMatchingPatterns(Patterns.EMAIL_ADDRESS, agent.getEmail());
        validateField(isValidField, "Please enter a valid email", R.color.dark_blue, email);
        if (! isValidField) {
            return;
        }


        isValidField = (agent.getAddress1() != null && agent.getAddress1().length() >= 3);
        validateField(isValidField, "Please enter a valid address", R.color.dark_blue, address);
        if (! isValidField) {
            return;
        }


        isValidField = (agent.getCity() != null && agent.getCity().length() >= 2);
        validateField(isValidField, "Please enter a valid city", R.color.dark_blue, city);
        if (! isValidField) {
            return;
        }

        ExpandableLayout expandableRealtorInfo = ((ExpandableLayout)rootView.findViewById(R.id.realtor_information_expandable));
        isValidField = ((agent.isWorkWithBuyers()) || (agent.isWorkWithSellers()) || (agent.isWorkWithRenters()));
        validateField(isValidField, "Please select at least one customer type", R.color.dark_blue, buyersCheckbox, sellersCheckbox, rentersCheckbox);
        if (! isValidField) {
            if (! expandableRealtorInfo.isOpened()) {
                expandableRealtorInfo.show();
            }
            return;
        }


    isValidField = isZipodesValid();
        validateField(isValidField, "Please enter ZIP codes separated by a comma", R.color.dark_blue, zipCodes);
        if (! isValidField) {
            if (! expandableRealtorInfo.isOpened()) {
                expandableRealtorInfo.show();
            }
            return;
        }

        isValid = true;
    }


    private void validateField(boolean isValidField, String errorMessage, int validColorId, TextView... views) {
        int colorId = isValidField ? validColorId : R.color.red;
        if (! isValidField) {
            Helper.getSnackbar(getActivity(), getView(), errorMessage).show();
        }
        for (TextView v : views) {
            v.setTextColor(getActivity().getResources().getColor(colorId));

            if (v instanceof EditText) {
                v.setHintTextColor(getActivity().getResources().getColor(colorId));
            }
        }
    }

    private boolean isZipodesValid() {
        Pattern pattern = Pattern.compile("\\d{5}");
        if (agent.getZipcodes() != null) {
            String[] codes = agent.getZipcodes().split(",");
            for (String c : codes) {
                if (! pattern.matcher(c.trim()).matches()) {
                    return false;
                }
            }
        }
        return true;

    }

    private boolean isMatchingPatterns(Pattern pattern, String field) {
        return (field != null) && (field.length() > 0) && (pattern.matcher(field).matches());
    }

    private void saveAgentState() {
        agent.setTitle(agentTitle.getText().toString());
        agent.setAbout(about.getText().toString());
        agent.setPhone(phone.getText().toString());
        agent.setEmail(email.getText().toString());
        agent.setAddress1(address.getText().toString());
        agent.setCity(city.getText().toString());
        agent.setZip(zip.getText().toString());
        agent.setState(statesList.get(statesSpinner.getSelectedItemPosition()));
        agent.setLicenseStates(licenseStates.getText().toString());
        agent.setZipcodes(zipCodes.getText().toString());
        agent.setLanguages(languages.getText().toString());
        agent.setWebsiteUrl(website.getText().toString());
        agent.setLinkedinProfileUrl(linkedin.getText().toString());
        agent.setFacebookProfileUrl(facebook.getText().toString());
        agent.setTwitterProfileUrl(twitter.getText().toString());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK) {
//            String selectedImagePath = "";
//            final int imageDimen = (int) Application.getAppContext().getResources().getDimension(R.dimen.profile_image_size);
//            final int imageSize = (int) Helper.pxlsToDp(getActivity(), imageDimen);
//            BitmapFactory.Options options = new BitmapFactory.Options();
//
//
//            if (requestCode == StaticParams.REQUEST_CAMERA) {
//                originalImage = (Bitmap) data.getExtras().get("data");
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                originalImage.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
//                File destination = new File(Environment.getExternalStorageDirectory(),
//                        System.currentTimeMillis() + ".jpg");
//                FileOutputStream fo;
//                try {
//                    destination.createNewFile();
//                    fo = new FileOutputStream(destination);
//                    fo.write(bytes.toByteArray());
//                    fo.close();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
////                agent.setNewProfileImage(ChatImagesUtils.getCircleBitmap(thumbnail, imageSize));
//                selectedImagePath = destination.getAbsolutePath();
//            } else if (requestCode == StaticParams.REQUEST_SELECT_FILE) {
//
//                Uri selectedImageUri = data.getData();
//                String[] projection = {MediaStore.MediaColumns.DATA};
//                Cursor cursor = getActivity().getContentResolver().query(selectedImageUri, projection, null, null, null);
//                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//                cursor.moveToFirst();
//                selectedImagePath = cursor.getString(column_index);
//
//            }
//            options.inJustDecodeBounds = true;
//            originalImage = BitmapFactory.decodeFile(selectedImagePath, options);
//            int scale = 1;
//            while (options.outWidth / scale / 2 >= imageSize
//                    && options.outHeight / scale / 2 >= imageSize) {
//                scale *= 2;
//            }
//            options.inSampleSize = scale;
//            options.inJustDecodeBounds = false;
//            Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);
//            agent.setNewProfileImage(ChatImagesUtils.getCircleBitmap(bm, imageSize));
//            thumb.setImageBitmap(agent.getNewProfileImage());
//            updatedAgentEvent.setDidUpdateImage(true);
//        }

        super.onActivityResult(requestCode, resultCode, data);
        if ((resultCode == Activity.RESULT_OK) &&(requestCode == StaticParams.REQUEST_PICK_IMAGE)) {
            agent.setNewProfileImage((Bitmap) data.getParcelableExtra(StaticParams.KEY_PROFILE_IMAGE));
            thumb.setImageBitmap(agent.getNewProfileImage());
        }
    }

    @Override
    public void onPause() {
        saveAgentState();
        super.onPause();
    }

    @Override
    public void onDateSet(int year, int monthOfYear, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);
        agent.setBirthDate(c);
        birthday.setText(agent.getBirthdayToDisplay());

    }

    public class UpdatedAgentDetailsEvent {
        boolean didUpdateImage;

        public boolean didUpdateImage() {
            return didUpdateImage;
        }

        public void setDidUpdateImage(boolean didUpdateImage) {
            this.didUpdateImage = didUpdateImage;
        }
    }

    @Subscribe
    public void onPickedProfileImage(Bitmap bitmap) {
        agent.setNewProfileImage(bitmap);
        thumb.setImageBitmap(agent.getNewProfileImage());
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getBus().register(this);
    }

    @Override
    public void onStop() {
        BusProvider.getBus().unregister(this);
        super.onStop();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
