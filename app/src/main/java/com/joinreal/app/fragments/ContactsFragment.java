package com.joinreal.app.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.joinreal.R;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.app.interfaces.ISearchToolbarListener;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.events.ContactsUpdatedEvent;
import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.ContactsSubtitleVM;
import com.joinreal.utils.ContactManager;
import com.joinreal.app.presenters.ContactsLayoutCreator;
import com.joinreal.model.view.ContactsActionVM;
import com.joinreal.model.view.ViewModelItem;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;


/**
 * Created by brittbarak on 7/6/15.
 */
public class ContactsFragment extends RealBaseFragment implements ContactsLayoutCreator.ContactsLayoutCreatorListener {
    public static final String TAG = "ContactsFragment";
    private ContactsLayoutCreator contactsLayoutCreator;
    AgentActionsListener agentActionsListener;
    ISearchToolbarListener searchToolbarListener;
    OpenChatListener openChatListener;
    private Menu menu;

    public static ContactsFragment getNewInstance() {
        return new ContactsFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        agentActionsListener = (AgentActionsListener) activity;
        openChatListener = (OpenChatListener) activity;
        searchToolbarListener = (ISearchToolbarListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.contacts_and_chats_layout, container, false);
        rootView.findViewById(R.id.included_toolbar_search).setVisibility(View.GONE);
        contactsLayoutCreator = new ContactsLayoutCreator(this, searchToolbarListener);
        ArrayList<ViewModelItem> viewModelItems = getViewModelItems();
        contactsLayoutCreator.createLayout(rootView, R.drawable.hamburger, viewModelItems);
//TODO: temp
        rootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
        rootView.findViewById(R.id.invite_contacts_bottom_layout).setVisibility(View.GONE);

        return rootView;
    }

    private ArrayList<ViewModelItem> getViewModelItems() {
        ArrayList<ViewModelItem> viewModelItems = new ArrayList<>();
        viewModelItems.add(new ContactsActionVM("Invite to download my app", R.drawable.contact_invite, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agentActionsListener.openInviteContactsFragment();
            }
        }));
        viewModelItems.add(new ContactsActionVM("Start Chat", R.drawable.start_chat, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                agentActionsListener.openChatDialogsFragment();
            }
        }));

        ArrayList<ViewModelItem> contactsWithApp = ContactManager.getContactsWithAppViewModelsList();
        if (!contactsWithApp.isEmpty()) {
            viewModelItems.add(new ContactsSubtitleVM("Contacts with my app"));
            viewModelItems.addAll(contactsWithApp);
        }

        ArrayList<ViewModelItem> contacts = ContactManager.getViewModelsList();
        if (! contacts.isEmpty()) {
            viewModelItems.add(new ContactsSubtitleVM("All Contacts"));
            viewModelItems.addAll(ContactManager.getViewModelsList());
        }
        //TODO: add "no contacts to display" line
        return viewModelItems;
    }

    @Subscribe
    public void onContactsUpdated(ContactsUpdatedEvent e) {
        contactsLayoutCreator.updateDataList(getViewModelItems());
    }

    @Override
    public void onResume() {
        super.onResume();
        contactsLayoutCreator.onResume();
        ((MainActivityAbstract) getActivity()).setToolbarTitle("Contacts");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburger);
        if (ContactManager.didMergeContactsOnce()) {
            contactsLayoutCreator.stopRefresh();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        System.out.println("contact onCreateOptionsMenu");
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_contacts, menu);
        this.menu = menu;
        menu.removeItem(R.id.action_compose);
        MainActivityAbstract.addCancelActionButton(menu, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelClick();
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                searchToolbarListener.onSearchActionClick();
                menu.findItem(R.id.action_add).setVisible(false);
                menu.findItem(MainActivityAbstract.ACTION_CANCEL_ID).setVisible(true);
                item.setVisible(false);
                Analytics.getInstance().trackToolbarNavigtionEvent(getScreenName(), "Search");
                return true;
            case R.id.action_add:
                Intent intent= new Intent(Intent.ACTION_PICK,  ContactsContract.Contacts.CONTENT_URI);
                startActivity(intent);
                Analytics.getInstance().trackToolbarNavigtionEvent(getScreenName(), "Add");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onCancelClick() {
        menu.findItem(MainActivityAbstract.ACTION_CANCEL_ID).setVisible(true);
        menu.findItem(R.id.action_search).setVisible(true);
        menu.findItem(R.id.action_add).setVisible(true);
        searchToolbarListener.onCancelSearchClick();
        contactsLayoutCreator.onCancelClick();
    }

    @Override
    public ActionBar getActivityActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();

    }

    @Override
    public Menu getActionMenu() {
        return menu;
    }

    @Override
    public void onContactRowClick(ContactVM contactVM) {
        onCancelClick();
        agentActionsListener.openContactInfoFragment(contactVM);
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getRestBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        contactsLayoutCreator.onPause();
        onCancelClick();
    }

    @Override
    public void onStop() {
        BusProvider.getRestBus().unregister(this);
        super.onStop();

    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
