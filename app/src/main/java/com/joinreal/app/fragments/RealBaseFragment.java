package com.joinreal.app.fragments;

import android.support.v4.app.Fragment;

import com.joinreal.app.common.Analytics;

/**
 * Created by brittbarak on 8/26/15.
 */
public abstract class RealBaseFragment extends Fragment{
    @Override
    public void onResume() {
        super.onResume();
        Analytics.getInstance().trackScreen(getScreenName());
    }

    protected abstract String getScreenName();
}
