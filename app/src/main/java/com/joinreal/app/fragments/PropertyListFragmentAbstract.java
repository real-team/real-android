package com.joinreal.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.joinreal.R;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.interfaces.OpenSearchFiltersListener;
import com.joinreal.app.interfaces.UpdateFavoritesHelper;
import com.joinreal.events.MorePropertiessAddedEvent;
import com.joinreal.events.ShouldOpenFavoritesEvent;
import com.joinreal.model.data.PropertyThin;
import com.squareup.otto.Subscribe;

import java.util.List;

/**
 * Created by britt on 6/14/15.
 */
public abstract class PropertyListFragmentAbstract extends RealBaseFragment{

    protected List<PropertyThin> properties;
    protected OpenSearchFiltersListener listener;
    protected UpdateFavoritesHelper updateFavoritesHelper;
    protected Object busSubscribee;
    protected String screenName = null;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OpenSearchFiltersListener) activity;
        updateFavoritesHelper = UpdateFavoritesHelper.getInstance();
    }


    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getBus().register(busSubscribee);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            this.properties = (List<PropertyThin>) getArguments().getSerializable(StaticParams.KEY_PROPERTY_LIST);
            this.busSubscribee = new Object() {

                @Subscribe
                public void onMorePropertiesAdded(MorePropertiessAddedEvent event) {
                    onItemsAddedToList(event.getItemsCount());
                }

                @Subscribe
                public void onShouldOpenFavoritesEvent(ShouldOpenFavoritesEvent e) {
                    listener.openFavoritesFragment(new Bundle());
                }
            };
        if (getArguments().getBoolean(StaticParams.KEY_IS_FAVORITES_LIST)) {
            setHasOptionsMenu(false);
        } else {
            setHasOptionsMenu(true);
        }
    }

    public void createView(){
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getArguments().getBoolean(StaticParams.KEY_SHOULD_LOCK_DRAWER)){
            ((PropertyListContainerFragment)getParentFragment()).setToolbar();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search_results, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.action_search:
                listener.openFiltersActivity(false);
                Analytics.getInstance().trackToolbarNavigtionEvent(getScreenName(), "Search");
                return true;
            case R.id.action_map_view:
                Analytics.getInstance().trackOverFlowNavigtionEvent(getScreenName(), "Map View");
                return true;
            case R.id.action_list_view:
                listener.openPropertyListViewFragment();
                Analytics.getInstance().trackOverFlowNavigtionEvent(getScreenName(), "List View");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        updateFavoritesHelper.updateChanges();
        Analytics.getInstance().trackEvent(getScreenName(), "properties viewed : " + getPropetiesViewedCount() + " , # total properties on list : " + properties.size());
        super.onPause();
    }



    @Override
    public void onStop() {
        BusProvider.getBus().unregister(busSubscribee);
        super.onStop();
    }

    @Override
    protected String getScreenName() {
        if (screenName != null) {
            setScreenName();
        }
        return screenName;
    }

    public void setScreenName() {
        screenName = "";
        if (getArguments().getString(StaticParams.KEY_CONTACT_ID) != null) {
            screenName += "Contacts ";
        }

        if (getArguments().getBoolean(StaticParams.KEY_IS_FAVORITES_LIST)) {
            screenName += "Favorites";
        } else {
            screenName += "Search Results ";
        }
    }

    protected abstract void onItemsAddedToList(int itemsCount);

    protected abstract int getPropetiesViewedCount();
}
