package com.joinreal.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.joinreal.R;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.tasks.SendMessageTask;
import com.joinreal.views.ExpandableFilterLayout;

import java.util.ArrayList;


/**
 * Created by brittbarak on 7/6/15.
 */
public class SendInvitesFragment extends RealBaseFragment {
    public static final String TAG = "SendInvitesFragment";
    private static final String KEY_CONTACT_PHONES = "KEY_CONTACT_PHONES";
    private static final String KEY_INVITE_VIA_PHONE_ONLY = "KEY_INVITE_VIA_PHONE_ONLY";

    private ArrayList<String> contactPhonesToInvite;
    private EditText msgText;
    private AgentActionsListener agentActionsListener;

    public static SendInvitesFragment getNewInstance(ArrayList<String> contactPhonesToInvite, boolean shouldInviteByPhoneOnly, String entryTagToReturnTo) {
        SendInvitesFragment fragment = new SendInvitesFragment();
        Bundle bd = new Bundle(2);
        bd.putStringArrayList(KEY_CONTACT_PHONES, contactPhonesToInvite);
        bd.putBoolean(KEY_INVITE_VIA_PHONE_ONLY, shouldInviteByPhoneOnly);
        bd.putString(StaticParams.ENTRY_TAG_TO_RETURN_TO, entryTagToReturnTo);
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        agentActionsListener = (AgentActionsListener)getActivity();
        setHasOptionsMenu(true);
        this.contactPhonesToInvite = getArguments().getStringArrayList(KEY_CONTACT_PHONES);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_send_invites, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        ExpandableFilterLayout emailExpandable = (ExpandableFilterLayout) rootView.findViewById(R.id.email_msg_expandable);
        msgText = (EditText) emailExpandable.getContentLayout().findViewById(R.id.edittext);
        msgText.setText(StaticParams.DEFAULT_EMAIL);
        msgText.requestFocus();
        emailExpandable.show();

        rootView.findViewById(R.id.send_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendMessageTask(contactPhonesToInvite.toArray(new String[contactPhonesToInvite.size()]), msgText.getText().toString()).execute();
                agentActionsListener.openInvitationSentFragment(contactPhonesToInvite.size(), getArguments().getString(StaticParams.ENTRY_TAG_TO_RETURN_TO));
            }
        });


        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        agentActionsListener.lockDrawer("Send Invites (" + contactPhonesToInvite.size() + ")");
    }


    @Override
    public void onPause() {
        super.onPause();
        agentActionsListener.unlockDrawer();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
