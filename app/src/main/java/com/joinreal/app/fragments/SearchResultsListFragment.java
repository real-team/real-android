package com.joinreal.app.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.adapters.SearchResultsAdapter;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.events.SortPropertiesEvent;
import com.joinreal.model.data.PropertyThin;
import com.joinreal.app.common.BusProvider;
import com.joinreal.events.LoadMorePropertiesEvent;

import java.util.ArrayList;

/**
 * Created by britt on 6/14/15.
 */
public class SearchResultsListFragment extends PropertyListFragmentAbstract {

    public static final String TAG = "SearchResultsFragment";
    private RecyclerView resultsRecyclerView;
    LinearLayoutManager linearLayoutManager;

    public static final SearchResultsListFragment getNewInstance(Bundle bd, ArrayList<PropertyThin> properties) {
        SearchResultsListFragment fragment = new SearchResultsListFragment();
        bd.putSerializable(StaticParams.KEY_PROPERTY_LIST, properties);
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search_results, container, false);
        setResultsList(rootView);
        super.createView();
        return rootView;
    }

    private boolean loading = true;
    int visibleItemCount, totalItemCount, firstVisibleItem, previousTotal;
    private int visibleThreshold = 3;

    private void setResultsList(View rootView) {
        resultsRecyclerView = (RecyclerView) rootView.findViewById(R.id.results_recyclerview);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        resultsRecyclerView.setLayoutManager(linearLayoutManager);
        resultsRecyclerView.setAdapter(new SearchResultsAdapter(getActivity(), updateFavoritesHelper, properties, (OpenChatListener) getActivity(), getScreenName()));
        resultsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    BusProvider.getBus().post(new LoadMorePropertiesEvent());
                    loading = true;
                }

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_list_view);
        if (!Application.getClient().isAgent()) {
            menu.removeItem(R.id.action_search);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String sortByValue = null;
        int id = item.getItemId();
        switch (id) {
            case R.id.action_recentyl_posted:
                sortByValue = SortPropertiesEvent.SORT_BY_NEWEST;
                Analytics.getInstance().trackOverFlowNavigtionEvent(getScreenName(), "Sort By Newest");
                break;
            case R.id.action_price_lth:
                sortByValue = SortPropertiesEvent.SORT_BY_CHEAPEST;
                Analytics.getInstance().trackOverFlowNavigtionEvent(getScreenName(), "Sort By Cheapest");
                break;
            case R.id.action_price_htl:
                sortByValue = SortPropertiesEvent.SORT_BY_MOST_EXPENSIVE;
                Analytics.getInstance().trackOverFlowNavigtionEvent(getScreenName(), "Sort By Most Expensive");
                break;
        }
        if (sortByValue != null) {
            BusProvider.getBus().post(new SortPropertiesEvent(sortByValue));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onItemsAddedToList(int itemsCount) {
        resultsRecyclerView.getAdapter().notifyItemRangeChanged(properties.size() - itemsCount, itemsCount);
    }


    public void setScreenName() {
        super.setScreenName();
        screenName += "List";
    }

    @Override
    protected int getPropetiesViewedCount() {
        return linearLayoutManager.findLastVisibleItemPosition();
    }
}
