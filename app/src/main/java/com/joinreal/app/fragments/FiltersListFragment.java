package com.joinreal.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.FiltersActivity;
import com.joinreal.app.adapters.FiltersAdapter;
import com.joinreal.app.api.RealClient;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.SendFiltersRequest;
import com.joinreal.app.presenters.FiltersButtonsPresenter;
import com.joinreal.events.ToggleBuyRentEvent;
import com.joinreal.model.RangeStep;
import com.joinreal.model.RangeStepsListGenerator;
import com.joinreal.model.data.SearchFilter;
import com.joinreal.model.view.ButtonsFiltersVM;
import com.joinreal.model.view.CheckListRowFilterItemVM;
import com.joinreal.model.view.FactoredRangeSeekBarFiltersVM;
import com.joinreal.model.view.SpinnerFilterItemVM;
import com.joinreal.model.view.EvenStepsRangeBarFiltersVM;
import com.joinreal.model.view.ExpandableItemFiltersVM;
import com.joinreal.model.view.FiltersResetVM;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.GridFiltersVM;
import com.joinreal.model.view.PlusMinusFiltersVM;
import com.joinreal.model.view.RecyclerFilterItemVM;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/2/15.
 */
public class FiltersListFragment extends RealBaseFragment implements FiltersButtonsPresenter.BuyRentToggleListener, FiltersActivity.BuildJsonListener {

    private static final int PRICE_BUY_FACTOR = 100;
    public static final String TAG = "FiltersFragment";
    private RealClient client;
    private ArrayList<FiltersViewModelItem> filtersViewModelList;
    private RecyclerView filtersRecyclerView;
    private ButtonsFiltersVM buyRentFilters;
    private boolean isBuyChecked;
    private SendFiltersRequest listener;
    private SearchFilter filter;

    public static FiltersListFragment getNewInstance(SearchFilter filter) {
        FiltersListFragment fragment = new FiltersListFragment();
        if (filter != null) {
            Bundle bd = new Bundle(1);
            bd.putSerializable(StaticParams.KEY_SEARCH_FILTER, filter);
            fragment.setArguments(bd);
        }
        return fragment;
    }

    @Subscribe
    public void onBuyRentToggle(ToggleBuyRentEvent event) {
        isBuyChecked = event.isBuySelected();
        buyRentFilters.setIsBuyChecked(event.isBuySelected());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (SendFiltersRequest)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = Application.getClient();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filters_list, container, false);
        if (getArguments() != null) {
            filter = (SearchFilter) getArguments().getSerializable(StaticParams.KEY_SEARCH_FILTER);
        }
        if ((filter != null) && (filter.isRental())) {
            isBuyChecked = false;
        } else {
            isBuyChecked = true;
        }

        createViewModelsList();
        setFiltersRecyclerView(rootView);
        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);

        return rootView;
    }

    private void setFiltersRecyclerView(View rootView) {
        filtersRecyclerView = (RecyclerView) rootView.findViewById(R.id.filters_recyclerview);
        filtersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        filtersRecyclerView.setAdapter(new FiltersAdapter(this, filtersViewModelList, true));
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getBus().register(this);
    }

    private void createViewModelsList() {
        Resources res = getActivity().getResources();
        filtersViewModelList = new ArrayList<>();

        buyRentFilters = new ButtonsFiltersVM(isBuyChecked);
        filtersViewModelList.add(buyRentFilters);


        addPropertyTypeVMItems();

        addPriceRangeVMItem();

        addBedsVMItem();

        addBathsVMItem();

        addMoreBasicInfoVMItem();

        addAmenitiesVMItem(res);

        addListingStatusVMItem();

        addListingTypesVMItem(res);


        filtersViewModelList.add(new FiltersResetVM());
    }

    private void addListingStatusVMItem() {
        Resources res = getActivity().getResources();
        if(client.isAgent()) {
            List<FiltersViewModelItem> listingStatusItems = new ArrayList<>();
            listingStatusItems.add(new CheckListRowFilterItemVM(res.getString(R.string.active), true, "is_active"));
            //TODO: should update json keys for pending, after yuval's decision
            listingStatusItems.add(new CheckListRowFilterItemVM(res.getString(R.string.pending), "is_pending"));
            listingStatusItems.add(new CheckListRowFilterItemVM(res.getString(R.string.sold), "is_sold"));
            filtersViewModelList.add(new ExpandableItemFiltersVM(res.getString(R.string.listing_status), listingStatusItems,
                    "Please select at least one listing status"));
        }
    }

    private void addListingTypesVMItem(Resources res) {
        List<FiltersViewModelItem> listingTypeItems = new ArrayList<>();
        addDaysOnMarketVMItem(listingTypeItems);

        listingTypeItems.add(new CheckListRowFilterItemVM(res.getString(R.string.price_reduction), "is_price_reduction").setSelected((filter != null) ? filter.isPriceReduction() : false));
        listingTypeItems.add(new CheckListRowFilterItemVM(res.getString(R.string.exclude_short_sales), "exclude_short_sales").setSelected((filter != null) ? filter.isExcludeShortSales() : false));
        listingTypeItems.add(new CheckListRowFilterItemVM(res.getString(R.string.foreclosure), "is_foreclosure").setSelected((filter != null) ? filter.isForeclosure() : false));
        listingTypeItems.add(new CheckListRowFilterItemVM(res.getString(R.string.new_construction), "is_new_construction").setSelected((filter != null) ? filter.isNewConstruction() : false));
        listingTypeItems.add(new CheckListRowFilterItemVM(res.getString(R.string.has_photos), "has_photos").setSelected((filter != null) ? filter.isHasPhotos() : false));
        filtersViewModelList.add(new ExpandableItemFiltersVM(res.getString(R.string.listing_type), listingTypeItems));
    }

    private void addDaysOnMarketVMItem(List<FiltersViewModelItem> listingTypeItems) {
        List<SpinnerFilterItemVM.SpinnerOption> daysOnMarketOptions = new ArrayList<>();
        daysOnMarketOptions.add(new SpinnerFilterItemVM.SpinnerOption("ANY DAYS ON MARKET", 0));
        daysOnMarketOptions.add(new SpinnerFilterItemVM.SpinnerOption("1 Day", 1));
        daysOnMarketOptions.add(new SpinnerFilterItemVM.SpinnerOption("1 Week", 7));
        daysOnMarketOptions.add(new SpinnerFilterItemVM.SpinnerOption("2 Weeks", 14));
        daysOnMarketOptions.add(new SpinnerFilterItemVM.SpinnerOption("1 Month", 30));
        daysOnMarketOptions.add(new SpinnerFilterItemVM.SpinnerOption("3 Months", 90));
        daysOnMarketOptions.add(new SpinnerFilterItemVM.SpinnerOption("1 Year", 365));
        listingTypeItems.add(new SpinnerFilterItemVM(daysOnMarketOptions, "dom")
                .setSelectedIndexByValue((filter != null) ? filter.getDaysOnMarket() : 0));
    }

    private void addAmenitiesVMItem(Resources res) {
        List<FiltersViewModelItem> amenitiesExpandableItems = new ArrayList<>();
        List<RecyclerFilterItemVM> amenities1 = new ArrayList<>();
        amenities1.add((new RecyclerFilterItemVM(res.getString(R.string.waterfront), R.drawable.waterfront_fill, R.drawable.waterfront, "has_waterfront")).setSelected((filter != null) ? filter.isHasWaterfront() : false));
        amenities1.add(new RecyclerFilterItemVM(res.getString(R.string.private_pool), R.drawable.private_pool_fill, R.drawable.private_pool, "has_pool_private").setSelected((filter != null) ? filter.isHasPoolPrivate() : false));
        amenities1.add(new RecyclerFilterItemVM(res.getString(R.string.public_pool), R.drawable.public_pool_fill, R.drawable.public_pool, "has_pool_area").setSelected((filter != null) ? filter.isHasPoolArea() : false));
        amenities1.add(new RecyclerFilterItemVM(res.getString(R.string.hot_tub), R.drawable.hot_tub_fill, R.drawable.hot_tub, "has_spa_hot_tub").setSelected((filter != null) ? filter.isHasSpaHotTub() : false));
        amenities1.add(new RecyclerFilterItemVM(res.getString(R.string.fireplace), R.drawable.fireplace_fill, R.drawable.fireplace, "has_fireplace").setSelected((filter != null) ? filter.isHasFireplace() : false));
        amenities1.add(new RecyclerFilterItemVM(res.getString(R.string.porch), R.drawable.porch_fill, R.drawable.porch, "has_porch").setSelected((filter != null) ? filter.isHasPorch() : false));
        amenitiesExpandableItems.add(new GridFiltersVM(amenities1));

        List<RecyclerFilterItemVM> amenities2 = new ArrayList<>();
        amenities2.add(new RecyclerFilterItemVM(res.getString(R.string.patio_deck), R.drawable.patio_fill, R.drawable.patio, "has_patio_deck").setSelected((filter != null) ? filter.isHasPatioDeck() : false));
        amenities2.add(new RecyclerFilterItemVM(res.getString(R.string.backyard), R.drawable.backyard_fill, R.drawable.backyard, "has_back_yard").setSelected((filter != null) ? filter.isHasBackYard() : false));
        amenities2.add(new RecyclerFilterItemVM(res.getString(R.string.tennis), R.drawable.tennis_fill, R.drawable.tennis, "has_tennis").setSelected((filter != null) ? filter.isHasTennis() : false));
        amenities2.add(new RecyclerFilterItemVM(res.getString(R.string.golf), R.drawable.golf_fill, R.drawable.golf, "has_golf").setSelected((filter != null) ? filter.isHasGolf() : false));
        amenities2.add(new RecyclerFilterItemVM(res.getString(R.string.controlled_access), R.drawable.controlled_access_fill, R.drawable.controlled_access, "has_controlled_access").setSelected((filter != null) ? filter.isHasControlledAccess() : false));
        amenities2.add(new RecyclerFilterItemVM(res.getString(R.string.wheelchair), R.drawable.wheelchair_fill, R.drawable.wheelchair, "is_wheelchair_handicap_access").setSelected((filter != null) ? filter.isWheelchairHandicapAccess() : false));
        amenitiesExpandableItems.add(new GridFiltersVM(amenities2));

        filtersViewModelList.add(new ExpandableItemFiltersVM(res.getString(R.string.amenities), amenitiesExpandableItems));
    }

    private void addMoreBasicInfoVMItem() {
        List<RangeStep> steps;
        double[] options;

        List<FiltersViewModelItem> items = new ArrayList<>();
        items.add(getSqFtRangeBar());

        final int ACRE_IN_SQFT = 43560;
        steps = new ArrayList<>();
        steps.add(RangeStepsListGenerator.generateMiddleStep(0, 0, 2000, "0"));
        steps.addAll(RangeStepsListGenerator.generateList(2000, 5000, 1000, "SQFT", 0));
        steps.add(RangeStepsListGenerator.generateMiddleStep(7500, 5000, (int) (0.25 * ACRE_IN_SQFT), " 7500 SQFT"));
        int[] values = {(int) (0.25*ACRE_IN_SQFT), (int) (0.5*ACRE_IN_SQFT), 1*ACRE_IN_SQFT, 2*ACRE_IN_SQFT, 5*ACRE_IN_SQFT, 10*ACRE_IN_SQFT};
        //TODO: fix
        String quarter = String.valueOf(Html.fromHtml("<sup>1</sup>/<sub>4</sub>"));
        String half = String.valueOf(Html.fromHtml("<sup>1</sup>/<sub>2</sub>"));
        String[] labels = {quarter, half, "1", "2", "5","10+"};
        steps.addAll(RangeStepsListGenerator.generateList(values, labels, "ACRE", steps.get(steps.size() - 1).getStepValue()));


        EvenStepsRangeBarFiltersVM lotSiteVMItem = new EvenStepsRangeBarFiltersVM("LOT", false, steps, "Any Lot Size", "acres_min", "acres_max");
        if (filter != null) {
            lotSiteVMItem.setSelectedIndicesByValues(filter.getLotSizeSqFtMin(), filter.getLotSizeSqFtMax());
        }
        items.add(lotSiteVMItem);


        steps = new ArrayList<>();
        steps.add(RangeStepsListGenerator.generateMinStepForList(1955, "Prior to 1955", 10));
        steps.addAll(RangeStepsListGenerator.generateList(1955, 2005, 10));
        steps.add(RangeStepsListGenerator.generateMaxStepForList(2015, 10));
        EvenStepsRangeBarFiltersVM yearBuiltVMItem = new EvenStepsRangeBarFiltersVM(steps, "Any Year Built", false, false, "year_built_min", "year_built_max");
        if (filter != null) {
            yearBuiltVMItem.setSelectedIndicesByValues(filter.getYearBuiltMin(), filter.getYearBuiltMax());
        }
        items.add(yearBuiltVMItem);


        options = new double[]{0, 2, 3};
        PlusMinusFiltersVM parkingVMItem = new PlusMinusFiltersVM("PARKING", options, "parking_lots_min");
        if (filter != null) {
            parkingVMItem.setSelectedValue(filter.getParkingLotsMin());
        }
        items.add(parkingVMItem);


        PlusMinusFiltersVM storiesVMItem = new PlusMinusFiltersVM("STORIES", options, "stories_min");
        if (filter != null) {
            storiesVMItem.setSelectedValue(filter.getStoriesMin());
        }
        items.add(storiesVMItem);


        filtersViewModelList.add(new ExpandableItemFiltersVM("MORE BASIC DETAILS", items));
    }

    @NonNull
    private EvenStepsRangeBarFiltersVM getSqFtRangeBar() {
        List<RangeStep> steps;
        steps = RangeStepsListGenerator.generateList(0, 3000, 250);
        steps.addAll(RangeStepsListGenerator.generateList(3000, 5000, 1000));
        steps.add(RangeStepsListGenerator.generateMaxStepForList(10000, 5000));
        EvenStepsRangeBarFiltersVM sqftVMItem = new EvenStepsRangeBarFiltersVM("SQ FT", false, steps, "Any Property Size", "sq_ft_min", "sq_ft_max");
        if (filter != null) {
            sqftVMItem.setSelectedIndicesByValues(filter.getSqFtMin(), filter.getSqFtMax());
        }
        return sqftVMItem;
    }

    private void addBathsVMItem() {
        double[] options;
        options = new double[] {0, 1, 1.5, 2, 2.5f, 3};
        PlusMinusFiltersVM item = new PlusMinusFiltersVM("BATHS", options, "baths_min");
        if (filter != null) {
            item.setSelectedValue(filter.getBathsMin());
        }
        filtersViewModelList.add(item);
    }

    private void addBedsVMItem() {
        double options[] = new double[] {0, 1, 2, 3, 4, 5};
        PlusMinusFiltersVM item = new PlusMinusFiltersVM("BEDS",options, "beds_min");
        if (filter != null) {
            item.setSelectedValue(filter.getBedsMin());
        }
        filtersViewModelList.add(item);
    }

    private void addPriceRangeVMItem() {
        List<RangeStep> steps;
        steps = RangeStepsListGenerator.generateList(0, 2000, 250);
        steps.addAll(RangeStepsListGenerator.generateList(2000, 3000, 500));
        steps.addAll(RangeStepsListGenerator.generateList(3000, 5000, 1000));
        steps.addAll(RangeStepsListGenerator.generateList(5000, 37500, 2500));
        steps.add(RangeStepsListGenerator.generateMaxStepForList(40000, 2500));
        FactoredRangeSeekBarFiltersVM priceItem = new FactoredRangeSeekBarFiltersVM(PRICE_BUY_FACTOR, true, steps, "ANY PRICE", "price_min", "price_max", isBuyChecked);
        if (filter != null) {
            priceItem.setSelectedIndicesByValues(filter.getPriceMin(), filter.getPriceMax());
        }
        filtersViewModelList.add(priceItem);
    }

    private void addPropertyTypeVMItems() {
        Resources res = getActivity().getResources();
        List<RecyclerFilterItemVM> prptyTypes = new ArrayList<>();
        RecyclerFilterItemVM item = new RecyclerFilterItemVM(res.getString(R.string.house), R.drawable.house_fill, R.drawable.house, true, "is_house");
        if (filter != null) {
            item.setSelected(filter.isHouse());
        }
        prptyTypes.add(item);

        item = new RecyclerFilterItemVM(res.getString(R.string.condo), R.drawable.condo_fill, R.drawable.condo, true, "is_condo");
        if (filter != null) {
            item.setSelected(filter.isCondo());
        }
        prptyTypes.add(item);

        item = new RecyclerFilterItemVM(res.getString(R.string.townhouse), R.drawable.townhouse_fill, R.drawable.townhouse, true, "is_townhouse");
        if (filter != null) {
            item.setSelected(filter.isTownhouse());
        }
        prptyTypes.add(item);

        item = new RecyclerFilterItemVM(res.getString(R.string.multifamily), R.drawable.multifamily_fill, R.drawable.multifamily, true, "is_multifamily");
        if (filter != null) {
            item.setSelected(filter.isMultifamily());
        }
        prptyTypes.add(item);

        item = new RecyclerFilterItemVM(res.getString(R.string.land), R.drawable.land_fill, R.drawable.land, true, "is_land");
        if (filter != null) {
            item.setSelected(filter.isLand());
        }
        prptyTypes.add(item);

        filtersViewModelList.add(new GridFiltersVM(prptyTypes, true));
    }

    @Override
    public void onBuyRentToggle(boolean isBuySelected) {
        this.isBuyChecked = isBuySelected;
        createViewModelsList();
//        filtersRecyclerView.getAdapter().notifyDataSetChanged();
        filtersRecyclerView.setAdapter(new FiltersAdapter(this, filtersViewModelList, true));

    }

    @Override
    public boolean getIsBuy() {
        return false;
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
        for(FiltersViewModelItem viewModel : filtersViewModelList){
            viewModel.addPropertiesToJson(jsonObject);
        }
    }

    @Override
    public boolean isValidated(Context context, View parentView) {
        boolean isValidated;
        for(FiltersViewModelItem viewModel : filtersViewModelList){
            isValidated = viewModel.isValidated(context, parentView);
            if (! isValidated){
                return false;
            }
        }
        return true;
    }

    @Override
    public void onPause() {
        BusProvider.getBus().unregister(this);
        super.onPause();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }

}
