package com.joinreal.app.fragments;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.app.interfaces.OpenSearchFiltersListener;
import com.joinreal.events.ChatDialogsReceivedEvent;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.ReceivedFiltersEvent;
import com.joinreal.events.ReceivedPropertiesEvent;
import com.joinreal.events.StartActivityEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.PropertyThin;
import com.joinreal.model.data.SearchFilter;
import com.joinreal.model.data.chat.ChatDialogDataVM;
import com.joinreal.model.view.ContactVM;
import com.joinreal.tasks.GetContactsFavoritesTask;
import com.joinreal.tasks.SendMessageTask;
import com.joinreal.utils.ChatConnect;
import com.joinreal.utils.ChatImagesUtils;
import com.joinreal.utils.TimeStringUtils;
import com.joinreal.utils.ChatKeyboardUtils;
import com.quickblox.users.model.QBUser;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by brittbarak on 7/6/15.
 */
public class ContactInfoFragment extends RealBaseFragment {

    public static final String TAG = "ContactInfoFragment";

    ContactVM contactVM;

    @Optional
    @InjectView(R.id.thumb)
    ImageView thumb;
    @InjectView(R.id.contact_initials)
    TextView initials;
    @InjectView(R.id.name)
    TextView name;
    @InjectView(R.id.last_login)
    TextView lastLogin;

    @InjectView(R.id.message_icon)
    TextView messageIcon;
    @InjectView(R.id.phone_icon)
    ImageView phoneIcon;
    @InjectView(R.id.mail_icon)
    ImageView mailIcon;
    @InjectView(R.id.location_icon)
    ImageView locationIcon;

//    @InjectView(R.id.add_notes_text)
//    TextView addNotesText;

    @InjectView(R.id.find_properties_text)
    TextView findPropertyText;
    @InjectView(R.id.property_search_icon)
    ImageView propertyIcon;

    @InjectView(R.id.favorites_text)
    TextView favoritesText;


    private ArrayList<PropertyThin> propertyList = null;
    private GetContactsFavoritesTask favoritesTask;
    private boolean didClickOnContactFavorites;
    private SearchFilter filter;
    private GetContactsLastFiltersTask lastFiltersTask;
    AgentActionsListener agentActionsListener;

    public static ContactInfoFragment getNewInstance(Bundle bd) {
        ContactInfoFragment fragment = new ContactInfoFragment();
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.agentActionsListener = (AgentActionsListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.contactVM = (ContactVM) getArguments().getSerializable(StaticParams.KEY_CONTACT_VM);
        setHasOptionsMenu(true);
        agentActionsListener.setHomeIcon(R.drawable.back_arrow_blue);
        //TODO:
//        new GetContactLastSearchTask(Integer.toString(contactVM.getContact().getId())).execute();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_info, container, false);

        ButterKnife.inject(this, rootView);
        ChatImagesUtils.setContactProfileThumb(contactVM, thumb, initials);

        name.setText(contactVM.getContact().getDisplayName());
        if (contactVM.getContact().getLastSignInAt() != null) {
            lastLogin.setText(TimeStringUtils.parseDateFromJsonFormat(contactVM.getContact().getLastSignInAt()));
        }

        setFavoritesCard(rootView);

        if (contactVM.getContact().getEmail() == null) {
            mailIcon.setImageResource(R.drawable.mail_disabled);
        } else {
            mailIcon.setImageResource(R.drawable.mail1);
        }

        if (contactVM.getContact().hasLocation()) {
            locationIcon.setImageResource(R.drawable.location1);
        } else {
            locationIcon.setImageResource(R.drawable.location_disabled);
        }

//        setAddNotesButtonView();

        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }

    private void setFavoritesCard(View rootView) {
        if (contactVM.getContact().getLastSignInAt() != null) {
            rootView.findViewById(R.id.invite_card).setVisibility(View.GONE);
            rootView.findViewById(R.id.favorites_card).setVisibility(View.VISIBLE);
            favoritesText.setText("Loading favorites...");
        } else {
            rootView.findViewById(R.id.favorites_card).setVisibility(View.GONE);
            rootView.findViewById(R.id.invite_card).setVisibility(View.VISIBLE);
        }
    }

    private void setFindPropertiesButton() {
        String text;
        if (filter == null) {
            text = "Find properties for " + ((contactVM.getContact().getFirstname() != null) && (!contactVM.getContact().getFirstname().equals("")) ? contactVM.getContact().getFirstname() : contactVM.getContact().getDisplayName());
        } else {

            text = Helper.getPriceRangeString(filter.getPriceMin(), filter.getPriceMax());
            if (filter.getBedsMin() != null) {
                text += " " + Helper.removeZeroFromFloatIfNeeded(filter.getBedsMin()) + "+ BEDS ";
            }
            if (filter.getBathsMin() != null) {
                text += Helper.removeZeroFromFloatIfNeeded(filter.getBathsMin()) + "+ BATHS";
            }


            propertyIcon.setImageResource(getPropertyTypeDrawable());
            propertyIcon.setVisibility(View.VISIBLE);
        }
        findPropertyText.setText(text);
    }

    private int getPropertyTypeDrawable() {
        if (filter.isHouse()) {
            return R.drawable.house;
        } else if (filter.isCondo()) {
            return R.drawable.condo;
        } else if (filter.isLand()) {
            return R.drawable.land;
        } else if (filter.isMultifamily()) {
            return R.drawable.multifamily;
        } else if (filter.isTownhouse()) {
            return R.drawable.townhouse;
        }
        return 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        agentActionsListener.lockDrawer("Contact Info");
        ChatKeyboardUtils.showKeyboard(getView());
        if (shouldRunTask()) {
            favoritesTask = new GetContactsFavoritesTask(Integer.toString(contactVM.getContact().getContactId()));
            favoritesTask.execute();
        }
        if (shouldRunTask()) {
            findPropertyText.setText("Loading properties...");
            lastFiltersTask = new GetContactsLastFiltersTask(Integer.toString(contactVM.getContact().getContactId()));
            lastFiltersTask.execute();
        } else {
            setFindPropertiesButton();
        }
    }

    private boolean shouldRunTask() {
        return (contactVM.getContact().getLastSignInAt() != null) &&
                (contactVM.getContact().getContactId() != null);
    }

    public void onEditButtonClick() {
        Intent i = new Intent(Intent.ACTION_EDIT);
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactVM.getContact().getAndroidContactId());
        i.setData(contactUri);
        i.putExtra("finishActivityOnSaveCompleted", true);
        startActivity(i);
    }

    @OnClick(R.id.message_layout)
    public void onMessageIconClick() {
        if (contactVM.getContact().getAgentId() == Application.getClient().getUser().getResourceId()) {
            ((OpenChatListener) getActivity()).openPrivateChat(contactVM);
        } else {
            Helper.getSnackbar(getActivity(), getView(), "Cannot perform chat. Please make sure contact isn't assigned to another agent.").show();
        }

    }

    @OnClick(R.id.phone_icon)
    public void onPhoneIconClick() {
        if (Helper.isFeatureAvailable(PackageManager.FEATURE_TELEPHONY)) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + contactVM.getContact().getPhone()));
            startActivity(intent);
        } else {
            Helper.getSnackbar(getActivity(), getView(), "The feature is not enabled on this device.").show();
        }
    }

    @OnClick(R.id.mail_icon)
    public void onMailIconClick() {
        if (contactVM.getContact().getEmail() == null) {
            Helper.getSnackbar(getActivity(), getView(), "There is no email address assigned to this contact.").show();
        } else {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
//        i.putExtra(Intent.EXTRA_SUBJECT, eMessage);
//        i.putExtra(Intent.EXTRA_TEXT  , buildEmailBody(eMessage, username, token, bodyJson));
            i.putExtra(Intent.EXTRA_EMAIL, contactVM.getContact().getEmail());
            try {
                Intent.createChooser(i, "Send mail...");
                BusProvider.getBus().post(new StartActivityEvent(i));
            } catch (android.content.ActivityNotFoundException ex) {
                Helper.getSnackbar(Application.getAppContext(), getView(), "The feature is not enabled on this device.").show();
            }
        }
    }

    @OnClick(R.id.location_icon)
    public void onLocationIconClick() {
        if (contactVM.getContact().hasLocation()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(contactVM.getContact().getLocationURI());
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Helper.getSnackbar(getActivity(), getView(), "The feature is not enabled on this device.").show();
            }
        } else {
            Helper.getSnackbar(getActivity(), getView(), "There is no location assigned to this contact.").show();
        }
    }

//    @OnClick(R.id.add_notes_card)
//    public void onAddNotesClick() {
//        agentActionsListener.openNotesFragment(contactVM.getContact().getAgentNotes(), contactVM.getContact().getChatLogin());
//    }

    @Subscribe
    public void getFavoritePropertyList(ReceivedPropertiesEvent event) {
        propertyList = event.getProperties();
        if (propertyList != null) {
            String name = contactVM.getContact().getFirstname() != null ? contactVM.getContact().getFirstname() : contactVM.getContact().getDisplayName();
            favoritesText.setText(name + "'s Favorites (" + propertyList.size() + ")");
        } else {
            Helper.showNetworkErrorToast(getActivity(), NetworkCodes.CANT_GET_FAVORITES);
        }
    }

    @Subscribe
    public void onGetContactFilterEvent(ReceivedFiltersEvent event) {
        filter = event.getFilter();
        setFindPropertiesButton();
    }

    @OnClick(R.id.favorites_card)
    public void onFavoritesCardClick() {
        didClickOnContactFavorites = true;
        Bundle bd = new Bundle();
        bd.putString(StaticParams.KEY_CONTACT_ID, String.valueOf(contactVM.getContact().getPhone()));
        bd.putString(StaticParams.KEY_CONTACT_NAME, (contactVM.getContact().getFirstname() != null) ? contactVM.getContact().getFirstname() : contactVM.getContact().getDisplayName());
        bd.putBoolean(StaticParams.KEY_IS_FAVORITES_LIST, true);
        bd.putSerializable(StaticParams.KEY_PROPERTY_LIST, propertyList);
        ((OpenSearchFiltersListener) getActivity()).openFavoritesFragment(bd);
    }

    @OnClick(R.id.invite_card)
    public void onInviteCardClick() {
        ArrayList<String> l = new ArrayList<>();
        l.add(contactVM.getContact().getPhone());
        agentActionsListener.openSendInvitesFragment(l, getTag());
    }

    @OnClick(R.id.find_properties_card)
    public void onFindPropertiesCardClick() {
        if ((!shouldRunTask()) || ((lastFiltersTask != null) && (lastFiltersTask.getStatus() == AsyncTask.Status.FINISHED))) {
            ((OpenSearchFiltersListener) getActivity()).openFiltersActivityForContact(String.valueOf(contactVM.getContact().getContactId()), contactVM.getContact().getDisplayName(), filter);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getBus().register(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        TextView tv = MainActivityAbstract.getActionButtonTextView("EDIT", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditButtonClick();
            }
        });
        menu.add(0, 0, 1, "EDIT").setActionView(tv).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);


        menu.removeItem(R.id.action_add);
    }

//    @Subscribe
//    public void onEditNotes(SaveContactNotesEvent event) {
//        contactVM.getContact().setAgentNotes(event.getNotes());
//        ContactManager.getContactByLogin(contactVM.getContact().getPhone()).setAgentNotes(event.getNotes());
//        setAddNotesButtonView();
//    }


//    private void setAddNotesButtonView() {
//        String notes = contactVM.getContact().getAgentNotes();
//        if ((notes == null) || notes.equals("")) {
//            addNotesText.setText("Add Notes");
//            addNotesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getActivity().getResources().getDimension(R.dimen.font_size_titles));
//        } else {
//            addNotesText.setText(notes);
//            addNotesText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getActivity().getResources().getDimension(R.dimen.font_size_main));
//        }
//    }

    @Override
    public void onPause() {
        super.onPause();
        agentActionsListener.unlockDrawer();
        if ((!didClickOnContactFavorites) && (favoritesTask != null)) {
            favoritesTask.cancel(true);
        }
        if (lastFiltersTask != null) {
            lastFiltersTask.cancel(true);
        }
//        ContactManager.setContactInMap(contactVM.getContact());
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getBus().unregister(this);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    public class GetContactsLastFiltersTask extends AsyncTask<Object, Void, SearchFilter> {
        private String contactId;

        public GetContactsLastFiltersTask(String contactId) {
            this.contactId = contactId;
        }


        @Override
        protected SearchFilter doInBackground(Object[] params) {
            try {
                return Application.getClient().getContactLastSearchFilters(contactId);
            } catch (NetworkException e) {
                BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.ERROR_GET_CONTACT_LAST_SEARCH));
                return null;
            }
        }

        @Override
        protected void onPostExecute(SearchFilter filter) {
            BusProvider.getBus().post(new ReceivedFiltersEvent(filter));
        }
    }

    @Subscribe
    public void onNewDialogsRecievedEvent(final ChatDialogsReceivedEvent e) {
        ChatConnect.getUserOrCreate(contactVM.getContact().getChatLogin(), new ChatConnect.UserResponseListener() {
            @Override
            public void onSuccess(QBUser user, boolean created) {
                for (ChatDialogDataVM dialog : e.getDialogs()) {
                    for (Integer occupantId : dialog.getOccupants()) {
                        if (user.getId().equals(occupantId)) {
                            int unreadMessagesCount = dialog.getUnreadMessageCount();
                            if (unreadMessagesCount > 0) {
                                if (messageIcon != null) {
                                    messageIcon.setText(String.valueOf(unreadMessagesCount));
                                    return;
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(List<String> errors) {}
        });
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
