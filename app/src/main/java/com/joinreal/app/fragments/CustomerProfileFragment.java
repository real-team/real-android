package com.joinreal.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.INavigationListener;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.StartActivityEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.Customer;
import com.joinreal.utils.ChatImagesUtils;
import com.joinreal.utils.ChatKeyboardUtils;
import com.joinreal.utils.CircleTransform;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by brittbarak on 8/1/15.
 */
public class CustomerProfileFragment extends RealBaseFragment implements ChatImagesUtils.ISelectImageListener {
    public static final String TAG = "CustomerProfileFragment";
    private static final String TERMS_OF_USE_URL = "http://www.joinreal.com/legal";

    @InjectView(R.id.thumb)
    ImageView profileImage;

    @InjectView(R.id.first_name_edittext)
    EditText firstNameEdittText;

    @InjectView(R.id.last_name_edittext)
    EditText lastNameEdittText;

    @InjectView(R.id.email_edittext)
    EditText emailEditText;

    private Customer customer;

    private INavigationListener listener;


    public static CustomerProfileFragment getNewInstance() {
        return new CustomerProfileFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (INavigationListener)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_profile, container, false);
        ButterKnife.inject(this, rootView);

        customer = Application.getClient().getUser().getCustomer();
        firstNameEdittText.setText(customer.getFirstname());
        lastNameEdittText.setText(customer.getLastname());

        if (customer.getEmail() != null) {
            emailEditText.setText(customer.getEmail());
        }

        View uploadImageOverlay = rootView.findViewById(R.id.upload_photo);
        uploadImageOverlay.setBackground(Helper.getTintedDrawable(R.drawable.circle, getResources().getColor(R.color.gray_light_shadow)));
        Picasso.with(getActivity()).load(customer.getProfileImage().getMedium()).transform(new CircleTransform()).into(profileImage);
        rootView.findViewById(R.id.img_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ChatImagesUtils.selectImage(CustomerProfileFragment.this);
                onImageLayoutClick();
            }
        });


        thumb = (ImageView) rootView.findViewById(R.id.thumb);
        if (customer.getNewProfileImage() != null) {
            thumb.setImageBitmap(customer.getNewProfileImage());
        } else {
            Picasso.with(getActivity()).load(customer.getImage()).transform(ChatImagesUtils.getCircleTransform()).into(thumb);
        }


        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }

    private void onImageLayoutClick() {
//        Intent intent = new Intent(getActivity(), CropImageActivity.class);
//        startActivityForResult(intent, StaticParams.REQUEST_PICK_IMAGE);
        listener.openCropImageFragment();


    }

    @Subscribe
    public void onPickedProfileImage(Bitmap bitmap) {
        customer.setNewProfileImage(bitmap);
        thumb.setImageBitmap(bitmap);

    }

    @OnClick(R.id.save_button)
    public void onSaveButtonClick() {
//        boolean isValid = validateAllFields();
//        if (isValid) {
            saveCustomerState();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        Application.getClient().updateCustomerDetails(customer);
                    } catch (NetworkException e) {
                        BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.CANT_UPDATE_CUSTOMER_DETAILS));
                    }
                }
            };
            new Thread(runnable).start();

            Helper.getSnackbar(getActivity(), getView(), "Saving profile").show();
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ChatKeyboardUtils.hideKeyboard(getView());
                getActivity().onBackPressed();
                }
            }, 700);
//        }
    }


    @OnClick(R.id.support_button)
    public void supportButton(){
        listener.sendSupportEmail();
    }

@OnClick(R.id.terms_use_button)
    public void termsOfUseButton(){
        listener.openWebviewFragment(TERMS_OF_USE_URL);
    }


    private void saveCustomerState() {
        customer.setFirstname(firstNameEdittText.getText().toString());
        customer.setLastname(lastNameEdittText.getText().toString());
        customer.setEmail(emailEditText.getText().toString());

        Application.getClient().setUserCustomer(customer);

    }
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_search);
        menu.removeItem(R.id.action_favorites);
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.setToolbarTitle("My Info");
        listener.setHomeIcon(R.drawable.ic_close);

    }

    @Override
    public void onPause() {
        super.onPause();
        listener.setHomeIcon(R.drawable.user_profile);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public void onStartActivityForResult(StartActivityEvent event) {
        startActivityForResult(event.getIntent(), event.getRequestCode());
    }

    private Bitmap originalImage;
    protected ImageView thumb;

    //TODO: unite with method on AgentProfileEditFragment
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((resultCode == Activity.RESULT_OK) &&(requestCode == StaticParams.REQUEST_PICK_IMAGE)) {
//            String selectedImagePath = "";
//            final int imageDimen = (int) Application.getAppContext().getResources().getDimension(R.dimen.profile_image_size);
//            final int imageSize = (int) Helper.pxlsToDp(getActivity(), imageDimen);
//            BitmapFactory.Options options = new BitmapFactory.Options();
//
//
//            if (requestCode == StaticParams.REQUEST_CAMERA) {
//                originalImage = (Bitmap) data.getExtras().get("data");
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                originalImage.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
//                File destination = new File(Environment.getExternalStorageDirectory(),
//                        System.currentTimeMillis() + ".jpg");
//                FileOutputStream fo;
//                try {
//                    destination.createNewFile();
//                    fo = new FileOutputStream(destination);
//                    fo.write(bytes.toByteArray());
//                    fo.close();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
////                agent.setNewProfileImage(ChatImagesUtils.getCircleBitmap(thumbnail, imageSize));
//                selectedImagePath = destination.getAbsolutePath();
//            } else if (requestCode == StaticParams.REQUEST_SELECT_FILE) {
//
//                Uri selectedImageUri = data.getData();
//                String[] projection = {MediaStore.MediaColumns.DATA};
//                Cursor cursor = getActivity().getContentResolver().query(selectedImageUri, projection, null, null, null);
//                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//                cursor.moveToFirst();
//                selectedImagePath = cursor.getString(column_index);
//
//            }
//            options.inJustDecodeBounds = true;
//            originalImage = BitmapFactory.decodeFile(selectedImagePath, options);
//            int scale = 1;
//            while (options.outWidth / scale / 2 >= imageSize
//                    && options.outHeight / scale / 2 >= imageSize) {
//                scale *= 2;
//            }
//            options.inSampleSize = scale;
//            options.inJustDecodeBounds = false;
//            Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);
//            if (bm != null) {
//                customer.setNewProfileImage(ChatImagesUtils.getCircleBitmap(bm, imageSize));
//                thumb.setImageBitmap(customer.getNewProfileImage());
//            }

//      Picasso.with(getActivity()).load(customer.getNewProfileImage()).into(thumb);


            Bitmap croppedImage = data.getParcelableExtra(StaticParams.KEY_PROFILE_IMAGE);
            customer.setNewProfileImage(croppedImage);
            thumb.setImageBitmap(croppedImage);

            Helper.getSnackbar(getActivity(), getView(), "Saving profile").show();
            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ChatKeyboardUtils.hideKeyboard(getView());
//                    getActivity().getSupportFragmentManager().beginTransaction().remove(CustomerProfileFragment.this).commit();
                    getActivity().onBackPressed();

                }
            }, 500);

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getBus().register(this);
    }

    @Override
    public void onStop() {
        BusProvider.getBus().unregister(this);
        super.onStop();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
