package com.joinreal.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.app.interfaces.AgentActionsListener;

/**
 * Created by brittbarak on 7/26/15.
 */
public class AgentProfileFragment extends AgentProfileReadOnlyFragment {

    public static final String TAG = "AgentProfileFragment";
    protected AgentActionsListener agentActionsListener;

    public static AgentProfileFragment getNewInstance() {
        return new AgentProfileFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.agentActionsListener = (AgentActionsListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        navigationListener.setHomeIcon(R.drawable.hamburger);
    }

    @Override
    public void onResume() {
        super.onResume();
        agentActionsListener.unlockDrawer("My Profile");
    }

    public void onEditButtonClick() {
        agentActionsListener.openEditAgentProfileFragment();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (Application.getClient().isAgent()) {
            setTopActionButton(menu);
        }
        menu.removeItem(R.id.action_add);
    }

    protected void setTopActionButton(Menu menu) {
        TextView tv = MainActivityAbstract.getActionButtonTextView("EDIT", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditButtonClick();
            }
        });
        menu.add(0, 0, 1, "EDIT").setActionView(tv).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
