package com.joinreal.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.events.SaveContactNotesEvent;
import com.joinreal.model.data.Contact;
import com.joinreal.utils.ChatKeyboardUtils;
import com.joinreal.utils.ContactManager;

/**
 * Created by brittbarak on 7/6/15.
 */
public class ContactNotesFragment extends RealBaseFragment {

    public static final String TAG = "ContactNotesFragment";
    private EditText notesEditText;
    AgentActionsListener agentActionsListener;

    public static ContactNotesFragment getNewInstance(Bundle bd) {
        ContactNotesFragment fragment = new ContactNotesFragment();
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        agentActionsListener = (AgentActionsListener) getActivity();
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_contact_notes, container, false);
        notesEditText = (EditText) rootView.findViewById(R.id.notes);
        notesEditText.setText(getArguments().getString(StaticParams.KEY_CONTACT_NOTES, ""));
        notesEditText.requestFocus();
        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        TextView tv = MainActivityAbstract.getActionButtonTextView("SAVE", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClick();
            }
        });
        menu.add(0, 0, 1, "SAVE").setActionView(tv).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



        menu.removeItem(R.id.action_add);
    }

    private void onSaveButtonClick() {
        ChatKeyboardUtils.hideKeyboard(notesEditText);
        getActivity().onBackPressed();
        String login = getArguments().getString(StaticParams.KEY_CONTACT_LOGIN, "");
        Contact contact = ContactManager.getContactByLogin(login);
        contact.setAgentNotes(notesEditText.getText().toString());
        BusProvider.getBus().post(new SaveContactNotesEvent(notesEditText.getText().toString()));

    }

    @Override
    public void onResume() {
        super.onResume();
        agentActionsListener.lockDrawer("Contacts Notes");
        ChatKeyboardUtils.showKeyboard(notesEditText);

    }

    @Override
    public void onPause() {
        super.onPause();
        agentActionsListener.unlockDrawer();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
