package com.joinreal.app.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.joinreal.R;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.utils.ChatKeyboardUtils;

import java.util.ArrayList;


/**
 * Created by brittbarak on 7/6/15.
 */
public class InviteNewContactFragment extends RealBaseFragment {
    public static final String TAG = "InviteNewContactFragment";
    private AgentActionsListener listener;

    public static InviteNewContactFragment getNewInstance(String entryTagToReturnTo) {
        InviteNewContactFragment fragment = new InviteNewContactFragment();
        Bundle bd = new Bundle(1);
        bd.putString(StaticParams.ENTRY_TAG_TO_RETURN_TO, entryTagToReturnTo);
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.listener = (AgentActionsListener)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_invite_new_contact, container, false);
        listener.setHomeIcon(R.drawable.ic_close);
        listener.setToolbarTitle("Invite New Contact");

        rootView.findViewById(R.id.invite_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rootView.findViewById(R.id.invitations_sent_layout).setVisibility(View.VISIBLE);
                ((MainActivityAbstract) getActivity()).setToolbarTitle("Invitations Sent");
                ChatKeyboardUtils.hideKeyboard(rootView);
                ArrayList<String> inputPhoneNumArray = new ArrayList<String>();
                final String phoneNum = ((EditText) rootView.findViewById(R.id.phone_number_edit_text)).getText().toString();
                inputPhoneNumArray.add(phoneNum);
                listener.openSendInvitesFragment(inputPhoneNumArray, true, getArguments().getString(StaticParams.ENTRY_TAG_TO_RETURN_TO));

            }
        });

        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }

    private void addToContacts(String phoneNum) {
            Intent intent = new Intent(Intent.ACTION_INSERT);
            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, phoneNum);
            getActivity().startActivity(intent);
    }


    @Override
    public void onResume() {
        super.onResume();
        ((AgentActionsListener)getActivity()).lockDrawer("Invite New Contact");
    }


    @Override
    public void onPause() {
        super.onPause();
        ((AgentActionsListener)getActivity()).unlockDrawer();
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
