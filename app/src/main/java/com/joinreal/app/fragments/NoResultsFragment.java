package com.joinreal.app.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.OpenSearchFiltersListener;

import java.util.Arrays;

/**
 * Created by brittbarak on 7/18/15.
 */
public class NoResultsFragment extends RealBaseFragment {

    public static final String TAG = "NoResultsFragment";
    private static final String[] SUPPORTED_STATES = {"TX", "TEXAS", "GA", "GEORGIA", "CO", "COLORADO"};
    OpenSearchFiltersListener listener;

    public static NoResultsFragment getNewInstance(String state, boolean isFavorites) {
        NoResultsFragment fragment = new NoResultsFragment();
        Bundle bd = new Bundle(2);
        bd.putSerializable(StaticParams.KEY_STATE, state);
        bd.putBoolean(StaticParams.KEY_IS_FAVORITES_LIST, isFavorites);
        fragment.setArguments(bd);
        return fragment;
    }

    private boolean isSupportedArea(String state) {
        return (state != null) && Arrays.asList(SUPPORTED_STATES).contains(state.toUpperCase());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OpenSearchFiltersListener) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_no_results, container, false);

        TextView noResultMsg = (TextView) rootView.findViewById(R.id.no_results_msg);
        Button updateButton = (Button) rootView.findViewById(R.id.update_button);
        Button resetButton = (Button) rootView.findViewById(R.id.reset_button);
        String state = getArguments().getString(StaticParams.KEY_STATE, null);

        if (getArguments().getBoolean(StaticParams.KEY_IS_FAVORITES_LIST)) {
            noResultMsg.setText("You have not marked any properties as favorite.");
            updateButton.setText("Go Back");
            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
            resetButton.setVisibility(View.GONE);
        } else if (state == null) {
            noResultMsg.setText("Sorry, in order to display properties, we need a location.");
            updateButton.setText("Insert Location");
            setUpdateButton(updateButton, "Insert Location");
            resetButton.setText("Enable Location Services");
            resetButton.setVisibility(View.VISIBLE);
            resetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent enableLocationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(enableLocationIntent);
                }
            });
        } else if (isSupportedArea(getArguments().getString(StaticParams.KEY_STATE))) {
            noResultMsg.setText("No Matching Results");
            setUpdateButton(updateButton, "Update Filters");
            resetButton.setText("Reset All Filters");
            resetButton.setVisibility(View.VISIBLE);
            resetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    getActivity().onBackPressed();
                    listener.openFiltersActivity(true);
                }
            });
        } else {
            noResultMsg.setText("Sorry, we do not serve this area yet!");
            setUpdateButton(updateButton, "Change Location");
            resetButton.setVisibility(View.GONE);


        }




        TypefaceManager.getInstance(getActivity()).assignTypeface(rootView);
        return rootView;
    }

    private void setUpdateButton(Button updateButton, String title) {
        updateButton.setText(title);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().onBackPressed();
                listener.openFiltersActivity(false);
            }
        });
    }

    @Override
    protected String getScreenName() {
        return TAG;
    }
}
