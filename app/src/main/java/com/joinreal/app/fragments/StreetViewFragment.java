package com.joinreal.app.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.StaticParams;

/**
 * Created by brittbarak on 8/9/15.
 */
public class StreetViewFragment extends SupportStreetViewPanoramaFragment implements StreetViewPanorama.OnStreetViewPanoramaChangeListener {

    public static final String TAG = "StreetViewFragment";

    public static StreetViewFragment getNewInstance(Double latitude, Double longitude) {
        Bundle bd = new Bundle(2);
        bd.putDouble(StaticParams.KEY_LATITUDE, latitude);
        bd.putDouble(StaticParams.KEY_LONGITUDE, longitude);
        StreetViewFragment fragment = new StreetViewFragment();
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = super.onCreateView(inflater, container, savedInstanceState);
        StreetViewPanorama panorama = getStreetViewPanorama();
        //Check we got a valid instance of the StreetViewPanorama
        if (panorama != null) {
            double latitude = getArguments().getDouble(StaticParams.KEY_LATITUDE, -1);
            double longitude = getArguments().getDouble(StaticParams.KEY_LONGITUDE, -1);
            panorama.setPosition(new LatLng(latitude, longitude));
            panorama.setOnStreetViewPanoramaChangeListener(this);

        }
        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        Analytics.getInstance().trackScreen(TAG);
    }

    @Override
    public void onStreetViewPanoramaChange(StreetViewPanoramaLocation streetViewPanoramaLocation){
        //Get the angle between the target location and road side location
        float bearing = getBearing(
                streetViewPanoramaLocation.position.latitude,
                streetViewPanoramaLocation.position.longitude,
                getArguments().getDouble(StaticParams.KEY_LATITUDE, -1),
                getArguments().getDouble(StaticParams.KEY_LONGITUDE, -1));

        //Remove the listener
        getStreetViewPanorama().setOnStreetViewPanoramaChangeListener(null);
        //Change the camera angle
        StreetViewPanoramaCamera camera = new StreetViewPanoramaCamera.Builder()
                .bearing(bearing)
                .build();
        getStreetViewPanorama().animateTo(camera, 1);
    }

    private float getBearing(double startLat, double startLng, double endLat, double endLng) {
        Location startLocation = new Location("startlocation");
        startLocation.setLatitude(startLat);
        startLocation.setLongitude(startLng);

        Location endLocation = new Location("endlocation");
        endLocation.setLatitude(endLat);
        endLocation.setLongitude(endLng);

        return startLocation.bearingTo(endLocation);
    }


}
