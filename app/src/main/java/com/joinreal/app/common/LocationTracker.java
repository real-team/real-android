package com.joinreal.app.common;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.joinreal.app.Application;
import com.joinreal.model.LocationObj;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by britt on 6/8/15.
 */
public class LocationTracker {
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;


    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private Activity activity;
    private LocationRecievedListener resultListener;
    private LocationListener mLocationListener;
    private static Geocoder geocoder;


    public final String TAG = getClass().getSimpleName();
    private String locationStr;
    private LocationObj locationObj;

    public interface LocationRecievedListener {
        void onLocationRecieved(String locationString, LocationObj locationObj);
    }

    public static LatLng getLatLngFromAddress(String location) {
        if (geocoder == null) {
            geocoder = new Geocoder(Application.getAppContext(), Locale.ENGLISH);
        }
        List<Address> list = null;
        try {
            list = geocoder.getFromLocationName(location, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if ((list != null) && !list.isEmpty()) {
            Address add = list.get(0);
            return new LatLng(add.getLatitude(), add.getLongitude());
        } else {
            return null;
        }
    }

    public LocationTracker() {
        geocoder = new Geocoder(Application.getAppContext(), Locale.ENGLISH);
    }

    protected synchronized void buildGoogleApiClient(final LocationRecievedListener locationRecievedListener) {
        mGoogleApiClient = new GoogleApiClient.Builder(activity.getApplicationContext())
                .addConnectionCallbacks(getConnectedListener(locationRecievedListener))
                .addOnConnectionFailedListener(getConnectionFailedListener())
                .addApi(LocationServices.API)
                .build();
    }

    private GoogleApiClient.OnConnectionFailedListener getConnectionFailedListener() {
        return new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
                System.out.println("onConnectionFailed, has resolution? " + connectionResult.hasResolution());
                if (connectionResult.hasResolution()) {
                    try {
                        // Start an Activity that tries to resolve the error
                        connectionResult.startResolutionForResult(activity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                        System.out.println("** onConnectionFailed , message: " + e.getMessage());
                    }
                } else {
                    System.out.println("** onConnectionFailed , with code: " + connectionResult.getErrorCode());

                }
            }
        };
    }

    private GoogleApiClient.ConnectionCallbacks getConnectedListener(final LocationRecievedListener locationRecievedListener) {
        return new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle connectionHint) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, getLocationChangedListener());

                if (mLastLocation == null) {
                    System.out.println("location is null");
                    if (isLocationServiceDisabled()) {
                        resultListener.onLocationRecieved("Can't Access Location", null);
                        return;
                    }
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, getLocationChangedListener());
                } else {
                    System.out.println("lat : " + mLastLocation.getLatitude() + "  long : " + mLastLocation.getLongitude());
                    //LocationTracker.stopTrackingLocationFromNetwork();
                    handleNewLocation();
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
                System.out.println("***SUSPENDED");

            }

        };
    }

    private boolean isLocationServiceDisabled() {
        LocationManager lm = (LocationManager) Application.getAppContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gpsEnabled = false;
        boolean networkEnabled = false;

        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        return ((!gpsEnabled) && (!networkEnabled));
    }

    private LocationListener getLocationChangedListener() {
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                System.out.println("onLocationChanged, location is null? " + (location == null));
                mLastLocation = location;
                handleNewLocation();
            }
        };
        return mLocationListener;
    }

    private void handleNewLocation() {
        parseLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        locationObj.setDescription(locationStr);
        resultListener.onLocationRecieved(locationStr, locationObj);
    }


    public void getUserLocation(Activity inputActivity, final LocationRecievedListener locationRecievedListener) {
        activity = inputActivity;
        resultListener = locationRecievedListener;
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        buildGoogleApiClient(locationRecievedListener);
        mGoogleApiClient.connect();
    }

    public void parseLocation(double lat, double lng) {
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
        } catch (IOException e) {
            return;
        }
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String zip = addresses.get(0).getPostalCode();
        String street = ""; //per server demand : sends "street_number street_name"
        String streetNum = addresses.get(0).getSubThoroughfare();
        if ((streetNum != null) && (! streetNum.contains("-"))) {
            street = addresses.get(0).getSubThoroughfare() + " ";
        }
        street += addresses.get(0).getThoroughfare();

        this.locationStr = city + " , " + ((state != null) ? state : country);
        this.locationObj = new LocationObj(state, city, street, zip);
        locationObj.setLat(lat);
        locationObj.setLng(lng);
    }

    public void stopTrackingLocationFromNetwork() {
        if ((mGoogleApiClient != null) && mGoogleApiClient.isConnected()) {
            if ((mLocationListener != null)) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationListener);
            }
            mGoogleApiClient.disconnect();
        }
    }

    public String getLocationString() {
        return locationStr;
    }

    public LocationObj getLocationObj() {
        return locationObj;
    }
}
