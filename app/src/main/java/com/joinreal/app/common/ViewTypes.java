package com.joinreal.app.common;

/**
 * Created by brittbarak on 6/28/15.
 */
public class ViewTypes {

    public static final int BASIC_INFO_VIEW_TYPE = 1001;
    public static final int OVERVIEW_VIEW_TYPE = 1002;
    public static final int ADDITIONAL_FRATURE_VIEW_TYPE = 1003;
    public static final int ADDRESS_MAP_VIEW_TYPE = 1004;
    public static final int SUMMARY_INFO_VIEW_TYPE = 1005;

    public static final int PROPERTY_SUBCATEGORY_VIEW_TYPE = 2001;
    public static final int PROPERTY_FIELD_VIEW_TYPE = 2002;

    public static final int CHAT_USER = 3001;
    public static final int CHAT_DIALOG = 3002;
    public static final int CONTACTS_SUBTITLE = 3003;
    public static final int CONTACTS_ACTION = 3004;
    public static final int CONTACTS_SUBTITLE_WITH_ACTION = 3005;
}
