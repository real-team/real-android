package com.joinreal.app.common;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.joinreal.app.Application;

/**
 * Created by brittbarak on 8/26/15.
 */
public class Analytics {
    private static final String GLOBAL_TRACKING_ID = "UA-63435661-5";
    private static final String CUSTOMER_TRACKING_ID = "UA-63435661-3";
    private static final String AGENT_TRACKING_ID = "UA-63435661-4";
    private static final String QA_APP_TRACKING_ID = "UA-63435661-2";

    public static Analytics instance;
    private Tracker mTracker;
    private Tracker appTracker;

    public void initInstance(Context context) {
        setDefaultTracker(context);
    }

    public static Analytics getInstance() {
        if (instance == null) {
            instance = new Analytics();
        }
        return instance;
    }

    synchronized public void setDefaultTracker(Context context) {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
            mTracker = analytics.newTracker(Application.isOriginallyQAVersion() ? QA_APP_TRACKING_ID : GLOBAL_TRACKING_ID);
        }
    }

    synchronized public void setAppTracker(Context context) {
        if (appTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
            if (! Application.isOriginallyQAVersion()) {
                if (Application.getClient().getUser() != null) {
                    appTracker = analytics.newTracker(Application.getClient().isAgent() ? AGENT_TRACKING_ID : CUSTOMER_TRACKING_ID);
                }
            }
        }
    }

    public void trackScreen(String screenName) {
        if (screenName != null) {
            if (mTracker != null) {
                mTracker.setScreenName(screenName);
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
            if (appTracker != null) {
                appTracker.setScreenName(screenName);
                appTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        }
    }

    public void trackEvent(String category, String action) {
        if (mTracker != null) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .build());
        }
        if (appTracker != null) {
            appTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .build());
        }

    }

    public void trackEvent(String category, String action, String label) {
        if (mTracker != null) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(label)
                    .build());
        }
        if (appTracker != null) {
            appTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(label)
                    .build());
        }

    }

    public void trackOverFlowNavigtionEvent(String screenName, String navigateTo) {
        trackEvent(screenName, "Overflow Navigation", navigateTo);
    }

    public void trackToolbarNavigtionEvent(String screenName, String navigateTo) {
        trackEvent(screenName, "Toolbar Navigation", navigateTo);
    }

    public void trackSnackbarNavigation(String screenName, String navigateTo) {
        trackEvent(screenName, "SnackBar Navigation", navigateTo);
    }
}
