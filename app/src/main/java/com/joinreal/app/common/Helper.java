package com.joinreal.app.common;

import android.content.Context;
import android.content.Intent;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.StartActivityEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.User;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * Created by britt on 6/1/15.
 */
public class Helper {
    public static String NEW_LINE = System.getProperty("line.separator");

    public static int dpToPxl(Context context, float dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    public static int convertPixelsToDpForWebView(Context context, float px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = (int) (px / (metrics.densityDpi / 160f));
        return dp;
    }

    public static float pxlsToDp(Context context, float pixels) {
        float scale = context.getResources().getDisplayMetrics().density;
        return pixels / scale;
    }

    public static Drawable getDrawable(Context context, int drawableId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(drawableId, null);
        } else {
            return context.getResources().getDrawable(drawableId);
        }
    }

    public static Snackbar getSnackbar(Context context, View parentView, String msg) {
        Snackbar snackbar = Snackbar.make(parentView, msg, Snackbar.LENGTH_LONG);
        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.white));
        textView.setTextSize(Helper.pxlsToDp(context, context.getResources().getDimension(R.dimen.font_size_titles)));
        textView.setTag("typeface:light");
        TypefaceManager.getInstance(context).assignTypeface(textView);
        return snackbar;
    }

    public static Snackbar getSnackbar(Context context, View parentView, String msg, View.OnClickListener listener) {
        Snackbar snackbar = getSnackbar(context, parentView, msg).setAction("VIEW", listener);
        TextView actionTv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_action);
        actionTv.setTextSize(Helper.pxlsToDp(context, context.getResources().getDimension(R.dimen.font_size_titles)));
        actionTv.setTextColor(context.getResources().getColor(R.color.blue));
        actionTv.setTag("typeface:book");
        TypefaceManager.getInstance(context).assignTypeface(actionTv, "book");
        return snackbar;
    }

    public static Snackbar getExceptionSnackbar(Context context, View activityWindow, String eMessage, String bodyJson) {
        User user = Application.getClient().getUser();
        Snackbar snackbar = getSnackbar(context, activityWindow, eMessage).setAction("EMAIL", getEmailActionClickListener(eMessage, user != null ? user.getUsername() : "user = null", user != null ? user.getToken() : "user = null", bodyJson));

        TextView actionTv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_action);
        actionTv.setTextSize(Helper.pxlsToDp(context, context.getResources().getDimension(R.dimen.font_size_titles)));
        actionTv.setTextColor(context.getResources().getColor(R.color.red));
        actionTv.setTag("typeface:book");
        TypefaceManager.getInstance(context).assignTypeface(actionTv);
        return snackbar;
    }

    private static View.OnClickListener getEmailActionClickListener(final String eMessage, final String username, final String token, final String bodyJson) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_SUBJECT, eMessage);
                i.putExtra(Intent.EXTRA_TEXT, buildEmailBody(eMessage, username, token, bodyJson));
                try {
                    Intent.createChooser(i, "Send mail...");
                    BusProvider.getBus().post(new StartActivityEvent(i));
                } catch (android.content.ActivityNotFoundException ex) {
                    handleError("There are no email clients installed.", NetworkCodes.ERROR_CANT_EMAIL);
                }
            }
        };
    }

    private static String buildEmailBody(String eMessage, String username, String token, String bodyJson) {
        return eMessage + NEW_LINE + NEW_LINE +
                "username : " + username + NEW_LINE +
                "token : " + token + NEW_LINE +
                "is agent ? " + ((Application.getClient().getUser() != null) ? Application.getClient().isAgent() : "user is null") + NEW_LINE + NEW_LINE +
                "body json : " + NEW_LINE +
                bodyJson;

    }

    public static String getNumberFormattedWithCommas(int num) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(num);
    }

    public static String getShortPrice(int num) {
        final int MILLION = 1000000;
        final int THOUSAND = 1000;
        if (num >= MILLION) {
            return String.format("%.1f", (float) num / MILLION) + "M";
        } else {
            return (num / THOUSAND) + "K";
        }
    }

    //FOR TESTS
    public static String loadJSONFromAsset(String fileNmae) {
        String json = null;
        try {
            InputStream is = Application.getAppContext().getAssets().open(fileNmae);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }


    public static Drawable getTintedDrawable(int drawableId, int colorResource) {
        Drawable originalDrawable = getDrawable(Application.getAppContext(), drawableId);
        Drawable wrapDrawable = DrawableCompat.wrap(originalDrawable);
        DrawableCompat.setTint(wrapDrawable, colorResource);
        return wrapDrawable;
    }

    public final static boolean isFeatureAvailable(String feature) {
        final PackageManager packageManager = Application.getAppContext().getPackageManager();
        final FeatureInfo[] featuresList = packageManager.getSystemAvailableFeatures();
        for (FeatureInfo f : featuresList) {
            if (f.name != null && f.name.equals(feature)) {
                return true;
            }
        }

        return false;
    }

    public static String getPriceRangeString(Integer priceMin, Integer priceMax) {
        if (priceMin == null) {
            if (priceMax == null) {
                return "ANY PRICE";
            } else {
                return "0-" + getShortPrice(priceMax);
            }
        } else {
            if (priceMax == null) {
                return Helper.getShortPrice(priceMin) + "+ ";
            } else {
                return Helper.getShortPrice(priceMin) + "-" + Helper.getShortPrice(priceMax);
            }
        }
    }

    public static String removeZeroFromFloatIfNeeded(Double number) {
        if (number == Math.floor(number)) {
            return String.valueOf(number.intValue());
        } else {
            return String.valueOf(number);
        }
    }

    public static boolean isMatchingPatterns(Pattern pattern, String field) {
        return (field != null) && (field.length() > 0) && (pattern.matcher(field).matches());
    }

    public static void showNetworkErrorToast(Context context, int errorCode) {
        Toast.makeText(context, context.getResources().getString(R.string.network_error_msg + errorCode), Toast.LENGTH_LONG).show();
    }

    public static int getExceptionCodeForResponse(int responseCode) {
        if (responseCode >= 500) {
            return NetworkCodes.EXCEPTION_INTERNAL;
        } else if (responseCode == 401) {
            return NetworkCodes.EXCEPTION_LOGIN;
        } else if (responseCode >= 400) {
            return NetworkCodes.EXCEPTION_NETWORK;
        } else {
            return NetworkCodes.EXCEPTION_OTHER;
        }
    }

    public static void handleApiException(NetworkException e, int apiCallId) {
            BusProvider.getRestBus().post(new NetworkExceptionEvent(e, apiCallId));
    }

    public static void handleError(String errorMsg, int apiCallId) {
        BusProvider.getRestBus().post(new NetworkExceptionEvent(new NetworkException(errorMsg), apiCallId));
    }
}
