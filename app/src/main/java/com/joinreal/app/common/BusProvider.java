package com.joinreal.app.common;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by britt on 6/13/15.
 */
public final class BusProvider {
    private static final Bus bus = new Bus(ThreadEnforcer.MAIN);
    private static final Bus restBus = new Bus(ThreadEnforcer.ANY);

    public static Bus getBus() {
        return bus;
    }

    public static Bus getRestBus() {
        return restBus;
    }
}
