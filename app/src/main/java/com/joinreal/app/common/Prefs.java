package com.joinreal.app.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.joinreal.app.Application;
import com.joinreal.model.data.Agent;
import com.joinreal.model.data.Contact;
import com.joinreal.model.data.SystemSettings;
import com.joinreal.model.data.chat.ChatDialogDataVM;
import com.joinreal.model.data.chat.ChatDialogInfo;
import com.joinreal.model.data.chat.ChatMessageData;
import com.joinreal.model.data.chat.ChatMessagesArray;
import com.joinreal.model.data.chat.ChatUsersPhonesMap;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.users.model.QBUser;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by britt on 5/27/15.
 */
public class Prefs {
    private final static String KEY_LATEST_UPDATE_VERSION = "latest_update_installation_version_fix";
    private final static String KEY_USER_JSON = "KEY_USER_JSON";
    private final static String KEY_PROPERTY_STRUCTURE_JSON = "KEY_PROPERTY_STRUCTURE_JSON";
    private static final String KEY_CONTACT_LIST_JSON = "KEY_CONTACT_LIST_JSON";
    private static final String KEY_ASSIGNED_AGENT_JSON = "KEY_ASSIGNED_AGENT_JSON";
    private static final String KEY_SYSTEM_SETTINGS_JSON = "KEY_SYSTEM_SETTINGS_JSON";
    private static final String KEY_LAST_SYNC_CONTACTS = "KEY_LAST_SYNC_CONTACTS";
    private static final String KEY_CONTACT_ID_TO_VERSION_MAP = "KEY_CONTACT_ID_TO_VERSION_MAP";


    private static Prefs instance;
    private static SharedPreferences prefs;
    private Context context;
    private Gson gson;
    private String latestUpdateVersion;
    private String userJson;
    private String propertyStructureJson;
    private String contactListJson;
    private String assignedAgentJson;
    private String systemSettingsJson;
    private String lastSyncContactsDate;
    private String contactIdToVersionMapJson;

    public static Prefs getInstance() {
        if (instance == null) {
            instance = new Prefs(Application.getAppContext());
            System.out.println("getInstance in if Application.getAppContext() == null ? " + (Application.getAppContext() == null) + " instance == null ? " + (instance == null));
        }
        return instance;
    }

    private Prefs(Context applicationContext) {
        context = applicationContext.getApplicationContext();
        prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext.getApplicationContext());
        gson = new Gson();
        latestUpdateVersion = prefs.getString(KEY_LATEST_UPDATE_VERSION, null);
        userJson = prefs.getString(KEY_USER_JSON, null);
        propertyStructureJson = prefs.getString(KEY_PROPERTY_STRUCTURE_JSON, null);
        contactListJson = prefs.getString(KEY_CONTACT_LIST_JSON, null);
        assignedAgentJson = prefs.getString(KEY_ASSIGNED_AGENT_JSON, null);
        systemSettingsJson = prefs.getString(KEY_SYSTEM_SETTINGS_JSON, null);
        lastSyncContactsDate = prefs.getString(KEY_LAST_SYNC_CONTACTS, null);
        contactIdToVersionMapJson = prefs.getString(KEY_CONTACT_ID_TO_VERSION_MAP, null);
    }

    public void save() {
        SharedPreferences.Editor edit = prefs.edit();
        if (latestUpdateVersion != null) {
            edit.putString(KEY_LATEST_UPDATE_VERSION, latestUpdateVersion);
        }
        if (userJson != null) {
            edit.putString(KEY_USER_JSON, userJson);
        }
        if (propertyStructureJson != null) {
            edit.putString(KEY_PROPERTY_STRUCTURE_JSON, propertyStructureJson);
        }
        if (contactListJson != null) {
            edit.putString(KEY_CONTACT_LIST_JSON, contactListJson);
        }
        if (assignedAgentJson != null) {
            edit.putString(KEY_ASSIGNED_AGENT_JSON, assignedAgentJson);
        }
        if (systemSettingsJson != null) {
            edit.putString(KEY_SYSTEM_SETTINGS_JSON, systemSettingsJson);
        }
        edit.putString(KEY_LAST_SYNC_CONTACTS, lastSyncContactsDate);
        edit.apply();
    }

    public String getLatestUpdateVersion() {
        return latestUpdateVersion;
    }

    public void setLatestUpdateVersion(String latestUpdateVersion) {
        this.latestUpdateVersion = latestUpdateVersion;
    }

    public String getUserJson() {
        return userJson;
    }

    public void setUserJson(String userJson) {
        this.userJson = userJson;
    }

    public String getPropertyStructureJson() {
        return propertyStructureJson;
    }

    public void setPropertyStructureJson(String propertyStructureJson) {
        this.propertyStructureJson = propertyStructureJson;
    }

    public List<Contact> getContactList() {
        Log.i("CONTACTS_TAG", " getContactList");
        if (contactListJson == null) {
            return new ArrayList<Contact>();
        } else {
            return gson.fromJson(contactListJson, new TypeToken<ArrayList<Contact>>() {
            }.getType());
        }
    }

    public void setContactList(List<Contact> contactList) {
        System.out.println("prefs: setContactList : " + ((contactList == null) ? 0 : contactList.size()));
        Log.i("CONTACTS_TAG", "prefs: setContactList : " + ((contactList == null) ? 0 : contactList.size()));
        this.contactListJson = new Gson().toJson(contactList);

        prefs.edit().putString(KEY_CONTACT_LIST_JSON, contactListJson).apply();
    }

    public Map<String, Integer> getContactIdToVersionMap() {
        if (contactIdToVersionMapJson == null) {
            return new HashMap<String, Integer>();
        } else {
            return gson.fromJson(contactIdToVersionMapJson, new TypeToken<HashMap<String, Integer>>() {
            }.getType());
        }
    }

    public void setContactIdToVersionMap(Map<String, Integer> contactIdToVersionMap) {
        this.contactIdToVersionMapJson = new Gson().toJson(contactIdToVersionMap);
        prefs.edit().putString(KEY_CONTACT_ID_TO_VERSION_MAP, contactIdToVersionMapJson).apply();
    }

    public Agent getAssaignedAgent() {
        return gson.fromJson(assignedAgentJson, Agent.class);
    }

    public void setAssaignedAgent(Agent agent) {
        assignedAgentJson = gson.toJson(agent);
    }

    public void setSystemSettings(SystemSettings systemSettings) {
        systemSettingsJson = gson.toJson(systemSettings);
    }

    /**************
     * FORM CHAT
     ****************/

    private static final String NAME = "prefs";
    private static final String REGID = "regid";
    private static final String COUNTRY_CODE = "country_code";
    private static final String IS_SUBSCRIBED = "is_subscribed";
    private static final String USER_PHONE = "user_phone";
    private static final String USER_REGISTERED = "registered";
    private static final String DIALOG_DATA = "dialogData";
    private static final String USERS_PHONES = "usersPhones";
    private static final String CURRENT_USER = "current_user";
    private static final String DIALOG_INFO = "dialogInfo";
    private static final String USER_FIRST_CREATED = "user_created";

    public static String getPushRegid() {
        return prefs.getString(REGID, "");
    }

    public static void saveRegid(String regid) {
        prefs.edit().putString(REGID, regid).commit();
    }

    public static String loadCountryCode() {
        return prefs.getString(COUNTRY_CODE, "972");
    }

    public static void saveCountryCode(String code) {
        prefs.edit().putString(COUNTRY_CODE, code).commit();
    }

    public static boolean isSubscribed() {
        return prefs.getBoolean(IS_SUBSCRIBED, false);
    }

    public static void setIsSubscribed() {
        prefs.edit().putBoolean(IS_SUBSCRIBED, true).commit();
    }

    public static boolean isUserRegistered() {
        return prefs.getBoolean(USER_REGISTERED, false);
    }

    public static void setUserIsRegistered(boolean registered) {
        prefs.edit().putBoolean(USER_REGISTERED, registered).commit();
    }

    public static String getUserPhone() {
        return prefs.getString(USER_PHONE, "");
    }

    public static void setUserPhone(String userPhone) {
        prefs.edit().putString(USER_PHONE, userPhone).commit();
    }

    public static void saveMessages(String id, ChatMessagesArray array) {
        prefs.edit().putString(id, new Gson().toJson(array)).commit();
    }

    public static void saveMessages(String id, List<ChatMessageData> array) {
        saveMessages(id, new ChatMessagesArray(array));
    }

    public static ChatMessagesArray getMessages(String id) {
        String data = prefs.getString(id, "");
        if (!data.equals("")) {
            return new Gson().fromJson(data, ChatMessagesArray.class);
        } else return null;
    }

    public static List<ChatDialogDataVM> getDialogs() {
        String data = prefs.getString(DIALOG_DATA, "");
        if (!data.equals("")) {
            Type type = new TypeToken<ArrayList<ChatDialogDataVM>>() {
            }.getType();
            try {
                ArrayList<ChatDialogDataVM> result = new Gson().fromJson(data, type);
                return result;
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    public static void saveDialogs(List<ChatDialogDataVM> chatDialogDataVMList) {
        String data = new Gson().toJson(chatDialogDataVMList);
        prefs.edit().putString(DIALOG_DATA, data).commit();
    }

    public static void saveDialogInfo(String id, ChatDialogInfo chatDialogInfo) {
        ChatDialogInfo saved = getDialogInfo(id);
        saved = ChatDialogInfo.merge(saved, chatDialogInfo);
        prefs.edit().putString(DIALOG_INFO + id, new Gson().toJson(saved)).commit();
    }

    public static void saveDialogInfo(String id, boolean dialogInfo) {
        saveDialogInfo(id, new ChatDialogInfo(dialogInfo));
    }

    public static ChatDialogInfo getDialogInfo(String id) {
        String data = prefs.getString(DIALOG_INFO + id, null);
        if (data != null) {
            return new Gson().fromJson(data, ChatDialogInfo.class);
        } else {
            return null;
        }
    }

    public static void saveCurrentUser(QBUser currentUser) {
        if (currentUser != null)
            prefs.edit().putString(CURRENT_USER, new Gson().toJson(currentUser)).commit();
    }

    public static QBUser getCurrentUser() {
        String user = prefs.getString(CURRENT_USER, null);
        if (user != null) {
            return new Gson().fromJson(user, QBUser.class);
        }
        return null;
    }

    public static ChatUsersPhonesMap getUsersPhones() {
        String data = prefs.getString(USERS_PHONES, null);
        if (data != null) {
            return new Gson().fromJson(prefs.getString(USERS_PHONES, ""), ChatUsersPhonesMap.class);
        } else {
            return null;
        }
    }

    public static void saveUsersPhones(ChatUsersPhonesMap map) {
        prefs.edit().putString(USERS_PHONES, new Gson().toJson(map)).commit();
    }

    public static void userCreated() {
        prefs.edit().putBoolean(USER_FIRST_CREATED, true).commit();
    }


    public String getLastSyncContactsDate() {
        return lastSyncContactsDate;
    }

    public void setLastSyncContactsDate(String lastSyncContactsDate) {
        this.lastSyncContactsDate = lastSyncContactsDate;
    }

}
