package com.joinreal.app.common;

/**
 * Created by brittbarak on 7/22/15.
 */
public class NetworkCodes {
    public static final int NO_PROPERTY_STRUCTURE = 1;
    public static final int SEND_MESSAGE_XMPPException = 2;
    public static final int SEND_MESSAGE_NotConnectedException = 3;
    public static final int SEND_MESSAGE_NO_CHAT_OBJECT = 4;
    public static final int CANT_GET_USER = 5;
    public static final int CANT_GET_FAVORITES = 6;
    public static final int CANT_GET_PROPERTIES = 7;
    public static final int CANT_GET_PROPERTY_METADATA = 8;
    public static final int CANT_REFRESH_USER_DETAILS = 9;
    public static final int CANT_UPDATE_AGENT_DETAILS = 10;
    public static final int ERROR_SENDOING_CONTACTS_TO_SERVER = 11;
    public static final int ERROR_SIGN_IN = 12;
    public static final int ERROR_CHAT_CONNECT = 13;
    public static final int ERROR_GET_CONTACT_LAST_SEARCH = 14;
    public static final int ERROR_GOOGLE_PLACES = 15;
    public static final int CANT_GET_CONTACT_FAVORITES = 16;
    public static final int CANT_UPDATE_CUSTOMER_DETAILS = 17;
    public static final int ERROR_GETTING_CONTACTS_FROM_SERVER = 18;
    public static final int CANT_GET_CONTACT_SEARCH = 19;
    public static final int ERROR_SEND_INVITE = 20;
    public static final int CANT_UPDATE_FAVORITES = 21;
    public static final int NO_SUCH_USER_ON_SIGNIN = 22;
    public static final int ERROR_INIT_DIALOGS = 23;
    public static final int ERROR_CANT_EMAIL = 24;

    public static final int TASK_ASSIAGNED_AGENT = 45;
    public static final int TASK_SYSTEM_SETTINGS = 46;

    public static final int EXCEPTION_INTERNAL = 105;
    public static final int EXCEPTION_LOGIN = 106;
    public static final int EXCEPTION_NETWORK = 107;
    public static final int EXCEPTION_OTHER = 108;
    public static final int EXCEPTION_RESPONSE_TO_STRING = 109;
    public static final int EXCEPTION_JSON_SYNTAX = 110;
    public static final int EXCEPTION_JSON = 111;
}
