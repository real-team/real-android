package com.joinreal.app.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.api.RealClient;

import java.util.HashMap;
import java.util.Map;

/**
 * A singleton that loads and assigns typefaces to views.
 * Created by britt on 5/31/15.
 */
public class TypefaceManager {
    private static final String DEFAULT_TYPEFACE = "light";
    private static String TAG = "TypefaceManager";

    private static TypefaceManager instance = null;
    private static Map<String, String> typefaceFileByTag;
    private Map<String, Typeface> typefaceByTag = new HashMap<String, Typeface>();
    private Context context;

    public static synchronized TypefaceManager getInstance(Context context) {
        if (instance == null) {
            instance = new TypefaceManager(context.getApplicationContext());
            typefaceFileByTag = new HashMap<>();
            typefaceFileByTag.put("bold", "GothamNarrow-Bold.otf");
            typefaceFileByTag.put("bold_italic", "GothamNarrow-BoldItalic.otf");
            typefaceFileByTag.put("book", "GothamNarrow-Book.otf");
            typefaceFileByTag.put("book_italic", "GothamNarrow-BookItalic.otf");
            typefaceFileByTag.put("light", "GothamNarrow-Light.otf");
            typefaceFileByTag.put("light_italic", "GothamNarrow-LightItalic.otf");
            typefaceFileByTag.put("medium", "GothamNarrow-Medium.otf");
            typefaceFileByTag.put("medium_italic", "GothamNarrow-MediumItalic.otf");
        }
        return instance;
    }

    private TypefaceManager(Context applicationContext) {
        this.context = applicationContext;
    }

    /**
     * Loads and caches a typeface.
     *
     * @param tagName The tag identifying the typeface.
     * @return
     */
    public Typeface getTypeface(String tagName) {
        Typeface typeface = typefaceByTag.get(tagName);
        if (typeface == null) {
            String fileName = typefaceFileByTag.get(tagName);
            if (fileName == null) {
                Log.e(TAG, "No typeface name for tag [" + tagName + "]");
                typeface = Typeface.DEFAULT;
            } else {
                typeface = Typeface.createFromAsset(context.getAssets(), "typeface/" + fileName);
            }
            typefaceByTag.put(tagName, typeface);
        }
        return typeface;
    }

    /**
     * Assigns a typeface to all views under the root view according to the tag name that the view has.
     *
     * @param rootView
     */

    public void assignTypeface(View rootView, String typefaceName) {
        if (rootView instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) rootView;
            for (int i = 0; i < group.getChildCount(); i++) {
                assignTypeface(group.getChildAt(i));
            }
        } else if ((rootView != null) && (rootView instanceof TextView)){
            if (typefaceName != null) {
                ((TextView) rootView).setTypeface(getTypeface(typefaceName));
            } else {
                getTypefaceFromTag(rootView);
            }
        }
    }

    private void getTypefaceFromTag(View rootView) {
        String tags = (String) rootView.getTag();
        if (tags != null) {
            int posStart = tags.indexOf("typeface:");
            if (posStart >= 0) {
                String tag = tags.substring(posStart + 9);
                int posEnd = tag.indexOf(',');
                if (posEnd >= 0) {
                    tag = tag.substring(0, posEnd);
                }
                if (tag.length() > 0) {
                    if (rootView instanceof TextView) {
                        ((TextView) rootView).setTypeface(getTypeface(tag));
                    }
                }
            }

        } else {
            ((TextView) rootView).setTypeface(getTypeface(DEFAULT_TYPEFACE));
        }
    }

    public void assignTypeface(View rootView) {
        if (rootView instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) rootView;
            for (int i = 0; i < group.getChildCount(); i++) {
                assignTypeface(group.getChildAt(i));
            }
        } else if ((rootView != null) && (rootView instanceof TextView)){
            getTypefaceFromTag(rootView);
        }
    }

    public void assignTypeface(Activity activity) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        assignTypeface(rootView);
    }

    public void applyCustomTypefaceForNavigationMenu(Menu menu) {
        for (int i=0;i<menu.size();i++) {
            MenuItem mi = menu.getItem(i);

            //for applying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuSubItem(subMenuItem);
                }
            }
            applyFontToMenuTitleItem(mi);
        }
    }

    private void applyFontToMenuSubItem(MenuItem mi) {
        applyFontToMenuItem(mi, R.dimen.font_size_medium);
    }

    private void applyFontToMenuTitleItem(MenuItem mi) {
        applyFontToMenuItem(mi, R.dimen.font_size_titles);
    }

    public void applyFontToMenuItem(MenuItem mi, int fontSizeId) {
        if ((mi != null) && (mi.getTitle() != null)) {
            Typeface font = getTypeface("book");
            SpannableString mNewTitle = new SpannableString(mi.getTitle());
            mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            mNewTitle.setSpan(new AbsoluteSizeSpan((int) Application.getAppContext().getResources().getDimension(fontSizeId)), 0, mNewTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mi.setTitle(mNewTitle);
        }
    }

    public SpannableString getToolbarTitle(String title) {
        SpannableString titleSpannable = new SpannableString(title);
        titleSpannable.setSpan(new CustomTypefaceSpan("", getTypeface("light")), 0, titleSpannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        titleSpannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.white)), 0, titleSpannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return titleSpannable;
    }

}
