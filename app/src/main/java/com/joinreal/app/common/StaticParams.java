package com.joinreal.app.common;

import com.joinreal.app.Application;

/**
 * Created by britt on 6/1/15.
 */
public class StaticParams {
    public static final String KEY_PHONE_NUM = "KEY_PHONE_NUMBER";
    public static final String KEY_FILTERS_JSON = "KEY_FILTERS_JSON";
    public static final String KEY_MLS_NUM = "mlsNum";
    public static final String KEY_MLS_ID = "mlsId";
    public static final String KEY_PROPERTY_DISPLAY_IMAGE_URL = "fileUrl";
    public static final String KEY_PROPERTY_LIST = "KEY_PROPERTY_LIST";
    public static final String KEY_LATITUDE = "KEY_LATITUDE";
    public static final String KEY_LONGITUDE = "KEY_LONGITUDE";
    public static final String KEY_CONTACT_VM = "KEY_CONTACT_VM";
    public static final String KEY_CONTACT_NOTES = "KEY_CONTACT_NOTES";
    public static final String KEY_CONTACT_ID = "KEY_CONTACT_ID";
    public static final String KEY_CONTACT_NAME = "KEY_CONTACT_NAME";
    public static final String KEY_SHOULD_LOCK_DRAWER = "KEY_SHOULD_LOCK_DRAWER";
    public static final String KEY_IS_FAVORITES_LIST = "KEY_IS_FAVORITES_LIST";
    public static final String KEY_SEARCH_FILTER = "KEY_SEARCH_FILTER";
    public static final String KEY_TITLE_PROPERTY_LIST = "KEY_TITLE_PROPERTY_LIST";
    public static final String KEY_STATE = "KEY_STATE";
    public static final String KEY_SHOULD_RESET_FILTER = "KEY_SHOULD_RESET_FILTER";
    public static final String KEY_ACTION_TYPE = "KEY_ACTION_TYPE";
    public static final String KEY_PROPERTY_FULL = "KEY_PROPERTY_FULL";
    public static final String KEY_PROPERTY_JSON = "KEY_PROPERTY_JSON";
    public static final String KEY_CONTACT_LOGIN = "KEY_CONTACT_LOGIN";
    public static final String KEY_OPEN_NO_RESULTS_PAGE = "KEY_OPEN_NO_RESULTS_PAGE";
    public static final String KEY_SHOULD_ANIMATE = "KEY_SHOULD_ANIMATE";
    public static final String KEY_LOCATION_STR = "KEY_LOCATION_STR";
    public static final String KEY_PROPERTY_FOR_CHAT_OBJ = "KEY_PROPERTY_FOR_CHAT_OBJ";

    public static final int OPEN_PROPERTY_LIST = 34531;
    public static final int OPEN_FAVORITES = 34532;
    public static final int SEND_PROPERTY_VIA_CHAT = 34533;
    public static final int OPEN_PROPERTY_PAGE = 34534;
    public static final int OPEN_AGENT_PROFILE = 34535;
    public static final int OPEN_CONTACT_PROFILE = 34536;
    public static final int SEND_INVITE = 34537;


    public static final int REQUEST_CAMERA = 1890;
    public static final int REQUEST_SELECT_FILE = 1891;
    public static final int REQUEST_PICK_IMAGE = 1892;


    public static final String PHONE = "phone";
    public static final String COUNTRY_CODE = "country_code";
    public static final String VERIFICATION_CODE = "verification_code";
    public static final String EXISTING_USER = "existing_user";
    public static final int VERIFICATION_NEXT = 1;
    public static final String IS_AGENT = "is_agent";

    public static final String DEFAULT_EMAIL = "Hi!" + Helper.NEW_LINE + Helper.NEW_LINE +
            "I want to invite you to my app." + Helper.NEW_LINE + Helper.NEW_LINE +
            "The app allows us to communicate seamlessly on all things we need to do to take care of your real estate needs."
            + Helper.NEW_LINE + Helper.NEW_LINE + Application.getClient().getUser().getAgent().getFirstName();

    public static final String KEY_PHOTO_LIST = "KEY_PHOTO_LIST";


    public static final String LOCATION_OBJ_JSON = "LOCATION_OBJ_JSON";
    public static final String ENTRY_TAG_TO_RETURN_TO = "ENTRY_TAG_TO_RETURN_TO";
    public static final String KEY_LOCATION_DESC = "KEY_LOCATION_DESC";
    public static final String KEY_CONTACT_PHONE = "KEY_CONTACT_PHONE";
    public static final String KEY_INTENT_SOURCE = "KEY_INTENT_SOURCE";
    public static final String KEY_DIALOG_ID = "KEY_DIALOG_ID";
    public static final String SOURCE_PN_CHAT = "SOURCE_PN_CHAT";
    public static final String KEY_PROFILE_IMAGE = "KEY_PROFILE_IMAGE";


    public static final String PN_ACTION_SYNC_CONTACTS = "syncContacts";
}
