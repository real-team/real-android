package com.joinreal.app.viewholders;

import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.joinreal.R;
import com.joinreal.app.adapters.SearchResultsAdapter;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.app.interfaces.PropertySummaryLayoutSetter;
import com.joinreal.model.view.PropertyViaChatVM;

import java.util.Scanner;

/**
 * Created by britt on 6/15/15.
 */
public class SearchResultsViewHolderCustomer extends SearchResultsViewHolder {
    protected FloatingActionButton heartFab;

    public SearchResultsViewHolderCustomer(View itemView, SearchResultsAdapter.UpdateFavoritesListener listener, View parentView, OpenChatListener openChatListener, String screenName) {
        super(itemView, listener, parentView, openChatListener, screenName);
    }

    @Override
    protected void initViews(View itemView) {
        chatFabColor = itemView.getContext().getResources().getColor(R.color.blue_transparent);
        heartFab = (FloatingActionButton)itemView.findViewById(R.id.fab_heart);
        heartFab.setVisibility(View.VISIBLE);
        super.initViews(itemView);

    }

    @Override
    public void onBindView(PropertySummaryLayoutSetter property) {
        super.onBindView(property);
        heartFab.setImageResource(getFavoriteIconId());
        heartFab.setOnClickListener(getHeartFabClickListener());
    }

    private View.OnClickListener getHeartFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(property.isFavorite()){
                    updateFavoritesListener.removeFromFavorites(property.getMlsId(), property.getMlsNum(), parentView, screenName);
                    Analytics.getInstance().trackEvent(screenName, "Remove from Favorites", "mls num : " + property.getMlsNum() + ", mlsId : " + property.getMlsId());

                } else {
                    updateFavoritesListener.addToFavorites(property.getMlsId(), property.getMlsNum(), parentView, screenName);
                    Analytics.getInstance().trackEvent(screenName, "Add to Favorites", "mls num : " + property.getMlsNum() + ", mlsId : " + property.getMlsId());

                }
                property.setFavorite(! property.isFavorite());
                heartFab.setImageResource(getFavoriteIconId());
            }
        };
    }

    private int getFavoriteIconId() {
        if(property.isFavorite()) {
            return R.drawable.heart_fill;
        } else {
            return R.drawable.heart;
        }
    }

}
