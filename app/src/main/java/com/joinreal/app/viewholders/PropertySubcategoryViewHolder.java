package com.joinreal.app.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.PropertySubcategoryVM;
import com.joinreal.model.view.ViewModelItem;

/**
 * Created by brittbarak on 6/30/15.
 */
public class PropertySubcategoryViewHolder extends ViewHolderAbstract {
    private TextView text;

    public static ViewHolderAbstract getNewViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_property_subcategory, parent, false);
        return new PropertySubcategoryViewHolder(view);
    }


    public PropertySubcategoryViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void findViewsByIds(View itemView) {
        text = (TextView)itemView.findViewById(R.id.text);
    }

    @Override
    public void onBindView(ViewModelItem viewModelItem) {
        text.setText((((PropertySubcategoryVM) viewModelItem).getSubcategoryName()).toUpperCase());
    }

}
