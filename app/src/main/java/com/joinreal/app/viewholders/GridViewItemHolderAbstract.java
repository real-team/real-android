package com.joinreal.app.viewholders;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.RecyclerFilterItemVM;
import com.joinreal.model.view.ViewModelItem;

/**
 * Created by britt on 6/2/15.
 */
public class GridViewItemHolderAbstract extends ViewHolderAbstract implements View.OnClickListener {

    private final Context context;
    public ImageView icon;
    public TextView title;
    private RecyclerFilterItemVM item;

    public GridViewItemHolderAbstract(Context context, View itemView) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
    }

    @Override
    protected void findViewsByIds(View itemView) {
        icon = (ImageView)itemView.findViewById(R.id.icon);
        title = (TextView)itemView.findViewById(R.id.title);
    }

    @Override
    public void onBindView(ViewModelItem viewModelItem) {
        item = (RecyclerFilterItemVM)viewModelItem;
        title.setText(item.getTitle());
        setColorsPerSelectionState(item);
    }

    @Override
    public void onClick(View v) {
        item.setSelected(! item.isSelected());
        setColorsPerSelectionState(item);
    }

    private void setColorsPerSelectionState(RecyclerFilterItemVM item) {
        Resources res = context.getResources();
        int drawableId;
        if (item.isSelected()) {
            title.setTextColor(res.getColor(item.getColorIdSelected()));
            drawableId = (R.drawable.circle_blue_dark);
            drawableId = item.getDrawableIdSelected();
        } else {
            title.setTextColor(res.getColor(item.getColorIdNotSelected()));
            drawableId = (R.drawable.circle_gray_light);
            drawableId = item.getDrawableIdNotSelected();
        }

        Drawable drawable;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable = context.getResources().getDrawable(drawableId, null);
        } else {
            drawable = context.getResources().getDrawable(drawableId);
        }
        icon.setBackground(drawable);
    }
}