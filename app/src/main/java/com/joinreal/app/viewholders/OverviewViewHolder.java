package com.joinreal.app.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.OverviewVM;
import com.joinreal.model.view.ViewModelItem;

/**
 * Created by brittbarak on 6/30/15.
 */
public class OverviewViewHolder extends ViewHolderAbstract{

    private TextView text;

    public static ViewHolderAbstract getNewViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_property_overview, parent, false);
        return new OverviewViewHolder(view);
    }

    public OverviewViewHolder(View itemView) {
        super(itemView);
    }
    @Override
    protected void findViewsByIds(View itemView) {
        text = (TextView)itemView.findViewById(R.id.text);
    }

    @Override
    public void onBindView(ViewModelItem viewModelItem) {
        text.setText(((OverviewVM) viewModelItem).getOverviewStr());
    }


}
