package com.joinreal.app.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.BasicInfoItemVM;
import com.joinreal.model.view.FiltersViewModelItem;
import com.joinreal.model.view.ViewModelItem;

/**
 * Created by brittbarak on 6/29/15.
 */
public class BasicInfoViewHolder extends ViewHolderAbstract {

    TextView title;
    TextView value;

    public static ViewHolderAbstract getNewViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_property_basic_info, parent, false);
        return new BasicInfoViewHolder(view);
    }

    public BasicInfoViewHolder(View itemView) {
        super(itemView);
    }


    @Override
    protected void findViewsByIds(View itemView) {
        title = (TextView) itemView.findViewById(R.id.title);
        value = (TextView) itemView.findViewById(R.id.value);
    }

    @Override
    public void onBindView(ViewModelItem viewModelItem) {
        title.setText(((BasicInfoItemVM)viewModelItem).getTitle());
        value.setText(((BasicInfoItemVM)viewModelItem).getValue());
    }

}
