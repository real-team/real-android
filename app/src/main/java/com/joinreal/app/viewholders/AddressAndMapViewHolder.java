package com.joinreal.app.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.data.PropertyFull;
import com.joinreal.model.view.AddressAndMapVM;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.views.ExpandableFilterLayout;
import com.squareup.picasso.Picasso;

/**
 * Created by brittbarak on 6/30/15.
 */
public class AddressAndMapViewHolder extends ViewHolderAbstract {

    private TextView addressLine1;
    private TextView addressLine2;
    private ImageView mapImage;
    private TextView subdivision;
    private TextView area;
    private TextView location;
    private TextView county;
    private TextView marketArea;
    private TextView sectionNumber;

    public static ViewHolderAbstract getNewViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_property_expandable_address, parent, false);
        return new AddressAndMapViewHolder(view);
    }

    public AddressAndMapViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void findViewsByIds(View itemView) {
    addressLine1 = (TextView) itemView.findViewById(R.id.address_line1);
    addressLine2 = (TextView) itemView.findViewById(R.id.address_line2);
    mapImage = (ImageView) itemView.findViewById(R.id.static_map);
        subdivision = (TextView) itemView.findViewById(R.id.subdivision);
        area = (TextView) itemView.findViewById(R.id.area);
        location = (TextView) itemView.findViewById(R.id.location);
        county = (TextView) itemView.findViewById(R.id.county);
        marketArea = (TextView) itemView.findViewById(R.id.market_area);
        sectionNumber = (TextView) itemView.findViewById(R.id.section_number);
    }

    @Override
    public void onBindView(ViewModelItem viewModelItem) {
        AddressAndMapVM viewModel =(AddressAndMapVM)viewModelItem;
        addressLine1.setText(viewModel.getAddressLine1());
        addressLine2.setText(viewModel.getAddressLine2());
        Picasso.with(itemView.getContext()).load(viewModel.getMapUrl()).into(mapImage);
        setNoteTextView(subdivision, "Subdivision: " + viewModel.getSubdivision());
        setNoteTextView(area, "Area: " + viewModel.getArea());
        setNoteTextView(location, "Location: " + viewModel.getLocation());
        setNoteTextView(county, "County: " + viewModel.getCounty());
        setNoteTextView(marketArea, "Market Area: " + viewModel.getMarketArea());
        setNoteTextView(sectionNumber, "Section Number " + viewModel.getSectionNumber());

    }

    private void setNoteTextView(TextView textView, String text) {
        if (text == null) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
        }
    }
}
