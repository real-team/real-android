package com.joinreal.app.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.adapters.FiltersAdapter;
import com.joinreal.app.adapters.PropertyFeaturesAdapter;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.AdditionalFeaturesItemVM;
import com.joinreal.model.view.BasicInfoItemVM;
import com.joinreal.model.view.ExpandableItemFiltersVM;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.views.ExpandableFilterLayout;

/**
 * Created by brittbarak on 6/29/15.
 */
public class AdditionalFeatureViewHolder extends ViewHolderAbstract {

    ExpandableFilterLayout expandableItem;

    public static ViewHolderAbstract getNewViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filters_expandable, parent, false);
        return new AdditionalFeatureViewHolder(view);
    }

    public AdditionalFeatureViewHolder(View itemView) {
        super(itemView);
    }


    @Override
    protected void findViewsByIds(View itemView) {
        expandableItem = (ExpandableFilterLayout) itemView.findViewById(R.id.expandlable_filter);
    }

    @Override
    public void onBindView(ViewModelItem viewModelItem) {
        AdditionalFeaturesItemVM viewModel = (AdditionalFeaturesItemVM) viewModelItem;
        expandableItem.setTitle(viewModel.getCategoryName().toUpperCase());
        expandableItem.setInnerItems(new PropertyFeaturesAdapter(itemView.getContext(), viewModel.getValues()));
    }

}
