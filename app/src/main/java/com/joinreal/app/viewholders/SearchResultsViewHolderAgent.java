package com.joinreal.app.viewholders;

import android.view.View;

import com.joinreal.R;
import com.joinreal.app.adapters.SearchResultsAdapter;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.model.view.PropertyViaChatVM;

/**
 * Created by britt on 6/15/15.
 */
public class SearchResultsViewHolderAgent extends SearchResultsViewHolder {

    public SearchResultsViewHolderAgent(View itemView, SearchResultsAdapter.UpdateFavoritesListener listener, View parentView, OpenChatListener openChatListener, String screenName) {
        super(itemView, listener, parentView, openChatListener, screenName);
    }

    @Override
    protected void initViews(View itemView) {
        chatFabColor = itemView.getContext().getResources().getColor(R.color.pink_transparent);
        super.initViews(itemView);
    }
}
