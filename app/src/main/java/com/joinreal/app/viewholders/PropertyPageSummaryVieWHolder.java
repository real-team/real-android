package com.joinreal.app.viewholders;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.adapters.ImagesPagerAdapter;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.Helper;
import com.joinreal.app.interfaces.PropertySummaryLayoutSetter;
import com.joinreal.app.interfaces.UpdateFavoritesHelper;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.data.PropertyFull;
import com.joinreal.model.view.PropertySummaryInfoVM;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.views.ParallaxTransformer;

import java.util.ArrayList;

/**
 * Created by brittbarak on 7/1/15.
 */
public class PropertyPageSummaryViewHolder extends ViewHolderAbstract {

    private static String screenName;
    private static int maxViewedImagesPosition = 0;
    private static int imageViews = 1;
    PropertySummaryLayoutSetter property;

    protected TextView addressTv;
    protected TextView priceTv;
    protected TextView bedsTv;
    protected TextView bathsTv;


    public static ViewHolderAbstract getNewViewHolder(ViewGroup parent, String screenName) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_property_page_sum_details, parent, false);
        return new PropertyPageSummaryViewHolder(view, screenName);
    }

    //TODO: refactor with search results view holders
    public PropertyPageSummaryViewHolder(View itemView, String screenName) {
        super(itemView);
        this.screenName = screenName;
    }

    public static void setImagesViewPager(ArrayList<String> photos, Context context, ViewPager pager) {
        pager.setAdapter(new ImagesPagerAdapter(context, photos));
        pager.setPageTransformer(true, new ParallaxTransformer(R.id.imageview_for_pager));
        pager.setOffscreenPageLimit(1);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                maxViewedImagesPosition = Math.max(maxViewedImagesPosition, position);
                imageViews++;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    protected void findViewsByIds(View itemView) {
        addressTv = (TextView) itemView.findViewById(R.id.address);
        priceTv = (TextView) itemView.findViewById(R.id.price);
        bedsTv = (TextView) itemView.findViewById(R.id.beds_num);
        bathsTv = (TextView) itemView.findViewById(R.id.baths_num);
    }

    @Override
    public void onBindView(ViewModelItem viewModelItem) {
        this.property = ((PropertySummaryInfoVM) viewModelItem).getProperty();
        setFieldsContent();
    }

    private void setFieldsContent() {
        addressTv.setText(property.getFullAddress());
        priceTv.setText(property.getFormattedPrice());

        String bedsStr = null;
        if (property.getBeds() != null) {
            bedsStr = String.valueOf(Helper.removeZeroFromFloatIfNeeded(property.getBeds()));
        }
        setFeatureText(bedsStr, bedsTv, "BEDS");

        String bathsStr = null;
        if (property.getBaths() != null) {
            bathsStr = String.valueOf(Helper.removeZeroFromFloatIfNeeded(property.getBaths()));
        }
        setFeatureText(bathsStr, bathsTv, "BATHS");
    }

    private void setFeatureText(String value, TextView textView, String title) {
        if ((value == null) || value.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(value + "  " + title);
        }
    }

    public static void setChatFab(final PropertyFull property, final View.OnClickListener chatClickListener, FloatingActionButton chatFab) {
        if (Application.getClient().isAgent()) {
            int chatFabColor = Application.getAppContext().getResources().getColor(R.color.pink_transparent);
            chatFab.setBackgroundTintList(ColorStateList.valueOf(chatFabColor));
            chatFab.setOnClickListener(chatClickListener);

        } else {
            int chatFabColor = Application.getAppContext().getResources().getColor(R.color.blue_transparent);
            chatFab.setBackgroundTintList(ColorStateList.valueOf(chatFabColor));
            chatFab.setOnClickListener(chatClickListener);

        }
        chatFab.setVisibility(View.VISIBLE);
    }

    public static void setFavoriteFab(PropertyFull propertyFull, UpdateFavoritesHelper updateFavoritesHelper, FloatingActionButton heartFab, View parentView) {
        if (Application.getClient().isAgent()) {
            heartFab.setVisibility(View.GONE);
        } else {
            makeFabVisible(heartFab);
            heartFab.setImageResource(getFavoriteIconId((propertyFull != null) && propertyFull.isFavorite()));
            heartFab.setOnClickListener(getHeartFabClickListener(propertyFull, heartFab, updateFavoritesHelper, parentView));
        }
    }

    private static void makeFabVisible(FloatingActionButton heartFab) {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) heartFab.getLayoutParams();
        params.setAnchorId(R.id.appbar);
        heartFab.setLayoutParams(params);
        heartFab.setVisibility(View.VISIBLE);

    }

    private static int getFavoriteIconId(boolean isFavorite) {
        if (isFavorite) {
            return R.drawable.heart_fill;
        } else {
            return R.drawable.heart;
        }
    }

    private static View.OnClickListener getHeartFabClickListener(final PropertyFull property, final FloatingActionButton heartFab, final UpdateFavoritesHelper listener, final View parentView) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (property.isFavorite()) {
                    listener.removeFromFavorites(property.getMlsId(), property.getMlsNum(), parentView, screenName);
                } else {
                    listener.addToFavorites(property.getMlsId(), property.getMlsNum(), parentView, screenName);
                }
                property.setFavorite(!property.isFavorite());
                heartFab.setImageResource(getFavoriteIconId(property.isFavorite()));
            }
        };
    }

}
