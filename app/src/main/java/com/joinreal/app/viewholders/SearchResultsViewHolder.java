package com.joinreal.app.viewholders;

import android.content.res.ColorStateList;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.adapters.ImagesPagerAdapter;
import com.joinreal.app.adapters.SearchResultsAdapter;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.app.interfaces.PropertySummaryLayoutSetter;
import com.joinreal.events.OpenPropertyEvent;
import com.joinreal.model.view.PropertyViaChatVM;
import com.joinreal.views.ParallaxTransformer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/15/15.
 */
public abstract class SearchResultsViewHolder extends RecyclerView.ViewHolder {

    protected ViewPager pager;
    protected TextView addressTv;
    protected TextView priceTv;
    protected TextView bedsTv;
    protected TextView bathsTv;
    protected View contentLayout;

    protected FloatingActionButton chatFab;

    PropertySummaryLayoutSetter property;

    protected int chatFabColor;

    protected SearchResultsAdapter.UpdateFavoritesListener updateFavoritesListener;
    protected OpenChatListener openChatListener;

    protected View parentView;
    protected final String screenName;


    public SearchResultsViewHolder(View itemView, SearchResultsAdapter.UpdateFavoritesListener updateFavoritesListener, View parentView, OpenChatListener openChatListener, String screenName) {
        super(itemView);
        this.updateFavoritesListener = updateFavoritesListener;
        this.openChatListener = openChatListener;
        this.parentView = parentView;
        this.screenName = screenName;
        initViews(itemView);
        TypefaceManager.getInstance(itemView.getContext()).assignTypeface(itemView);
    }

    protected void initViews(View itemView) {
        pager = (ViewPager)itemView.findViewById(R.id.pager);
        addressTv = (TextView)itemView.findViewById(R.id.address);
        priceTv = (TextView)itemView.findViewById(R.id.price);
        bedsTv = (TextView)itemView.findViewById(R.id.beds_num);
        bathsTv = (TextView)itemView.findViewById(R.id.baths_num);

        chatFab = (FloatingActionButton)itemView.findViewById(R.id.fab_chat);
        chatFab.setBackgroundTintList(ColorStateList.valueOf(chatFabColor));

        contentLayout = itemView.findViewById(R.id.content);
    }

    public void onBindView(final PropertySummaryLayoutSetter property) {
        this.property = property;
        setFieldsContent();
        setImagesPager();
        chatFab.setOnClickListener(getChatClickListener());
        contentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusProvider.getBus().post(new OpenPropertyEvent(property.getMlsId(), property.getMlsNum(), ((ImagesPagerAdapter)pager.getAdapter()).getItemList()));
            }
        });
    }

    private void setFieldsContent() {
        addressTv.setText(property.getFullAddress());
        priceTv.setText(property.getFormattedPrice());

        String bedsStr = null;
        if (property.getBeds() != null) {
            bedsStr = String.valueOf(Helper.removeZeroFromFloatIfNeeded(property.getBeds()));
        }
        setFeatureText(bedsStr, bedsTv, "BEDS");


        String bathsStr = null;
        if (property.getBaths() != null) {
            if (property.getBaths() == Math.floor(property.getBaths())) {
                bathsStr = String.valueOf(property.getBaths().intValue());
            } else {
                bathsStr = String.valueOf(property.getBaths());
            }
        }
        setFeatureText(bathsStr, bathsTv, "BATHS");
    }

    private void setFeatureText(String value, TextView textView, String title) {
        if((value == null) || value.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(value + "  " + title);
        }
    }

    private void setImagesPager() {
        ArrayList<String> photosList = property.getPhotosMeduim();
        photosList.add(property.getStaticMapUrl());
        pager.setAdapter(new ImagesPagerAdapter(itemView.getContext(), photosList, property.getMlsId(), property.getMlsNum()));
        pager.setPageTransformer(true, new ParallaxTransformer(R.id.imageview_for_pager));
        pager.setOffscreenPageLimit(2);
    }

    protected View.OnClickListener getChatClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChatListener.sendPropertyViaChat(new PropertyViaChatVM(property));
                Analytics.getInstance().trackEvent(screenName, "Add property to chat", "mls num : " + property.getMlsNum() + ", mlsId : " + property.getMlsId());
            }
        };
    }

}
