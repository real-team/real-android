package com.joinreal.app.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.Prefs;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.Agent;
import com.joinreal.model.data.Contact;
import com.joinreal.model.data.Customer;
import com.joinreal.model.data.SearchFilter;
import com.joinreal.model.data.SystemSettings;
import com.joinreal.model.data.ValidContacts;
import com.joinreal.app.common.Helper;
import com.joinreal.app.interfaces.ApiCallbackInterface;
import com.joinreal.model.data.PropertyFull;
import com.joinreal.model.data.PropertyCategory;
import com.joinreal.model.data.PropertyThin;
import com.joinreal.model.data.User;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by britt on 5/27/15.
 */
public class Api {
    public static final String JOINREAL_API_DOMAIN = ".realapis.com";
    public static final String API_SIGN_IN = "/users/sign_in";
    private static final String API_GET_AGENT_DETAILS = "/agents/";
    private static final String API_SEND_NEW_PASSWORD = "/users";
    private static final String API_AGENTS = "/agents/";
    private static final String API_GET_AGENT = "/agent";
    private static final String API_CUSTOMERS = "/customers/";
    private static final String API_FAVORITES = "/favorites/";
    private static final String API_PROPERTY = "/properties/";
    private static final String API_PROPERTY_METADATA = "/properties/metadata";
    private static final String API_SYNC = "/sync";
    private static final String API_CONTACTS = "/contacts/";
    private static final String API_FILTERS = "/filters";
    private static final String API_QUICKBLOX = "quickblox/";
    private static final String API_SEND_INVITE = "/" + API_QUICKBLOX + "send_message";
    private static final String API_FIND_PROPERTIES = "/properties/find";
    private static final String API_PROFILE_IMAGE = "/profile_image";
    private static final String API_SYSTEM_SETTINGS = "/system_settings/android";

    private String apiPrefix;
    private static String API_CONTEXT;
    private final Context appContext;
    private final RealClient client;
    NetworkCaller caller;
    Gson gson;

    private static Api instance;

    public static Api getInstance() {
        if (instance == null) {
            instance = new Api(Application.getAppContext());
        }
        return instance;
    }

    private Api(Context appContext) {
        this.appContext = appContext;
        caller = new NetworkCaller();
        gson = new Gson();
        client = Application.getRealClient();
        updateApisDevLevel();
        if (client.getUser() != null) {
            API_CONTEXT = client.isAgent() ? API_AGENTS : API_CUSTOMERS;
        }
    }

    public String getUsersDetails(int id, String token, String username) throws NetworkException {
        String url = apiPrefix + API_CONTEXT + id;
        Response response = caller.getResponse(url, token, username);
        System.out.println(" getUsersDetails returned: " + response.code() + " success ? " + response.isSuccessful());
        try {
            return response.body().string();
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
//            showQAExceptionSnackbar(e.getMessage(), client.getUser().getToken(), client.getUser().getUsername(), "");
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public void sendPasswordToUser(User user) throws NetworkException {
        String url = apiPrefix + API_SEND_NEW_PASSWORD;
        System.out.println(url);
        System.out.println(getUserJson(user));
        Response response = caller.postResponse(url, getUserJson(user));
        System.out.println(" returned: " + response.code() + " success ? " + response.isSuccessful());
    }

    private String getUserJson(User user) {
        return getUserJson(user.getUsername(), user.getPassword());
    }

    private String getUserJson(String userName, String password) {
        JsonObject userObject = new JsonObject();
        userObject.addProperty("username", userName);
        userObject.addProperty("password", password);
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("user", userObject);
        return gson.toJson(jsonObject);
    }

    public void signIn(final String username, final String password, final ApiCallbackInterface apiCallback) {
        String url = apiPrefix + API_SIGN_IN;
        caller.postWithCallback(url, getUserJson(username, password), new Callback() {
            Handler mainHandler = new Handler(appContext.getMainLooper());

            @Override
            public void onFailure(final Request request, final IOException e) {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("api onFailure");
                        String msg = request.urlString() + " user : " + getUserJson(username, password) + " threw IOException : " + e.getMessage();
                        NetworkException apiException = new NetworkException(Helper.getExceptionCodeForResponse(NetworkCodes.EXCEPTION_OTHER), msg, getUserJson(username, password));
                        apiCallback.onFailure(apiException);
                    }
                });
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                final String json = response.body().string();
                System.out.println("api response : " + response.code() + " body : " + json);
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            apiCallback.onSuccess(json);
                        } else {
                            String msg = response.request().urlString() + " user : " + getUserJson(username, password) + " returned : " + response.code();
                            NetworkException apiException = new NetworkException(Helper.getExceptionCodeForResponse(response.code()), msg, response.request().body().toString());
                            if (response.code() == 401) {
                                apiCallback.onWrongPassword(apiException);
                            } else {
                                apiCallback.onFailure(apiException);
                            }
                        }
                    }
                });
            }
        });
    }

    public ArrayList<PropertyThin> findProperties(JsonObject filtersJson, int pageIndex, String contactId) throws NetworkException {
        //TODO: edit url!
        String url = apiPrefix + API_FIND_PROPERTIES;
        System.out.println(url);
        String body = getSearchFiltersJson(filtersJson, pageIndex, contactId);
        System.out.println(getSearchFiltersJson(filtersJson, pageIndex, contactId));
        try {
            Response response = caller.postResponse(url, getSearchFiltersJson(filtersJson, pageIndex, contactId), client.getUser().getToken(), client.getUser().getUsername());
            Type type = new TypeToken<ArrayList<PropertyThin>>() {
            }.getType();
            return gson.fromJson(response.body().string(), type);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e, body);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), body);
        }
    }

    private String getSearchFiltersJson(JsonObject filtersJson, int pageIndex, String contactId) {
        JsonObject filtersObject = new JsonObject();
        filtersObject.add("filter", filtersJson);
        filtersObject.addProperty("page_number", pageIndex);
        if (contactId != null) {
            filtersObject.addProperty("contact_id", contactId);
        }
        return gson.toJson(filtersObject);
    }

    public void addToFavorites(String mlsId, String mlsnum, String token, String username) throws NetworkException {
        String url = apiPrefix + API_CONTEXT + client.getUserId() + API_FAVORITES;
        System.out.println(url);
        JsonObject mlsObject = new JsonObject();
        mlsObject.addProperty("mls_id", mlsId);
        mlsObject.addProperty("mlsnum", mlsnum);

        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("customer_id", client.getUserId());
        requestBody.add("favorite", mlsObject);

        String bodyStr = gson.toJson(requestBody);
        System.out.println("addToFavorites : " + url + "  " + bodyStr);
        try {
            caller.postResponse(url, bodyStr, token, username);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e, bodyStr);
        }

    }

    public void removeFromFavorites(String mlsId, String mlsnum, String token, String username) throws NetworkException {
        String url = apiPrefix + API_CONTEXT + client.getUserId() + API_FAVORITES + mlsId + "/" + mlsnum;

        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("customer_id", client.getUserId());
        requestBody.addProperty("mls_id", mlsId);
        requestBody.addProperty("mlsnum", mlsnum);

        System.out.println("removeFromFavorites : " + gson.toJson(requestBody));

        try {
            caller.performDeleteRequest(url, gson.toJson(requestBody), token, username);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e, requestBody.toString());
        }
    }

    public void setApiContext(boolean isAgent) {
        API_CONTEXT = isAgent ? API_AGENTS : API_CUSTOMERS;
    }

    public PropertyCategory[] getPropertyStructure(String token, String username) throws NetworkException {
        String url = apiPrefix + API_PROPERTY_METADATA;
        System.out.println("getPropertyStructure : " + url);
        try {
            System.out.println("in try");
            Response response = caller.getResponse(url, token, username);
            System.out.println("before get string :");
            String j = response.body().string();
            System.out.println("after :  " + j);
            return gson.fromJson(j, PropertyCategory[].class);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public PropertyFull getProperty(String mlsId, String mlsnum, String token, String username) throws NetworkException {
        String url = apiPrefix + API_PROPERTY + mlsId + "/" + mlsnum;
        System.out.println("getProperty url : " + url);
        try {
            Response response = caller.getResponse(url, token, username);
            String j = response.body().string();
            System.out.println(j);
            if (j != null) {
                PropertyFull[] result = gson.fromJson(j, PropertyFull[].class);
                if (result.length > 0) {
                    return gson.fromJson(j, PropertyFull[].class)[0];
                }
            }
            return null;
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public JSONObject getPropertyJsonObject(String mlsId, String mlsnum, String token, String username) throws NetworkException {
        String url = apiPrefix + API_PROPERTY + mlsId + "/" + mlsnum;
        System.out.println("getProperty url : " + url);
        try {
            Response response = caller.getResponse(url, token, username);
            String j = response.body().string();
            System.out.println(j);
            if ((j != null) && (j.length() > 0)) {
                JSONObject result = new JSONObject(j.substring(1, j.length() - 1));
                return result;
            }
        } catch (JSONException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON, e.getMessage(), e);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
        return null;
    }

    public String getPropertyJson(String mlsId, String mlsnum, String token, String username) throws NetworkException {
        String url = apiPrefix + API_PROPERTY + mlsId + "/" + mlsnum;
        System.out.println("getProperty url : " + url);
        try {
            Response response = caller.getResponse(url, token, username);
            String j = response.body().string();
            System.out.println(j);
            return j;
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public void updateApisDevLevel() {
        if (Application.getDevLevel().equals(appContext.getString(R.string.stg))) {
            apiPrefix = "http://stg";
        } else if (Application.getDevLevel().equals(appContext.getString(R.string.qa1))) {
            apiPrefix = "http://qa1";
        } else if (Application.getDevLevel().equals(appContext.getString(R.string.dev))) {
            apiPrefix = "http://dev";
        } else if (Application.getDevLevel().equals(appContext.getString(R.string.prod))) {
            apiPrefix = "http://www";
        }
        apiPrefix += JOINREAL_API_DOMAIN;
    }

    public Contact[] sendContactsToServer(ArrayList<Contact> contactsToSyncWithServer) throws NetworkException {
        String url = apiPrefix + API_AGENTS + client.getUserId() + API_SYNC;
        JsonObject requestBody = getContactsArrayJson(contactsToSyncWithServer);
        System.out.println("syncContactsWithServer : " + url);
        System.out.println(gson.toJson(requestBody));
        try {
            Response response = caller.postResponse(url, gson.toJson(requestBody), client.getUser().getToken(), client.getUser().getUsername());
            System.out.println(" returned: " + response.code() + " success ? " + response.isSuccessful());
            ValidContacts validContacts = gson.fromJson(response.body().string(), ValidContacts.class);
            return validContacts.getContacts();
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public JsonObject getContactsArrayJson(ArrayList<Contact> contactsToSyncWithServer) {
        JsonArray contactsJsonList = new JsonArray();
        Iterator<Contact> iter = new ArrayList<>(contactsToSyncWithServer).iterator();
        while (iter.hasNext()) {
            Contact contact = iter.next();
            for (int i = 0; i < contact.getPhoneNumbers().size(); i++) {
                contactsJsonList.add(getContactJsonObject(contact, i));
                if (i > 0) {
                    System.out.println("many phones");
                }
            }
        }
        JsonObject j = getResponseBodyJsonObject(contactsJsonList);
        System.out.println("Application.getPrefs().getLastSyncContactsDate() : " + Application.getPrefs().getLastSyncContactsDate());
        return j;
    }

    @NonNull
    private JsonObject getResponseBodyJsonObject(JsonArray resultList) {
        JsonObject j = new JsonObject();
        j.add("contacts", resultList);
        String sinceDate = Application.getPrefs().getLastSyncContactsDate();
        if (sinceDate != null) {
            j.addProperty("since", sinceDate);
        }
        return j;
    }

    @NonNull
    private JsonObject getContactJsonObject(Contact contact, int i) {
        JsonObject jsonObject = new JsonObject();

        if (contact.getFirstname() != null) {
            jsonObject.addProperty("firstname", contact.getFirstname());
        }
        if (contact.getLastname() != null) {
            jsonObject.addProperty("lastname", contact.getLastname());
        }
        if (contact.getMiddleName() != null) {
            jsonObject.addProperty("middlename", contact.getMiddleName());
        }

        if (contact.getEmail() != null) {
            jsonObject.addProperty("email", contact.getEmail());
        }
        if (contact.getImageId() > 0) {
            jsonObject.addProperty("profile_image_file_name", contact.getProfileImageFileName());
        }

        if ((contact.getPhone() != null) && (!contact.getPhoneNumbers().isEmpty())) {
            jsonObject.addProperty("phone", contact.getPhoneNumbers().get(i).getPhone());
        }
        if ((contact.getPhone() != null) && (!contact.getPhoneNumbers().isEmpty())) {
            jsonObject.addProperty("label", contact.getPhoneNumbers().get(i).getLabel());
        }
        return jsonObject;
    }


    public Contact[] getContactsFromServer() throws NetworkException {
        String url = apiPrefix + API_CONTACTS;
        System.out.println(url);
        try {
            long t = System.currentTimeMillis();
            Response response = caller.getResponse(url, client.getUser().getToken(), client.getUser().getUsername());
            long timeFromNet = (System.currentTimeMillis() - t);
//            System.out.println("--------- getAgentDetails returned: " + response.code() + " time : " + timeFromNet);
            Contact[] result = gson.fromJson(response.body().string(), Contact[].class);
//            System.out.println("--------- getAgentDetails parse result time : " + (System.currentTimeMillis() - timeFromNet) + " size : " + result.length);
//            Crashlytics.logException(new Throwable("Api.getAgentDetails() back from network time : " + timeFromNet + "  , parse time : " + (System.currentTimeMillis() - timeFromNet) + " size : " + result.length));
            return result;
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public SearchFilter getContactLastSearchFilters(String contactId) throws NetworkException {
        String url = apiPrefix + API_CONTACTS + contactId + API_FILTERS;
        System.out.println(url);
        try {
            Response response = caller.getResponse(url, client.getUser().getToken(), client.getUser().getUsername());
            String j = response.body().string();
            System.out.println(j);
            return gson.fromJson(j, SearchFilter.class);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public ArrayList<PropertyThin> getContactFavorites(String contactId) throws NetworkException {
        String url = apiPrefix + API_CONTACTS + contactId + API_FAVORITES;
        System.out.println(url);
        try {
            Response response = caller.getResponse(url, client.getUser().getToken(), client.getUser().getUsername());
            Type type = new TypeToken<ArrayList<PropertyThin>>() {
            }.getType();
            return gson.fromJson(response.body().string(), type);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }


    public ArrayList<PropertyThin> getCustomerFavorites() throws NetworkException {
        String url = apiPrefix + API_CUSTOMERS + Application.getClient().getUser().getId() + API_FAVORITES;
        System.out.println(url);
        try {
            Response response = caller.getResponse(url, client.getUser().getToken(), client.getUser().getUsername());
//            if (response.code() == 404) {
//                //TODO: re-sign in for other calls
//                return new ArrayList<>();
//            }
            Type type = new TypeToken<ArrayList<PropertyThin>>() {
            }.getType();
            return gson.fromJson(response.body().string(), type);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public void sendInvitation(String[] contactPhones, String msg) throws NetworkException {
        String url = apiPrefix + API_SEND_INVITE;
        System.out.println(url);
        JsonObject msgObject = new JsonObject();
        msgObject.addProperty("to", gson.toJson(contactPhones));
        msgObject.addProperty("content", msg);

        JsonObject requestBody = new JsonObject();
        requestBody.add("message", msgObject);

        caller.postResponse(url, gson.toJson(requestBody), client.getUser().getToken(), client.getUser().getUsername());
    }

    public Agent getAssignedAgent() throws NetworkException {
        String url = apiPrefix + API_CUSTOMERS + client.getUser().getId() + API_GET_AGENT;
        System.out.println(url);
        try {
            Response response = caller.getResponse(url, client.getUser().getToken(), client.getUser().getUsername());

            return gson.fromJson(response.body().string(), Agent.class);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public void updateAgentDetails(Agent agent) throws NetworkException {
        updateUserProfile(gson.toJson(agent), "agent", agent.getImageToUpload());
    }

    public void updateCustomerDetails(Customer customer) throws NetworkException {
        updateUserProfile(gson.toJson(customer), "customer", customer.getImageToUpload());
    }

    private void updateUserProfile(String json, String jsonElemName, Bitmap profileImage) throws NetworkException {
        String url = apiPrefix + API_CONTEXT + client.getUser().getId();
        updateProfileDetails(json, url);
        if (profileImage != null) {
            updateProfileImage(url, jsonElemName, profileImage);
        }
    }


    private void updateProfileDetails(String json, String url) throws NetworkException {
        caller.performPutRequest(url, json, client.getUser().getToken(), client.getUser().getUsername());
    }

    private void updateProfileImage(String url, String jsonName, Bitmap bitmap) throws NetworkException {
        if (bitmap != null) {
            JsonObject requestBody;
            url += API_PROFILE_IMAGE;
            requestBody = new JsonObject();
            JsonObject imageElem = new JsonObject();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            System.out.println(encodedImage);
            imageElem.addProperty("profile_image", "data:image/png;base64," + encodedImage);
            requestBody.add(jsonName, imageElem);
            caller.performPutRequest(url, gson.toJson(requestBody), client.getUser().getToken(), client.getUser().getUsername());
        }
    }

    public SystemSettings getSystemSettings() throws NetworkException {
        String url = apiPrefix + API_SYSTEM_SETTINGS;
        System.out.println(url);
        Response response = caller.getResponse(url);
        try {
            return gson.fromJson(response.body().string(), SystemSettings.class);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }

    public Contact getContactFromQBId(String qbUserId) throws NetworkException {
        String url = apiPrefix + API_CONTACTS + API_QUICKBLOX + qbUserId;
        System.out.println(url);
        Response response = caller.getResponse(url);
        try {
            return gson.fromJson(response.body().string(), Contact.class);
        } catch (JsonSyntaxException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_JSON_SYNTAX, e.getMessage(), e);
        } catch (IOException e) {
            throw new NetworkException(NetworkCodes.EXCEPTION_RESPONSE_TO_STRING, e.getMessage(), e);
        }
    }
}