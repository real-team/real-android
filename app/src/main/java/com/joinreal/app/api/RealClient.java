package com.joinreal.app.api;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.joinreal.app.Application;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.Prefs;
import com.joinreal.app.interfaces.ApiCallbackInterface;
import com.joinreal.events.TaskBackFromNetworkEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.Agent;
import com.joinreal.model.data.Contact;
import com.joinreal.model.data.Customer;
import com.joinreal.model.data.PropertyFull;
import com.joinreal.model.data.PropertyCategory;
import com.joinreal.model.data.PropertyThin;
import com.joinreal.model.data.SearchFilter;
import com.joinreal.model.data.SystemSettings;
import com.joinreal.model.data.User;
import com.joinreal.services.SyncUsersService;
import com.joinreal.utils.ChatConnect;
import com.quickblox.users.model.QBUser;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by britt on 5/27/15.
 * <p/>
 * A client that provides all communication and state for the FTBProService.
 */

public class RealClient {

    private Api api;
    private Prefs prefs;
    private User user;
    private Agent assignedAgent;
    private Gson gson;
    private PropertyCategory[] propertyStructure;
    private SearchFilter usersLastFilter;
    private String usersLastSearchLocation;
    private String usersLastSearchLocationDesc;
    private SystemSettings systemSettings;


    public void initSession(Context context) {
        Crashlytics.log("client.initSession");
        this.gson = new Gson();
        this.api = Api.getInstance();
//        initUser();
//        setAssignedAgent();
    }

    public void initSystemSettings() {
        new AsyncTask() {
            @Override
            protected Void doInBackground(Object[] params) {
                TaskBackFromNetworkEvent event = new TaskBackFromNetworkEvent(NetworkCodes.TASK_SYSTEM_SETTINGS);
                try {
                    systemSettings = api.getSystemSettings();
                } catch (NetworkException e) {
                    event.setException(e);
                }
                if (systemSettings != null) {
                    Application.getPrefs().setSystemSettings(systemSettings);
                }
                BusProvider.getRestBus().post(event);
                return null;
            }
        }.execute();

    }

    public void setAssignedAgent() {
        if ((user != null) && (!isAgent())) {
            assignedAgent = Application.getPrefs().getAssaignedAgent();
            Crashlytics.log("client.setAssignedAgent() , (assignedAgent == null) ? " + (assignedAgent == null));
            final TaskBackFromNetworkEvent event = new TaskBackFromNetworkEvent(NetworkCodes.TASK_ASSIAGNED_AGENT);
            if (assignedAgent != null) {
                BusProvider.getRestBus().post(event);
            }

            new AsyncTask() {
                @Override
                protected Void doInBackground(Object[] params) {
                    try {
                        assignedAgent = api.getAssignedAgent();
                    } catch (NetworkException e) {
                        e.printStackTrace();
                        event.setException(e);
                    }
                    if (assignedAgent != null) {
                        BusProvider.getRestBus().post(event);
                        Application.getPrefs().setAssaignedAgent(assignedAgent);
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                }
            }.execute();
        }
    }

    public Agent getAssignedAgent() {
        return assignedAgent;
    }

    public void setPropertyMetadata() {
        String json;
        if (propertyStructure == null) {
            json = Application.getPrefs().getPropertyStructureJson();
            if (json != null) {
                propertyStructure = gson.fromJson(json, PropertyCategory[].class);
            } else {
                getPropertyMetadataFromNetwork();
            }
        }
    }

    private AsyncTask getPropertyMetadataFromNetwork() {
        return new AsyncTask() {
            @Override
            protected Void doInBackground(Object[] params) {
                try {
                    propertyStructure = api.getPropertyStructure(user.getToken(), user.getUsername());
                } catch (NetworkException e) {
                    e.printStackTrace();
                    Helper.handleApiException(e, NetworkCodes.CANT_GET_PROPERTY_METADATA);
                }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    Toast.makeText(Application.getAppContext(), "No internet connection, please connect and try again", Toast.LENGTH_LONG);
//                }
                if (propertyStructure != null) {
                    for (PropertyCategory category : propertyStructure) {
                        category.adjustFields();
                    }
                    Prefs.getInstance().setPropertyStructureJson(gson.toJson(propertyStructure));
                }
                return null;
            }
        }.execute();
    }

    public PropertyCategory[] getPropertyStructure() {
        if (propertyStructure == null) {
            setPropertyMetadata();
        }
        return propertyStructure;
    }

    public String getUserDetails() throws NetworkException {
        return api.getUsersDetails(getUser().getId(), getUser().getToken(), getUser().getUsername());
    }

    public void signUpOrResetPassword(User user) throws NetworkException {
        api.sendPasswordToUser(user);
    }

    public void signIn(String username, String password, ApiCallbackInterface callback) {
        api.signIn(username, password, callback);
    }

    public void getUserFromNetwork(String json) {
        System.out.println("on client " + user);
        this.user = parseUser(json);
        Application.getPrefs().setUserJson(json);
        api.setApiContext(user.isAgent());
        setChatUser();

        Analytics.getInstance().setAppTracker(Application.getAppContext());

    }

    private void setChatUser() {
        //TODO: temp
        if ((user != null) && (user.getUsername() != null)) {
            QBUser chatUser = new QBUser(getQBUserName(), Application.USER_PASSWORD);
            Application.setCurrentChatUser(chatUser);
        }
    }

    public String getQBUserName() {
        return user.getUsername();
    }

    private User parseUser(String json) {
        User user = gson.fromJson(json, User.class);
        if (user != null) {
            //TODO: try to do more efficiently
            String resourceJson = gson.toJson(user.getResource());
            if (user.getResourceType().equals(User.AGENT)) {
                user.setAgent(gson.fromJson(resourceJson, Agent.class));
            } else {
                user.setCustomer(gson.fromJson(resourceJson, Customer.class));
            }
        }
        return user;
    }

    public ArrayList<PropertyThin> findProperties(JsonObject filtersJson, int pageIndex, String contactId) throws NetworkException {
        return api.findProperties(filtersJson, pageIndex, contactId);
    }

    public void addToFavorites(String mlsId, String mlsnum) throws NetworkException {
        api.addToFavorites(mlsId, mlsnum, user.getToken(), user.getUsername());
    }

    public void removeFromFavorites(String mlsId, String mlsnum) throws NetworkException {
        api.removeFromFavorites(mlsId, mlsnum, user.getToken(), user.getUsername());
    }

    public void initUser() {
        if (user == null) {
            String json = Application.getPrefs().getUserJson();
            if (json != null) {
                user = parseUser(json);
                setChatUser();
                api.setApiContext(user.isAgent());
                refreshUserFromNetwork();
            }
            setAssignedAgent();
        }
    }

    private void refreshUserFromNetwork() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                String json = null;
                try {
                    json = getUserDetails();
                } catch (NetworkException e) {
                    Helper.handleApiException(e, NetworkCodes.CANT_REFRESH_USER_DETAILS);
                }
                if (user != null) {
                    if (user.isAgent()) {
                        user.setAgent(gson.fromJson(json, Agent.class));
                    } else {
                        user.setCustomer(gson.fromJson(json, Customer.class));
                    }
                    Application.getPrefs().setUserJson(gson.toJson(user));

                    setUserCrashlytics();
                }
            }
        };
        new Thread(runnable).start();
    }

    private void setUserCrashlytics() {
        Crashlytics.setUserIdentifier(String.valueOf(user.getUserId()));
        Crashlytics.setUserName(String.valueOf(user.getUsername()));
//        Crashlytics.setUserEmail(user.getEmail());
        Crashlytics.setBool("is_agent", isAgent());
    }


    public User getUser() {
        return user;
    }

    public PropertyFull getProperty(String mlsId, String mlsnum) throws NetworkException {
        return api.getProperty(mlsId, mlsnum, user.getToken(), user.getUsername());
    }

    public JSONObject getPropertyJsonObject(String mlsId, String mlsnum) throws NetworkException {
        return api.getPropertyJsonObject(mlsId, mlsnum, user.getToken(), user.getUsername());
    }

    public String getPropertyJson(String mlsId, String mlsnum) throws NetworkException {
        return api.getPropertyJson(mlsId, mlsnum, user.getToken(), user.getUsername());
    }

    public int getUserId() {
        return user.getId();
    }

    public boolean isAgent() {
        return user.isAgent();
    }

    public void updateApisDevLevel() {
        api.updateApisDevLevel();
    }


    public Contact[] syncContactsWithServer(ArrayList<Contact> contactsToSyncWithServer) throws NetworkException {
        return api.sendContactsToServer(contactsToSyncWithServer);
    }

    public Contact[] getContactsFromServer() throws NetworkException {
        return api.getContactsFromServer();
    }

    public SearchFilter getContactLastSearchFilters(String contactId) throws NetworkException {
        return api.getContactLastSearchFilters(contactId);
    }

    public ArrayList<PropertyThin> getContactFavorites(String contactId) throws NetworkException {
        return api.getContactFavorites(contactId);
    }

    public ArrayList<PropertyThin> getCustomerFavorites() throws NetworkException {
        return api.getCustomerFavorites();
    }

    public void sendInvitation(String[] contactPhones, String msgEmail) throws NetworkException {
        api.sendInvitation(contactPhones, msgEmail);
    }

    public void setUsersLastFilter(SearchFilter usersLastFilter) {
        this.usersLastFilter = usersLastFilter;
    }

    public SearchFilter getUsersLastFilter() {
        return usersLastFilter;
    }

    public void setUsersLastSearchLocation(String usersLastSearchLocation) {
        this.usersLastSearchLocation = usersLastSearchLocation;
    }

    public String getUsersLastSearchLocation() {
        return usersLastSearchLocation;
    }

    public String getUsersLastSearchLocationDesc() {
        return usersLastSearchLocationDesc;
    }

    public void setUsersLastSearchLocationDesc(String usersLastSearchLocationDesc) {
        this.usersLastSearchLocationDesc = usersLastSearchLocationDesc;
    }

    public void updateAgentDetails(Agent agent) throws NetworkException {
        api.updateAgentDetails(agent);
    }

    public void updateCustomerDetails(Customer customer) throws NetworkException {
        api.updateCustomerDetails(customer);
    }

    public void setUserAgent(Agent agent) {
        user.setAgent(agent);
        Application.getPrefs().setUserJson(gson.toJson(user));
    }

    public void setUserCustomer(Customer customer) {
        user.setCustomer(customer);
        Application.getPrefs().setUserJson(gson.toJson(user));
    }

    public SystemSettings getSystemSettings() {
        return systemSettings;
    }

    public String getNameForPN() {
        return user.getNameForPN();
    }

    public void syncContacts(Context context) {
        if ((user != null) && (user.isAgent())) {
            context.startService(new Intent(context, SyncUsersService.class));
        }
    }

    public Contact getContactFromQBId(String ChatUserLogin) throws NetworkException {
        return api.getContactFromQBId(ChatUserLogin);
    }

    public void setNewProfileImage(Bitmap bitmap) {
        if (user != null) {
            if (isAgent()) {
                user.getAgent().setNewProfileImage(bitmap);
            } else {
                user.getCustomer().setNewProfileImage(bitmap);
            }

        }
    }

    public String getProfileImage() {
        if (user != null) {
            if (isAgent()) {
                return user.getAgent().getProfileImage().getMain();
            } else {
                return user.getCustomer().getProfileImage().getMain();
            }
        }
        return null;
    }

    public Bitmap getNewProfileImage() {
        if (user != null) {
            if (isAgent()) {
                return user.getAgent().getNewProfileImage();
            } else {
                return user.getCustomer().getNewProfileImage();
            }
        }
        return null;
    }

    public boolean hasNewProfileImage() {
        return getNewProfileImage() != null;
    }
}
