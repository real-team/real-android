package com.joinreal.app.api;

import com.crashlytics.android.Crashlytics;
import com.joinreal.app.common.Helper;
import com.joinreal.exceptions.NetworkException;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by britt on 5/27/15.
 */
public class NetworkCaller {

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    private static final String HEADER_TOKEN = "X-User-Token";
    private static final String HEADER_USERNAME = "X-User-Username";

    public Response postResponse(String url, String json, String token, String username) throws NetworkException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .header(HEADER_TOKEN, token)
                .header(HEADER_USERNAME, username)
                .post(body)
                .build();
        return getResponseForHttpRequest(request);

    }

    public Response postResponse(String url, String json) throws NetworkException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        return getResponseForHttpRequest(request);

    }

    public Call postWithCallback(String url, String json, Callback callback) {
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    public Response getResponse(String url, String token, String username) throws NetworkException {
        Request request = new Request.Builder()
                .url(url)
                .header(HEADER_TOKEN, token)
                .header(HEADER_USERNAME, username)
                .build();
        return getResponseForHttpRequest(request);
    }

    public Response getResponse(String url) throws NetworkException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        return getResponseForHttpRequest(request);
    }

    public Response performDeleteRequest(String url, String json, String token, String username) throws NetworkException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .header(HEADER_TOKEN, token)
                .header(HEADER_USERNAME, username)
                .delete(body)
                .build();
        return getResponseForHttpRequest(request);


    }

    public Response performPutRequest(String url, String json, String token, String username) throws NetworkException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .header(HEADER_TOKEN, token)
                .header(HEADER_USERNAME, username)
                .put(body)
                .build();
        return getResponseForHttpRequest(request);
    }

    private Response getResponseForHttpRequest(Request request) throws NetworkException {
        System.out.println("********* request.urlString() : " + request.urlString());
        OkHttpClient client = new OkHttpClient();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            String body = ((request != null) && (request.body() != null)) ? request.body().toString() : "";
            int code = (response != null) ? response.code() : -1;
            throw new NetworkException(Helper.getExceptionCodeForResponse(code), e.getMessage(), e, body);
        }
        handleResponse(request.urlString(), response);
        return response;
    }

    private void handleResponse(String url, Response response) throws NetworkException {
        String msg = url + " returned: " + response.code() + " message: " + response.message();
        Crashlytics.log(msg);
        if (!response.isSuccessful()) {
            throw new NetworkException(Helper.getExceptionCodeForResponse(response.code()), msg, (response.request().body() != null) ? response.request().body().toString() : "");
        }
    }

}
