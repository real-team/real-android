package com.joinreal.app.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.fragments.AgentProfileEditFragment;
import com.joinreal.app.fragments.AgentProfileFragment;
import com.joinreal.app.fragments.ChatDialogsFragment;
import com.joinreal.app.fragments.ContactInfoFragment;
import com.joinreal.app.fragments.FavoritesContainerFragment;
import com.joinreal.app.fragments.InviteContactsFragment;
import com.joinreal.app.fragments.InviteNewContactFragment;
import com.joinreal.app.fragments.InviteSentFragment;
import com.joinreal.app.fragments.PropertyListContainerFragment;
import com.joinreal.app.fragments.SendInvitesFragment;
import com.joinreal.app.interfaces.AgentActionsListener;
import com.joinreal.app.interfaces.ISearchToolbarListener;
import com.joinreal.events.ChatDialogsReceivedEvent;
import com.joinreal.events.OpenPropertyEvent;
import com.joinreal.events.ShouldFetchDialogsEvent;
import com.joinreal.events.StartActivityEvent;
import com.joinreal.model.data.SearchFilter;
import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.PropertyViaChatVM;
import com.joinreal.utils.ChatImagesUtils;
import com.joinreal.utils.ChatKeyboardUtils;
import com.joinreal.utils.ContactManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;


public class MainActivityAgent extends MainActivityAbstract implements AgentActionsListener, ISearchToolbarListener {

    private static final String GUIDE_URL = "http://realagents.screenstepslive.com/s/9412/m/29511";

    public static LinearLayout searchLayout;
    public static EditText searchText;
    private TextView cancelSearchButton;
    public static DrawerLayout drawerLayout;
    NavigationView navigationView;
    private int unreadMessagesCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_agent);
        ContactManager.init(this);
        super.onCreateActivity();
        setupNavigationView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.setCreatedMainActivity();
        syncContactsIfNeeded();
    }

    private void syncContactsIfNeeded() {
        if (! didComeFromAnotherActivityOrOnCreate) {
            client.syncContacts(this);
        }
        didComeFromAnotherActivityOrOnCreate = false;
    }

    @Subscribe
    public void onShouldFetchDialogsEvent(ShouldFetchDialogsEvent e) {
        getDialogsFromNetwork();
    }

    @Override
    protected void setupToolBar() {
        searchLayout = (LinearLayout) findViewById(R.id.search_layout);
        searchText = (EditText) findViewById(R.id.search_text);
        cancelSearchButton = (TextView) findViewById(R.id.cancel_search);
        cancelSearchButton.setOnClickListener(getCancelClickListener());
        super.setupToolBar();
    }


    @Override
    public void onCancelSearchClick() {
        searchLayout.setVisibility(View.GONE);
        searchText.setText("");
        ChatKeyboardUtils.hideKeyboard(searchText);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        menu.findItem(ACTION_CANCEL_ID).setVisible(false);
    }

    private View.OnClickListener getCancelClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchLayout.setVisibility(View.GONE);
                searchText.setText("");
                ChatKeyboardUtils.hideKeyboard(searchText);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                v.setVisibility(View.GONE);
            }
        };
    }


    private void setupNavigationView() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setNavigationViewMenu();
        setNavigationViewHeader();
        setInviteClientsButton();
        TypefaceManager.getInstance(this).assignTypeface(navigationView);
    }

    private void setInviteClientsButton() {
        findViewById(R.id.invite_clients_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInviteContactsFragment();
            }
        });
    }

    private void setNavigationViewMenu() {
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                String clickName = "";
                switch (menuItem.getItemId()) {
                    case R.id.nav_item_search:
                        openFiltersActivity(false);
                        clickName = "Search";
                        break;
                    case R.id.nav_item_chats:
                        openChatDialogsFragment();
                        clickName = "Chats";
                        break;
                    case R.id.nav_item_guide:
                        setToolbarTitle("Guide");
                        openWebviewFragment(GUIDE_URL);
                        clickName = "Guide";
                        break;
                    case R.id.nav_item_support:
                        sendSupportEmail();
                        clickName = "Support";
                        break;
                }
//                menuItem.setChecked(true);
                drawerLayout.closeDrawers();

                Analytics.getInstance().trackEvent("Left Navigation", clickName);

                return true;
            }
        });

        //View icon = getLayoutInflater().inflate(R.layout.icon_badge_menu, null, false);
        navigationView.setItemIconTintList(null);
        navigationView.getMenu().findItem(R.id.nav_item_chats)
                .setIcon(getChatNavItemIcon());

        TypefaceManager.getInstance(this).applyCustomTypefaceForNavigationMenu(navigationView.getMenu());
    }

    private void setNavigationViewHeader() {
        findViewById(R.id.close_drawer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });

        findViewById(R.id.agent_nav_header).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAgentProfileFragment();
            }
        });
        if (client.getUser().getAgent() != null) { //happened when session timed out
            ChatImagesUtils.setAgentProfileImage(client.getUser().getAgent(), (ImageView) findViewById(R.id.agent_image));

            TextView tv = ((TextView) findViewById(R.id.agent_name));
            tv.setText(client.getUser().getAgent().getFullName());
        }
    }


    private Drawable getChatNavItemIcon() {
        if (unreadMessagesCount > 0) {
            int iconSize = (int) getResources().getDimension(android.R.dimen.app_icon_size);
            int radius = (iconSize) / 2;
            Bitmap b = Bitmap.createBitmap(iconSize, iconSize, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(b);
            circlePaint = new Paint();
            circlePaint.setColor(getResources().getColor(R.color.pink));
            canvas.drawCircle(iconSize / 2, iconSize / 2, radius, circlePaint);
            circlePaint.setColor(getResources().getColor(R.color.white));
            circlePaint.setTypeface(TypefaceManager.getInstance(this).getTypeface("book"));
            circlePaint.setTextSize(getResources().getDimension(R.dimen.font_size_large));
            circlePaint.setTextAlign(Paint.Align.CENTER);
            int yPos = (int) ((canvas.getHeight() / 2) - ((circlePaint.descent() + circlePaint.ascent()) / 2));
            canvas.drawText(String.valueOf(unreadMessagesCount), iconSize / 2, yPos, circlePaint);
            return new BitmapDrawable(getResources(), b);
        } else {
            return Helper.getDrawable(this, R.drawable.new_message_white);
        }
    }

    private Paint circlePaint;


    @Override
    protected void openAgentProfileFragment() {
        AgentProfileFragment fragment = AgentProfileFragment.getNewInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, AgentProfileFragment.TAG)
                .addToBackStack(AgentProfileFragment.TAG)
                .commit();
        drawerLayout.closeDrawers();
    }

    @Override
    public void openEditAgentProfileFragment() {
        AgentProfileEditFragment fragment = AgentProfileEditFragment.getNewInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, AgentProfileEditFragment.TAG)
                .addToBackStack(AgentProfileEditFragment.TAG)
                .commit();
    }

    @Override
    public void openFavoritesFragment(Bundle bd) {
        FavoritesContainerFragment fragment = FavoritesContainerFragment.getNewInstance(bd);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, FavoritesContainerFragment.TAG)
                .addToBackStack(FavoritesContainerFragment.TAG)
                .commit();
    }


    @Override
    public void openContactInfoFragment(ContactVM contactVM) {
        Bundle bd = new Bundle();
        bd.putSerializable(StaticParams.KEY_CONTACT_VM, contactVM);
        ContactInfoFragment fragment = ContactInfoFragment.getNewInstance(bd);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, ContactInfoFragment.TAG)
                .addToBackStack(ContactInfoFragment.TAG)
                .commit();
    }

//    @Override
//    public void openNotesFragment(String notes, String login) {
//        Bundle bd = new Bundle();
//        if (notes != null) {
//            bd.putString(StaticParams.KEY_CONTACT_NOTES, notes);
//        }
//        bd.putString(StaticParams.KEY_CONTACT_LOGIN, login);
//        ContactNotesFragment fragment = ContactNotesFragment.getNewInstance(bd);
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.container, fragment, ContactNotesFragment.TAG)
//                .addToBackStack(ContactNotesFragment.TAG)
//                .commit();
//    }

    @Override
    public void openInviteContactsFragment() {
        String entryTagBeforeInviteFragment = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        openInviteContactsFragment(entryTagBeforeInviteFragment);
    }

    @Override
    public void openInviteContactsFragment(String entryTagBeforeInviteFragment) {
        InviteContactsFragment fragment = InviteContactsFragment.getNewInstance(entryTagBeforeInviteFragment);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, InviteContactsFragment.TAG)
                .addToBackStack(InviteContactsFragment.TAG)
                .commit();
    }

    @Override
    public void openInviteNewContactFragment(String entryTagToReturnTo) {
        InviteNewContactFragment fragment = InviteNewContactFragment.getNewInstance(entryTagToReturnTo);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, InviteNewContactFragment.TAG)
                .addToBackStack(InviteNewContactFragment.TAG)
                .commit();
    }

    @Override
    public void openSendInvitesFragment(ArrayList<String> contactPhonesToInvite, boolean shouldInviteByPhoneOnly, String entryTagToReturnTo) {
        SendInvitesFragment fragment = SendInvitesFragment.getNewInstance(contactPhonesToInvite, shouldInviteByPhoneOnly, entryTagToReturnTo);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, SendInvitesFragment.TAG)
                .addToBackStack(SendInvitesFragment.TAG)
                .commit();
    }

    @Override
    public void openSendInvitesFragment(ArrayList<String> contactPhonesToInvite, String entryTagToReturnTo) {
        openSendInvitesFragment(contactPhonesToInvite, false, entryTagToReturnTo);
    }

    @Override
    public void openInvitationSentFragment(int count, String entryTagToReturnTo) {
        openInvitationSentFragment(count, null, entryTagToReturnTo);
    }

    @Override
    public void openInvitationSentFragment(String displayName, String entryTagToReturnTo) {
        openInvitationSentFragment(1, displayName, entryTagToReturnTo);

    }

    public void openInvitationSentFragment(int count, String displayName, String entryTagToReturnTo) {
        InviteSentFragment fragment = InviteSentFragment.getNewInstance(count, displayName, entryTagToReturnTo);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, InviteSentFragment.TAG)
                .addToBackStack(InviteSentFragment.TAG)
                .commit();
    }

    @Subscribe
    public void openPropertyFragmentEvent(OpenPropertyEvent event) {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        super.startPropertyActivity(event);
    }

    @Subscribe
    public void onUpdateAgentDetails(AgentProfileEditFragment.UpdatedAgentDetailsEvent event) {
        if (event.didUpdateImage()) {
            setupNavigationView();
        }
    }

//    @Override
//    public void openContactsFragment() {
//        ContactsFragment fragment = ContactsFragment.getNewInstance();
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.container, fragment, ContactsFragment.TAG)
//                .addToBackStack(ContactsFragment.TAG)
//                .commit();
//    }


    @Override
    public void openFiltersActivityForContact(String contactId, String displayName, SearchFilter filter) {
        Intent intent = new Intent(this, FiltersActivity.class);
        intent.putExtra(StaticParams.KEY_CONTACT_ID, contactId);
        intent.putExtra(StaticParams.KEY_CONTACT_NAME, displayName);
        intent.putExtra(StaticParams.KEY_SHOULD_LOCK_DRAWER, true);
        intent.putExtra(StaticParams.KEY_SEARCH_FILTER, filter);
        startActivityForResult(intent, REQUEST_OPEN_FILTERS);
        didComeFromAnotherActivityOrOnCreate = true;
    }

    @Override
    public void openChatDialogsFragment() {
        ChatDialogsFragment fragment = ChatDialogsFragment.getInstance(new Bundle());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, ChatDialogsFragment.TAG)
                .addToBackStack(ChatDialogsFragment.TAG)
                .commit();
    }


    @Override
    public void sendPropertyViaChat(PropertyViaChatVM propertyForChat) {
        Bundle bd = new Bundle();
        bd.putBoolean(StaticParams.EXISTING_USER, false);
        bd.putParcelable(StaticParams.KEY_PROPERTY_FOR_CHAT_OBJ, propertyForChat);

        ChatDialogsFragment fragment = ChatDialogsFragment.getInstance(bd);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, ChatDialogsFragment.TAG)
                .addToBackStack(PropertyListContainerFragment.TAG)
                .commit();
    }

    @Override
    public void openMapViewFragment() {
        //TODO: if cntainer exists -- replace.
        PropertyListContainerFragment containerFragment = (PropertyListContainerFragment) getSupportFragmentManager().findFragmentByTag(PropertyListContainerFragment.TAG);
        if (containerFragment != null) {
            containerFragment.openMapView();
        }
    }

    @Override
    public void openPropertyListViewFragment() {
        PropertyListContainerFragment containerFragment = (PropertyListContainerFragment) getSupportFragmentManager().findFragmentByTag(PropertyListContainerFragment.TAG);
        if (containerFragment != null) {
            containerFragment.openListView();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_agent, menu);
        this.menu = menu;
        menu.findItem(R.id.action_search).setVisible(false);
        return true;
    }

    public static void addCancelActionButton(Menu menu, View.OnClickListener clickListener) {
        TextView tv = getActionButtonTextView("Cancel", clickListener);
        menu.add(0, ACTION_CANCEL_ID, 1, "Cancel").setActionView(tv).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.findItem(ACTION_CANCEL_ID).setVisible(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawerLayout.getDrawerLockMode(GravityCompat.START) == DrawerLayout.LOCK_MODE_UNLOCKED) {
                    drawerLayout.openDrawer(GravityCompat.START);
                } else {
                    ChatKeyboardUtils.hideKeyboard(getCurrentFocus());
                    getSupportFragmentManager().popBackStack();
                }
                return true;


        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSearchActionClick() {
        searchLayout.setVisibility(View.VISIBLE);
        searchText.requestFocus();
        ChatKeyboardUtils.showKeyboard(searchText);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        menu.findItem(ACTION_CANCEL_ID).getActionView().setVisibility(View.VISIBLE);
    }


    @Override
    public void lockDrawer(String title) {
        setToolbarTitle(title);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow_blue);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    @Override
    public void lockDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    @Override
    public void unlockDrawer(String title) {
        setToolbarTitle(title);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburger);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void unlockDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Subscribe
    public void onStartActivityEvent(StartActivityEvent event) {
        super.onStartActivityEvent(event);
        didComeFromAnotherActivityOrOnCreate = true;
    }

    @Subscribe
    public void onNewDialogsRecievedEvent(ChatDialogsReceivedEvent e) {
        System.out.println("main onNewDialogsRecievedEvent : " + e.getDialogs().size());
        unreadMessagesCount = e.getTotalUnreadMessages();
        setupNavigationView();
        if (unreadMessagesCount == 0) {
            cancelNotificationFromStatusBar();
        }

    }

    @Override
    public void addTextChangedListener(TextWatcher textWatcher) {
        searchText.addTextChangedListener(textWatcher);
    }

    @Override
    public void removeTextChangedListener(TextWatcher textWatcher) {
        searchText.removeTextChangedListener(textWatcher);

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void handleIntent(Intent recievedIntent) {
        if ((recievedIntent != null) && (recievedIntent.getExtras() != null)) {
            int actionCode = recievedIntent.getIntExtra(StaticParams.KEY_ACTION_TYPE, -1);
            if (actionCode == StaticParams.OPEN_CONTACT_PROFILE) {
                openContactInfoFragment(new ContactVM(ContactManager.getContactByLogin(recievedIntent.getStringExtra(StaticParams.KEY_PHONE_NUM))));
            } else if (actionCode == StaticParams.SEND_INVITE){
                ArrayList<String> arr = new ArrayList<>();
                arr.add(recievedIntent.getExtras().getString(StaticParams.KEY_CONTACT_PHONE));
                openSendInvitesFragment(arr, false, null);
            } else {
                super.handleIntent(recievedIntent);
            }
        }
    }

    @Override
    public void handleCameFromChatPN(Bundle extras) {
        super.handleCameFromChatPN(extras);
        if (extras.getString(StaticParams.KEY_DIALOG_ID) != null) {
            openChatDialogsFragment();
        }
    }
}
