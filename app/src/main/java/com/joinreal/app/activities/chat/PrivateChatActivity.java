package com.joinreal.app.activities.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.RealBaseActivity;
import com.joinreal.app.activities.MainActivityAgent;
import com.joinreal.app.activities.MainActivityCustomer;
import com.joinreal.app.activities.PropertyPageActivity;
import com.joinreal.app.adapters.chat.ChatAdapter;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.Prefs;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.Contact;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.interfaces.OpenPropertyListener;
import com.joinreal.model.data.chat.ChatDialogDataVM;
import com.joinreal.model.data.chat.ChatDialogInfo;
import com.joinreal.model.data.chat.ChatMessageData;
import com.joinreal.model.data.chat.ChatMessagesArray;
import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.PropertyViaChatVM;
import com.joinreal.tasks.SendMessageTask;
import com.joinreal.utils.ChatConnect;
import com.joinreal.utils.ChatConnectListener;
import com.joinreal.utils.ChatDialogsManager;
import com.joinreal.utils.ChatImagesUtils;
import com.joinreal.utils.ChatPushNotification;
import com.joinreal.utils.TimeStringUtils;
import com.quickblox.chat.QBChat;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBPrivateChat;
import com.quickblox.chat.QBPrivateChatManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBMessageListener;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBRequestCanceler;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.util.ArrayList;
import java.util.List;


import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Nick on 17.05.2015.
 */
public class PrivateChatActivity extends RealBaseActivity implements OpenPropertyListener {
    public final static String PROPERTY_SAVE_TO_HISTORY = "save_to_history";
    public static final String CONTACT_SERIALIZABLE = "user_serializable";
    public static final String DIALOG = "dialog";

    private StickyListHeadersListView list;
    private TextView send;
    private QBPrivateChatManager privateChatManager;
    private QBPrivateChat privateChat;
    private EditText messageText;
    private ChatAdapter adapter;
    private ProgressBar progressBar;
    private QBDialog dialog;
    private static String activeDialogId;
    private ChatImagesUtils.IImageObject opponent;
    private TextView opponentName;
    private ImageView opponentPhoto;
    private boolean userInTheApp = true;
    private QBUser userIfNewDialog;
    private TextView lastSeen;
    private View notification;
    private boolean databaseLoaded = false;
    private boolean shouldAnimateTransition;
    private boolean visible = false;
    private Prefs prefs;
    private TextView opponentInitials;
    private boolean didSendMessageOnActivityCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_chat);
        prefs = Prefs.getInstance();
        initVarsFromIntent();
        setLayoutContent();
        ChatConnect.initialize(getChatConnectListener());
        setMessagesFromPrefs();
        TypefaceManager.getInstance(this).assignTypeface(this);
        if (shouldAnimateTransition) {
            overridePendingTransition(R.anim.in_from_bottom, R.anim.hold);
        }
        sendMessageIfNeeded();
    }

    private void initVarsFromIntent() {
        Contact contact = (Contact) getIntent().getSerializableExtra(CONTACT_SERIALIZABLE);
        opponent = new ContactVM(contact);
        dialog = (QBDialog) getIntent().getSerializableExtra(DIALOG);
        shouldAnimateTransition = getIntent().getBooleanExtra(StaticParams.KEY_SHOULD_ANIMATE, false);
    }

    private void setLayoutContent() {
        findViewsByIds();
        setOnClickListeners();
        setMessageEditTextView();
        adapter = new ChatAdapter(new ArrayList<ChatMessageData>(), this);
        list.setAdapter(adapter);
        getUserNameAndPhoto();
    }

    private void getUserNameAndPhoto() {
        if (! Application.getClient().isAgent()) {
            opponent = Application.getClient().getAssignedAgent();
            setUiWithOpponontDetails();
            ChatConnect.getUserOrCreate(opponent.getChatLogin(), getUserResponseListener());
        } else if (dialog != null) {
            ChatConnect.getNameAndPhotoFromDialog(dialog, getGetUserListener());
        } else if (opponent != null) {
            ChatConnect.getNameAndPhotoFromLogin(opponent.getChatLogin(), getGetUserListener());
        }
    }

    private void setMessageEditTextView() {
        messageText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (privateChat != null) {
                    try {
                        privateChat.sendIsTypingNotification();
                    } catch (XMPPException e) {
                    } catch (SmackException.NotConnectedException e) {
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (messageText.getText().length() == 0) {
                    send.setTextColor(getResources().getColor(R.color.light_gray));
                    send.setClickable(false);
                } else {
                    send.setTextColor(getResources().getColor(R.color.blue_dark));
                    send.setClickable(true);
                }
            }
        });
    }

    private ChatConnectListener getChatConnectListener() {
        return new ChatConnectListener() {
            @Override
            public void connected() {
                privateChatManager = QBChatService.getInstance().getPrivateChatManager();
                if ((privateChatManager != null) && (dialog != null)) {
                    onChatConnectedSuccessfully();
                }
            }

            @Override
            public void onError(List<String> errors) {
                BusProvider.getRestBus().post(new NetworkExceptionEvent(new NetworkException( "private chat error getting connected : " + errors.get(0)), NetworkCodes.ERROR_CHAT_CONNECT));
            }
        };
    }

    private void onChatConnectedSuccessfully() {
        createPrivateChat(ChatDialogsManager.getOpponentId(dialog), false);
        loadChatHistory();
    }

    private void sendMessageIfNeeded() {
        PropertyViaChatVM propertyToSend = getPropertyToSend();
        if ((! didSendMessageOnActivityCreate) && (propertyToSend != null)) {
            Log.d(Application.CHAT_TAG, "sendMessageIfNeeded");
            sendMessage(propertyToSend);
            didSendMessageOnActivityCreate = true;
        }
    }

    private PropertyViaChatVM getPropertyToSend() {
        if (getIntent().hasExtra(StaticParams.KEY_PROPERTY_FOR_CHAT_OBJ)){
            return (PropertyViaChatVM)getIntent().getParcelableExtra(StaticParams.KEY_PROPERTY_FOR_CHAT_OBJ);
        }
        return null;
    }

    private void setOnClickListeners() {
        View xButton = findViewById(R.id.x_button);
        xButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notification.setVisibility(View.GONE);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(Application.CHAT_TAG, "OnClickListener");
                sendMessage(null);
            }
        });

        ImageView backButton = (ImageView) findViewById(R.id.back_button);
        backButton.setImageResource(Application.getClient().isAgent() ? R.drawable.back_arrow_blue : R.drawable.ic_close);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                if (shouldAnimateTransition) {
                    overridePendingTransition(R.anim.hold, R.anim.out_to_bottom);
                }
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrivateChatActivity.this, MainActivityAgent.class);
                intent.putExtra(StaticParams.KEY_ACTION_TYPE, StaticParams.SEND_INVITE);
                intent.putExtra(StaticParams.KEY_CONTACT_PHONE, opponent.getPhone());
                startActivity(intent);

            }
        });

        findViewById(R.id.header).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (Application.getClient().isAgent()) {
                    intent = new Intent(Application.getAppContext(), MainActivityAgent.class);
                    intent.putExtra(StaticParams.KEY_ACTION_TYPE, StaticParams.OPEN_CONTACT_PROFILE);
                    intent.putExtra(StaticParams.KEY_PHONE_NUM, opponent.getChatLogin());
                } else {
                    intent = new Intent(Application.getAppContext(), MainActivityCustomer.class);
                    intent.putExtra(StaticParams.KEY_ACTION_TYPE, StaticParams.OPEN_AGENT_PROFILE);
                }
                startActivity(intent);
                finish();
            }
        });
    }

    private void findViewsByIds() {
        list = (StickyListHeadersListView) findViewById(R.id.chat_messages);
        send = (TextView) findViewById(R.id.send_message);
        messageText = (EditText) findViewById(R.id.message_text);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        opponentPhoto = (ImageView) findViewById(R.id.opponent_photo);
        opponentInitials = (TextView) findViewById(R.id.opponent_initials);
        opponentName = (TextView) findViewById(R.id.chat_opponent_name);
        lastSeen = (TextView) findViewById(R.id.last_seen_chat);
        notification = findViewById(R.id.send_notification);

    }

    private void sendMessage(PropertyViaChatVM propertyOnChat) {
        if ((privateChat != null) && ((hasTextMsgToSend() || hasPropertyToSend(propertyOnChat)))) {
            Log.d(Application.CHAT_TAG, "sendMessage in if");
            performMessageSending(propertyOnChat);
        } else {
            Log.d(Application.CHAT_TAG, "sendMessage in else");
            handleIfPrivateChatIsNull();
        }
    }

    private boolean hasPropertyToSend(PropertyViaChatVM propertyOnChat) {
        return (propertyOnChat != null) || (getIntent().hasExtra(StaticParams.KEY_PROPERTY_FOR_CHAT_OBJ));
    }

    private boolean hasPropertyToSend() {
        return hasPropertyToSend(null);
    }

    private boolean hasTextMsgToSend() {
        return (messageText.getText() != null) && (! messageText.getText().toString().equals(""));
    }

    private void performMessageSending(PropertyViaChatVM propertyViaChatVM) {
        QBChatMessage msg = getMessage(propertyViaChatVM);
        boolean didSendMessageSucceed = tryToSendMessage(msg);
        if (! didSendMessageSucceed) {
            return;
        }

        sendMsgIfUserHasntDownloadedApp();
        setMessageSentUi(msg);
        sendNewMsgPN();
        Prefs.saveMessages(dialog.getDialogId(), adapter.getMessages());
    }

    private void setMessageSentUi(QBChatMessage msg) {
        Log.d(Application.CHAT_TAG, "setMessageSentUi");

        messageText.setText("");
        msg.setDateSent(System.currentTimeMillis() / 1000);
        showMessage(msg);
    }

    private boolean tryToSendMessage(QBChatMessage msg) {
        try {
            Log.d(Application.CHAT_TAG, "tryToSendMessage");
            privateChat.sendMessage(msg);
        } catch (XMPPException e) {
            Crashlytics.log(getClass().getSimpleName() + " sendMessage --> XMPPException e : " + e.getMessage());
            Toast.makeText(this, getString(R.string.cant_send_message_error) + NetworkCodes.SEND_MESSAGE_XMPPException, Toast.LENGTH_LONG).show();
            return false;
        } catch (SmackException.NotConnectedException e) {
            Crashlytics.log(getClass().getSimpleName() + " sendMessage --> SmackException.NotConnectedException e : " + e.getMessage());
            Toast.makeText(this, getString(R.string.cant_send_message_error) + NetworkCodes.SEND_MESSAGE_NotConnectedException, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void handleIfPrivateChatIsNull() {
        if (privateChat == null) { //TODO dont think is needed
            if (userIfNewDialog != null) {
                createPrivateChat(userIfNewDialog.getId(), (hasTextMsgToSend() || hasPropertyToSend())); //TODO: maybe false?
                progressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    private void sendNewMsgPN() {
        StringifyArrayList<Integer> ar = new StringifyArrayList<Integer>();
        ar.add(ChatDialogsManager.getOpponentId(dialog));
        ChatPushNotification.sendMessagePush(ar, "New Message", dialog.getDialogId());
    }

    private void sendMsgIfUserHasntDownloadedApp() {
        if (!userInTheApp) {
            new SendMessageTask(opponent.getPhone(), messageText.getText().toString()).execute();
            ChatDialogInfo chatDialogInfo = new ChatDialogInfo();
            chatDialogInfo.setSentByEmail(!userInTheApp);
            chatDialogInfo.setMessageRead(false);
            prefs.saveDialogInfo(dialog.getDialogId(), chatDialogInfo);
        }
    }

    private QBChatMessage getMessage(PropertyViaChatVM propertyOnChat) {
        Log.d(Application.CHAT_TAG, "getMessage");

        QBChatMessage msg = new QBChatMessage();
        msg.setMarkable(true);
        if (hasPropertyToSend()) {
            propertyOnChat = getIntent().getParcelableExtra(StaticParams.KEY_PROPERTY_FOR_CHAT_OBJ);
        }
        if (propertyOnChat == null) {
            msg.setBody(messageText.getText().toString());
        } else {
            msg.setProperty(StaticParams.KEY_PROPERTY_DISPLAY_IMAGE_URL, propertyOnChat.getDisplayImgUrl());
        }
        msg.setProperty(PROPERTY_SAVE_TO_HISTORY, "1");

        if (propertyOnChat != null) {
            msg.setProperty(StaticParams.KEY_MLS_ID, propertyOnChat.getMlsId());
            msg.setProperty(StaticParams.KEY_MLS_NUM, propertyOnChat.getMlsnum());
            msg.setProperty(StaticParams.KEY_PROPERTY_DISPLAY_IMAGE_URL, propertyOnChat.getDisplayImgUrl());

            setIntent(new Intent());
        }
        return msg;
    }

    private ChatConnect.GetContactListener getGetUserListener() {
        return new ChatConnect.GetContactListener() {
            @Override
            public void onContactFound(final Contact contact) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        PrivateChatActivity.this.opponent = new ContactVM(contact);
                        setUiWithOpponontDetails();
                        ChatConnect.getUserOrCreate(opponent.getChatLogin(), getUserResponseListener());
                    }
                });
            }

            @Override
            public void onContactNotFound() {
                System.out.println("onContactNotFound");
            }
        };
    }

    public ChatConnect.UserResponseListener getUserResponseListener() {
        return new ChatConnect.UserResponseListener() {
            @Override
            public void onSuccess(QBUser user, boolean created) {
                Application.addUserPhone(user.getId(), user.getLogin());
                userIfNewDialog = user;
                if (user.getTags().contains(Application.SMS) && Application.getClient().isAgent()) {
                    userInTheApp = false;
                    notification.setVisibility(View.VISIBLE);
                    adapter.setuserInTheApp(false);
                }
                getPrivateChat(user.getId());
                progressBar.setVisibility(View.GONE);
                if (user.getLastRequestAt() != null) {
                    lastSeen.setText(getResources().getString(R.string.last_seen) + " " + TimeStringUtils.secondsToTime(user.getLastRequestAt().getTime() / 1000));
                }
            }

            @Override
            public void onError(List<String> errors) {

            }
        };
    }

    public void setUiWithOpponontDetails() {
        if (opponent.getDisplayName() != null) {
            opponentName.setText(opponent.getDisplayName());
        } else {
            opponentName.setText(opponent.getPhone());
        }
        ChatImagesUtils.setContactProfileThumb(opponent, opponentPhoto, opponentInitials);

        if (adapter != null) {
            adapter.setContact(opponent);
            adapter.notifyDataSetChanged();
        }
    }

    private void setMessagesFromPrefs() {
        if ((!databaseLoaded) && (dialog != null) && (adapter != null)) {
            ChatMessagesArray data = prefs.getMessages(dialog.getDialogId());
            if (data != null) {
                adapter.setMessages(data.getData());
                progressBar.setVisibility(View.GONE);
                scrollDown();
                databaseLoaded = true;
            }
        }
    }

    private final QBMessageListener messageListener = getQbMessageListener();

    private QBMessageListener getQbMessageListener() {
        return new QBMessageListener() {
            @Override
            public void processMessage(QBChat qbChat, final QBChatMessage qbChatMessage) {
                if (Application.isActivityVisible() && visible) {
//                    showMessage(qbChatMessage);

                    markMessagesAsReadOnService(qbChatMessage);

                    markMessagesAsReadOnUi();

                    Log.d(Application.CHAT_TAG, "processMessage");
                }
            }

            @Override
            public void processError(QBChat qbChat, QBChatException e, QBChatMessage qbChatMessage) {

            }

            @Override
            public void processMessageDelivered(QBChat qbChat, String s) {

            }

            @Override
            public void processMessageRead(QBChat qbChat, String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.markMessagesRead();
                        prefs.saveMessages(dialog.getDialogId(), adapter.getMessages());
                        ChatDialogInfo chatDialogInfo = new ChatDialogInfo();
                        chatDialogInfo.setMessageRead(true);
                        prefs.saveDialogInfo(dialog.getDialogId(), chatDialogInfo);
                    }
                });
            }
        };
    }

    public void markMessagesAsReadOnUi() {
        final StringifyArrayList<Integer> users = new StringifyArrayList<>();
        users.add(ChatDialogsManager.getOpponentId(dialog));

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.markMessagesRead();
                ChatPushNotification.sendMessageRead(users, dialog.getDialogId());
                ChatConnect.getUserOrCreate(opponent.getPhone(), new ChatConnect.UserResponseListener() {
                    @Override
                    public void onSuccess(QBUser user, boolean created) {
                        if (user.getLastRequestAt() != null) {
                            lastSeen.setText(getResources().getString(R.string.last_seen) + " " + TimeStringUtils.secondsToTime(user.getLastRequestAt().getTime() / 1000));
                        }
                    }

                    @Override
                    public void onError(List<String> errors) {

                    }
                });
            }
        });
    }

    public void markMessagesAsReadOnService(QBChatMessage qbChatMessage) {
        StringifyArrayList<String> ids = new StringifyArrayList<String>();
        ids.add(qbChatMessage.getId());
        try {
            QBChatService.markMessagesAsRead(dialog.getDialogId(), ids);
            privateChat.readMessage(qbChatMessage.getId());
        } catch (QBResponseException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
        } catch (SmackException.NotConnectedException e) {
        }
    }

    public void showMessage(final QBChatMessage message) {
        Log.d(Application.CHAT_TAG, "showMessage");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.add(message);
                scrollDown();
                prefs.saveMessages(dialog.getDialogId(), adapter.getMessages());
            }
        });
    }

    private void loadChatHistory() {
        Log.d(Application.CHAT_TAG, "loadChatHistory");

        if (dialog != null) {
            QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
            customObjectRequestBuilder.setPagesLimit(50);
            customObjectRequestBuilder.sortDesc("date_sent");
            setOnGetMessagesListener(customObjectRequestBuilder);
        }
    }

    @NonNull
    private QBRequestCanceler setOnGetMessagesListener(QBRequestGetBuilder customObjectRequestBuilder) {
        return QBChatService.getDialogMessages(dialog, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(final ArrayList<QBChatMessage> messages, Bundle args) {
                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      boolean needUpdate;

                                      ChatMessageData lastCachedMessage = null;
                                      QBChatMessage lastNewMessage = null;
                                      if (adapter.getMessages().size() > 0) {
                                          lastCachedMessage = adapter.getMessages().get(adapter.getMessages().size() - 1);
                                      }
                                      if (messages.size() > 0) { //from qb messages return from oldest to newest
                                          lastNewMessage = messages.get(0);
                                      }
                                      needUpdate = (lastCachedMessage == null || lastNewMessage == null
                                              || lastCachedMessage.getDateSent() < lastNewMessage.getDateSent() ||
                                              lastCachedMessage.getSenderId() == null);

                                      progressBar.setVisibility(View.GONE);

                                      if (needUpdate) {
                                          updateChat(messages);
                                      }

                                      saveToPrefs(lastNewMessage);

                                      sendReadMsgPN();
                                  }

                              }

                );
            }

            @Override
            public void onError(List<String> errors) {
            }
        });
    }

    private void sendReadMsgPN() {
        StringifyArrayList<Integer> users = new StringifyArrayList<>();
        users.add(ChatDialogsManager.getOpponentId(dialog));
        ChatPushNotification.sendMessageRead(users, dialog.getDialogId());
    }

    private void saveToPrefs(QBChatMessage lastNewMessage) {
        prefs.saveMessages(dialog.getDialogId(), adapter.getMessages());
        if (lastNewMessage != null && new ChatMessageData(lastNewMessage).isOutgoing()) {
            prefs.saveDialogInfo(dialog.getDialogId(), lastNewMessage.isRead());
        }
    }

    private void updateChat(final ArrayList<QBChatMessage> newMessages) {
        if (newMessages != null && newMessages.size() > 0) {
            adapter.clearLocalMessages(newMessages.get(newMessages.size() - 1).getDateSent());
        }

        addNewMessagesToAdapter(newMessages);

        scrollDown();

        markFirstMessageAsRead(newMessages);
    }

    private void markFirstMessageAsRead(final ArrayList<QBChatMessage> messages) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (privateChat != null && messages.size() > 0) {
                        privateChat.readMessage(messages.get(0).getId());
                    }
                    Log.d(Application.CHAT_TAG, "marked");
                } catch (XMPPException e) {
                    e.printStackTrace();
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void addNewMessagesToAdapter(ArrayList<QBChatMessage> messages) {
        long lastCachedMessageDate = getLastCachedMessageDate();
        for (int i = messages.size() - 1; i >= 0; i--) {
            if (messages.get(i).getDateSent() > lastCachedMessageDate) {
                adapter.add(messages.get(i));
            }
        }
        adapter.notifyDataSetChanged();
    }

    private long getLastCachedMessageDate() {
        long lastCachedMessageDate;
        if (adapter.getMessages().size() > 0) {
            ChatMessageData lastCachedMessage = adapter.getMessages().get(adapter.getMessages().size() - 1);
            lastCachedMessageDate = lastCachedMessage.getDateSent();
        } else {
            lastCachedMessageDate = 0;
        }
        return lastCachedMessageDate;
    }

    private void scrollDown() {
        if (adapter.getCount() > 0) {
            list.setSelection(adapter.getCount() - 1);
        }
    }

    private QBPrivateChat getPrivateChat(final Integer userId) {
        setDialogIfExistsWithThisUser(userId);
        QBPrivateChat chat = null;
        chat = privateChatManager.getChat(userId);

        if (chat != null) {
            chat.addMessageListener(messageListener);
            if (dialog != null) {
                setPrivateChat(userId);
            }
        } else {
            if (dialog != null) {
                privateChatManager.createDialog(userId, getCreateDialogCallback(userId, (hasTextMsgToSend() || hasPropertyToSend())));
            }
        }
        return chat;
    }

    public void setPrivateChat(Integer userId) {
        privateChat = privateChatManager.getChat(userId);
        if (privateChat != null) {
            privateChat.addMessageListener(messageListener);
        }
        setMessagesFromPrefs();
            loadChatHistory();
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setDialogIfExistsWithThisUser(Integer userId) {
        List<ChatDialogDataVM> dialogs = prefs.getDialogs();
        if (dialogs != null) {
            for (ChatDialogDataVM dialog : dialogs) {
                if (dialog.getOccupants().contains(userId)) {
                    this.dialog = dialog.getQBDialog();
                    activeDialogId = dialog.getDialogId();
                }
            }
        }
    }

    private void createPrivateChat(final Integer userId, final boolean shouldSendMessage) {
        privateChat = getPrivateChat(userId);
        if (privateChat == null) {
            privateChatManager.createChat(userId, null);
            privateChatManager.createDialog(userId, getCreateDialogCallback(userId, shouldSendMessage));
        }
    }

    private QBEntityCallbackImpl<QBDialog> getCreateDialogCallback(final Integer userId, final boolean shouldSendMessage) {
        return new QBEntityCallbackImpl<QBDialog>() {
            @Override
            public void onSuccess(QBDialog result, Bundle params) {
                Log.d(Application.CHAT_TAG, "getCreateDialogCallback.onSuccess()");
                super.onSuccess(result, params);
                dialog = result;
                activeDialogId = dialog.getDialogId();
                setPrivateChat(userId);
                if (shouldSendMessage) {
                    sendMessage(null);
                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        visible = true;
        if (dialog != null) {
            activeDialogId = dialog.getDialogId();
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        visible = false;
        activeDialogId = null;
    }

    @Override
    public void openPropertyPage(String mlsid, String mlsnum, ArrayList<String> photoList) {
        //TODO: unite with main activity
        Intent intent = new Intent(this, PropertyPageActivity.class);
        intent.putExtra(StaticParams.KEY_MLS_ID, mlsid);
        intent.putExtra(StaticParams.KEY_MLS_NUM, mlsnum);
        intent.putExtra(StaticParams.KEY_PHOTO_LIST, photoList);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (shouldAnimateTransition) {
            overridePendingTransition(R.anim.hold, R.anim.out_to_bottom);
        }
    }

    public static String getActiveDialogID() {
        return activeDialogId;
    }

    @Override
    protected String getScreenName() {
        return "Private Chat Activity";
    }
}
