package com.joinreal.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;

import com.joinreal.R;
import com.joinreal.app.activities.chat.PrivateChatActivity;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.fragments.AgentProfileReadOnlyFragment;
import com.joinreal.app.fragments.CustomerProfileFragment;
import com.joinreal.app.fragments.FavoritesContainerFragment;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.OpenPropertyEvent;
import com.joinreal.events.PNActionEvent;
import com.joinreal.events.ShouldFetchDialogsEvent;
import com.joinreal.events.StartActivityEvent;
import com.joinreal.model.view.PropertyViaChatVM;
import com.joinreal.utils.ChatMainBottomBarPresenter;
import com.squareup.otto.Subscribe;


public class MainActivityCustomer extends MainActivityAbstract {

    private ChatMainBottomBarPresenter chatMainBottomBarPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_customer);
        super.onCreateActivity();
        setupBottomChatBar();
    }

    private void setupBottomChatBar() {
        chatMainBottomBarPresenter = new ChatMainBottomBarPresenter(this);
        chatMainBottomBarPresenter.init(findViewById(android.R.id.content));
    }

    @Override
    protected void onResume() {
        super.onResume();
        chatMainBottomBarPresenter.onActivityResumed();

    }

    @Subscribe
    public void onShouldFetchDialogsEvent(ShouldFetchDialogsEvent e) {
        chatMainBottomBarPresenter.onNewMessageReceived();
    }


    @Override
    public void openFavoritesFragment(Bundle bd) {
        bd.putBoolean(StaticParams.KEY_IS_FAVORITES_LIST, true);
        FavoritesContainerFragment fragment = FavoritesContainerFragment.getNewInstance(bd);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, FavoritesContainerFragment.TAG)
                .addToBackStack(FavoritesContainerFragment.TAG)
                .commit();
    }

    @Subscribe
    public void openPropertyActivityEvent(OpenPropertyEvent event) {
        startPropertyActivity(event);
    }

    @Override
    public void sendPropertyViaChat(PropertyViaChatVM property) {
        Intent intent = new Intent(this, PrivateChatActivity.class);
        intent.putExtras(chatMainBottomBarPresenter.getExtrasForPrivateChat());
        intent.putExtra(StaticParams.KEY_PROPERTY_FOR_CHAT_OBJ, property);
        startActivity(intent);
    }

    @Override
    public void openPrivateChat(Intent intent) {
        super.openPrivateChat(intent);
        cancelNotificationFromStatusBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_customer, menu);
        this.menu = menu;
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if ((getSupportFragmentManager().findFragmentByTag(CustomerProfileFragment.TAG) != null)
                        || (getSupportFragmentManager().findFragmentByTag(AgentProfileReadOnlyFragment.TAG) != null)) {
                    onBackPressed();
                } else {
                    openCustomerProfile();
                }
                return true;

            case R.id.action_search:
                openFiltersActivity(false);
                Analytics.getInstance().trackToolbarNavigtionEvent(getScreenName(), "Search");
                return true;

            case R.id.action_favorites:
                openFavoritesFragment(new Bundle());
                Analytics.getInstance().trackOverFlowNavigtionEvent(getScreenName(), "Favorites");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void openCustomerProfile() {
        CustomerProfileFragment fragment = CustomerProfileFragment.getNewInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, CustomerProfileFragment.TAG)
                .addToBackStack(CustomerProfileFragment.TAG)
                .commit();
    }

    @Override
    protected void handleIntent(Intent recievedIntent) {
        if ((recievedIntent != null) && (recievedIntent.getExtras() != null)) {
            int actionCode = recievedIntent.getIntExtra(StaticParams.KEY_ACTION_TYPE, -1);
            switch (actionCode) {
                case StaticParams.OPEN_AGENT_PROFILE:
                    openAgentProfileFragment();
                    setIntent(null);
                    break;
                case StaticParams.OPEN_FAVORITES:
                    openFavoritesFragment(new Bundle());
                    setIntent(null);
                    break;
                default:
                    super.handleIntent(recievedIntent);
            }
        }
    }

    @NonNull
    @Override
    protected Bundle getInitialSearchBundle() {
        Bundle bd = super.getInitialSearchBundle();
        bd.putBoolean(StaticParams.KEY_SHOULD_LOCK_DRAWER, true);
        return bd;
    }

    protected void openAgentProfileFragment() {
        AgentProfileReadOnlyFragment fragment = AgentProfileReadOnlyFragment.getNewInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, AgentProfileReadOnlyFragment.TAG)
                .addToBackStack(AgentProfileReadOnlyFragment.TAG)
                .commit();
    }

    @Subscribe
    public void onStartActivityEvent(StartActivityEvent event) {
        super.onStartActivityEvent(event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == REQUEST_OPEN_PROPERTY_PAGE) && (resultCode == RESULT_OK)) {
            if (data != null) {
                if (data.getIntExtra(StaticParams.KEY_ACTION_TYPE, -1) == StaticParams.OPEN_FAVORITES) {
                    openFavoritesFragment(new Bundle());
                    return;
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.getBus().register(chatMainBottomBarPresenter);
    }

    @Override
    protected void onStop() {
        BusProvider.getBus().unregister(chatMainBottomBarPresenter);
        super.onStop();
    }

}
