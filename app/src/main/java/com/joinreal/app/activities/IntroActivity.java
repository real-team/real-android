package com.joinreal.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.joinreal.R;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.PNActionEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by britt on 5/31/15.
 */
public class IntroActivity extends RealBaseActivity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        findViewById(R.id.start_button).setBackground(Helper.getTintedDrawable(R.drawable.rounded_rectangle_white, getResources().getColor(R.color.blue)));
        findViewById(R.id.start_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                IntroActivity.this.startActivity(intent);
                IntroActivity.this.finish();
            }
        });

        TypefaceManager.getInstance(this).assignTypeface(this);
    }

    @Override
    protected String getScreenName() {
        return "Intro Activity";
    }

}
