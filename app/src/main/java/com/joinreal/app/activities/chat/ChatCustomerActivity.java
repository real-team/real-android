package com.joinreal.app.activities.chat;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.RealBaseActivity;
import com.joinreal.app.common.Prefs;
import com.joinreal.app.common.StaticParams;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.ChatDialogsReceivedEvent;
import com.joinreal.events.PNActionEvent;
import com.joinreal.events.ShouldFetchDialogsEvent;
import com.joinreal.model.data.Contact;
import com.joinreal.model.data.chat.ChatDialogDataVM;
import com.joinreal.utils.ChatConnect;
import com.joinreal.utils.ChatConnectListener;
import com.joinreal.utils.ChatDialogsManager;
import com.joinreal.utils.ChatPushNotification;
import com.quickblox.core.helper.StringifyArrayList;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nick on 02.06.2015.
 */
public class ChatCustomerActivity extends RealBaseActivity {

    private ChatDialogDataVM dialog;
    private View newAgentMessage;
    private View inputMessage;
    private ImageView agentImage;
    private TextView newMessageFrom;
    private TextView newMessage;
    private boolean firstOpened = true;
    private TextView unreadCount;
    private boolean firstInstall = false;

    @Override
    protected String getScreenName() {
        return "ChatCustomerActivity";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_chat_bottom_bar);
        System.out.println("-                        - ChatCustomerActivity");
        ChatConnect.initialize(new ChatConnectListener() {
            @Override
            public void connected() {
                if (! Prefs.getInstance().isSubscribed()) {
                    Application.subscribeToPushNotifications();
                    firstInstall = true;
                }
                getDialogs();
            }

            @Override
            public void onError(List<String> errors) {

            }
        });

        newAgentMessage = findViewById(R.id.new_agent_message_layout);
        inputMessage = findViewById(R.id.input_message);
        agentImage = (ImageView) findViewById(R.id.image);
        newMessage = (TextView) findViewById(R.id.message_text_from_agent);
        newMessageFrom = (TextView) findViewById(R.id.new_message_from);
        unreadCount = (TextView) findViewById(R.id.unread_agent_count);

        newAgentMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChat();
            }
        });
        inputMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChat();
            }
        });

        List<ChatDialogDataVM> dialogsFromDatabase = Prefs.getInstance().getDialogs();
        if (dialogsFromDatabase != null && dialogsFromDatabase.size() > 0) {
            dialog = dialogsFromDatabase.get(0);
            showMessage(dialog);
        }

    }

    private void openChat() {
        if (dialog != null) {
            System.out.println("dialog != null");
            Intent intent = new Intent(Application.getAppContext(), PrivateChatActivity.class);
            intent.putExtras(getIntent());
            intent.putExtra(PrivateChatActivity.DIALOG, dialog.getQBDialog());
            intent.putExtra(StaticParams.KEY_SHOULD_ANIMATE, true);
            startActivity(intent);
        }
    }

    private void getDialogs() {
        ChatDialogsManager.getInstance().getDialogsNetwork();
    }

    @Subscribe
    public void onNewDialogsRecievedEvent(ChatDialogsReceivedEvent e) {
        for (ChatDialogDataVM agentDialog : e.getDialogs()) {
            if (ChatDialogsManager.getOpponentId(agentDialog.getQBDialog()) == Application.AGENT_ID || Application.FILTER_AGENT_DIALOG == false) {
                dialog = agentDialog;
                showMessage(dialog);
                if (firstInstall) {
                    firstInstall = false;
                    StringifyArrayList<Integer> ar = new StringifyArrayList<Integer>();
                    ar.add(ChatDialogsManager.getOpponentId(dialog.getQBDialog()));
                    ChatPushNotification.sendMessagePush(ar, "One of your customers installed the app", agentDialog.getDialogId());
                }
                break;
            }
        }

        if (dialog != null) {
            ArrayList<ChatDialogDataVM> list = new ArrayList<>();
            list.add(dialog);
            Prefs.getInstance().saveDialogs(list);
        }
//                if (dialogAdapter != null) {
//                    Prefs.saveDialogs(dialogAdapter.getDataSource());
//                }

    }

    private void showMessage(final ChatDialogDataVM dialog) {
        if (dialog.getUnreadMessageCount() > 0) {
            newMessage.setText(dialog.getLastMessage());
            unreadCount.setText(dialog.getUnreadMessageCount() + "");
            ChatConnect.getNameAndPhotoFromDialog(this.dialog.getQBDialog(), new ChatConnect.GetContactListener() {
                @Override
                public void onContactFound(Contact contact) {
                    newAgentMessage.setVisibility(View.VISIBLE);
                    inputMessage.setVisibility(View.GONE);
                    if (contact.getDisplayName() != null) {
                        newMessageFrom.setText(getResources().getString(R.string.new_message_from) + " " + contact.getDisplayName());
                    } else {
                        newMessageFrom.setText(contact.getPhone());
                    }
                    if (contact.getBitmap() != null) {
                        agentImage.setImageBitmap(contact.getBitmap());
                    }
                }

                @Override
                public void onContactNotFound() {
                    System.out.println("showMessage.onContactNotFound getNameAndPhotoFromDialog, dialog id : " + dialog.getQBDialog());

                }
            });
        } else {
            newAgentMessage.setVisibility(View.GONE);
            inputMessage.setVisibility(View.VISIBLE);
        }
    }

//    private BroadcastReceiver receiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (intent.getAction().equals(Application.NEW_MESSAGE)) {
//                getDialogs();
//            }
//        }
//    };

    @Subscribe
    public void onShouldFetchDialogsEvent(ShouldFetchDialogsEvent e) {
        getDialogs();

    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
//        filter.addAction(Application.NEW_MESSAGE);
//
//        registerReceiver(receiver, filter);
        if (!firstOpened) {
            getDialogs();
        }
        firstOpened = false;

        //TODO: should refactor this activity
//        System.out.println("before openChat");
//        openChat();
//        System.out.println("after openChat");
    }

}
