package com.joinreal.app.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.EditText;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.fragments.VerifyUserFragment;
import com.joinreal.app.api.RealClient;
import com.joinreal.app.common.StaticParams;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.PNActionEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.User;
import com.squareup.otto.Subscribe;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by britt on 5/31/15.
 */
public class LoginActivity extends RealBaseActivity {

    private RealClient client;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
        client = Application.getClient();
        TypefaceManager.getInstance(this).assignTypeface(this);
    }


    @OnClick(R.id.next_button)
    public void onNextBackButtonClick() {
        EditText phoneNumberEditText = (EditText) findViewById(R.id.phone_number_edit_text);
        final String phoneNumber = phoneNumberEditText.getText().toString();

        sendUserPassword(client, phoneNumber);
        openVerifyUserFrag(phoneNumber);

    }

    private void sendUserPassword(final RealClient client, final String phoneNumber) {
        AsyncTask signUpTask = new AsyncTask<Object, Void, Void>() {
            @Override
            protected Void doInBackground(Object... params) {
                try {
                    client.signUpOrResetPassword(new User(phoneNumber));
                } catch (NetworkException e) {
                    BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.ERROR_SIGN_IN));
                }
                return null;
            }
        };
        signUpTask.execute();
    }

    private void openVerifyUserFrag(String phoneNumber) {
        Bundle bd = new Bundle();
        bd.putString(StaticParams.KEY_PHONE_NUM, phoneNumber);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.login_activity_layout, VerifyUserFragment.getNewInstance(bd), VerifyUserFragment.TAG)
                .addToBackStack(VerifyUserFragment.TAG)
                .commit();
    }

    @Override
    protected String getScreenName() {
        return "Login Activity";
    }
}
