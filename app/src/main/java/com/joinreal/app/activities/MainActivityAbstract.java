package com.joinreal.app.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.chat.PrivateChatActivity;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.fragments.CropImageFragment;
import com.joinreal.app.fragments.PropertyListContainerFragment;
import com.joinreal.app.api.RealClient;
import com.joinreal.app.common.Prefs;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.fragments.WebviewFragment;
import com.joinreal.app.interfaces.INavigationListener;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.app.interfaces.OpenSearchFiltersListener;
import com.joinreal.events.StartActivityEvent;
import com.joinreal.model.data.SearchFilter;
import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.PropertyViaChatVM;
import com.joinreal.utils.ChatConnect;
import com.joinreal.utils.ChatConnectListener;
import com.joinreal.utils.ChatDialogsManager;
import com.joinreal.utils.ChatKeyboardUtils;
import com.quickblox.chat.model.QBDialog;

import com.joinreal.events.OpenPropertyEvent;

import java.util.ArrayList;
import java.util.List;


public abstract class MainActivityAbstract extends RealBaseActivity implements OpenSearchFiltersListener, OpenChatListener, INavigationListener {

    protected static final int REQUEST_OPEN_FILTERS = 98;
    protected static final int REQUEST_OPEN_PROPERTY_PAGE = 88;

    public static final int ACTION_CANCEL_ID = 2342;
    private static final int DELAY_FOR_CLICKING_TWICE_TO_QUITE = 2000;
    protected RealClient client;
    protected Toolbar toolbar;
    protected TextView titleTv;
    protected Menu menu;
    protected ArrayList<QBDialog> userDialogs;
    protected boolean firstCreated = true;
    protected boolean needUpdate = false;
    protected boolean visible = true;
    protected static final Handler handler = new Handler();
    protected final Runnable refreshDialogs = new Runnable() {
        @Override
        public void run() {
            if (visible) {
                getDialogsFromNetwork();
                handler.postDelayed(refreshDialogs, 15000);
            }
        }
    };
    public static boolean didComeFromAnotherActivityOrOnCreate;
    private View container;
    private boolean didPressToQuite;

    protected void onCreateActivity() {
        client = Application.getRealClient();
        client.setPropertyMetadata();
        setupToolBar();
        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    public void onBackStackChanged() {
                        invalidateOptionsMenu();
                    }
                });
        userDialogs = new ArrayList<QBDialog>();
        initDialogs();

        container = findViewById(R.id.container);

        openInitialSearchResults();

        TypefaceManager.getInstance(this).assignTypeface(this);
    }

    protected void openInitialSearchResults() {
        Bundle bd = getInitialSearchBundle();
        openProperyListFragment(bd);
    }

    @NonNull
    protected Bundle getInitialSearchBundle() {
        Bundle bd = new Bundle();
        bd.putInt(StaticParams.KEY_ACTION_TYPE, StaticParams.OPEN_PROPERTY_LIST);
        bd.putString(StaticParams.KEY_TITLE_PROPERTY_LIST, "Listings");
        bd.putString(StaticParams.KEY_FILTERS_JSON, getInitialFilterJson());
        bd.putString(StaticParams.KEY_STATE, "Texas");
        return bd;
    }

    private String getInitialFilterJson() {
        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("latitude", latitude);
//        jsonObject.addProperty("longitude", longitude);
        jsonObject.addProperty("state", "Texas");
        return new Gson().toJson(jsonObject);
    }

    protected void initDialogs() {
        ChatConnect.initialize(new ChatConnectListener() {

            @Override
            public void connected() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getDialogsFromNetwork();
                        if (!Prefs.getInstance().isSubscribed()) {
                            Application.subscribeToPushNotifications();
                        }
                    }
                });
            }

            @Override
            public void onError(List<String> errors) {
                Log.e(Application.CHAT_TAG, "intializing error: " + errors.toString());
                Helper.handleError("on MainActivityAbstract.initDialogs() error : " + errors.toString(), NetworkCodes.ERROR_INIT_DIALOGS);
            }
        });
    }

    protected void handleIntent(Intent recievedIntent) {
        if ((recievedIntent != null) && (recievedIntent.getExtras() != null)) {
            if (recievedIntent.getStringExtra(StaticParams.KEY_INTENT_SOURCE) != null) {
                if (recievedIntent.getStringExtra(StaticParams.KEY_INTENT_SOURCE).equals(StaticParams.SOURCE_PN_CHAT)) {
                    handleCameFromChatPN(recievedIntent.getExtras());
                }
            } else {
                int actionCode = recievedIntent.getIntExtra(StaticParams.KEY_ACTION_TYPE, -1);
                switch (actionCode) {
                    case StaticParams.OPEN_PROPERTY_LIST:
                        openProperyListFragment(recievedIntent.getExtras());
                        break;
                    case StaticParams.OPEN_PROPERTY_PAGE:
                        String mlsId = recievedIntent.getStringExtra(StaticParams.KEY_MLS_ID);
                        String mlsNum = recievedIntent.getStringExtra(StaticParams.KEY_MLS_NUM);
                        ArrayList<String> photos = recievedIntent.getStringArrayListExtra(StaticParams.KEY_PHOTO_LIST);
                        if ((mlsId != null) && (mlsNum != null)) {
                            startPropertyActivity(new OpenPropertyEvent(mlsId, mlsNum, photos));
                        }
                        break;
                    case StaticParams.SEND_PROPERTY_VIA_CHAT:
                        sendPropertyViaChat((PropertyViaChatVM) recievedIntent.getParcelableExtra(StaticParams.KEY_PROPERTY_FOR_CHAT_OBJ));
                        break;
                }
            }
        }
        setIntent(null);
    }

    public void handleCameFromChatPN(Bundle extras) {
        Analytics.getInstance().trackEvent("Open App Source", "Chat PN");
        Crashlytics.log("App Open From PN");
        Crashlytics.setString(StaticParams.KEY_INTENT_SOURCE, StaticParams.SOURCE_PN_CHAT);
    }

    @Override
    public void getDialogsFromNetwork() {
        ChatDialogsManager.getInstance().getDialogsNetwork();
    }


    @Override
    protected void onResume() {
        super.onResume();
        handleIntent(getIntent());

        if (!firstCreated || needUpdate) {
            getDialogsFromNetwork();
        }
        firstCreated = false;
        visible = true;

        handler.postDelayed(refreshDialogs, 15000);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);

    }

    protected void setupToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        titleTv = (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.user_profile);
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowTitleEnabled(false);
            ab.show();
        }
    }


    @Override
    public void setToolbarTitle(String title) {
        titleTv.setText(title);
    }

    @Override
    public void setHomeIcon(int drawableId) {
        getSupportActionBar().setHomeAsUpIndicator(drawableId);
    }


    @Override
    public void openWebviewFragment(String url) {
        WebviewFragment fragment = WebviewFragment.getNewInstance(url);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, WebviewFragment.TAG)
                .addToBackStack(WebviewFragment.TAG)
                .commit();
    }


    @Override
    public void openProperyListFragment(Bundle bd) {
        PropertyListContainerFragment fragment = PropertyListContainerFragment.getInstance(bd);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, PropertyListContainerFragment.TAG)
                .addToBackStack(PropertyListContainerFragment.TAG)
                .commit();
    }

    @Override
    public abstract void openFavoritesFragment(Bundle bd);

    public void startPropertyActivity(OpenPropertyEvent event) {
        long t = System.currentTimeMillis();
        Intent intent = new Intent(this, PropertyPageActivity.class);
        intent.putExtra(StaticParams.KEY_MLS_ID, event.getMlsId());
        intent.putExtra(StaticParams.KEY_MLS_NUM, event.getMlsNum());
        intent.putExtra(StaticParams.KEY_PHOTO_LIST, event.getPhotoList());
        startActivityForResult(intent, REQUEST_OPEN_PROPERTY_PAGE);
        didComeFromAnotherActivityOrOnCreate = true;
        System.out.println("*** startPropertyActivity time : " + (System.currentTimeMillis() - t));
    }


    @Override
    public void openFiltersActivity(boolean shouldResetFilter) {
        Intent intent = new Intent(this, FiltersActivity.class);
        intent.putExtra(StaticParams.KEY_SHOULD_RESET_FILTER, shouldResetFilter);
        startActivityForResult(intent, REQUEST_OPEN_FILTERS);
        didComeFromAnotherActivityOrOnCreate = true;
    }

    @Override
    public void openFiltersActivityForContact(String contactId, String displayName, SearchFilter filter) {
        Intent intent = new Intent(this, FiltersActivity.class);
        intent.putExtra(StaticParams.KEY_CONTACT_ID, contactId);
        intent.putExtra(StaticParams.KEY_CONTACT_NAME, displayName);
        intent.putExtra(StaticParams.KEY_SHOULD_LOCK_DRAWER, true);
        intent.putExtra(StaticParams.KEY_SEARCH_FILTER, filter);
        startActivityForResult(intent, REQUEST_OPEN_FILTERS);
        didComeFromAnotherActivityOrOnCreate = true;

        //TODO: add animation
    }

    @Override
    public void openPrivateChat(Intent intent) {
        startActivity(intent);
        didComeFromAnotherActivityOrOnCreate = true;
    }

    @Override
    public void openPrivateChat(ContactVM contactVM) {
        Intent intent = new Intent(Application.getAppContext(), PrivateChatActivity.class);
        intent.putExtra(PrivateChatActivity.CONTACT_SERIALIZABLE, contactVM.getContact());
        startActivity(intent);
        didComeFromAnotherActivityOrOnCreate = true;

    }

    protected void cancelNotificationFromStatusBar() {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(Application.NOTIFICATION_ID);
    }

    @Override
    public void openMapViewFragment() {
        //TODO: if cntainer exists -- replace.
        PropertyListContainerFragment containerFragment = (PropertyListContainerFragment) getSupportFragmentManager().findFragmentByTag(PropertyListContainerFragment.TAG);
        if (containerFragment != null) {
            containerFragment.openMapView();
        }
    }

    @Override
    public void openPropertyListViewFragment() {
        PropertyListContainerFragment containerFragment = (PropertyListContainerFragment) getSupportFragmentManager().findFragmentByTag(PropertyListContainerFragment.TAG);
        if (containerFragment != null) {
            containerFragment.openListView();
        }
    }

    public static void addCancelActionButton(Menu menu, View.OnClickListener clickListener) {
        TextView tv = getActionButtonTextView("Cancel", clickListener);
        menu.add(0, ACTION_CANCEL_ID, 1, "Cancel").setActionView(tv).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.findItem(ACTION_CANCEL_ID).setVisible(false);
    }

    public static TextView getActionButtonTextView(String text, View.OnClickListener clickListener) {
        TextView tv = new TextView(Application.getAppContext());
        tv.setText("  " + text + "  ");
        tv.setTextColor(Application.getAppContext().getResources().getColor(R.color.blue_dark));
        tv.setOnClickListener(clickListener);
        tv.setPadding(5, 0, 5, 0);
        tv.setTypeface(TypefaceManager.getInstance(Application.getAppContext()).getTypeface("light"));
        tv.setGravity(View.TEXT_ALIGNMENT_CENTER);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, Application.getAppContext().getResources().getDimension(R.dimen.font_size_main));
        return tv;
    }

    @Override
    public void onBackPressed() {
        ChatKeyboardUtils.hideKeyboard(findViewById(android.R.id.content));
        if (getSupportFragmentManager().findFragmentByTag(WebviewFragment.TAG) != null) {
            super.onBackPressed();
            return;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            if (didPressToQuite) {
                finish();
            } else {
                Toast.makeText(this, "Press twice to quit", Toast.LENGTH_SHORT).show();
                didPressToQuite = true;
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        didPressToQuite = false;
                    }
                }, DELAY_FOR_CLICKING_TWICE_TO_QUITE);
            }

        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        Prefs.getInstance().save();
        visible = false;

        super.onPause();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_OPEN_FILTERS) {
                openProperyListFragment(data.getExtras());
            } else if (requestCode == REQUEST_OPEN_PROPERTY_PAGE) {
                if (data != null) {
                    if (data.getIntExtra(StaticParams.KEY_ACTION_TYPE, -1) == StaticParams.SEND_PROPERTY_VIA_CHAT) {
                        sendPropertyViaChat((PropertyViaChatVM) data.getSerializableExtra(StaticParams.KEY_PROPERTY_FOR_CHAT_OBJ));
                    }
                }
            }
        }
    }

    public void onStartActivityEvent(StartActivityEvent event) {
        didComeFromAnotherActivityOrOnCreate = true;
        if (event.shouldStartActivityForResult()) {
            startActivityForResult(event.getIntent(), event.getRequestCode());

        } else {
            startActivity(event.getIntent());
        }

    }

    @Override
    public void alignContainerAndToolbarTop() {
        RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) container.getLayoutParams();
        p.addRule(RelativeLayout.BELOW, 0);
        container.setLayoutParams(p);
    }

    @Override
    public void containerBelowToolbar() {
        RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) container.getLayoutParams();
        p.addRule(RelativeLayout.BELOW, R.id.toolbar);
        container.setLayoutParams(p);
    }

    @Override
    public void sendSupportEmail() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        int ticketNum = (int) (Math.random() * 100000000);
        i.putExtra(Intent.EXTRA_SUBJECT, "Real Support - Ticket Number : " + ticketNum);
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.support_email)});
//        i.putExtra(Intent.EXTRA_TEXT  , buildEmailBody(eMessage, username, token, bodyJson));
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
            Crashlytics.logException(new Throwable("User sent feedback num : " + ticketNum));
        } catch (android.content.ActivityNotFoundException ex) {
            Snackbar.make(findViewById(android.R.id.content), "There are no email clients installed.", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void openCropImageFragment(){
        CropImageFragment fragment = CropImageFragment.getInstance();
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment, CropImageFragment.TAG)
                .addToBackStack(CropImageFragment.TAG)
                .commit();
    }

    @Override
    protected String getScreenName() {
        return "Main Activity";
    }

    protected abstract void openAgentProfileFragment();
}
