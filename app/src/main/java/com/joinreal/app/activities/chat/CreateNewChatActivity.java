package com.joinreal.app.activities.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.RealBaseActivity;
import com.joinreal.app.activities.MainActivityAbstract;
import com.joinreal.app.adapters.chat.ChatContactsAdapter;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.interfaces.ISearchToolbarListener;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.PNActionEvent;
import com.joinreal.model.view.ContactVM;
import com.joinreal.utils.ContactManager;
import com.joinreal.app.presenters.ContactsLayoutCreator;
import com.joinreal.utils.ChatKeyboardUtils;
import com.squareup.otto.Subscribe;


/**
 * Created by Nick on 21.05.2015.
 */
public class CreateNewChatActivity extends RealBaseActivity implements ChatContactsAdapter.onChatUserClickListener, ContactsLayoutCreator.ContactsLayoutCreatorListener, ISearchToolbarListener {

    private ContactsLayoutCreator contactsLayoutCreator;

    private Toolbar toolbar;
    public static LinearLayout searchLayout;
    public static EditText searchText;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_and_chats_layout);
        setupToolBar();
        contactsLayoutCreator = new ContactsLayoutCreator(this, this);
        contactsLayoutCreator.createLayout(findViewById(android.R.id.content), R.drawable.ic_close, ContactManager.getViewModelsList());

        //TODO: temp
        findViewById(R.id.progressBar).setVisibility(View.GONE);

    }

    private void setupToolBar() {
        toolbar = (Toolbar) findViewById(R.id.included_toolbar_search);
        searchLayout = (LinearLayout) findViewById(R.id.search_layout);
        searchText = (EditText) findViewById(R.id.search_text);
        setSupportActionBar(toolbar);
        // Show menu icon
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.hamburger);
            ab.setDisplayHomeAsUpEnabled(true);
            ab.show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        contactsLayoutCreator.onResume();
        setToolbarTitle("New Chat");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contacts, menu);
        this.menu = menu;
        menu.removeItem(R.id.action_add);
        menu.removeItem(R.id.action_compose);
        MainActivityAbstract.addCancelActionButton(menu, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelSearchClick();
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_search:
                onSearchActionClick();
                menu.findItem(MainActivityAbstract.ACTION_CANCEL_ID).setVisible(true);
                item.setVisible(false);
                Analytics.getInstance().trackToolbarNavigtionEvent(getScreenName(), "Search");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onContactRowClick(ContactVM contactVM) {
        Intent intent = new Intent(Application.getAppContext(), PrivateChatActivity.class);
        intent.putExtra(PrivateChatActivity.CONTACT_SERIALIZABLE, contactVM.getContact());
        startActivity(intent);
    }

    @Override
    public ActionBar getActivityActionBar() {
        return getSupportActionBar();
    }

    @Override
    public Menu getActionMenu() {
        return getActionMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
        contactsLayoutCreator.onPause();
    }

    @Override
    public void addTextChangedListener(TextWatcher textWatcher) {
        searchText.addTextChangedListener(textWatcher);
    }

    @Override
    public void removeTextChangedListener(TextWatcher textWatcher) {
        searchText.removeTextChangedListener(textWatcher);
    }

    public void setToolbarTitle(String title) {
        TextView titleTv = (TextView) findViewById(R.id.toolbar_title);
        titleTv.setText("New Chat");
    }

    //TODO: share with main activity

    @Override
    public void onCancelSearchClick() {
        menu.findItem(MainActivityAbstract.ACTION_CANCEL_ID).setVisible(true);
        menu.findItem(R.id.action_search).setVisible(true);
        searchLayout.setVisibility(View.GONE);
        searchText.setText("");
        ChatKeyboardUtils.hideKeyboard(searchText);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        menu.findItem(MainActivityAbstract.ACTION_CANCEL_ID).setVisible(false);
        contactsLayoutCreator.onCancelClick();
    }

    @Override
    public void onSearchActionClick() {
        searchLayout.setVisibility(View.VISIBLE);
        searchText.requestFocus();
        ChatKeyboardUtils.showKeyboard(searchText);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    protected String getScreenName() {
        return "Create New Chat Activity";
    }
}
