package com.joinreal.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.fragments.FiltersListFragment;
import com.joinreal.app.fragments.SearchLocationFragment;
import com.joinreal.app.interfaces.SendFiltersRequest;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.PNActionEvent;
import com.joinreal.events.SearchButtonVisibilityEvent;
import com.joinreal.model.LocationObj;
import com.joinreal.model.data.SearchFilter;
import com.squareup.otto.Subscribe;

/**
 * Created by britt on 6/2/15.
 */
public class FiltersActivity extends RealBaseActivity implements SendFiltersRequest {

    private SearchFilter filter;
    private String locationStr;
    private View searchButton;
    private String locationDesc;
    private View whiteShade;

    public interface BuildJsonListener {
        void addPropertiesToJson(JsonObject jsonObject);

        boolean isValidated(Context context, View parentView);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);
        setFilter();
        setFragments();
        setSearchButton();
        TypefaceManager.getInstance(this).assignTypeface(this);
    }

    private void setFilter() {
        if ((getIntent() != null) && (getIntent().getExtras() != null)) {
            Bundle extras = getIntent().getExtras();
            if (extras.getString(StaticParams.KEY_CONTACT_ID) != null) {
                filter = (SearchFilter) extras.getSerializable(StaticParams.KEY_SEARCH_FILTER);
                locationStr = null;
                locationDesc = null;
            } else {
                if (extras.getBoolean(StaticParams.KEY_SHOULD_RESET_FILTER, false)) {
                    filter = null;
                    locationStr = null;
                    locationDesc = null;
                } else {
                    filter = Application.getClient().getUsersLastFilter();
                    locationStr = Application.getClient().getUsersLastSearchLocation();
                    locationDesc = Application.getClient().getUsersLastSearchLocationDesc();
                }
            }
        }
    }

    private void setFragments() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_search_location, SearchLocationFragment.getNewInstance(filter != null ? new LocationObj(filter) : null, locationStr, locationDesc), SearchLocationFragment.TAG)
                .commit();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_filters_list, FiltersListFragment.getNewInstance(filter), FiltersListFragment.TAG)
                .commit();

    }

    private void setSearchButton() {
        searchButton = findViewById(R.id.search_button);
        whiteShade = findViewById(R.id.white_shade_filters);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean areAllFieldsValidated = validateAllFields();
                if (areAllFieldsValidated) {
                    sendFindPropertiesRequest();
                }
            }
        });
    }

    private boolean validateAllFields() {
        boolean isValidated;
        //TODO: temp:
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof BuildJsonListener) {
                isValidated = ((BuildJsonListener) fragment).isValidated(this, findViewById(android.R.id.content));
                if (!isValidated) {
                    return false;
                }
            }
        }
        return true;
    }


    @Override
    public void sendFindPropertiesRequest() {
        String state = null;
        String locationString = null;
        JsonObject jsonObject = new JsonObject();

        //TODO: temp:
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof BuildJsonListener) {
                ((BuildJsonListener) fragment).addPropertiesToJson(jsonObject);
                if (fragment instanceof SearchLocationFragment) {
                    state = ((SearchLocationFragment) fragment).getState();
                    locationString = ((SearchLocationFragment) fragment).getLocationShortString();
                    locationDesc = ((SearchLocationFragment) fragment).getLocationDesc();
                }
            }
        }

        Intent data = new Intent();
        if ((getIntent() != null) && (getIntent().getExtras() != null)) {
            data.putExtras(getIntent().getExtras());

            if (getIntent().getStringExtra(StaticParams.KEY_CONTACT_ID) == null) {
                Application.getClient().setUsersLastFilter(new Gson().fromJson(jsonObject, SearchFilter.class));
                Application.getClient().setUsersLastSearchLocation(locationString);
                Application.getClient().setUsersLastSearchLocationDesc(locationDesc);
            }
        }
        data.putExtra(StaticParams.KEY_FILTERS_JSON, new Gson().toJson(jsonObject));
        data.putExtra(StaticParams.KEY_STATE, state);
        data.putExtra(StaticParams.KEY_TITLE_PROPERTY_LIST, locationString);

        setResult(RESULT_OK, data);
        finish();

    }

    @Subscribe
    public void onSearchButtonVisibilityEvent(SearchButtonVisibilityEvent event) {
            searchButton.setVisibility(event.shouldShowButton() ? View.VISIBLE : View.GONE);
            whiteShade.setVisibility(event.shouldShowButton() ? View.VISIBLE : View.GONE);
        }

    @Override
    protected String getScreenName() {
        return "Filters Activity";
    }

}
