package com.joinreal.app.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.PNActionEvent;
import com.joinreal.events.TaskBackFromNetworkEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by britt on 5/31/15.
 */
public class SplashActivity extends RealBaseActivity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private boolean isWaitingForAssaignedAgentToReturn;
    private boolean isWaitingForSystemSettings;
    private boolean shouldGetSystemSettings;
    private AlertDialog devLevelPicker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Application.isProductionEnv()) {
            onAssignedEnvironment();
        } else {
            showDevLevelPickerDialog();
        }
        TypefaceManager.getInstance(this).assignTypeface(this);
    }

    private void onAssignedEnvironment() {
        isWaitingForSystemSettings = true;
        Application.getClient().initSystemSettings();
    }

    private void startNextActivityIfPossible() {
        Application.getClient().initUser();
        if (canStartNextActivity()) {
            startNextActivity();
        }
    }

    private boolean canStartNextActivity() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        if ((!isWaitingForSystemSettings) && (Application.getClient().getSystemSettings() != null)) {
            boolean shouldUpgradeApp = false;
            if (Application.getClient().getSystemSettings() != null) {
                if (Application.getClient().getUser() == null) {
                    shouldUpgradeApp = Math.max(Application.getClient().getSystemSettings().getMinVersionAgent(), Application.getClient().getSystemSettings().getMinVersionCustomer()) > pInfo.versionCode;
                } else {
                    if (Application.getClient().isAgent()) {
                        shouldUpgradeApp = Application.getClient().getSystemSettings().getMinVersionAgent() > pInfo.versionCode;
                    } else {
                        shouldUpgradeApp = Application.getClient().getSystemSettings().getMinVersionCustomer() > pInfo.versionCode;
                    }
                }
            }
            if (shouldUpgradeApp) {
                askToUpgradeApp();
            } else {
                return true;
            }
        }
        return false;
    }

    private void askToUpgradeApp() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("App Upgrade Required");
        alertDialogBuilder
                .setMessage("Please upgrade the app to continue")
                .setCancelable(false)
                .setPositiveButton("Upgrade", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                        dialog.dismiss();
                    }
                });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    private void startNextActivity() {
        Intent intent = getNextActivityIntent();
        if (intent != null) {
            runIntent(intent);
//            Application.getClient().syncContacts(this);
        }
    }

    private Intent getNextActivityIntent() {
        Intent intent = null;
        if (Application.getClient().getUser() == null) {
            intent = new Intent(SplashActivity.this, IntroActivity.class);
        } else {
            if (Application.getClient().getUser().isAgent()) {
                intent = new Intent(SplashActivity.this, MainActivityAgent.class);
            } else {
                if (Application.getClient().getAssignedAgent() != null) {
                    intent = new Intent(SplashActivity.this, CustomerWelcomeActivity.class);
                } else {
                    isWaitingForAssaignedAgentToReturn = true;
                }
            }
        }

        if (intent != null) {
            if ((SplashActivity.this.getIntent() != null) && (SplashActivity.this.getIntent().getExtras() != null)) {
                intent.putExtras(getIntent().getExtras());
            }
        }
        return intent;
    }

    private void runIntent(Intent intent) {
        final Intent finalIntent = intent;
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                SplashActivity.this.startActivity(finalIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Subscribe
    public void onRecievedAssignedAgentEvent(TaskBackFromNetworkEvent e) {
        if (e.getException() != null) {
            Helper.handleApiException(e.getException(), e.getTaskCode());
        } else {
            switch (e.getTaskCode()) {
                case NetworkCodes.TASK_SYSTEM_SETTINGS:
                    isWaitingForSystemSettings = false;
                    break;
                case NetworkCodes.TASK_ASSIAGNED_AGENT:
                    isWaitingForAssaignedAgentToReturn = false;
                    break;
            }
            startNextActivityIfPossible();
        }
    }

    private void showDevLevelPickerDialog() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                final String[] devLevelTitles = {"STAGING", "PRODUCTION", "QA1", "DEV"};
                final String[] devLevelOptions = {getString(R.string.stg), getString(R.string.prod), getString(R.string.qa1), getString(R.string.dev)};
                AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                builder.setTitle("Choose Environment").setItems(devLevelTitles, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int pos) {
                        updateDevLevel(devLevelOptions[pos]);
                        onAssignedEnvironment();
                        dialog.dismiss();
                    }
                });
                devLevelPicker = builder.create();
                devLevelPicker.setCanceledOnTouchOutside(false);
                devLevelPicker.show();
            }
        });

    }

    private void updateDevLevel(String devLevel) {
        if (devLevelPicker != null) {
            devLevelPicker.dismiss();
        }
        Application.setDevLevel(devLevel);
        Application.getClient().updateApisDevLevel();
    }

    @Override
    protected String getScreenName() {
        return "Splash Activity";
    }
}
