package com.joinreal.app.activities;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.StaticParams;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.PNActionEvent;
import com.joinreal.utils.ChatDialogsManager;
import com.squareup.otto.Subscribe;


public abstract class RealBaseActivity extends AppCompatActivity {

    Object busSubscribee = new Object(){
        @Subscribe
        public void handleApiExceptionEvent(NetworkExceptionEvent event){
            handleApiException(event);
        }

        @Subscribe
        public void handlePushActionEvent(PNActionEvent event){
            handlePNAction(event);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Get a result from selecting picture intent
     */

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.getBus().register(this);
        BusProvider.getRestBus().register(this);
        BusProvider.getBus().register(busSubscribee);
        BusProvider.getRestBus().register(busSubscribee);
        ChatDialogsManager.getInstance().registerBus();
        //Get an Analytics tracker to report app starts & uncaught exceptions etc.
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!(this instanceof MainActivityAbstract)) {
            MainActivityAbstract.didComeFromAnotherActivityOrOnCreate = true;
        }
        Application.activityResumed();

        Analytics.getInstance().trackScreen(getScreenName());
    }

    @Override
    protected void onPause() {
        Application.activityPaused();
        super.onPause();
    }

    @Override
    protected void onStop() {
        BusProvider.getBus().unregister(this);
        BusProvider.getRestBus().unregister(this);
        BusProvider.getBus().unregister(busSubscribee);
        BusProvider.getRestBus().unregister(busSubscribee);
        ChatDialogsManager.getInstance().unregisterBus();
        //Stop the analytics tracking
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
        super.onStop();
    }

    public void handleApiException(final NetworkExceptionEvent event) {
        String errorMsg = "";
        if (event.getException() != null) {
            if (event.getException().getCode() == NetworkCodes.EXCEPTION_LOGIN) {
                errorMsg = getString(R.string.login_error_msg);
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            } else {
                if (event.getException().getCode() == NetworkCodes.EXCEPTION_INTERNAL) {
                    errorMsg = getString(R.string.server_error_msg) + event.getApiCallId();
                } else {
                    errorMsg = getString(R.string.network_error_msg) + event.getApiCallId();
                }
            }
        } else {
            errorMsg = getString(R.string.network_error_msg) + event.getApiCallId();
        }

        final String msg = errorMsg;

        if (!Application.isProductionEnv()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(RealBaseActivity.this, msg, Toast.LENGTH_LONG).show();
                    Helper.getExceptionSnackbar(RealBaseActivity.this, findViewById(android.R.id.content), event.getException().getMessage(), event.getBodyJson()).show();
                }
            });
        }

        if (event.shouldReportToUser() || (!Application.isProductionEnv())) {

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Helper.getExceptionSnackbar(RealBaseActivity.this, findViewById(android.R.id.content), event.getException().getMessage(), event.getBodyJson()).show();
                    Toast.makeText(RealBaseActivity.this, msg, Toast.LENGTH_LONG).show();
                }
            });
        }

        Crashlytics.log(event.getLocalizedMessage());
    }

    public void handlePNAction(PNActionEvent event){
        if (event.getActionStr().equals(StaticParams.PN_ACTION_SYNC_CONTACTS)) {
            Application.getClient().syncContacts(this);
        }
    }

    protected abstract String getScreenName();

}
