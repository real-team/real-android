package com.joinreal.app.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextPaint;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.adapters.PropertyAdapter;
import com.joinreal.app.api.RealClient;
import com.joinreal.app.common.Analytics;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.LocationTracker;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.fragments.StreetViewFragment;
import com.joinreal.app.interfaces.UpdateFavoritesHelper;
import com.joinreal.app.viewholders.PropertyPageSummaryViewHolder;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.PNActionEvent;
import com.joinreal.events.ShouldOpenFavoritesEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.PropertyCategory;
import com.joinreal.model.data.PropertyField;
import com.joinreal.model.data.PropertyFull;
import com.joinreal.model.data.PropertySubcategory;
import com.joinreal.model.view.AdditionalFeaturesItemVM;
import com.joinreal.model.view.AddressAndMapVM;
import com.joinreal.model.view.BasicInfoItemVM;
import com.joinreal.model.view.OverviewVM;
import com.joinreal.model.view.PropertyFieldVM;
import com.joinreal.model.view.PropertySubcategoryVM;
import com.joinreal.model.view.PropertySummaryInfoVM;
import com.joinreal.model.view.PropertyViaChatVM;
import com.joinreal.model.view.ViewModelItem;
import com.joinreal.utils.TimeStringUtils;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by britt on 6/22/15.
 */
public class PropertyPageActivity extends RealBaseActivity {

    private static final String SHOULD_OPEN_STREETVIEW = "SHOULD_OPEN_STREETVIEW";
    private RealClient client;
    private AsyncTask task;
    PropertyFull propertyFull;
    private ArrayList<ViewModelItem> propertyDataViewModels;
    private RecyclerView propertyRecyclerView;
    private JSONObject propertyJsonObject;
    protected UpdateFavoritesHelper updateFavoritesHelper;
    private Gson gson;
    private AppBarLayout appbar;
    private long timeSentOnBus;
    private TextView pagerLabel;
    private FrameLayout container;
    private Toolbar toolbar;
    private ViewPager pager;
    private ProgressBar progressbar;
    private long timeSetLayout;
    private long timeBackFromNetwork;
    private long timeParse1;
    private long timeParse2;
    private int maxViewedImagesPosition = 0;
    private int imageViews = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        long t = System.currentTimeMillis();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_page);
        client = Application.getClient();
        gson = new Gson();
        updateFavoritesHelper = UpdateFavoritesHelper.getInstance();
        propertyRecyclerView = (RecyclerView) findViewById(R.id.property_recyclerview);

        progressbar = (ProgressBar) findViewById(R.id.progressBar);
        progressbar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.blue_dark),
                PorterDuff.Mode.SRC_ATOP);


        if (savedInstanceState == null) {
            setPreviewLayout();
            getPropertyDataFromNetwork();
        } else {
            String propertyJson = savedInstanceState.getString(StaticParams.KEY_PROPERTY_JSON);
            if ((propertyJson != null) && (!propertyJson.equals("null"))) {
//                propertyFull = gson.fromJson(propertyJson, PropertyFull.class);
                try {
                    propertyJsonObject = new JSONObject(propertyJson);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                propertyFull = savedInstanceState.getParcelable(StaticParams.KEY_PROPERTY_FULL);
//                if (propertyRecyclerView != null) {
//                    propertyJsonObject = gson.fromJson(propertyJson, JSONObject.class);
//                }
                setLayoutContent();
                if (savedInstanceState.getBoolean(SHOULD_OPEN_STREETVIEW)) {
                    openStreetViewFragment();
                }
            }
        }
        TypefaceManager.getInstance(this).assignTypeface(this);

        System.out.println("*** property activity onCreate time : " + (System.currentTimeMillis() - t));

    }

    private void setPreviewLayout() {
        setViewPager();
        setFabs();

    }

    private void getPropertyDataFromNetwork() {
        task = new AsyncTask() {
            @Override
            protected Void doInBackground(Object... params) {
                long t = System.currentTimeMillis();
                String j = null;
                try {
                    j = client.getPropertyJson(getIntent().getStringExtra(StaticParams.KEY_MLS_ID), getIntent().getStringExtra(StaticParams.KEY_MLS_NUM));
                } catch (NetworkException e) {
                    BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.CANT_GET_PROPERTIES));

                }
                timeBackFromNetwork = System.currentTimeMillis() - t;
                try {
                    t = System.currentTimeMillis();
                    JSONObject jsonObject = new JSONObject(j.substring(1, j.length() - 1));
                    timeParse1 = System.currentTimeMillis() - t;
                    BusProvider.getRestBus().post(new PropertyReturnedFromNetworkEvent(jsonObject));
                } catch (JSONException e) {
                    BusProvider.getRestBus().post(new NetworkExceptionEvent(
                            new NetworkException(NetworkCodes.EXCEPTION_JSON, e.getMessage(), e),
                            NetworkCodes.CANT_GET_PROPERTIES));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
            }
        };
        task.execute();
    }

    private void setupToolBar() {
        toolbar = (Toolbar) findViewById(R.id.property_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setHomeAsUpIndicator(Helper.getDrawable(this, R.drawable.back_arrow_white));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(0);
        }
        setToolbarTitle();
    }

    private void setToolbarTitle() {
        if (propertyFull != null) {
            CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
            if (collapsingToolbarLayout != null) {
                toolbar.setTitle("");
                collapsingToolbarLayout.setTitle(propertyFull.getAddress1());
                collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
                collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.transparent));
                setTypefaceForCollapsableLayout(collapsingToolbarLayout);
            } else {
                SpannableString title = TypefaceManager.getInstance(this).getToolbarTitle(propertyFull.getAddress1());
                if (title != null) {
                    toolbar.setTitle(title);
                }
            }
        }
    }

    private void setTypefaceForCollapsableLayout(CollapsingToolbarLayout collapsingToolbarLayout) {
        try {
            final Field field = collapsingToolbarLayout.getClass().getDeclaredField("mCollapsingTextHelper");
            field.setAccessible(true);

            final Object object = field.get(collapsingToolbarLayout);
            final Field tpf = object.getClass().getDeclaredField("mTextPaint");
            tpf.setAccessible(true);

            ((TextPaint) tpf.get(object)).setTypeface(TypefaceManager.getInstance(this).getTypeface("light"));
        } catch (Exception ignored) {
        }
    }

    @Subscribe
    public void onPropertyReturnredFromNetwork(PropertyReturnedFromNetworkEvent e) {
        propertyJsonObject = e.getPopertyJsonObject();
        long t = System.currentTimeMillis();
        propertyFull = new PropertyFull(propertyJsonObject);

        timeParse2 = System.currentTimeMillis() - t;
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (propertyFull != null) {
                    setLayoutContent();
                }

            }
        });
    }

    @Subscribe
    public void onShouldOpenFavoritesEvent(ShouldOpenFavoritesEvent e) {
        //TODO extract!
        Intent intent;
        if (Application.getClient().isAgent()) {
            intent = new Intent(PropertyPageActivity.this, MainActivityAgent.class);
        } else {
            intent = new Intent(PropertyPageActivity.this, MainActivityCustomer.class);
        }
        intent.putExtra(StaticParams.KEY_ACTION_TYPE, StaticParams.OPEN_FAVORITES);
        startActivity(intent);
    }


    private void setLayoutContent() {
        long t = System.currentTimeMillis();
        setupToolBar();
        setFabs();
        setViewPager();
        container = (FrameLayout) findViewById(R.id.container);
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            container.setVisibility(View.VISIBLE);
        }
        progressbar.setVisibility(View.GONE);
        if (propertyRecyclerView != null) {
            setItemsViewModelsList();
            setRecyclerView();
        }
        timeSetLayout = System.currentTimeMillis() - t;

        String s = "timeBackFromNetwork : " + timeBackFromNetwork + " , timeParse1 : " + timeParse1 + " , timeParse2 : " + timeParse2 + " timeSetLayout : " + timeSetLayout;
        System.out.println(s);
        Crashlytics.logException(new Throwable(s));
    }

    private void setFabs() {
        FloatingActionButton chatFab = (FloatingActionButton) findViewById(R.id.fab_chat);
        if (chatFab != null) {
            PropertyPageSummaryViewHolder.setChatFab(propertyFull, getChatFabClickListener(), chatFab);
        }
        FloatingActionButton favoriteFab = (FloatingActionButton) findViewById(R.id.fab_heart);
        if (favoriteFab != null) {
            PropertyPageSummaryViewHolder.setFavoriteFab(propertyFull, updateFavoritesHelper, favoriteFab, findViewById(android.R.id.content));
        }
    }

    private void setViewPager() {
        ArrayList<String> photos;
        if (propertyFull != null) {
            photos = propertyFull.getPhotosMeduim();
        } else {
            photos = getIntent().getStringArrayListExtra(StaticParams.KEY_PHOTO_LIST);
        }
        pager = (ViewPager) findViewById(R.id.pager);
        PropertyPageSummaryViewHolder.setImagesViewPager(photos, this, pager);

        pagerLabel = (TextView) findViewById(R.id.pager_label);
        pagerLabel.setText("1 / " + pager.getAdapter().getCount());

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pagerLabel.setText((position + 1) + " / " + pager.getAdapter().getCount());
                maxViewedImagesPosition = Math.max(maxViewedImagesPosition, position);
                imageViews++;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public ArrayList<String> getPhotosForPager() {
        ArrayList<String> photosForPager = propertyFull.getPhotosMeduim();
        photosForPager.add(propertyFull.getStaticMapUrl());
        return photosForPager;
    }

    private void setRecyclerView() {
        propertyRecyclerView.setVisibility(View.VISIBLE);
        propertyRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        propertyRecyclerView.setAdapter(new PropertyAdapter(this, propertyDataViewModels, getScreenName()));
    }

    private void setItemsViewModelsList() {
        propertyDataViewModels = new ArrayList<>();
        addPropertySummaryInfoToList();
        addOverViewItemToList();
        addBasicInfoItemsToList();
        addAddressAndMapToList();
        addAdditionalFeaturesItemsToList();
    }

    private void addPropertySummaryInfoToList() {
        propertyDataViewModels.add(new PropertySummaryInfoVM(propertyFull));

    }

    private void addAddressAndMapToList() {
        propertyDataViewModels.add(new AddressAndMapVM(propertyFull));

    }


    private void addOverViewItemToList() {
        if (Application.getClient().isAgent()) {
            propertyDataViewModels.add(new OverviewVM("MLS#: " + propertyFull.getMlsNum()));

            if (propertyFull.getListStatus() != null) {
                propertyDataViewModels.add(new OverviewVM("Status: " + propertyFull.getListStatus()));
            }
            if (propertyFull.getDateAvailable() != null) {
                propertyDataViewModels.add(new OverviewVM("Date Available: " + TimeStringUtils.getDateForPropertyPage(propertyFull.getDateAvailable())));
            }
        }
        if (propertyFull.getDescription() != null) {
            propertyDataViewModels.add(new OverviewVM(propertyFull.getDescription()));
        }

        if (propertyFull.getRemarks() != null) {
            propertyDataViewModels.add(new OverviewVM(propertyFull.getRemarks()));

        }

        if (propertyFull.getNotes() != null) {
            propertyDataViewModels.add(new OverviewVM(propertyFull.getNotes()));

        }
    }

    private void addBasicInfoItemsToList() {
        if (propertyFull.getType() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Type", propertyFull.getType()));
        }
        if (propertyFull.getCategory() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Category", propertyFull.getCategory()));
        }
        if ((propertyFull.getOwnership() != null) && ((propertyFull.getOwnership().equals(PropertyFull.TIMESHARE) || (propertyFull.getOwnership().equals(PropertyFull.FRAQTIONAL))))) {
            propertyDataViewModels.add(new BasicInfoItemVM("Ownership", propertyFull.getOwnership()));
        }
        if (propertyFull.getForLease() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Also For Lease", propertyFull.getForLease().toString()));
        }
        if (propertyFull.getForSale() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Also for Sale", propertyFull.getForSale().toString()));
        }
        if (propertyFull.getBeds() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Beds", Helper.removeZeroFromFloatIfNeeded(propertyFull.getBeds()).toString()));
        }
        if (propertyFull.getBaths() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Bathrooms", Helper.removeZeroFromFloatIfNeeded(propertyFull.getBaths()).toString()));
        }
        if (propertyFull.getSqFt() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("House Size", propertyFull.getSqFt().toString()));
        }
        if (propertyFull.getPriceSqFt() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Price per Size", propertyFull.getPriceSqFt().toString()));
        }
        if (propertyFull.getLandSize() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Land Size", propertyFull.getLandSize()));
        }
        if (propertyFull.getLandUse() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Land Use", propertyFull.getLandUse()));
        }
        if (propertyFull.getLotSize() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Lot Size", propertyFull.getLotSize()));
        }
        if (propertyFull.getLotDesc() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Lot Description", propertyFull.getLotDesc()));
        }
        if (propertyFull.getAcres() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Area Size", propertyFull.getAcres().toString()));
        }
        if (propertyFull.getAcresDesc() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Area Description", propertyFull.getAcresDesc()));
        }
        if (propertyFull.getYearBuilt() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Year Built", propertyFull.getYearBuilt().toString()));
        }
        if (propertyFull.getDaysOnMarket() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("DOM", TimeStringUtils.getDaysDifferenceFromToday(propertyFull.getDaysOnMarket())));
        }
        if (propertyFull.getDateModified() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Date Modified", TimeStringUtils.getDateForPropertyPage(propertyFull.getDateModified())));
        }
        if (propertyFull.getMaintFees() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Maint Fees", propertyFull.getMaintFees()));
        }
        if (propertyFull.getDepositSecurity() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Security Deposit", propertyFull.getDepositSecurity().toString()));
        }
        if (propertyFull.getDepositPets() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Pet Deposit Required", propertyFull.getDepositPets().toString()));
        }
        if (propertyFull.getHoaMngmnt() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("HOA / Mngmnt Co", propertyFull.getHoaMngmnt()));
        }
        if (propertyFull.getFeesOther() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Other Fees", propertyFull.getFeesOther()));
        }
        if (propertyFull.getBuildingFeesOther() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Other Building Fees", propertyFull.getBuildingFeesOther()));
        }
        if (propertyFull.getApplicationFee() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Application Fee:", propertyFull.getApplicationFee().toString()));
        }
        if (propertyFull.getApprovalRequired() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Approval Required", propertyFull.getApprovalRequired()));
        }
        if (propertyFull.getNewConstruction() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("New Construction", propertyFull.getNewConstruction()));
        }
        if (propertyFull.getDefects() != null) {
            propertyDataViewModels.add(new BasicInfoItemVM("Defects", propertyFull.getDefects()));
        }
    }

    private void addAdditionalFeaturesItemsToList() {
        if (client.getPropertyStructure() != null) {
            for (PropertyCategory metadataCategory : client.getPropertyStructure()) {
                if (metadataCategory.getSubcategories() != null) {
                    addCategoryToVMList(metadataCategory);
                }
            }
        } else {
            Toast.makeText(this, "Please check your network connection and try again. Error code : " + NetworkCodes.NO_PROPERTY_STRUCTURE, Toast.LENGTH_LONG).show();
        }
    }

    private void addCategoryToVMList(PropertyCategory metadataCategory) {
        List<ViewModelItem> items = new ArrayList<>();
        for (PropertySubcategory metaSubcategory : metadataCategory.getSubcategories()) {
            addCategoryInnerItems(items, metaSubcategory);
        }
        if (!items.isEmpty()) {
            propertyDataViewModels.add(new AdditionalFeaturesItemVM(metadataCategory.getCategoryName(), items));
        }
    }

    private void addCategoryInnerItems(List<ViewModelItem> items, PropertySubcategory metaPropertySubcategory) {
        if (metaPropertySubcategory.getFields() != null) {
            List<ViewModelItem> fields = getFieldsVMList(metaPropertySubcategory);
            if (!fields.isEmpty()) {
                items.add(new PropertySubcategoryVM(metaPropertySubcategory.getSubcategoryName()));
                items.addAll(fields);
            }
        }
    }

    private List<ViewModelItem> getFieldsVMList(PropertySubcategory metaPropertySubcategory) {
        List<ViewModelItem> fields = new ArrayList<>();
        for (PropertyField metaField : metaPropertySubcategory.getFields()) {
            addFieldIfExistsInData(fields, metaField);
        }
        return fields;
    }

    private void addFieldIfExistsInData(List<ViewModelItem> fields, PropertyField metaField) {
        if (!propertyJsonObject.isNull(metaField.getKey())) {
            try {
                fields.add(new PropertyFieldVM(metaField.getName(), propertyJsonObject.get(metaField.getKey()).toString(), metaField.getDisplayOrder(), metaField.getDisplayInstructions()));
            } catch (JSONException e) {
            }
        }
    }

    static List getKeysFromJson(String json) throws Exception {
        Object things = new Gson().fromJson(json, Object.class);
        List keys = new ArrayList();
        collectAllTheKeys(keys, things);
        return keys;
    }

    static void collectAllTheKeys(List keys, Object o) {
        Collection values = null;
        if (o instanceof Map) {
            Map map = (Map) o;
            keys.addAll(map.keySet()); // collect keys at current level in hierarchy
            values = map.values();
        } else if (o instanceof Collection)
            values = (Collection) o;
        else // nothing further to collect keys from
            return;

        for (Object value : values)
            collectAllTheKeys(keys, value);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_property, menu);
        if (!Application.getClient().isAgent()) {
            menu.removeItem(R.id.action_call_agent);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getFragmentManager().getBackStackEntryCount() < 1) {
                    finish();
                } else {
                    onBackPressed();
                }
                break;
            case R.id.action_streetview:
                openStreetViewFragment();
                break;
            case R.id.action_driving_dir:
                openDrivingDirections();
                break;
            case R.id.action_send_chat:
                setPropertyViaChat();
                break;
            case R.id.action_call_agent:
                callListingAgent();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void callListingAgent() {
        if (propertyFull.getListingAgentPhone() != null) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + propertyFull.getListingAgentPhone()));
            startActivity(intent);
        } else {
            Toast.makeText(this, "Sorry, listing agent phone number is unavailable for this property.", Toast.LENGTH_SHORT).show();
        }
    }

    private void openDrivingDirections() {
        LatLng latLng = getLatLng();
        if (latLng != null) {
            String uri = "http://maps.google.com/maps?f=d&hl=en&daddr=" + latLng.latitude + "," + latLng.longitude;
//        String uri = "http://maps.google.com/maps?f=d&hl=en&saddr="+latitude1+","+longitude1+"&daddr="+latitude2+","+longitude2;
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(Intent.createChooser(intent, "Select an application"));
        } else {
            Toast.makeText(this, "Sorry, driving directions are currently unavailable for this property.", Toast.LENGTH_SHORT).show();
        }
    }

    private LatLng getLatLng() {
        LatLng latLng = null;
        if ((propertyFull.getLatitude() != null) && (propertyFull.getLongitude() != null)) {
            latLng = new LatLng(propertyFull.getLatitude(), propertyFull.getLongitude());
        } else {
            latLng = LocationTracker.getLatLngFromAddress(propertyFull.getAddressForLocation());
        }
        if (latLng == null) {
            Crashlytics.log("Can't get location for property. mlsid : " + propertyFull.getMlsId() + " , mlsnum : " + propertyFull.getMlsNum());
        }
        return latLng;
    }

    private void openStreetViewFragment() {
        StreetViewFragment fragment;
        container.setVisibility(View.VISIBLE);
        LatLng latLng = getLatLng();
        if (latLng == null) {
            Toast.makeText(this, "Sorry, street view is currently unavailable for this property.", Toast.LENGTH_SHORT).show();
            return;
        } else {
            fragment = StreetViewFragment.getNewInstance(latLng.latitude, latLng.longitude);
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, StreetViewFragment.TAG)
                .addToBackStack(StreetViewFragment.TAG)
                .commit();
    }

    @Override
    public void onPause() {
        updateFavoritesHelper.updateChanges();
        Analytics.getInstance().trackEvent(getScreenName(), "max image position viewed : " + (maxViewedImagesPosition + 1) + " , # total image views : " + imageViews);
        super.onPause();
    }


    public View.OnClickListener getChatFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO extract!
                setPropertyViaChat();
            }
        };
    }

    private void setPropertyViaChat() {
        Intent intent;
        if (Application.getClient().isAgent()) {
            intent = new Intent(PropertyPageActivity.this, MainActivityAgent.class);
        } else {
            intent = new Intent(PropertyPageActivity.this, MainActivityCustomer.class);
        }
        intent.putExtra(StaticParams.KEY_ACTION_TYPE, StaticParams.SEND_PROPERTY_VIA_CHAT);
        intent.putExtra(StaticParams.KEY_PROPERTY_FOR_CHAT_OBJ, new PropertyViaChatVM(propertyFull));
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(StaticParams.KEY_PROPERTY_FULL, propertyFull);
        outState.putString(StaticParams.KEY_PROPERTY_JSON, propertyJsonObject.toString());
        if (getFragmentManager().findFragmentByTag(StreetViewFragment.TAG) != null) {
            outState.putBoolean(SHOULD_OPEN_STREETVIEW, true);
        }
    }

    @Override
    public void onBackPressed() {
        if ((container != null) && (container.getVisibility() == View.VISIBLE)) {
            getFragmentManager().popBackStack();
            container.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    private class PropertyReturnedFromNetworkEvent {
        private JSONObject popertyJsonObject;

        public PropertyReturnedFromNetworkEvent(JSONObject jsonObject) {
            this.popertyJsonObject = jsonObject;
        }

        public JSONObject getPopertyJsonObject() {
            return popertyJsonObject;
        }
    }

    @Override
    protected String getScreenName() {
        return "Property Page Activity";
    }
}
