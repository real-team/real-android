package com.joinreal.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.PNActionEvent;
import com.joinreal.model.data.Agent;
import com.joinreal.utils.CircleTransform;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

/**
 * Created by brittbarak on 7/19/15.
 */
public class CustomerWelcomeActivity extends RealBaseActivity {
    public static final String TAG = "CustomerWelcomeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customenr_welcome);
//        final LocationTracker locationTracker = new LocationTracker();
//        locationTracker.getUserLocation(this, new LocationTracker.LocationRecievedListener() {
//            @Override
//            public void onLocationRecieved(String locationString, LocationObj locationObj) {
//                System.out.println("-------- onLocationRecieved, locationObj = null ? " + (locationObj == null));
//                Bundle bd = new Bundle();
//                bd.putInt(StaticParams.KEY_ACTION_TYPE, StaticParams.OPEN_PROPERTY_LIST);
//                bd.putString(StaticParams.KEY_TITLE_PROPERTY_LIST, locationString);
//                bd.putBoolean(StaticParams.KEY_SHOULD_LOCK_DRAWER, true);
//                if (locationObj == null) {
//                    bd.putBoolean(StaticParams.KEY_OPEN_NO_RESULTS_PAGE, true);
//                } else {
//                    bd.putString(StaticParams.KEY_FILTERS_JSON, getInitialFilterJson());
//                    bd.putString(StaticParams.KEY_STATE, locationObj.getState());
//                }
//                locationTracker.stopTrackingLocationFromNetwork();
//
//                setTapToContinueCilck(bd);
//            }
//        });

        Agent agent = Application.getClient().getAssignedAgent();
        if (agent != null) {
            ((TextView) findViewById(R.id.agent_name)).setText(agent.getFullName());
            ((TextView) findViewById(R.id.title)).setText(agent.getTitle());
            Picasso.with(this).load(agent.getImage()).transform(new CircleTransform()).into((ImageView) findViewById(R.id.agent_img));
            TypefaceManager.getInstance(this).assignTypeface(this);
        } else {
            Toast.makeText(this, "An error has occurred. Please contact us: support@joinreal.com", Toast.LENGTH_LONG).show();
            Crashlytics.log("No assigned agent");
        }

        Bundle bd = new Bundle();
//        bd.putInt(StaticParams.KEY_ACTION_TYPE, StaticParams.OPEN_PROPERTY_LIST);
//        bd.putString(StaticParams.KEY_TITLE_PROPERTY_LIST, "Listings");
//        bd.putBoolean(StaticParams.KEY_SHOULD_LOCK_DRAWER, true);
//        bd.putString(StaticParams.KEY_FILTERS_JSON, getInitialFilterJson());
//        bd.putString(StaticParams.KEY_STATE, "Texas");
        setTapToContinueCilck(bd);
    }

    private void setTapToContinueCilck(final Bundle bd) {
        View clickableOverlay = findViewById(R.id.clickable_overlay);
        TextView tapToContinueTV = (TextView) findViewById(R.id.tap_to_continue);
        tapToContinueTV.setText("Tap anywhere to continue.");
        clickableOverlay.setClickable(true);
        clickableOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomerWelcomeActivity.this, MainActivityCustomer.class);
//                intent.putExtras(bd);
                CustomerWelcomeActivity.this.startActivity(intent);
                CustomerWelcomeActivity.this.finish();
            }
        });

    }
//
//    private String getInitialFilterJson() {
//        JsonObject jsonObject = new JsonObject();
////        jsonObject.addProperty("latitude", latitude);
////        jsonObject.addProperty("longitude", longitude);
//        jsonObject.addProperty("state", "Texas");
//        return new Gson().toJson(jsonObject);
//    }

    @Override
    protected String getScreenName() {
        return "Welcome Activity";
    }

}
