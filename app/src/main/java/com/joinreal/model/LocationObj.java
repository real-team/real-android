package com.joinreal.model;

import com.joinreal.model.data.SearchFilter;

/**
 * Created by brittbarak on 8/12/15.
 */
public class LocationObj {
    String city;
    String state;
    String street;
    String zip;
    private double lat;
    private double lng;
    private String description;

    public LocationObj(String state, String city, String street, String zip) {
        this.city = city;
        this.state = state;
        this.street = street;
        this.zip = zip;
    }

    public LocationObj(SearchFilter filter) {
        this.city = filter.getCity();
        this.state = filter.getState();
        this.street = filter.getStreetName();
        this.zip = filter.getZip();
        this.lat = filter.getLatitude();
        this.lng = filter.getLongitude();
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getStreet() {
        return street;
    }

    public String getZip() {
        return zip;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getLocationDesctiption() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
