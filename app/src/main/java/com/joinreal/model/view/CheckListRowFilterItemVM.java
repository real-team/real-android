package com.joinreal.model.view;

import com.joinreal.R;

/**
 * Created by britt on 6/7/15.
 */
public class CheckListRowFilterItemVM extends RecyclerFilterItemVM {
    public CheckListRowFilterItemVM(String label, String jsonKey) {
        super(label, R.drawable.check_blue, R.drawable.check_gray, R.color.blue, R.color.gray_light, false, jsonKey);
    }

    public CheckListRowFilterItemVM(String label, boolean isChecked, String jsonKey) {
        super(label, R.drawable.check_blue, R.drawable.check_gray, R.color.blue, R.color.gray_light, isChecked, jsonKey);
    }

    @Override
    public FilterViewType getViewType() {
        return FilterViewType.CHECKLIST_ITEM;
    }
}
