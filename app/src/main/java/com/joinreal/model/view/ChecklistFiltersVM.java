package com.joinreal.model.view;

import java.util.List;

/**
 * Created by britt on 6/3/15.
 */
public class ChecklistFiltersVM extends RecyclerFiltersVM{

    public ChecklistFiltersVM(List<RecyclerFilterItemVM> items) {
        super(items);
    }

    @Override
    public FilterViewType getViewType() {
        return FilterViewType.CHECK_LIST;
    }
}
