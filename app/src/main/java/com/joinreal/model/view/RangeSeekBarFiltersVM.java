package com.joinreal.model.view;

import com.google.gson.JsonObject;
import com.joinreal.app.common.Helper;
import com.joinreal.model.RangeStep;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by britt on 6/3/15.
 */
public class RangeSeekBarFiltersVM extends FiltersViewModelItem {
    private final String label;
    protected int absoluteMin;
    protected int absoluteMax;
    protected Integer[] defaultRange;
    protected final String fullRangeLabel;

    protected int selectedMinStepIndex;
    protected int selectedMaxStepIndex;

    protected List<RangeStep> rangeSteps;
    protected final boolean isDollars;
    protected final boolean isAmount;
    protected String keyForJsonMin;
    protected String keyForJsonMax;

    public RangeSeekBarFiltersVM(String label, Integer[] defaultRange, List<RangeStep> rangeSteps, String fullRangeLabel, boolean isDollars, boolean isAmount, String keyForJsonMin, String keyForJsonMax) {
        this.rangeSteps = rangeSteps;
        this.defaultRange = defaultRange;
        this.keyForJsonMin = keyForJsonMin;
        this.keyForJsonMax = keyForJsonMax;
        setAbsolusteMinMaxValues();

        this.label = label;
        this.fullRangeLabel = fullRangeLabel;
        this.isDollars = isDollars;
        this.isAmount = isAmount;
    }

    protected void setAbsolusteMinMaxValues() {
        this.absoluteMin = rangeSteps.get(0).getValueForRange();
        this.absoluteMax = rangeSteps.get(rangeSteps.size() - 1).getValueForRange();
        this.defaultRange = (defaultRange != null) ? defaultRange : new Integer[]{absoluteMin, absoluteMax};
        initSelectedIndicesToRangeEdges();

    }


    public RangeSeekBarFiltersVM(boolean isDollars, List<RangeStep> rangeSteps, String allRangeLabel, String keyForJsonMin, String keyForJsonMax) {
        this("", null, rangeSteps, allRangeLabel, isDollars, true, keyForJsonMin, keyForJsonMax);
    }

    public RangeSeekBarFiltersVM(String label, boolean isDollars, List<RangeStep> rangeSteps, String fullRangeLabel, String keyForJsonMin, String keyForJsonMax) {
        this(label, null, rangeSteps, fullRangeLabel, isDollars, true, keyForJsonMin, keyForJsonMax);
    }

    public RangeSeekBarFiltersVM(List<RangeStep> steps, String fullRangeLabel, boolean isDollars, boolean isAmount, String keyForJsonMin, String keyForJsonMax) {
        this("", null, steps, fullRangeLabel, isDollars, isAmount, keyForJsonMin, keyForJsonMax);
    }

    @Override
    public FilterViewType getViewType() {
        return FilterViewType.RANGE;
    }

    @Override
    public void resetValues() {
        setSelectedMinMaxValues(this.defaultRange);
    }

    public String getLabel() {
        return label;
    }

    public Integer[] getDefaultRange() {
        return defaultRange;
    }

    public boolean isDollars() {
        return isDollars;
    }

    public int getSelectedMinValue() {
        return getStepValue(getSelectedMinStepIndex());
    }

    public int getSelectedMaxValue() {
        return getStepValue(getSelectedMaxStepIndex());
    }

    public int getSelectedMinValueForRange() {
        return rangeSteps.get(getSelectedMinStepIndex()).getValueForRange();
    }

    public int getSelectedMaxValueForRange() {
        return rangeSteps.get(getSelectedMaxStepIndex()).getValueForRange();
    }

    public String getSelectedMinLabel() {
        return getStepLabel(getSelectedMinStepIndex());
    }

    public String getSelectedMaxLabel() {
        return getStepLabel(getSelectedMaxStepIndex());
    }

    private String getStepLabel(int stepIndex) {
        String label = rangeSteps.get(stepIndex).getStepLabel();
        if (label != null) {
            return label;
        } else {
            if (isAmount) {
                label = Helper.getNumberFormattedWithCommas(getStepValue(stepIndex));
            } else {
                label = Integer.toString(getStepValue(stepIndex));
            }
            if (isDollars) {
                label = "$" + label;
            }

            //TODO: temporary! switch to bitmask!
            if ((stepIndex == rangeSteps.size() - 1) && (isAmount || isDollars)) {
                label += "+";
            }
        }
        return label;
    }

    protected int getStepValue(int stepIndex) {
        return rangeSteps.get(stepIndex).getStepValue();
    }

    public List<RangeStep> getRangeSteps() {
        return rangeSteps;
    }

    public int getAbsoluteMin() {
        return absoluteMin;
    }

    public int getAbsoluteMax() {
        return absoluteMax;
    }

    public String getFullRangeLabel() {
        return fullRangeLabel;
    }

    public boolean isAllRangeSelected() {
        return (getSelectedMinValueForRange() == absoluteMin) && (getSelectedMaxValueForRange() == absoluteMax);
    }

    public void setSelectedMinMaxValues(Integer minValue, Integer maxValue) {
        int i = 0;
        if (minValue != null) {
            for (i = 0; i < getRangeSteps().size(); i++) {
                RangeStep step = getRangeSteps().get(i);
                if ((step.getMinValueToSelectStep() <= minValue) && (minValue <= step.getMaxValueToSelectStep())) {
                    setSelectedMinStepIndex(i);
                    break;
                }
            }
        } else {
            setSelectedMinStepIndex(0);
        }
        if (maxValue != null) {
            for (int j = i; j < getRangeSteps().size(); j++) {
                RangeStep step = getRangeSteps().get(j);
                if ((step.getMinValueToSelectStep() <= maxValue) && (maxValue <= step.getMaxValueToSelectStep())) {
                    setSelectedMaxStepIndex(j);
                    break;
                }
            }
        } else {
            setSelectedMaxStepIndex(getRangeSteps().size() - 1);

        }
    }

    public void setSelectedMinMaxValues(Integer[] range) {
        setSelectedMinMaxValues(range[0], range[1]);
    }

    protected void initSelectedIndicesToRangeEdges() {
        selectedMinStepIndex = 0;
        selectedMaxStepIndex = rangeSteps.size() - 1;
    }

    public void setSelectedMinStepIndex(int selectedMinStepIndex) {
        this.selectedMinStepIndex = selectedMinStepIndex;
    }

    public void setSelectedMaxStepIndex(int selectedMaxStepIndex) {
        this.selectedMaxStepIndex = selectedMaxStepIndex;
    }

    public int getSelectedMinStepIndex() {
        return selectedMinStepIndex;
    }

    public int getSelectedMaxStepIndex() {
        return selectedMaxStepIndex;
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
        if (getSelectedMinStepIndex() > 0) {
            jsonObject.addProperty(keyForJsonMin, getStepValue(getSelectedMinStepIndex()));
        }
        if (getSelectedMaxStepIndex() < rangeSteps.size() - 1) {
            jsonObject.addProperty(keyForJsonMax, getStepValue(getSelectedMaxStepIndex()));
        }
    }

    //TODO: absract in parent?
    public void toggleButRent(boolean isBuySelected) {
    }
}
