package com.joinreal.model.view;

import com.joinreal.app.common.ViewTypes;
import com.joinreal.model.data.PropertyCategory;

import java.util.List;

/**
 * Created by brittbarak on 6/30/15.
 */
public class AdditionalFeaturesItemVM extends ViewModelItem {
    private String categoryName;
    private List<ViewModelItem> values;

    public AdditionalFeaturesItemVM(String categoryName, List<ViewModelItem> values) {
        this.categoryName = categoryName;
        this.values = values;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<ViewModelItem> getValues() {
        return values;
    }

    @Override
    public int getTypeIntCode() {
        return ViewTypes.ADDITIONAL_FRATURE_VIEW_TYPE;
    }

}
