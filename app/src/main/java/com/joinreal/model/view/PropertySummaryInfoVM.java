package com.joinreal.model.view;

import com.joinreal.app.common.ViewTypes;
import com.joinreal.model.data.PropertyFull;

/**
 * Created by brittbarak on 7/1/15.
 */
public class PropertySummaryInfoVM extends ViewModelItem {
    private final PropertyFull property;

    public PropertySummaryInfoVM(PropertyFull propertyFull) {
    this.property = propertyFull;
    }

    @Override
    public int getTypeIntCode() {
        return ViewTypes.SUMMARY_INFO_VIEW_TYPE;
    }

    public PropertyFull getProperty() {
        return property;
    }
}
