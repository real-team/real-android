package com.joinreal.model.view;

import com.google.gson.JsonObject;

/**
 * Created by britt on 6/3/15.
 */
public class PlusMinusFiltersVM extends FiltersViewModelItem {
    protected String label;
    protected final int defuaultValueIndex;
    private double[] values;
    private int selectedValueIndex;
    private String keyForJson;

    public PlusMinusFiltersVM(String label, double[] values, String keyForJson){
        this.label = label;
        this.values = values;
        this.keyForJson = keyForJson;
        this.defuaultValueIndex = 0;
        this.selectedValueIndex = this.defuaultValueIndex;
    }

    public String getLabel() {
        return label;
    }

    public double getSelectedValue() {
        return values[selectedValueIndex];
    }

    @Override
    public FilterViewType getViewType() {
        return FilterViewType.PLUS_MINUS_BAR;
    }

    @Override
    public void resetValues() {
        selectedValueIndex = defuaultValueIndex;
    }

    public void decrementSelectedValue() {
        selectedValueIndex--;
    }

    public void incrementSelectedValue() {
        selectedValueIndex++;
    }

    public void setSelectedValue (double value){
        for (int i= 0; i < values.length; i++) {
            if (value == values[i]) {
                this.selectedValueIndex = i;
                break;
            }
        }
    }

    public boolean isMinValueSelected() {
        return selectedValueIndex == 0;
    }

    public boolean isMaxValueSelected() {
        return selectedValueIndex == values.length - 1;
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
        jsonObject.addProperty(keyForJson, values[selectedValueIndex]);
    }
}
