package com.joinreal.model.view;

import com.joinreal.app.common.ViewTypes;

/**
 * Created by brittbarak on 6/30/15.
 */
public class OverviewVM extends ViewModelItem{
    private final String overviewStr;

    public OverviewVM(String overviewStr) {
        this.overviewStr = overviewStr;
    }

    public String getOverviewStr() {
        return overviewStr;
    }

    @Override
    public int getTypeIntCode() {
        return ViewTypes.OVERVIEW_VIEW_TYPE;
    }
}
