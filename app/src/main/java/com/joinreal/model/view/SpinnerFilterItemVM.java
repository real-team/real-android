package com.joinreal.model.view;

import com.google.gson.JsonObject;

import java.util.List;

/**
 * Created by britt on 6/7/15.
 */
public class SpinnerFilterItemVM extends FiltersViewModelItem {

    public static class SpinnerOption {
        public SpinnerOption(String label, int value) {
            this.label = label;
            this.value = value;
        }

        public String getLabel() {
            return label;
        }

        public int getValue() {
            return value;
        }

        String label;
        int value;
    }

    private final List<SpinnerOption> options;
    int selectedValueIndex = 0;
    int defaultValueIndex = 0;
    String keyForJson;

    public SpinnerFilterItemVM(List<SpinnerOption> options, String keyForJson) {
        super();
        this.options = options;
        this.keyForJson = keyForJson;
    }

    @Override
    public FilterViewType getViewType() {
        return FilterViewType.SPINNER;
    }

    @Override
    public void resetValues() {
        this.selectedValueIndex = defaultValueIndex;
    }

    public List<SpinnerOption> getOptions() {
        return options;
    }

    public void setSelectedIndex(int index) {
        this.selectedValueIndex = index;
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
        jsonObject.addProperty(keyForJson, options.get(selectedValueIndex).getValue());
    }


    public FiltersViewModelItem setSelectedIndexByValue(int value) {
        for (int i = 0; i < options.size(); i++) {
            if(value == options.get(i).getValue()) {
                selectedValueIndex = i;
                break;
            }
        }
        return this;
    }

    public int getSelectedValueIndex() {
        return selectedValueIndex;
    }
}
