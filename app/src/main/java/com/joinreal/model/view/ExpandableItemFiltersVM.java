package com.joinreal.model.view;

import android.content.Context;
import android.view.View;

import com.google.gson.JsonObject;
import com.joinreal.app.common.Helper;

import java.util.List;

/**
 * Created by britt on 6/3/15.
 */

public class ExpandableItemFiltersVM extends FiltersViewModelItem {

    String title;
    List<FiltersViewModelItem> items;
    private String msgIfNonSelected;

    public ExpandableItemFiltersVM(String title, List<FiltersViewModelItem> items) {
        this(title, items, null);
    }

    public ExpandableItemFiltersVM(String title, List<FiltersViewModelItem> items, String msgIfNonSelected) {
        this.title = title;
        this.items = items;
        this.msgIfNonSelected= msgIfNonSelected;
    }

    public String getTitle() {
        return title;
    }

    public List<FiltersViewModelItem> getItems() {
        return items;
    }

    @Override
    public FilterViewType getViewType() {
        return FilterViewType.EXPANDABLE;
    }

    @Override
    public void resetValues() {
        for (FiltersViewModelItem item : items) {
            item.resetValues();
        }
    }

    @Override
    public boolean isValidated(Context context, View parentView) {
        boolean isValid = (msgIfNonSelected == null) || (! areNoneSelected());
        if (! isValid) {
            Helper.getSnackbar(context, parentView, msgIfNonSelected).show();
        }
        return isValid;
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
            for (FiltersViewModelItem item : items) {
                item.addPropertiesToJson(jsonObject);
        }
    }

    private boolean areNoneSelected() {
        for (FiltersViewModelItem item : items) {
            if (item instanceof CheckListRowFilterItemVM) {
                if (((CheckListRowFilterItemVM) item).isSelected()){
                    return false;
                }
            }
        }
        return true;
    }
}
