package com.joinreal.model.view;

import android.view.View;

import com.joinreal.app.common.ViewTypes;

/**
 * Created by brittbarak on 7/6/15.
 */
public class ContactsActionVM extends ViewModelItem{
    String title;
    int iconId;
    private View.OnClickListener onClickListener;

    public ContactsActionVM(String title, int iconId, View.OnClickListener onClickListener) {
        this.title = title;
        this.iconId = iconId;
        this.onClickListener = onClickListener;
    }

    public String getTitle() {
        return title;
    }

    public int getIconId() {
        return iconId;
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    @Override
    public int getTypeIntCode() {
        return ViewTypes.CONTACTS_ACTION;
    }
}
