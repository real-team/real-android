package com.joinreal.model.view;

import android.os.Parcel;
import android.os.Parcelable;

import com.joinreal.app.interfaces.PropertySummaryLayoutSetter;
import com.joinreal.model.data.PropertyFull;

import java.io.Serializable;

/**
 * Created by brittbarak on 8/9/15.
 */
public class PropertyViaChatVM implements Parcelable{
    private String mlsId;
    private String mlsnum;
    private String displayImgUrl;

    public PropertyViaChatVM(PropertySummaryLayoutSetter property) {
        mlsId = property.getMlsId();
        mlsnum = property.getMlsNum();
        displayImgUrl = property.getFirstPhoto();
    }

    public String getMlsId() {
        return mlsId;
    }

    public String getMlsnum() {
        return mlsnum;
    }

    public String getDisplayImgUrl() {
        return displayImgUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mlsId);
        dest.writeString(this.mlsnum);
        dest.writeString(this.displayImgUrl);
    }

    protected PropertyViaChatVM(Parcel in) {
        this.mlsId = in.readString();
        this.mlsnum = in.readString();
        this.displayImgUrl = in.readString();
    }

    public static final Creator<PropertyViaChatVM> CREATOR = new Creator<PropertyViaChatVM>() {
        public PropertyViaChatVM createFromParcel(Parcel source) {
            return new PropertyViaChatVM(source);
        }

        public PropertyViaChatVM[] newArray(int size) {
            return new PropertyViaChatVM[size];
        }
    };
}
