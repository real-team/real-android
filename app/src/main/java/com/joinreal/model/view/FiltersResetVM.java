package com.joinreal.model.view;

import com.google.gson.JsonObject;

/**
 * Created by britt on 6/7/15.
 */
public class FiltersResetVM extends FiltersViewModelItem {

    @Override
    public FilterViewType getViewType() {
        return FilterViewType.RESET_BUTTON;
    }

    @Override
    public void resetValues() {
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
    }
}
