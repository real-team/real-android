package com.joinreal.model.view;

/**
 * Created by britt on 6/3/15.
 */
public enum FilterViewType {
    PLUS_MINUS_BAR(0), GRID(1), CHECK_LIST(2), EXPANDABLE(3), RANGE(4), BUY_RENT_BUTTONS(5), CHECKLIST_ITEM(6), RESET_BUTTON(7), EMPTY_VIEW(8), SPINNER(9);

    private final int code;

    FilterViewType(int code) {
        this.code = code;
    }

    public final int getCode() {
        return code;
    }

    public static FilterViewType getViewTypeByCode(int viewTypeCode) {
        for (FilterViewType v : FilterViewType.values()) {
            if (v.getCode() == viewTypeCode) {
                return v;
            }
        }
        return null;
    }
}
