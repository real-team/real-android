package com.joinreal.model.view;

import android.content.Intent;
import android.graphics.Bitmap;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.model.data.Agent;
import com.joinreal.model.data.Contact;
import com.joinreal.app.common.ViewTypes;
import com.joinreal.utils.ChatImagesUtils;

import java.io.Serializable;

/**
 * Created by brittbarak on 7/5/15.
 */
public class ContactVM extends ViewModelItem implements Serializable, ChatImagesUtils.IImageObject {
    Contact contact;
    private boolean isSelected;

    public ContactVM(Contact contact) {
        this.contact = contact;
    }

    public Contact getContact() {
        return contact;
    }

    @Override
    public int getTypeIntCode() {
        return ViewTypes.CHAT_USER;
    }

    public boolean hasImage() {
        return (contact.getImageId() > 0);
    }

    @Override
    public String getProfileImageFileName() {
        return contact.getProfileImageFileName();
    }

    public int getColorResource() {
        int index = (contact.getPhone() != null) ? Character.getNumericValue(contact.getPhone().charAt(contact.getPhone().length() - 1)) : 0;
        return Application.getAppContext().getResources().getIntArray(R.array.initials_colors)[index];
    }

    public String getInitials() {
        String initials = "";
        if((contact.getFirstname() != null) && (contact.getFirstname().length() > 0)){
            initials += contact.getFirstname().charAt(0);
        }
        if((contact.getLastname() != null) && (contact.getLastname().length() > 0)) {
            initials += contact.getLastname().charAt(0);
        }
        if ((contact.getFirstname() == null) && (contact.getLastname() == null) && contact.getDisplayName().length() > 0) {
            initials += contact.getDisplayName().charAt(0);
        }
        return initials;
    }

    @Override
    public Bitmap getBitmap() {
        return contact.getBitmap();
    }

    @Override
    public String getDisplayName() {
        return contact.getDisplayName();
    }

    @Override
    public String getId() {
        return String.valueOf(contact.getContactId());
    }

    @Override
    public String getChatLogin() {
        return contact.getChatLogin();
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getPhone() {
        return contact.getPhone();
    }



}
