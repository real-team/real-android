package com.joinreal.model.view;

import com.google.gson.annotations.SerializedName;
import com.joinreal.app.common.ViewTypes;
import com.joinreal.model.data.PropertyField;

import java.util.ArrayList;
import java.util.List;

import static com.joinreal.model.view.AdditionalFeaturesItemVM.*;

/**
 * Created by brittbarak on 6/30/15.
 */
public class PropertySubcategoryVM extends ViewModelItem {
    String subcategoryName;

    public PropertySubcategoryVM(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    @Override
    public int getTypeIntCode() {
        return ViewTypes.PROPERTY_SUBCATEGORY_VIEW_TYPE;
    }
}
