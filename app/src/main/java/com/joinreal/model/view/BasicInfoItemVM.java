package com.joinreal.model.view;

import com.joinreal.app.common.ViewTypes;

/**
 * Created by brittbarak on 6/28/15.
 */
public class BasicInfoItemVM extends ViewModelItem{
    String title;
    String value;

    public BasicInfoItemVM(String title, String value) {
        this.title = title;
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    @Override
    public int getTypeIntCode() {
        return ViewTypes.BASIC_INFO_VIEW_TYPE;
    }
}
