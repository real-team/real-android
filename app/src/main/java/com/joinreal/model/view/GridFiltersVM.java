package com.joinreal.model.view;

import com.google.gson.JsonObject;

import java.util.List;

/**
 * Created by britt on 6/3/15.
 */
public class GridFiltersVM extends RecyclerFiltersVM{

    private final boolean shouldSelectAllIfNonAreSelected;

    public GridFiltersVM(List<RecyclerFilterItemVM> itemVMs) {
        this(itemVMs, false);
    }

    public GridFiltersVM(List<RecyclerFilterItemVM> itemVMs, boolean shouldSelectAllIfAllNotSelected) {
        super(itemVMs);
        this.shouldSelectAllIfNonAreSelected = shouldSelectAllIfAllNotSelected;
    }

    @Override
    public FilterViewType getViewType() {
        return FilterViewType.GRID;
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
        if (shouldSelectAllIfNonAreSelected && areNonSelected()){
                selectAll();
        }
        addItemsToJson(jsonObject);
    }

    private void addItemsToJson(JsonObject jsonObject) {
        for(RecyclerFilterItemVM item : items){
            item.addPropertiesToJson(jsonObject);
        }
    }

    private void selectAll() {
        for(RecyclerFilterItemVM item : items){
            item.setSelected(true);
        }
    }

    private boolean areNonSelected() {
        for(RecyclerFilterItemVM item : items){
            if(item.isSelected()){
                return false;
            }
        }
        return true;
    }
}
