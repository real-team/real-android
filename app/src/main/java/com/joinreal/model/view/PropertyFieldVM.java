package com.joinreal.model.view;

import com.google.gson.annotations.SerializedName;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.ViewTypes;
import com.joinreal.model.data.PropertyField;

/**
 * Created by brittbarak on 6/30/15.
 */
public class PropertyFieldVM extends ViewModelItem{
    private static final String VALUE_PLACEHOLDER = "{value}";

    String name;
    String value;
    int displayOrder;
    String displayInstructions;

    public PropertyFieldVM(String name, String value, int displayOrder, String displayInstructions) {
        this.name = name;
        this.value = value;
        this.displayOrder = displayOrder;
        this.displayInstructions = displayInstructions;
    }

    public String getname() {
        return name;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    @Override
    public int getTypeIntCode() {
        return ViewTypes.PROPERTY_FIELD_VIEW_TYPE;
    }

    public String getDisplayText() {
        return displayInstructions.replace(VALUE_PLACEHOLDER, value);
//        switch (displayOrder) {
//            case PropertyField.DISPLAY_TITLE:
//                return name;
//            case PropertyField.DISPLAY_TITLE_VALUE:
//                return name + " " + value;
//            case PropertyField.DISPLAY_VALUE_TITLE:
//                return value + " " + name;
//            default:
//                return "";
//        }
    }
}
