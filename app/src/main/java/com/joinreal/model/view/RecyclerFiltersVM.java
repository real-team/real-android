package com.joinreal.model.view;

import android.graphics.drawable.Drawable;

import com.google.gson.JsonObject;
import com.joinreal.R;

import java.util.List;

/**
 * Created by britt on 6/3/15.
 */
public abstract class RecyclerFiltersVM extends FiltersViewModelItem{

    protected List<RecyclerFilterItemVM> items;

    public RecyclerFiltersVM(List<RecyclerFilterItemVM> items){
        this.items = items;
    }

    public List<RecyclerFilterItemVM> getItems() {
        return items;
    }

    @Override
    public void resetValues() {
        for (FiltersViewModelItem item : items) {
            item.resetValues();
        }
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
            for(RecyclerFilterItemVM item : items){
                item.addPropertiesToJson(jsonObject);
            }
    }
}
