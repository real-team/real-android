package com.joinreal.model.view;

import com.google.gson.JsonObject;
import com.joinreal.R;

/**
 * Created by britt on 6/3/15.
 */
public class RecyclerFilterItemVM extends FiltersViewModelItem{
    private String title;
    private int drawableIdSelected;
    private int drawableIdNotSelected;
    private int colorIdSelected;
    private int colorIdNotSelected;
    private boolean isSelected;
    private final boolean isSelectedByDefault;
    private final String keyForJson;

    public RecyclerFilterItemVM(String title, int drawableIdSelected, int drawableIdNotSelected
            , int colorIdSelected, int colorIdNotSelected, boolean isSelectedByDefault, String keyForJson) {
        this.title = title;
        this.drawableIdSelected = drawableIdSelected;
        this.drawableIdNotSelected = drawableIdNotSelected;
        this.colorIdSelected = colorIdSelected;
        this.colorIdNotSelected = colorIdNotSelected;
        this.isSelectedByDefault = isSelectedByDefault;
        this.isSelected = isSelectedByDefault;
        this.keyForJson = keyForJson;
    }

    public RecyclerFilterItemVM(String title, int drawableIdSelected, int drawableIdNotSelected
            , int colorIdSelected, int colorIdNotSelected, String keyForJson) {
        this(title, drawableIdSelected, drawableIdNotSelected, colorIdSelected, colorIdNotSelected, false, keyForJson);
    }

    public RecyclerFilterItemVM(String title, int drawableIdSelected, int drawableIdNotSelected, String keyForJson) {
        this(title, drawableIdSelected, drawableIdNotSelected, R.color.blue, R.color.gray_light, false, keyForJson);
    }

    public RecyclerFilterItemVM(String title, int drawableIdSelected, int drawableIdNotSelected, boolean isSelectedByDefault, String keyForJson) {
        this(title, drawableIdSelected, drawableIdNotSelected, R.color.blue, R.color.gray_light, isSelectedByDefault, keyForJson);
    }

    public String getTitle() {
        return title;
    }

    public int getColorIdSelected() {
        return colorIdSelected;
    }

    public int getDrawableIdNotSelected() {
        return drawableIdNotSelected;
    }

    public int getDrawableIdSelected() {
        return drawableIdSelected;
    }

    public int getColorIdNotSelected() {
        return colorIdNotSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public RecyclerFilterItemVM setSelected(boolean isSelected) {
        this.isSelected = isSelected;
        return this;
    }

    //TODO:???
    @Override
    public FilterViewType getViewType() {
        return null;
    }

    @Override
    public void resetValues() {
        this.isSelected = isSelectedByDefault;
    }

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
        jsonObject.addProperty(keyForJson, isSelected);
    }
}
