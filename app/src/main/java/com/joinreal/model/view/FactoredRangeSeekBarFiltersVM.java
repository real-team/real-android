package com.joinreal.model.view;

import com.joinreal.model.RangeStep;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by britt on 6/3/15.
 */
public class FactoredRangeSeekBarFiltersVM extends EvenStepsRangeBarFiltersVM {
    protected int factor;
    protected boolean isFactored;

    protected int selectedFactoredMinStepIndex;
    protected int selectedFactoredMaxStepIndex;

    public FactoredRangeSeekBarFiltersVM(int factor, boolean isDollars, List<RangeStep> steps, String fullRangeLabel, String keyForJsonMin, String keyForJsonMax, boolean isFactored) {
        super(isDollars, steps, fullRangeLabel, keyForJsonMin, keyForJsonMax);
        this.factor = factor;
        //TODO: producer?
        this.isFactored = isFactored;
    }

    @Override
    public int getSelectedMinValue() {
        if (isFactored) {
            return rangeSteps.get(selectedFactoredMinStepIndex).getStepValue() * factor;
        } else {
            return rangeSteps.get(selectedMinStepIndex).getStepValue();
        }
    }

    @Override
    public int getSelectedMaxValue() {
        if (isFactored) {
            return rangeSteps.get(selectedFactoredMaxStepIndex).getStepValue() * factor;
        } else {
            return rangeSteps.get(selectedMaxStepIndex).getStepValue();
        }
    }

    @Override
    public void setSelectedMinStepIndex(int selectedMinStepIndex) {
        if (isFactored) {
            this.selectedFactoredMinStepIndex = selectedMinStepIndex;
        } else {
            super.setSelectedMinStepIndex(selectedMinStepIndex);
        }
    }

    @Override
    public void setSelectedMaxStepIndex(int selectedMaxStepIndex) {
        if (isFactored) {
            this.selectedFactoredMaxStepIndex = selectedMaxStepIndex;
        } else {
            super.setSelectedMaxStepIndex(selectedMaxStepIndex);
        }
    }

    @Override
    public int getSelectedMinStepIndex() {
        if (isFactored) {
            return this.selectedFactoredMinStepIndex;
        } else {
            return super.getSelectedMinStepIndex();
        }
    }

    @Override
    public int getSelectedMaxStepIndex() {
        if (isFactored) {
            return this.selectedFactoredMaxStepIndex;
        } else {
            return super.getSelectedMaxStepIndex();
        }
    }

    @Override
    protected void initSelectedIndicesToRangeEdges() {
        super.initSelectedIndicesToRangeEdges();
        selectedFactoredMinStepIndex = selectedMinStepIndex;
        selectedFactoredMaxStepIndex = selectedMaxStepIndex;
    }

    @Override
    protected int getStepValue(int stepIndex) {
        if (isFactored) {
            return rangeSteps.get(stepIndex).getStepValue() * factor;
        } else {
            return super.getStepValue(stepIndex);
        }
    }

    @Override
    public void toggleButRent(boolean isBuySelected) {
        isFactored = isBuySelected;
    }

    public void setSelectedIndicesByValues(Integer min, Integer max) {
        int i = 0;
        if (min != null) {
            if (isFactored) {
                min /= factor;
            }
            for (i = 0; i < rangeSteps.size(); i++) {
                if (rangeSteps.get(i).getStepValue() == min) {
                    setSelectedMinStepIndex(i);
                    break;
                }
            }
        } else {
            setSelectedMinStepIndex(0);
        }

        if (max != null) {
            if (isFactored) {
                max /= factor;
            }
            for (int j = i; j < rangeSteps.size(); j++) {
                if (rangeSteps.get(j).getStepValue() == max) {
                    setSelectedMaxStepIndex(j);
                    break;
                }
            }
        } else {
            setSelectedMaxStepIndex(rangeSteps.size() - 1);
        }

    }
}
