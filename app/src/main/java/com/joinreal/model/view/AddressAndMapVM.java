package com.joinreal.model.view;

import com.joinreal.app.common.ViewTypes;
import com.joinreal.model.data.PropertyFull;
import com.joinreal.utils.MapUtils;

/**
 * Created by brittbarak on 6/30/15.
 */
public class AddressAndMapVM extends ViewModelItem {
    String addressLine1;
    String addressLine2;
    String mapUrl;
    String subdivision;
    String county;
    String area;
    String marketArea;
    String location;
    String sectionNumber;
//    String legalDescription;
//    String legalSubdivision;

    public AddressAndMapVM(PropertyFull property) {
    setAddressLine1(property.getAddress1(), property.getAddress2());
        setAddressLine2(property.getCity(), property.getState(), property.getZip());
        mapUrl = MapUtils.getStaticMapUrl(property.getLatitude(), property.getLongitude(), addressLine1 + " " + addressLine2);
        subdivision = property.getSubdivision();
        county = property.getCounty();
        area = property.getArea();
        marketArea = property.getMarketArea();
        location = property.getLocation();
        sectionNumber = property.getSectionNumber();
    }


    @Override
    public int getTypeIntCode() {
        return ViewTypes.ADDRESS_MAP_VIEW_TYPE;
    }

    public void setAddressLine1(String address1, String address2) {
        addressLine1 = address1;
        if (address2 != null){
            addressLine1 += ", " + address2;
        }
    }

    public void setAddressLine2(String city, String state, String zip) {
        addressLine2 = "";
        if(city != null) {
            addressLine2 = city;
            if (state != null || zip != null) {
                addressLine2 += ", ";
            }
        }
        if (state != null) {
            addressLine2 += state;

            if (zip != null) {
                addressLine2 += " ";
            }
        }
        if (zip != null) {
            addressLine2 += zip;
        }
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getMapUrl() {
        return mapUrl;
    }

    public String getSubdivision() {
        return subdivision;
    }

    public String getArea() {
        return area;
    }

    public String getLocation() {
        return location;
    }

    public String getCounty() {
        return county;
    }

    public String getMarketArea() {
        return marketArea;
    }

    public String getSectionNumber() {
        return sectionNumber;
    }
}
