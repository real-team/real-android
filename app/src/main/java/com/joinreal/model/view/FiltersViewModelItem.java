package com.joinreal.model.view;

import android.content.Context;
import android.view.View;

import com.joinreal.app.activities.FiltersActivity;

/**
 * Created by britt on 6/3/15.
 */
public abstract class FiltersViewModelItem extends ViewModelItem implements FiltersActivity.BuildJsonListener{

    public int getTypeIntCode() {
        return getViewType().getCode();
    }

    public abstract FilterViewType getViewType();

    public abstract void resetValues();

    @Override
    public boolean isValidated(Context context, View parentView) {
        return true;
    }
}
