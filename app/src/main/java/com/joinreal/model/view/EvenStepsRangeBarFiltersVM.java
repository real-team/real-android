package com.joinreal.model.view;

import com.joinreal.model.RangeStep;
import com.joinreal.model.RangeStepsListGenerator;

import java.util.List;

/**
 * Created by britt on 6/7/15.
 */
public class EvenStepsRangeBarFiltersVM extends RangeSeekBarFiltersVM {

    public EvenStepsRangeBarFiltersVM(String label, Integer[] defaultRange, List<RangeStep> steps, String fullRangeLabel, boolean isDollars, boolean isAmount, String keyForJsonMin, String keyForJsonMax) {
        super(label, defaultRange, steps, fullRangeLabel, isDollars, isAmount, keyForJsonMin, keyForJsonMax);
        RangeStepsListGenerator.setEvenStepsRanges(steps);
        this.defaultRange = defaultRange;
        setAbsolusteMinMaxValues();
    }

    public EvenStepsRangeBarFiltersVM(boolean isDollars, List<RangeStep> steps, String fullRangeLabel, String keyForJsonMin, String keyForJsonMax) {
        this("", null, steps, fullRangeLabel, isDollars, true, keyForJsonMin, keyForJsonMax);
    }

    public EvenStepsRangeBarFiltersVM(String label, boolean isDollars, List<RangeStep> rangeSteps, String fullRangeLabel, String keyForJsonMin, String keyForJsonMax) {
        this(label, null, rangeSteps, fullRangeLabel, isDollars, true, keyForJsonMin, keyForJsonMax);
    }

    public EvenStepsRangeBarFiltersVM(List<RangeStep> steps, String fullRangeLabel, boolean isDollars, boolean isAmount, String keyForJsonMin, String keyForJsonMax) {
        this("", null, steps, fullRangeLabel, isDollars, isAmount, keyForJsonMin, keyForJsonMax);
    }

    public void setSelectedIndicesByValues(Integer min, Integer max) {
        int i = 0;
        if (min != null) {
            for (i = 0; i < rangeSteps.size(); i++) {
                if (rangeSteps.get(i).getStepValue() == min) {
                    setSelectedMinStepIndex(i);
                    break;
                }
            }
        } else {
            setSelectedMinStepIndex(0);
        }

        if (max != null) {
            for (int j = i; j < rangeSteps.size(); j++) {
                if (rangeSteps.get(j).getStepValue() == max) {
                    setSelectedMaxStepIndex(j);
                    break;
                }
            }
        } else {
            setSelectedMaxStepIndex(rangeSteps.size() - 1);
        }

    }

}