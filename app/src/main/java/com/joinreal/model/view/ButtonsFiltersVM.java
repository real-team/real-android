package com.joinreal.model.view;

import com.google.gson.JsonObject;

/**
 * Created by britt on 6/3/15.
 */
public class ButtonsFiltersVM extends FiltersViewModelItem{

    boolean isBuyChecked;

    public ButtonsFiltersVM(boolean isBuyChecked) {
        this.isBuyChecked = isBuyChecked;
    }

    public boolean isBuyChecked() {
        return isBuyChecked;
    }

    public void setIsBuyChecked(boolean isBuyChecked) {
        this.isBuyChecked = isBuyChecked;
    }

    @Override
    public FilterViewType getViewType() {
        return FilterViewType.BUY_RENT_BUTTONS;
    }

    @Override
    public void resetValues() {}

    @Override
    public void addPropertiesToJson(JsonObject jsonObject) {
        jsonObject.addProperty("is_rental", ! isBuyChecked);
    }
}
