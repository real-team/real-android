package com.joinreal.model.view;

import com.joinreal.app.common.ViewTypes;

/**
 * Created by brittbarak on 7/6/15.
 */
public class ContactsSubtitleVM extends ViewModelItem{
    String title;
    private String selectionText;
    private String deselectionText;
    boolean isSelectionTextShown;

    public ContactsSubtitleVM(String title, String selectionText, String deselectionText) {
        this.title = title;
        this.selectionText = selectionText;
        this.deselectionText = deselectionText;
        this.isSelectionTextShown = true;
    }

    public ContactsSubtitleVM(String title) {
        this(title, null, null);
    }

    public String getTitle() {
        return title;
    }

    public String getSelectionText() {
        return selectionText;
    }

    public String getDeselectionText() {
        return deselectionText;
    }

    public boolean isSelectionTextShown() {
        return isSelectionTextShown;
    }

    public void setIsSelectionTextShown(boolean isSelectionTextShown) {
        this.isSelectionTextShown = isSelectionTextShown;
    }

    @Override
    public int getTypeIntCode() {
        return (selectionText == null) ? ViewTypes.CONTACTS_SUBTITLE : ViewTypes.CONTACTS_SUBTITLE_WITH_ACTION;
    }
}
