package com.joinreal.model.view;

/**
 * Created by brittbarak on 6/28/15.
 */
public abstract class ViewModelItem {
    public abstract int getTypeIntCode();
}
