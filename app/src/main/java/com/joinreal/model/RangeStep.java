package com.joinreal.model;

import java.text.DecimalFormat;

/**
 * Created by britt on 6/5/15.
 */
public class RangeStep{
    private int stepValue;
    private float minValueToSelectStep;
    private float maxValueToSelectStep;
    private String stepLabel;

    private int valueForRange;

    public RangeStep(int stepValue, int minValueToSelectStep, int maxValueToSelectStep, String stepLabel) {
        this.stepValue = stepValue;
        this.valueForRange = stepValue;
        this.minValueToSelectStep = minValueToSelectStep;
        this.maxValueToSelectStep = maxValueToSelectStep;
        this.stepLabel = stepLabel;

    }

    public RangeStep(int stepValue, int minValueToSelectStep, int maxValueToSelectStep) {
        this(stepValue, minValueToSelectStep, maxValueToSelectStep, null);
    }

    public RangeStep(int stepValue, String stepLabel) {
        this(stepValue, stepValue, stepValue, stepLabel);
    }

    public int getStepValue() {
        return stepValue;
    }

    public void setStepValue(int stepValue) {
        this.stepValue = stepValue;
    }

    public float getMinValueToSelectStep() {
        return minValueToSelectStep;
    }

    public void setMinValueToSelectStep(float minValueToSelectStep) {
        this.minValueToSelectStep = minValueToSelectStep;
    }

    public float getMaxValueToSelectStep() {
        return maxValueToSelectStep;
    }

    public void setMaxValueToSelectStep(float maxValueToSelectStep) {
        this.maxValueToSelectStep = maxValueToSelectStep;
    }

    public String getStepLabel() {
        return stepLabel;
    }

    public void setStepLabel(String stepLabel) {
        this.stepLabel = stepLabel;
    }


    public void setValueForRange(int valueForRange) {
        this.valueForRange = valueForRange;
    }

    public int getValueForRange() {
        return valueForRange;
    }
}
