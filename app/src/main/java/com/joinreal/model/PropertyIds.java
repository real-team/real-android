package com.joinreal.model;


/**
 * Created by britt on 6/18/15.
 */
public class PropertyIds {
    String mlsId;
    String mlsnum;

    public PropertyIds(String mlsId, String mlsnum) {
        this.mlsId = mlsId;
        this.mlsnum = mlsnum;
    }

    public String getMlsId() {
        return mlsId;
    }

    public String getMlsnum() {
        return mlsnum;
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        } else {
            return this.mlsId.equals(((PropertyIds) obj).getMlsId())
                    && this.mlsnum.equals(((PropertyIds) obj).getMlsnum());
        }
    }
}
