package com.joinreal.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.joinreal.app.common.Helper;
import com.joinreal.app.interfaces.PropertySummaryLayoutSetter;
import com.joinreal.utils.MapUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/22/15.
 */
public class PropertyFull implements Parcelable, PropertySummaryLayoutSetter {
    public static final String TIMESHARE = "Timeshare";
    public static final String FRAQTIONAL = "Fraqtional";
    private final ArrayList<String> photosArr;

    /*******************************
     ***** OVERVIEW FIELDS : *****
     *******************************/
    @SerializedName("mls")
    private String mlsId;
    private String mlsnum;
    @SerializedName("list_status")
    private String listStatus;
    @SerializedName("date_available")
    private String dateAvailable;
    private String description;


    /*******************************
     ***** BASIC INFO FIELDS : *****
     *******************************/
    /******* PROPERTY TYPE FIELDS: ***/
    @SerializedName("property_type")
    private String propertyType;
    @SerializedName("property_type_description")
    private String propertyTypeDesc;
    @SerializedName("unit_features")
    private String unitFeatures;
    @SerializedName("house_on_property")
    private String houseOnProperty;
    @SerializedName("unit_level")
    private String unitLevel;
    @SerializedName("unit_location")
    private String unitLocation;

    @SerializedName("property_category")
    private String propertyCategory;
    private String ownership;
    @SerializedName("for_lease")
    private Boolean isForLease;
    @SerializedName("for_sale")
    private Boolean isForSale;
    private Double beds;
    private Double baths;
    @SerializedName("sq_ft")
    private Double sqFt;
    @SerializedName("price_sq_ft")
    private Double priceSqFt;
    @SerializedName("land_size")
    private String landSize;
    @SerializedName("land_use")
    private String landUse;
    @SerializedName("lot_size")
    private String lotSize;
    @SerializedName("lot_desc")
    private String lotDesc;
    private Double acres;
    @SerializedName("acres_desc")
    private String acresDesc;
    @SerializedName("year_built")
    private Integer yearBuilt;
    @SerializedName("days_on_market")
    private String daysOnMarket;
    @SerializedName("modified")
    private String dateModified;
    @SerializedName("maint_fee")
    private String maintFees;
    @SerializedName("deposit_security")
    private Boolean depositSecurity;
    @SerializedName("deposit_pets")
    private String depositPets;
    @SerializedName("hoa_mngmnt")
    private String hoaMngmnt;
    @SerializedName("fees_other")
    private String feesOther;
    @SerializedName("building_fees_other")
    private String buildingFeesOther;
    @SerializedName("application_fee")
    private Boolean applicationFee;
    @SerializedName("application_aproval_required")
    private String approvalRequired;

    /******* NEW CONSTRUCTION FIELDS: ***/
    @SerializedName("construction_new_desc")
    private String constructionNewDesc;
    @SerializedName("construction_completed_date")
    private String constructionCompletedDate;
    @SerializedName("construction_completed_date_aprrx")
    private String constructionCompletedDateApprx;
    @SerializedName("construction_builder")
    private String constructionBuilder;

    private String defects;

    /**********************************
     ***** ADDRESS & MAP FIELDS : *****
     **********************************/
    private String address1;
    private String address2;
    private String state;
    private String city;
    private String zip;
    private String subdivision;
    private String county;
    private String area;
    @SerializedName("geo_market_area")
    private String marketArea;
    private String location;
    @SerializedName("section_number")
    private String sectionNumber;
    @SerializedName("latitude")
    private Double lat;
    @SerializedName("longitude")
    private Double lng;

    @SerializedName("is_active")
    private boolean isActive;
    @SerializedName("is_pending")
    private boolean isPending;
    @SerializedName("is_sold")
    private boolean isSold;
    private String status;
    @SerializedName("listing_status")
    private String listingStatus;
    @SerializedName("is_land")
    private boolean isLand;
    @SerializedName("list_price")
    private Integer price;
    @SerializedName("listing_agent_phone")
    private String listingAgentPhone;
    private String remarks;
    private String notes;
//    @SerializedName("photos_count")
//    private int photosCount;
//    private List<Photo> photos = new ArrayList<>();
//    @SerializedName("has_photos")
//    private boolean hasPhotos;
    private boolean favorite;
    private String staticMapUrl;

    public PropertyFull(JSONObject propertyJson) {
        this.mlsId = propertyJson.optString("mls");
        this.mlsnum = propertyJson.optString("mlsnum");
        try {
            this.price = propertyJson.getInt("list_price");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.beds = propertyJson.getDouble("beds");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.baths = propertyJson.getDouble("baths");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.lat = propertyJson.getDouble("latitude");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.lng = propertyJson.getDouble("longitude");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.favorite = propertyJson.getBoolean("favorite");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.address1 = propertyJson.optString("address1", null);
        this.city = propertyJson.optString("city", null);
        this.state = propertyJson.optString("state", null);
        this.county = propertyJson.optString("county", null);
        this.zip = propertyJson.optString("zip", null);
        this.subdivision = propertyJson.optString("subdivision", null);
        this.area = propertyJson.optString("area", null);
        this.marketArea = propertyJson.optString("geo_market_area", null);
        this.location = propertyJson.optString("location", null);
        this.sectionNumber = propertyJson.optString("section_number", null);

        this.listingStatus = propertyJson.optString("listing_status", null);
        this.dateAvailable = propertyJson.optString("date_available", null);
        this.description = propertyJson.optString("description", null);
        this.remarks = propertyJson.optString("remarks", null);
        this.notes = propertyJson.optString("notes", null);

        this.propertyType = propertyJson.optString("property_type", null);
        this.propertyTypeDesc = propertyJson.optString("property_type_description", null);
        this.unitFeatures = propertyJson.optString("unit_features", null);
        this.houseOnProperty = propertyJson.optString("house_on_property", null);
        this.unitLevel = propertyJson.optString("unit_level", null);
        this.unitLocation = propertyJson.optString("unit_location", null);

        this.propertyCategory = propertyJson.optString("property_category", null);
        this.ownership = propertyJson.optString("ownership", null);
        try {
            this.isForLease = propertyJson.getBoolean("for_lease");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.isForSale = propertyJson.getBoolean("for_sale");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.unitLocation = propertyJson.optString("for_sale", null);

        try {
            this.sqFt = propertyJson.getDouble("sq_ft");
        } catch (JSONException e) {
        }
        try {
            this.priceSqFt = propertyJson.getDouble("price_sq_ft");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.landSize = propertyJson.optString("land_size", null);
        this.landUse = propertyJson.optString("land_use", null);
        this.lotSize = propertyJson.optString("lot_size", null);
        this.lotDesc = propertyJson.optString("lot_desc", null);
        try {
            this.acres = propertyJson.getDouble("acres");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.acresDesc = propertyJson.optString("acres_desc", null);
        try {
            this.yearBuilt = propertyJson.getInt("year_built");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.daysOnMarket = propertyJson.optString("days_on_market", null);
        this.dateModified = propertyJson.optString("modified", null);
        this.maintFees = propertyJson.optString("maint_fee", null);
        try {
            this.depositSecurity = propertyJson.getBoolean("deposit_security");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.depositPets = propertyJson.optString("deposit_pets", null);

        this.hoaMngmnt = propertyJson.optString("hoa_mngmnt", null);
        this.feesOther = propertyJson.optString("fees_other", null);
        this.buildingFeesOther = propertyJson.optString("building_fees_other", null);
        try {
            this.applicationFee = propertyJson.getBoolean("application_fee");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.approvalRequired = propertyJson.optString("application_aproval_required", null);

        this.constructionNewDesc = propertyJson.optString("construction_new_desc", null);
        this.constructionCompletedDate = propertyJson.optString("construction_completed_date", null);
        this.constructionCompletedDateApprx = propertyJson.optString("construction_completed_date_aprrx", null);
        this.constructionBuilder = propertyJson.optString("construction_builder", null);

        this.defects = propertyJson.optString("defects", null);


        photosArr = new ArrayList<>();
        JSONArray a = propertyJson.optJSONArray("photos");
        for (int i = 0; i < a.length(); i ++) {
            photosArr.add(a.optJSONObject(i).optString("medium"));
        }
        photosArr.add(getStaticMapUrl());
    }


    public String getType() {
        if (propertyType != null) {
            return propertyType;
        } else if (propertyTypeDesc != null) {
            return propertyTypeDesc;
        } else if (unitFeatures != null) {
            return unitFeatures;
        } else if (houseOnProperty != null) {
            return houseOnProperty;
        } else if (unitLevel != null) {
            return unitLevel;
        } else {
            return unitLocation;
        }
    }

    public String getCategory() {
        return propertyCategory;
    }

    public String getOwnership() {
        return ownership;
    }

    public Boolean getForLease() {
        return isForLease;
    }

    public Boolean getForSale() {
        return isForSale;
    }

    public Double getBeds() {
        return beds;
    }

    public Double getBaths() {
        return baths;
    }

    public ArrayList<String> getPhotosMeduim() {
//        ArrayList<String> list = new ArrayList<>();
//        if (photos != null) {
//            for (Photo photo : photos){
//                list.add(photo.getMedium());
//            }
//        }
//        return list;
        return photosArr;
    }

    @Override
    public Double getLatitude() {
        return lat;
    }

    @Override
    public Double getLongitude() {
        return lng;
    }

    @Override
    public boolean isFavorite() {
        return favorite;
    }

    @Override
    public void setFavorite(boolean isFavorite) {
        this.favorite = isFavorite;
    }

    public Double getSqFt() {
        return sqFt;
    }

    public Double getPriceSqFt() {
        return priceSqFt;
    }

    public String getLandSize() {
        return landSize;
    }

    public String getLandUse() {
        return landUse;
    }

    public String getLotSize() {
        return lotSize;
    }

    public String getLotDesc() {
        return lotDesc;
    }

    public Double getAcres() {
        return acres;
    }

    public String getAcresDesc() {
        return acresDesc;
    }

    public Integer getYearBuilt() {
        return yearBuilt;
    }

    public String getDaysOnMarket() {
        return daysOnMarket;
    }

    public String getDateModified() {
        return dateModified;
    }

    public String getMaintFees() {
        return maintFees;
    }

    public Boolean getDepositSecurity() {
        return depositSecurity;
    }

    public String getDepositPets() {
        return depositPets;
    }

    public String getHoaMngmnt() {
        return hoaMngmnt;
    }

    public String getFeesOther() {
        return feesOther;
    }

    public String getBuildingFeesOther() {
        return buildingFeesOther;
    }

    public Boolean getApplicationFee() {
        return applicationFee;
    }

    public String getApprovalRequired() {
        return approvalRequired;
    }

    public String getNewConstruction() {
        if(constructionNewDesc != null) {
            return constructionNewDesc;
        } else if(constructionCompletedDate != null) {
            return constructionCompletedDate;
        } else if (constructionCompletedDateApprx != null) {
            return constructionCompletedDateApprx;
        } else {
            return constructionBuilder;
        }
    }

    public String getDefects() {
        return defects;
    }

    @Override
    public String getMlsId() {
        return mlsId;
    }

    @Override
    public String getMlsNum() {
        return mlsnum;
    }

    @Override
    public String getFullAddress() {
        String fullAddress = "";
        if((address1 != null) && (address1.length() > 0)){
            fullAddress = address1;
            if (city != null) {
                fullAddress = fullAddress + ", ";
            }
        }
        if (city != null) {
            fullAddress = fullAddress + city;
        }
        return fullAddress;
    }

    @Override
    public String getFormattedPrice() {
        if (price == null) {
            return "N/A";
        } else {
            return "$" + Helper.getNumberFormattedWithCommas(price);
        }
    }

    public String getShortPrice() {
        if (price == null) {
            return "N/A";
        } else {
            return "$" + Helper.getShortPrice(price);
        }
    }

    public String getListStatus() {
        return listStatus;
    }

    public String getDateAvailable() {
        return dateAvailable;
    }

    public String getDescription() {
        return description;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public String getSubdivision() {
        return subdivision;
    }

    public String getCounty() {
        return county;
    }

    public String getArea() {
        return area;
    }

    public String getMarketArea() {
        return marketArea;
    }

    public String getLocation() {
        return location;
    }

    public String getSectionNumber() {
        return sectionNumber;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getNotes() {
        return notes;
    }

    public String getFirstPhoto() {
//        if ((photos != null) && (! photos.isEmpty())){
//            return photos.get(0).getMedium();
//        }
        if ((photosArr != null) && (photosArr.size() > 0)) {
            return photosArr.get(0);
        }
        return staticMapUrl;
    }

    public String getStaticMapUrl() {
        if (staticMapUrl == null) {
            setStaticMapUrl();
        }
        return staticMapUrl;
    }

    public void setStaticMapUrl(String staticMapUrl) {
        this.staticMapUrl = staticMapUrl;
    }

    public void setStaticMapUrl() {
        this.staticMapUrl = MapUtils.getStaticMapUrl(getLatitude(), getLongitude(), getFullAddress());
    }

    public String getAddressForLocation() {
        return getFullAddress() + " , " + getState();
    }

    public String getListingAgentPhone() {
        return listingAgentPhone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.photosArr);
        dest.writeString(this.mlsId);
        dest.writeString(this.mlsnum);
        dest.writeString(this.listStatus);
        dest.writeString(this.dateAvailable);
        dest.writeString(this.description);
        dest.writeString(this.propertyType);
        dest.writeString(this.propertyTypeDesc);
        dest.writeString(this.unitFeatures);
        dest.writeString(this.houseOnProperty);
        dest.writeString(this.unitLevel);
        dest.writeString(this.unitLocation);
        dest.writeString(this.propertyCategory);
        dest.writeString(this.ownership);
        dest.writeValue(this.isForLease);
        dest.writeValue(this.isForSale);
        dest.writeValue(this.beds);
        dest.writeValue(this.baths);
        dest.writeValue(this.sqFt);
        dest.writeValue(this.priceSqFt);
        dest.writeString(this.landSize);
        dest.writeString(this.landUse);
        dest.writeString(this.lotSize);
        dest.writeString(this.lotDesc);
        dest.writeValue(this.acres);
        dest.writeString(this.acresDesc);
        dest.writeValue(this.yearBuilt);
        dest.writeString(this.daysOnMarket);
        dest.writeString(this.dateModified);
        dest.writeString(this.maintFees);
        dest.writeValue(this.depositSecurity);
        dest.writeValue(this.depositPets);
        dest.writeString(this.hoaMngmnt);
        dest.writeString(this.feesOther);
        dest.writeString(this.buildingFeesOther);
        dest.writeValue(this.applicationFee);
        dest.writeString(this.approvalRequired);
        dest.writeString(this.constructionNewDesc);
        dest.writeString(this.constructionCompletedDate);
        dest.writeString(this.constructionCompletedDateApprx);
        dest.writeString(this.constructionBuilder);
        dest.writeString(this.defects);
        dest.writeString(this.address1);
        dest.writeString(this.address2);
        dest.writeString(this.state);
        dest.writeString(this.city);
        dest.writeString(this.zip);
        dest.writeString(this.subdivision);
        dest.writeString(this.county);
        dest.writeString(this.area);
        dest.writeString(this.marketArea);
        dest.writeString(this.location);
        dest.writeString(this.sectionNumber);
        dest.writeValue(this.lat);
        dest.writeValue(this.lng);
        dest.writeByte(isActive ? (byte) 1 : (byte) 0);
        dest.writeByte(isPending ? (byte) 1 : (byte) 0);
        dest.writeByte(isSold ? (byte) 1 : (byte) 0);
        dest.writeString(this.status);
        dest.writeString(this.listingStatus);
        dest.writeByte(isLand ? (byte) 1 : (byte) 0);
        dest.writeInt(this.price);
        dest.writeString(this.listingAgentPhone);
        dest.writeString(this.remarks);
        dest.writeString(this.notes);
        dest.writeByte(favorite ? (byte) 1 : (byte) 0);
        dest.writeString(this.staticMapUrl);
    }

    protected PropertyFull(Parcel in) {
        this.photosArr = in.createStringArrayList();
        this.mlsId = in.readString();
        this.mlsnum = in.readString();
        this.listStatus = in.readString();
        this.dateAvailable = in.readString();
        this.description = in.readString();
        this.propertyType = in.readString();
        this.propertyTypeDesc = in.readString();
        this.unitFeatures = in.readString();
        this.houseOnProperty = in.readString();
        this.unitLevel = in.readString();
        this.unitLocation = in.readString();
        this.propertyCategory = in.readString();
        this.ownership = in.readString();
        this.isForLease = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isForSale = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.beds = (Double) in.readValue(Double.class.getClassLoader());
        this.baths = (Double) in.readValue(Double.class.getClassLoader());
        this.sqFt = (Double) in.readValue(Double.class.getClassLoader());
        this.priceSqFt = (Double) in.readValue(Double.class.getClassLoader());
        this.landSize = in.readString();
        this.landUse = in.readString();
        this.lotSize = in.readString();
        this.lotDesc = in.readString();
        this.acres = (Double) in.readValue(Double.class.getClassLoader());
        this.acresDesc = in.readString();
        this.yearBuilt = (Integer) in.readValue(Integer.class.getClassLoader());
        this.daysOnMarket = in.readString();
        this.dateModified = in.readString();
        this.maintFees = in.readString();
        this.depositSecurity = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.depositPets = (String) in.readValue(Boolean.class.getClassLoader());
        this.hoaMngmnt = in.readString();
        this.feesOther = in.readString();
        this.buildingFeesOther = in.readString();
        this.applicationFee = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.approvalRequired = in.readString();
        this.constructionNewDesc = in.readString();
        this.constructionCompletedDate = in.readString();
        this.constructionCompletedDateApprx = in.readString();
        this.constructionBuilder = in.readString();
        this.defects = in.readString();
        this.address1 = in.readString();
        this.address2 = in.readString();
        this.state = in.readString();
        this.city = in.readString();
        this.zip = in.readString();
        this.subdivision = in.readString();
        this.county = in.readString();
        this.area = in.readString();
        this.marketArea = in.readString();
        this.location = in.readString();
        this.sectionNumber = in.readString();
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lng = (Double) in.readValue(Double.class.getClassLoader());
        this.isActive = in.readByte() != 0;
        this.isPending = in.readByte() != 0;
        this.isSold = in.readByte() != 0;
        this.status = in.readString();
        this.listingStatus = in.readString();
        this.isLand = in.readByte() != 0;
        this.price = in.readInt();
        this.listingAgentPhone = in.readString();
        this.remarks = in.readString();
        this.notes = in.readString();
        this.favorite = in.readByte() != 0;
        this.staticMapUrl = in.readString();
    }

    public static final Creator<PropertyFull> CREATOR = new Creator<PropertyFull>() {
        public PropertyFull createFromParcel(Parcel source) {
            return new PropertyFull(source);
        }

        public PropertyFull[] newArray(int size) {
            return new PropertyFull[size];
        }
    };
}

