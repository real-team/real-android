package com.joinreal.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by brittbarak on 8/13/15.
 */
public class SystemSettings {

    @SerializedName("min_version_agent")
    int minVersionAgent;
    @SerializedName("min_version_client")
    int minVersionClient;

    public int getMinVersionAgent() {
        return minVersionAgent;
    }

    public int getMinVersionCustomer() {
        return minVersionClient;
    }
}
