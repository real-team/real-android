package com.joinreal.model.data;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by brittbarak on 7/5/15.
 */
public class Contact implements Serializable{
    @SerializedName("id")
    private Integer contactId;
    @SerializedName("agent_id")
    private Integer agentId;
    private String firstname;
    private String lastname;
    private String middlename;
    private String phone; //phone to display
    private String label; //label for phone
    private String username; //normalized phone
    private String email;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    @SerializedName("profile_image_file_name")
    private String profileImageFileName;
    @SerializedName("profile_image_content_type")
    private String profileImageContentType;
    @SerializedName("profile_image_file_size")
    private Integer profileImageFileSize;
    @SerializedName("profile_image_updated_at")
    private String profileImageUpdatedAt;
    private Integer status;
    @SerializedName("agent_notes")
    private String agentNotes;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
//    @SerializedName("profile_image")
//    private Photo profileImage;
    @SerializedName("last_activity")
    private String lastSignInAt;
//    @SerializedName("invited_at")
//    private String invitedAt;
    @SerializedName("user_id")
    private Integer userId;

    private int imageId = -1;
    private String displayName;
    private ArrayList<PhoneListing> phoneNumbers;
    private int dialogNum = 0;
    private int newMessageNum = 0;
    private transient Bitmap photo;
    private long androidContactId;
//    private String emailAdress;
    private String chatLogin;
    private String name;

    private int version;

    public Contact(String name) {
        phoneNumbers = new ArrayList<>();
        this.displayName = name;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getDisplayName() {
        if (displayName == null) {
            displayName = getFullName();
        }
        if (displayName.equals("")) {
            displayName = phone;
        }
        return displayName;
    }

    private String getFullName() {
        String fullName = "";
        if ((name != null) && (! name.isEmpty())){
            fullName = name;
        } else {
            fullName = buildFullName();
        }
        return fullName;
    }

    @NonNull
    private String buildFullName() {
        String fullName;
        fullName = ((firstname == null) ? "" : firstname);
        if (middlename != null) {
            if (!fullName.equals("")) {
                fullName += " ";
            }
            fullName += middlename;
        }
        if (lastname != null) {
            if (!fullName.equals("")) {
                fullName += " ";
            }
            fullName += lastname;
        }
        return fullName;
    }

    public void setDisplayName(String name) {
        this.displayName = name;
    }

    public ArrayList<PhoneListing> getPhoneNumbers() {
        return phoneNumbers;
    }

    public String getPhone() {
        if(phone !=null) {
            return phone;
        }
        if ((phoneNumbers == null) || phoneNumbers.isEmpty()) {
            return null;
        } else {
            String phone = phoneNumbers.get(0).getPhone();
            return phone.replace("+", "");
        }
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }



    public void addPhone(PhoneListing phoneListing) {
        phoneNumbers.add(phoneListing);
    }

    public int getDialogNum() {
        return dialogNum;
    }

    public void setDialogNum(int dialogNum) {
        this.dialogNum = dialogNum;
    }

    public int getNewMessageNum() {
        return newMessageNum;
    }

    public void setNewMessageNum(int newMessageNum) {
        this.newMessageNum = newMessageNum;
    }

    public Bitmap getBitmap() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public String getLastSignInAt() {
        return lastSignInAt;
    }

    public void setLastSignInAt(String lastSignInAt) {
        this.lastSignInAt = lastSignInAt;
    }

    public String getProfileImageFileName() {
        return profileImageFileName;
    }

    public void setProfileImageFileName(String profileImageFileName) {
        this.profileImageFileName = profileImageFileName;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddleName() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setAndroidContactId(long androidContactId) {
        this.androidContactId = androidContactId;
    }

    public long getAndroidContactId() {
        return androidContactId;
    }

    public String getAgentNotes() {
        return agentNotes;
    }

    public void setAgentNotes(String agentNotes) {
        this.agentNotes = agentNotes;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public boolean hasLocation() {
        return ((city != null) && (state != null)) ||
                ((city != null) && (zip != null)) ||
                ((zip != null) && (state != null));
    }

    public String getAddress(){
        return city + ", " + zip + ", " + state;
    }

    public Uri getLocationURI() {
        String s = "geo:0,0?q=";
        if (city != null) {
            s += city;

            if ((zip != null) || (state != null)) {
                s += ", ";
            }
        }

        if (zip != null) {
            s += zip;
        }

        if (state != null) {
            s += state;
        }
    return Uri.parse(Uri.encode(s));
    }

    public String getChatLogin() {
        return username;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getVersion() {
        return version;
    }

    public void addPhone(String phone, String label) {
        phoneNumbers.add(new PhoneListing(phone, label));
    }

    public class PhoneListing {
        private final String phone;
        private final String label;

        public PhoneListing(String phone, String label) {
            this.phone = phone;
            this.label = label;
        }

        public String getPhone() {
            return phone;
        }

        public String getLabel() {
            return label;
        }
    }


}
