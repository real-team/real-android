package com.joinreal.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by britt on 6/22/15.
 */
public class ProperyCategory {

    private String name;


    @SerializedName("value")
    private List<PropertySubCategory> subCategories;

    public List<PropertySubCategory> getSubCategories() {
        return subCategories;
    }

    public String getName() {
        return name;
    }

    private class PropertySubCategory {
        private String name;
        @SerializedName("value")
        private PropertyField field;

        public String getName() {
            return name;
        }

        public PropertyField getField() {
            return field;
        }

    }

    private class PropertyField {
        private String display;
        private String key;

        public String getDisplay() {
            return display;
        }

        public String getKey() {
            return key;
        }
    }
}
