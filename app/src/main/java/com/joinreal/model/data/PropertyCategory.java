package com.joinreal.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/25/15.
 */
public class PropertyCategory{
    @SerializedName("name")
    String categoryName;
    @SerializedName("value")
    List<PropertySubcategory> subcategories;

    public String getCategoryName() {
        return categoryName;
    }

    public List<PropertySubcategory> getSubcategories() {
        return subcategories;
    }

    public void adjustFields() {
        if (subcategories != null) {
            for (PropertySubcategory subcategory : subcategories) {
                subcategory.adjustFields();
            }
        }
    }
}
