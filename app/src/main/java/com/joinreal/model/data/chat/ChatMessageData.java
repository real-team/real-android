package com.joinreal.model.data.chat;

import com.joinreal.app.Application;
import com.joinreal.app.common.StaticParams;
import com.qb.gson.annotations.SerializedName;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatMessage;

import org.jivesoftware.smack.packet.Message;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Nick on 10.05.2015.
 */
public class ChatMessageData implements Serializable {

    @SerializedName("dataFromWeb")
    QBChatMessage message;
    @SerializedName("isOutgoing")
    private boolean isOutgoing;
//    @SerializedName("isRead")
//    private boolean isRead;
    @SerializedName("showAvatar")
    private boolean showAvatar;
    @SerializedName("read")
    private Boolean isRead = null;

//    public ChatMessageData(String id,String text,Long time,String url,boolean isOutgoing,int timer){
//        this.text = text;
//        this.time = time;
//        this.imageUrl = url;
//        this.isOutgoing = isOutgoing;
//        this.timer = timer;
//        this.id = id;
//    }

    public ChatMessageData(QBChatMessage message){
        this.message = message;
        if (message.getSenderId() == null || message.getSenderId().equals(Application.getCurrentChatUser().getId())){
            isOutgoing = true;
        } else {
            isOutgoing = false;
        }
    }

    public boolean isRead() {
        if (isRead!=null){
            return isRead;
        }
        return message.isRead();
    }

    public QBChatMessage getMessage() {
        return message;
    }

    public String getMsgType(){
        return message.getProperty("type");
    }

    public void setMessage(QBChatMessage message) {
        this.message = message;
    }

    public boolean isShowAvatar() {
        return showAvatar;
    }

    public void setShowAvatar(boolean showAvatar) {
        this.showAvatar = showAvatar;
    }

    public String getId() {
        return message.getId();
    }

    public boolean isOutgoing() {
        return isOutgoing;
    }

    public void setIsOutgoing(boolean isOutgoing) {
        this.isOutgoing = isOutgoing;
    }

    public String getProperty(String name) {
        return message.getProperty(name);
    }

    public void setRecipientId(Integer recipientId) {
        message.setRecipientId(recipientId);
    }

    public long getDateSent() {
        return message.getDateSent();
    }

    public void setBody(String body) {
        message.setBody(body);
    }

    public String setProperty(String name, String value) {
        return message.setProperty(name, value);
    }

    public void setMarkable(boolean markable) {
        message.setMarkable(markable);
    }

    public Collection<String> getPropertyNames() {
        return message.getPropertyNames();
    }

    public Collection<QBAttachment> getAttachments() {
        return message.getAttachments();
    }

    public Integer getSenderId() {
        return message.getSenderId();
    }

    public Integer getRecipientId() {
        return message.getRecipientId();
    }

    public String removeProperty(String name) {
        return message.removeProperty(name);
    }

    public Message getSmackMessage() {
        return message.getSmackMessage();
    }

    public boolean addAttachment(QBAttachment attachment) {
        return message.addAttachment(attachment);
    }

    public String getBody() {
        return message.getBody();
    }

    public String getDialogId() {
        return message.getDialogId();
    }

    public void setDateSent(long dateSent) {
        message.setDateSent(dateSent);
    }

    public void setAttachments(Collection<QBAttachment> attachments) {
        message.setAttachments(attachments);
    }

    public void setId(String id) {
        message.setId(id);
    }

    public boolean isMarkable() {
        return message.isMarkable();
    }

    public Map<String, String> getProperties() {
        return message.getProperties();
    }

    public boolean removeAttachment(QBAttachment attachment) {
        return message.removeAttachment(attachment);
    }

    public void setDialogId(String dialogId) {
        message.setDialogId(dialogId);
    }

    public void setSaveToHistory(boolean saveToHistory) {
        message.setSaveToHistory(saveToHistory);
    }

    public String getImageUrl() {
        return message.getProperty(StaticParams.KEY_PROPERTY_DISPLAY_IMAGE_URL);
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }
}
