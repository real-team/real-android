package com.joinreal.model.data.chat;

/**
 * Created by nataraj on 3/11/15.
 */
public class ChatCountryCode implements Comparable<ChatCountryCode> {
    private String code = "";
    private String country = "";


    public ChatCountryCode() {
    }

    public ChatCountryCode(String code, String country) {
        this.code = code;
        this.country  =country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int compareTo(ChatCountryCode obj) {
        return country.compareTo(obj.country);
    }
}
