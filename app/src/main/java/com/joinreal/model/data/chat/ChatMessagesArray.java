package com.joinreal.model.data.chat;

import com.qb.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nick on 10.05.2015.
 */
public class ChatMessagesArray {

//    @SerializedName("opponentEntrance")
//    private Long lastOpponentEntrance;
    @SerializedName("data")
    private List<ChatMessageData> data = new ArrayList<>();

    public ChatMessagesArray(List<ChatMessageData> messages) {
        this.data = messages;
    }

//    public Long getLastOpponentEntrance() {
//        return lastOpponentEntrance;
//    }
//
//    public void setLastOpponentEntrance(Long lastOpponentEntrance) {
//        this.lastOpponentEntrance = lastOpponentEntrance;
//    }

    public List<ChatMessageData> getData() {
        return data;
    }

    public void setData(List<ChatMessageData> data) {
        this.data = data;
    }
}
