package com.joinreal.model.data;

import java.io.Serializable;

/**
 * Created by britt on 6/16/15.
 */
public class Photo implements Serializable{

    private String large;
    private String medium;
    private String small;
    private String main;

    public String getLarge() {
        return large;
    }

    public String getMedium() {
        return medium;
    }

    public String getSmall() {
        return small;
    }

    public String getMain() {
        return main;
    }
}
