package com.joinreal.model.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by britt on 6/9/15.
 */
public class SearchFilter implements Serializable{
    @SerializedName("resource_id")
    int resourceId;
    @SerializedName("resource_type")
    String resourceType;
    @SerializedName("location_type")
    String locationType;
    @SerializedName("is_active")
    boolean isActive;
    @SerializedName("is_pending")
    boolean isPending;
    @SerializedName("is_sold")
    boolean isSold;
    @SerializedName("is_rental")
    boolean isRental;
    @SerializedName("is_house")
    Boolean isHouse;
    @SerializedName("is_condo")
    Boolean isCondo;
    @SerializedName("is_townhouse")
    Boolean isTownhouse;
    @SerializedName("is_land")
    Boolean isLand;
    @SerializedName("is_multifamily")
    Boolean isMultifamily;
    @SerializedName("state")
    String state;
    @SerializedName("city")
    String city;
    @SerializedName("zip")
    String zip;
    @SerializedName("dom")
    int daysOnMarket;
    @SerializedName("agent_id")
    int agentId;
    @SerializedName("sort_by")
    String sortBy;
    @SerializedName("price_min")
    Integer priceMin;
    @SerializedName("price_max")
    Integer priceMax;
    @SerializedName("baths_min")
    Double bathsMin;
    @SerializedName("beds_min")
    Double bedsMin;
    @SerializedName("stories_min")
    int storiesMin;
    @SerializedName("stories_max")
    int storiesMax;
    @SerializedName("sq_ft_min")
    Integer sqFtMin;
    @SerializedName("sq_ft_max")
    Integer sqFtMax;
    @SerializedName("acres_min")
    Integer lotSizeSqFtMin;
    @SerializedName("acres_max")
    Integer lotSizeSqFtMax;
    @SerializedName("year_built_min")
    int yearBuiltMin;
    @SerializedName("year_built_max")
    int yearBuiltMax;
    @SerializedName("latitude")
    float latitude;
    @SerializedName("longitude")
    float longitude;
    @SerializedName("is_price_reduction")
    boolean isPriceReduction;
    @SerializedName("exclude_short_sales")
    boolean excludeShortSales;
    @SerializedName("is_new_construction")
    boolean isNewConstruction;
    @SerializedName("is_foreclosure")
    boolean isForeclosure;
    @SerializedName("is_wheelchair_handicap_access")
    boolean isWheelchairHandicapAccess;
    @SerializedName("has_photos")
    boolean hasPhotos;
    @SerializedName("has_waterfront")
    boolean hasWaterfront;
    @SerializedName("has_pool_private")
    boolean hasPoolPrivate;
    @SerializedName("has_pool_area")
    boolean hasPoolArea;
    @SerializedName("has_spa_hot_tub")
    boolean hasSpaHotTub;
    @SerializedName("has_fireplace")
    boolean hasFireplace;
    @SerializedName("has_porch")
    boolean hasPorch;
    @SerializedName("has_patio_deck")
    boolean hasPatioDeck;
    @SerializedName("has_back_yard")
    boolean hasBackYard;
    @SerializedName("has_tennis")
    boolean hasTennis;
    @SerializedName("has_golf")
    boolean hasGolf;
    @SerializedName("has_controlled_access")
    boolean hasControlledAccess;
    @SerializedName("parking_lots_min")
    float parkingLotsMin;

    @SerializedName("street_name")
    String streetName;

    public Double getBedsMin() {
        return bedsMin;
    }

    public Double getBathsMin() {
        return bathsMin;
    }

    public Integer getPriceMin() {
        return priceMin;
    }

    public Integer getPriceMax() {
        return priceMax;
    }

    public boolean isHouse() {
        return (isHouse == null) ? true : isHouse;
    }

    public boolean isCondo() {
        return (isCondo == null) ? true : isCondo;
    }

    public boolean isTownhouse() {
        return (isTownhouse == null) ? true : isTownhouse;
    }

    public boolean isLand() {
        return (isLand == null) ? true : isLand;
    }

    public boolean isMultifamily() {
        return (isMultifamily == null) ? true : isMultifamily;
    }

    public Integer getSqFtMin() {
        return sqFtMin;
    }

    public Integer getSqFtMax() {
        return sqFtMax;
    }

    public Integer getLotSizeSqFtMin() {
        return lotSizeSqFtMin;
    }

    public Integer getLotSizeSqFtMax() {
        return lotSizeSqFtMax;
    }

    public int getYearBuiltMin() {
        return yearBuiltMin;
    }

    public int getYearBuiltMax() {
        return yearBuiltMax;
    }

    public float getParkingLotsMin() {
        return parkingLotsMin;
    }

    public int getStoriesMin() {
        return storiesMin;
    }

    public boolean isHasPhotos() {
        return hasPhotos;
    }

    public boolean isHasWaterfront() {
        return hasWaterfront;
    }

    public boolean isHasPoolPrivate() {
        return hasPoolPrivate;
    }

    public boolean isHasPoolArea() {
        return hasPoolArea;
    }

    public boolean isHasSpaHotTub() {
        return hasSpaHotTub;
    }

    public boolean isHasFireplace() {
        return hasFireplace;
    }

    public boolean isHasPorch() {
        return hasPorch;
    }

    public boolean isHasPatioDeck() {
        return hasPatioDeck;
    }

    public boolean isHasBackYard() {
        return hasBackYard;
    }

    public boolean isHasTennis() {
        return hasTennis;
    }

    public boolean isHasGolf() {
        return hasGolf;
    }

    public boolean isHasControlledAccess() {
        return hasControlledAccess;
    }

    public boolean isWheelchairHandicapAccess() {
        return isWheelchairHandicapAccess;
    }

    public boolean isForeclosure() {
        return isForeclosure;
    }

    public boolean isNewConstruction() {
        return isNewConstruction;
    }

    public boolean isExcludeShortSales() {
        return excludeShortSales;
    }

    public boolean isPriceReduction() {
        return isPriceReduction;
    }

    public float getLongitude() {
        return longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public boolean isRental() {
        return isRental;
    }

    public boolean isSold() {
        return isSold;
    }

    public boolean isPending() {
        return isPending;
    }

    public boolean isActive() {
        return isActive;
    }

    public int getDaysOnMarket() {
        return daysOnMarket;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }

    public String getStreetName() {
        return streetName;
    }
}
