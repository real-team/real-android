package com.joinreal.model.data.chat;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by Nick on 25.05.2015.
 */
public class ChatUsersPhonesMap {

    @SerializedName("usersPhones")
    private Map<Integer,String> map;

    public Map<Integer, String> getMap() {
        return map;
    }

    public void setMap(Map<Integer, String> map) {
        this.map = map;
    }

    public String toString(){
        String result = "";
        if (getMap()!=null) {
            for (Map.Entry<Integer,String> name :getMap().entrySet()){
                result = result +" { " + name.getKey() + " , " + name.getValue() + " } ,";
            }
        }
        return result;
    }
}
