package com.joinreal.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brittbarak on 6/30/15.
 */
public class PropertySubcategory{
    @SerializedName("name")
    String subcategoryName;
    @SerializedName("value")
    List<PropertyField> fields;

    public PropertySubcategory(String subcategoryName, List<PropertyField> fields) {
        this.subcategoryName = subcategoryName;
        this.fields = fields;
    }

    public PropertySubcategory(String subcategoryName) {
        this(subcategoryName, new ArrayList<PropertyField>());
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public List<PropertyField> getFields() {
        return fields;
    }

    public void adjustFields() {
        if (fields != null) {
            for(PropertyField field : fields){
                field.setnameAndDisplayOrder();
            }
        }
    }
}
