package com.joinreal.model.data;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;
import com.joinreal.utils.ChatImagesUtils;
import com.joinreal.utils.TimeStringUtils;

import java.util.Calendar;

/**
 * Created by britt on 6/21/15.
 */
public class Agent implements ChatImagesUtils.IImageObject {

    private int id;
    @SerializedName("website_url")
    private String websiteUrl;
    @SerializedName("facebook_profile_url")
    private String facebookProfileUrl;
    @SerializedName("linkedin_profile_url")
    private String linkedinProfileUrl;
    @SerializedName("twitter_profile_url")
    private String twitterProfileUrl;
    private String about;
    @SerializedName("agent_since")
    private String agentSince;
    private String certifications;
    private String languages;
    @SerializedName("do_buys")
    private Boolean isWorkWithBuyers;
    @SerializedName("do_sells")
    private Boolean isWorkWithSellers;
    @SerializedName("do_rents")
    private Boolean isWorkWithRenters;
    private String firstname;
    private String name;
    private String lastname;
    private String middlename;
    private String phone;
    private String username; //normalized phone
    private String fax;
    private String email;
    @SerializedName("birth_date")
    private String birthDate;
    private String state;
    private String city;
    private String zip;
    private String address1;
    private String address2;
    private String title;
    @SerializedName("welcome_signup")
    private String welcomeSignup;
    @SerializedName("welcome_chat")
    private String welcomeChat;
    @SerializedName("license_states")
    private String licenseStates;
    private String zipcodes;
    @SerializedName("profile_image")
    private Photo profileImage;
    private transient Calendar birthDateCalendar;
    private transient Bitmap newProfileImage;
    private transient Bitmap imageToUpload;

    public Agent(Agent otherAgent) {
        this.websiteUrl = otherAgent.getWebsiteUrl();
        this.facebookProfileUrl = otherAgent.getFacebookProfileUrl();
        this.linkedinProfileUrl = otherAgent.getLinkedinProfileUrl();
        this.twitterProfileUrl = otherAgent.getTwitterProfileUrl();
        this.about = otherAgent.getAbout();
        this.agentSince = otherAgent.getAgentSince();
        this.certifications = otherAgent.getCertifications();
        this.languages = otherAgent.getLanguages();
        this.isWorkWithBuyers = otherAgent.isWorkWithBuyers();
        this.isWorkWithSellers = otherAgent.isWorkWithSellers();
        this.isWorkWithRenters = otherAgent.isWorkWithRenters();
        this.name = otherAgent.getDisplayName();
        this.phone = otherAgent.getPhone();
        this.email = otherAgent.getEmail();
        this.birthDate = otherAgent.getBirthDate();
        this.state = otherAgent.getState();
        this.city = otherAgent.getCity();
        this.zip = otherAgent.getZip();
        this.address1 = otherAgent.getAddress1();
        this.title = otherAgent.getTitle();
        this.welcomeSignup = otherAgent.getWelcomeSignup();
        this.welcomeChat = otherAgent.getWelcomeChat();
        this.zipcodes = otherAgent.getZipcodes();
        this.profileImage = otherAgent.getProfileImage();
        this.birthDateCalendar = otherAgent.getBirthDateCalendar();
        this.newProfileImage = otherAgent.getNewProfileImage();
        this.licenseStates = otherAgent.getLicenseStates();
    }

    public String getFirstName() {
        return firstname;
    }

    public String getFullName() {
        if (name != null) {
            return name;
        } else {
            return firstname + (middlename == null ? "" : (" " + middlename)) + " " + lastname;
        }
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        if (profileImage != null) {
            return profileImage.getMedium();
        }
        return null;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public boolean hasImage() {
     return (newProfileImage != null) || (profileImage != null);
    }

    @Override
    public String getProfileImageFileName() {
        return getImage();
    }

    @Override
    public int getColorResource() {
        return 0;
    }

    public String getInitials() {
        String initials = "";
        if((firstname != null) && (firstname.length() > 0)){
            initials += firstname.charAt(0);
        }
        if((lastname != null) && (lastname.length() > 0)) {
            initials += lastname.charAt(0);
        }
        return initials;
    }

    @Override
    public Bitmap getBitmap() {
        return newProfileImage;
    }

    @Override
    public String getDisplayName() {
        return getFullName();
    }

    @Override
    public String getId() {
        return String.valueOf(id);
    }

    @Override
    public String getChatLogin() {
        return username;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getFacebookProfileUrl() {
        return facebookProfileUrl;
    }

    public void setFacebookProfileUrl(String facebookProfileUrl) {
        this.facebookProfileUrl = facebookProfileUrl;
    }

    public String getLinkedinProfileUrl() {
        return linkedinProfileUrl;
    }

    public void setLinkedinProfileUrl(String linkedinProfileUrl) {
        this.linkedinProfileUrl = linkedinProfileUrl;
    }

    public String getTwitterProfileUrl() {
        return twitterProfileUrl;
    }

    public void setTwitterProfileUrl(String twitterProfileUrl) {
        this.twitterProfileUrl = twitterProfileUrl;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAgentSince() {
        return agentSince;
    }

    public void setAgentSince(String agentSince) {
        this.agentSince = agentSince;
    }

    public String getCertifications() {
        return certifications;
    }

    public void setCertifications(String certifications) {
        this.certifications = certifications;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public boolean isWorkWithBuyers() {
        return (isWorkWithBuyers == null) ? false : isWorkWithBuyers;
    }

    public void setIsWorkWithBuyers(boolean isWorkWithBuyers) {
        this.isWorkWithBuyers = isWorkWithBuyers;
    }

    public boolean isWorkWithSellers() {
        return (isWorkWithSellers == null) ? false : isWorkWithSellers;
    }

    public void setIsWorkWithSellers(boolean isWorkWithSellers) {
        this.isWorkWithSellers = isWorkWithSellers;
    }

    public boolean isWorkWithRenters() {
        return (isWorkWithRenters == null) ? false : isWorkWithRenters;
    }

    public void setIsWorkWithRenters(boolean isWorkWithRenters) {
        this.isWorkWithRenters = isWorkWithRenters;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String getBirthDate() {
        return birthDate;
    }

    public String getBirthdayToDisplay() {
        if (birthDate == null) {
            return null;
        } else {
            if (birthDateCalendar == null) {
                birthDateCalendar = TimeStringUtils.getCalendarFromJsonDate(birthDate);
            }
            return TimeStringUtils.parseBirthdayFormatToDisplay(birthDateCalendar);
        }
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setBirthDate(Calendar calendar) {
        this.birthDateCalendar = calendar;
        setBirthDate(TimeStringUtils.parseCalendarToJson(calendar));
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public Photo getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(Photo profileImage) {
        this.profileImage = profileImage;
    }

    public void setNewProfileImage(Bitmap newProfileImage) {
        this.newProfileImage = newProfileImage;
    }

    public Bitmap getNewProfileImage() {
        return newProfileImage;
    }

    public String getZipcodes() {
        return zipcodes;
    }

    public void setZipcodes(String zipcodes) {
        this.zipcodes = zipcodes;
    }

    public String getWelcomeChat() {
        return welcomeChat;
    }

    public void setWelcomeChat(String welcomeChat) {
        this.welcomeChat = welcomeChat;
    }

    public String getWelcomeSignup() {
        return welcomeSignup;
    }

    public void setWelcomeSignup(String welcomeSignup) {
        this.welcomeSignup = welcomeSignup;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFullAddress() {
        String fullAddress = "";
        if (address1 != null) {
            fullAddress = address1;
        }
        if (address2 != null) {
            fullAddress += " " + address2;
        }
        return fullAddress;
    }

    public Calendar getBirthDateCalendar() {
        return birthDateCalendar;
    }

    public String getLicenseStates() {
        return licenseStates;
    }

    public void setLicenseStates(String licenseStates) {
        this.licenseStates = licenseStates;
    }

    public Bitmap getImageToUpload() {
//        return imageToUpload;
        return newProfileImage;
    }

    public void setImageToUpload(Bitmap imageToUpload) {
        this.imageToUpload = imageToUpload;
    }

}
