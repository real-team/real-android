package com.joinreal.model.data;

/**
 * Created by britt on 6/16/15.
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.joinreal.app.common.Helper;
import com.joinreal.app.interfaces.PropertySummaryLayoutSetter;
import com.joinreal.utils.MapUtils;

public class PropertyThin implements Serializable, PropertySummaryLayoutSetter {

    @SerializedName("mls_id")
    private String mlsId;
    private String mlsnum;
    @SerializedName("property_type")
    private String propertyType;
    @SerializedName("list_status")
    private String listStatus;
    @SerializedName("list_price")
    private Integer price;
    private String state;
    private String city;
//    @SerializedName("street_name")
//    private String streetName;
//    @SerializedName("street_number")
//    private int streetNumber;
    private String zip;
    private String address1;
    private String address2;
    private Double latitude;
    private Double longitude;
    //TODO: to switch to int
    private Double beds;
    private Double baths;
    @SerializedName("sq_ft")
    private double sqFt;
    @SerializedName("is_new_construction")
    private boolean isNewConstruction;
    @SerializedName("is_foreclosure")
    private boolean isForeclosure;
    @SerializedName("is_rent")
    private boolean isRent;
    @SerializedName("is_open_house")
    private boolean isOpenHouse;
    @SerializedName("listing_agent_name")
    private String listingAgentName;
    @SerializedName("listing_agent_phone")
    private String listingAgentPhone;
    @SerializedName("listing_agent_email")
    private Object listingAgentEmail;
    private List<Photo> photos = new ArrayList<>();
    @SerializedName("is_sold")
    private boolean isSold;
    private boolean favorite;
    private String staticMapUrl;

    public String getFullAddress() {
        String fullAddress = "";
        if((address1 != null) && (address1.length() > 0)){
            fullAddress = address1;
            if (city != null) {
                fullAddress = fullAddress + ", ";
            }
        }
        if (city != null) {
            fullAddress = fullAddress + city;
        }
        return fullAddress;
    }

    public String getFormattedPrice() {
        if (price == null) {
            return "N/A";
        } else {
            return "$" + Helper.getNumberFormattedWithCommas(price);
        }
    }

    public String getShortPrice() {
        if (price == null) {
            return "N/A";
        } else {
            return "$" + Helper.getShortPrice(price);
        }
    }

    public Double getBeds() {
        return beds;
    }

    public Double getBaths() {
        return baths;
    }

    public ArrayList<String> getPhotosMeduim() {
        ArrayList<String> list = new ArrayList<>();
        if (photos != null) {
            for (Photo photo : photos){
                list.add(photo.getMedium());
            }
        }
        return list;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getMlsId() {
        return mlsId;
    }

    public String getMlsNum() {
        return mlsnum;
    }

    public String getState() {
        return state;
    }

    public String getFirstPhoto() {
        //TODO: for test :

        if ((photos != null) && (! photos.isEmpty())){
            return photos.get(0).getMedium();

        } else {
            return staticMapUrl;
        }
    }

    public String getStaticMapUrl() {
        if (staticMapUrl == null) {
            setStaticMapUrl();
        }
        return staticMapUrl;
    }

    public void setStaticMapUrl(String staticMapUrl) {
        this.staticMapUrl = staticMapUrl;
    }

    public void setStaticMapUrl() {
        this.staticMapUrl = MapUtils.getStaticMapUrl(getLatitude(), getLongitude(), getFullAddress());
    }

    public String getAddressForLocation() {
        return getFullAddress() + " , " + getState();
    }
}
