package com.joinreal.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by brittbarak on 8/31/15.
 */
public class ValidContacts {
    @SerializedName("contacts")
    Contact[] validContacts;

    public Contact[] getContacts() {
        return validContacts;
    }
}
