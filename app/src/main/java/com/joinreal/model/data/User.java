package com.joinreal.model.data;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * Created by britt on 6/1/15.
 */
public class User {
    public static final String AGENT = "Agent";
    public static final String CUSTOMER = "Customer";

    @SerializedName("user_id")
    private int  userId;
    private String username;
    private String password;
    @SerializedName("resource_type")
    private String resourceType;
    @SerializedName("resource_id")
    private int resourceId;
    //TODO: check if casting is enough
    private Object resource;
    private String name; //For Customer -  this is the name as saved by assigned agent

    @SerializedName("authentication_token")
    private String token;
    private Agent agent;
    private Customer customer;

    public User(String userName) {
        this(userName, "");
    }

    public User(String userName, String password) {
        this.username = userName;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getResourceType() {
        System.out.println("***************    resourceType : " + resourceType);
        return resourceType;
    }

    public Object getResource() {
        return resource;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Agent getAgent() {
        return agent;
    }

    public Customer getCustomer() {
        return customer;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return resourceId;
    }

    public boolean isAgent() {
        return agent != null;
    }

    public int getResourceId() {
        return resourceId;
    }

    public int getUserId() {
        return userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return isAgent() ? agent.getEmail() : customer.getEmail();
    }

    public String getNameForPN() {
        if (isAgent()) {
            return agent.getFirstName();
        } else {
            return customer.getName();
        }
    }


}
