package com.joinreal.model.data.chat;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Nick on 04.06.2015.
 */
public class ChatDialogInfo implements Serializable {

    @SerializedName("messageRead")
    private Boolean messageRead;
    @SerializedName("sentByEmail")
    private Boolean sentByEmail;

    public ChatDialogInfo(boolean isMessageRead) {
        messageRead = isMessageRead;
    }

    public ChatDialogInfo(boolean isMessageRead, boolean sentEmail) {
        messageRead = isMessageRead;
        sentByEmail = sentEmail;
    }

    public ChatDialogInfo() {

    }

    public Boolean isMessageRead() {
        return messageRead;
    }

    public void setMessageRead(boolean messageRead) {
        this.messageRead = messageRead;
    }

    public Boolean isSentByEmail() {
        return sentByEmail;
    }

    public void setSentByEmail(boolean sentByEmail) {
        this.sentByEmail = sentByEmail;
    }

    public static ChatDialogInfo merge(ChatDialogInfo saved, ChatDialogInfo chatDialogInfo) {
        if (saved == null){
            return chatDialogInfo;
        } else {
            if (chatDialogInfo.isMessageRead() != null){
                saved.setMessageRead(chatDialogInfo.isMessageRead());
            }
            if (chatDialogInfo.isSentByEmail() != null){
                saved.setSentByEmail(chatDialogInfo.isSentByEmail());
            }
            return saved;
        }
    }

    public boolean isMessageReadBool() {
        if (isMessageRead()==null){
            return false;
        }
        return isMessageRead();
    }

    public boolean isSentByEmailBool() {
        if (isSentByEmail() == null){
            return false;
        }
        return isSentByEmail();
    }
}
