package com.joinreal.model.data;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

/**
 * Created by britt on 6/21/15.
 */
public class Customer {
    private int id;
    private String firstname;
    private String lastname;
    private String name;
    private String phone;
    private String username; //normalized phone
    private String email;
    @SerializedName("birth_date")
    private String birthDate;
    private String state;
    private String city;
    private String zip;
    private String address1;
    private String address2;
    @SerializedName("agent_id")
    private int agentId;
    @SerializedName("profile_image")
    private Photo profileImage;
    @SerializedName("authentication_token")
    private String authenticationToken;
    private transient Bitmap newProfileImage;
    private transient Bitmap imageToUpload;

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Photo getProfileImage() {
        return profileImage;
    }

    public String getName() {
        if (this.name == null) {
            String name = "";
            if (firstname != null) {
                name = firstname;
                if (lastname != null) {
                    name += " ";
                }
            }
            if (lastname != null) {
                name += lastname;
            }
            this.name = name;
        }
        return name;
    }

    public Bitmap getNewProfileImage() {
        return newProfileImage;
    }

    public void setNewProfileImage(Bitmap newProfileImage) {
        this.newProfileImage = newProfileImage;
    }

    public Bitmap getImageToUpload() {
        return newProfileImage;

    }

    public void setImageToUpload(Bitmap imageToUpload) {
        this.imageToUpload = imageToUpload;
    }

    public String getImage() {
        if (profileImage != null) {
            return profileImage.getMedium();
        }
        return null;
    }
}
