package com.joinreal.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by brittbarak on 6/30/15.
 */
public class PropertyField{
    private static final int VALUE_PLACEHOLDER_SIZE = 7; //length of the string "{value}", received from server on property metadata structure

    public final static int DISPLAY_TITLE_VALUE = 1;
    public final static int DISPLAY_VALUE_TITLE = 2;
    public final static int DISPLAY_TITLE = 3;

    @SerializedName("display")
    String displayInstructions;
    String key;
    String name;
    int displayOrder;

    public String getName() {
        if (name == null) {
            setnameAndDisplayOrder();
        }
        return name;
    }

    public void setnameAndDisplayOrder() {
        if(displayInstructions == null) {
            System.out.println("(displayInstructions == null)");
        } else {
            if (displayInstructions.charAt(displayInstructions.length() - 1) == '}') {
                displayOrder = DISPLAY_TITLE_VALUE;
                name = displayInstructions.substring(0, displayInstructions.length() - VALUE_PLACEHOLDER_SIZE).trim();
            } else if (displayInstructions.charAt(0) == '{') {
                displayOrder = DISPLAY_VALUE_TITLE;
                name = displayInstructions.substring(VALUE_PLACEHOLDER_SIZE).trim();
            } else {
                name = displayInstructions;
                displayOrder = DISPLAY_TITLE;
            }
        }
    }

    public String getKey() {
        return key;
    }

    public int getDisplayOrder() {
        if (displayOrder <= 0) {
            setnameAndDisplayOrder();
        }
        return displayOrder;
    }

    public String getDisplayInstructions() {
        return displayInstructions;
    }
}
