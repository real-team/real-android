package com.joinreal.model.data.chat;

import com.joinreal.app.Application;
import com.joinreal.app.common.ViewTypes;
import com.joinreal.model.view.ViewModelItem;
import com.qb.gson.annotations.SerializedName;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.model.QBEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by Nick on 10.05.2015.
 */
public class ChatDialogDataVM extends ViewModelItem implements Serializable {

    @SerializedName("dataFromWeb")
    private QBDialog dialog;
    @SerializedName("database")
    private ChatDialogInfo info;
    @SerializedName("chatNameFromContacts")
    private String chatNameFromContacts;
    @SerializedName("contactChatLogin")
    private String chatLogin;
    @SerializedName("lastMessageRead")
    private boolean lastMessageRead;

    public ChatDialogDataVM(QBDialog dialog){
        this.dialog = dialog;
    }

    public QBDialog getQBDialog() {
        return dialog;
    }

    public void setDialog(QBDialog dialog) {
        this.dialog = dialog;
    }

    public String getChatLogin() {
        return chatLogin;
    }

    public void setChatLogin(String chatLogin) {
        this.chatLogin = chatLogin;
    }

    public String getChatNameFromContacts() {
        return chatNameFromContacts;
    }

    public void setChatNameFromContacts(String chatNameFromContacts) {
        this.chatNameFromContacts = chatNameFromContacts;
    }

    public String getDialogId() {
        return dialog.getDialogId();
    }

    public void setUnreadMessageCount(Integer unreadMessageCount) {
        dialog.setUnreadMessageCount(unreadMessageCount);
    }

    public void copyFieldsTo(QBEntity entity) {
        dialog.copyFieldsTo(entity);
    }

    public String getRoomJid() {
        return dialog.getRoomJid();
    }

    public void setLastMessageDateSent(long lastMessageDateSent) {
        dialog.setLastMessageDateSent(lastMessageDateSent);
    }

    public String getFUpdatedAt() {
        return dialog.getFUpdatedAt();
    }

    public String getPhoto() {
        return dialog.getPhoto();
    }

    public Integer getId() {
        return dialog.getId();
    }

    public Integer getRecipientId() {
        return dialog.getRecipientId();
    }

    public void setLastMessage(String lastMessage) {
        dialog.setLastMessage(lastMessage);
    }

    public void setPhoto(String photo) {
        dialog.setPhoto(photo);
    }

    public Date getCreatedAt() {
        return dialog.getCreatedAt();
    }

    public void setUserId(Integer userId) {
        dialog.setUserId(userId);
    }

    public String getName() {
        return dialog.getName();
    }

    public Integer getUnreadMessageCount() {
        return dialog.getUnreadMessageCount();
    }

    public String getLastMessage() {
        return dialog.getLastMessage();
    }

    public ArrayList<Integer> getOccupants() {
        return dialog.getOccupants();
    }

    public void setType(QBDialogType type) {
        dialog.setType(type);
    }

    public void setCreatedAt(Date createdAt) {
        dialog.setCreatedAt(createdAt);
    }

    public void setUpdatedAt(Date updatedAt) {
        dialog.setUpdatedAt(updatedAt);
    }

    public void setId(int id) {
        dialog.setId(id);
    }

    public Date getUpdatedAt() {
        return dialog.getUpdatedAt();
    }

    public String getFCreatedAt() {
        return dialog.getFCreatedAt();
    }

    public QBDialogType getType() {
        return dialog.getType();
    }

    public Integer getUserId() {
        return dialog.getUserId();
    }

    public void setName(String name) {
        dialog.setName(name);
    }

    public void setDialogId(String dialogId) {
        dialog.setDialogId(dialogId);
    }

    public void setLastMessageUserId(Integer lastMessageUserId) {
        dialog.setLastMessageUserId(lastMessageUserId);
    }

    public void setOccupantsIds(ArrayList<Integer> occupantsIds) {
        dialog.setOccupantsIds(occupantsIds);
    }

    public Integer getLastMessageUserId() {
        return dialog.getLastMessageUserId();
    }

    public void setRoomJid(String roomJid) {
        dialog.setRoomJid(roomJid);
    }

    public void setData(Map<String, String> data) {
        dialog.setData(data);
    }

    public long getLastMessageDateSent() {
        return dialog.getLastMessageDateSent();
    }

    public Map<String, String> getData() {
        return dialog.getData();
    }

    public boolean isLastMessageRead() {
        return lastMessageRead;
    }

    public void setLastMessageRead(boolean lastMessageRead) {
        this.lastMessageRead = lastMessageRead;
    }

    public ChatDialogInfo getInfo() {
        return info;
    }

    public void setInfo(ChatDialogInfo info) {
        this.info = info;
    }

    @Override
    public int getTypeIntCode() {
        return ViewTypes.CHAT_DIALOG;
    }

    public Integer getOccupantId() {
        for (int i = 0; i < getOccupants().size(); i++) {
            if (! getOccupants().get(i).equals(Application.getCurrentChatUser().getId())) {
                return getOccupants().get(i);
            }
        }
        return null;
    }
}
