package com.joinreal.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/5/15.
 */
public class RangeStepsListGenerator {

    public static List<RangeStep> generateList(int min, int max, int margin) {
        return generateList(min, max, margin, null);
    }

    public static List<RangeStep> generateList(int min, int max, int margin, String label) {
        String stepLabel = label;
        ArrayList<RangeStep> steps = new ArrayList();
        for (int i = min; i <= max; i = i + margin) {
            if (stepLabel != null) {
                stepLabel = i + " " + label;
            }
            steps.add(generateMiddleStep(i, i - margin, i + margin, stepLabel));
        }
        return steps;
    }

    public static List<RangeStep> generateList(int min, int max, int margin, String label, int previousValue) {
        ArrayList<RangeStep> steps = new ArrayList();
        steps.add(generateMiddleStep(min, previousValue, min + margin, label));
        steps.addAll(generateList(min + margin, max, margin, label));
        return steps;

    }

    public static List<RangeStep> generateList(int[] values, String[] labels, String generalLabel, int previousValue) {
        ArrayList<RangeStep> steps = new ArrayList();
        for (int i = 0; i < values.length; i++) {
            steps.add(generateMiddleStep(values[i], (i == 0) ? previousValue : values[i - 1], (i == values.length - 1) ? values[i] : values[i + 1], labels[i] + " " + generalLabel));
        }
        return steps;
    }

    private static int getMaxValueToSelectStep(int stepValue, int nextValue) {
        int margin = nextValue - stepValue;
        return (margin == 0) ? stepValue : stepValue + (margin / 2) - 1;
    }

    private static int getMinValueToSelectStep(int stepValue, int previousValue) {
        int margin = stepValue - previousValue;
        return stepValue - (margin / 2);
    }

    public static RangeStep generateMinStepForList(int value, String label, int margin) {
        int minValueToSelectStep = getMinValueToSelectStep(value, margin);
        return new RangeStep(value, minValueToSelectStep, value, label);
    }

    public static RangeStep generateMaxStepForList(int value, int margin) {
        int minValueToSelectStep = getMaxValueToSelectStep(value, margin);
        return new RangeStep(value, minValueToSelectStep, value, null);
    }

    public static RangeStep generateMiddleStep(int value, int previousValue, int nextValue, String label) {
        int minValueToSelectStep = getMinValueToSelectStep(value, previousValue);
        int maxValueToSelectStep = getMaxValueToSelectStep(value, nextValue);
        return new RangeStep(value, minValueToSelectStep, maxValueToSelectStep, label);

    }

    public static void setEvenStepsRanges(List<RangeStep> steps) {
        for (int i = 0; i < steps.size(); i++) {
            steps.get(i).setValueForRange(i);
            steps.get(i).setMinValueToSelectStep((i == 0) ? i : i - 0.49f);
            steps.get(i).setMaxValueToSelectStep((i == steps.size() -1) ? i : i + 0.5f);
        }
    }

}
