package com.joinreal.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.joinreal.app.Application;
import com.joinreal.app.api.RealClient;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.events.ContactsUpdatedEvent;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.utils.ContactManager;
import com.joinreal.model.data.Contact;
import com.joinreal.utils.TimeStringUtils;

import java.util.ArrayList;


/**
 * Created by brittbarak on 7/2/15.
 */
public class SyncUsersService extends IntentService {

    private long startTime;
    private long loadContactsFromDeviceTime;
    private long sendContactsToServerTime;
    //    private long getContactsFromServerTime;
    private long mergeContactsWithServerTime;

    public SyncUsersService() {
        super("SyncUsersService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i("CONTACTS_TAG", " onHandleIntent ContactManager.isContactsLoaded() : " + ContactManager.isContactsLoaded() + " , ContactManager.isContactsLoading() : " + ContactManager.isContactsLoading());

        if ((!ContactManager.isContactsLoaded()) && (!ContactManager.isContactsLoading())) {
            ContactManager.setContactsLoading(true);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    BusProvider.getRestBus().post(new ContactsUpdatedEvent(false));
                }
            });
            startTime = System.currentTimeMillis();
            ContactManager.getModifiedContactsFromDevice(getContactsFromDeviceCallback());
        }
        ContactManager.setContactsLoading(false);
        stopSelf();
    }

    @NonNull
    private ContactManager.ContactsCallback getContactsFromDeviceCallback() {
        return new ContactManager.ContactsCallback() {
            @Override
            public void loaded(ArrayList<Contact> contactsToSyncWithServer) {
                loadContactsFromDeviceTime = System.currentTimeMillis();
                System.out.println("------ sync service on loaded from device, time : " + (loadContactsFromDeviceTime - startTime));
                Log.i("CONTACTS_TAG", "sync service on loaded from device, time : " + (loadContactsFromDeviceTime - startTime));
                RealClient client = Application.getClient();
                Log.i("CONTACTS_TAG", " getContactsFromDeviceCallback contactsToSyncWithServer.size : " + (contactsToSyncWithServer == null ? 0 : contactsToSyncWithServer.size()));
                System.out.println(" getContactsFromDeviceCallback contactsToSyncWithServer.size : " + (contactsToSyncWithServer == null ? 0 : contactsToSyncWithServer.size()));
                if ((contactsToSyncWithServer != null) && (!contactsToSyncWithServer.isEmpty())) {
                    Contact[] updatedContactsFromServer = null;
                    try {
                        updatedContactsFromServer = client.syncContactsWithServer(contactsToSyncWithServer);
                        Log.i("CONTACTS_TAG", " getContactsFromDeviceCallback updatedContactsFromServer.size : " + updatedContactsFromServer.length);
                        System.out.println(" getContactsFromDeviceCallback updatedContactsFromServer.size : " + updatedContactsFromServer.length);
                    } catch (NetworkException e) {
                        BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.ERROR_SENDOING_CONTACTS_TO_SERVER, false));
                    }
                    Application.getPrefs().setLastSyncContactsDate(TimeStringUtils.getCurrentTimeForJson());
                    sendContactsToServerTime = System.currentTimeMillis();
                    System.out.println("------ sync service syncContactsWithServer, time : " + (sendContactsToServerTime - loadContactsFromDeviceTime));

                    if ((updatedContactsFromServer != null) && (updatedContactsFromServer.length > 0)) {
                        ContactManager.addContactsFromServer(updatedContactsFromServer);
                        mergeContactsWithServerTime = System.currentTimeMillis();
                        System.out.println("------ sync service addContactsFromServer, time : " + (mergeContactsWithServerTime - sendContactsToServerTime));
                    }

                    System.out.println("------ sync service on loaded complete, time : " + (System.currentTimeMillis() - startTime));

//                        Crashlytics.logException(new Throwable("SyncService loaded from device, time : " + (loadContactsFromDeviceTime - startTime)
//                                + "  , send contacts to server, time : " + (sendContactsToServerTime - loadContactsFromDeviceTime)
////                            + "  , get contacts from server, time : " + (getContactsFromServerTime - sendContactsToServerTime)
//                                + "  , addContactsFromServer time : " + (mergeContactsWithServerTime - sendContactsToServerTime)));
                }
                BusProvider.getRestBus().post(new ContactsUpdatedEvent(true));

            }
        };
    }
}
