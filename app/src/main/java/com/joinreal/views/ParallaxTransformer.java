package com.joinreal.views;

import android.os.Build;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/15/15.
 */
public class ParallaxTransformer implements ViewPager.PageTransformer {

    private int mImageId;
    public ParallaxTransformer(int mImageId) {
        this.mImageId = mImageId;
    }

    @Override
    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();
        // Find target View.
        if (view.findViewById(mImageId) != null) {
            view = view.findViewById(mImageId);
        }

        if (position <= 0) { // [-1,0]

            if ((view.getId() == mImageId) ){
                view.setTranslationX((pageWidth * -position) / 2);
            } else {
                // Use the default slide transition when moving to the left page if the target view
                // is not found.
                view.setTranslationX(0);
            }
        } else if (position <= 1) { // (0,1]
            // Counteract the default slide transition
            view.setTranslationX((pageWidth * -position) / 2);

        }

    }



}