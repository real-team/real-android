package com.joinreal.views;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by brittbarak on 7/29/15.
 */
public class StartDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private int startYear;
    private int startMonth;
    private int startDay;
    private ISetDateListener listener;

    public interface ISetDateListener {
        void onDateSet(int year, int monthOfYear, int dayOfMonth);
    }

    public void setListener(ISetDateListener listener) {
        this.listener = listener;
    }

    public void setDate(int year, int monthOfYear, int dayOfMonth){
        startYear = year;
        startMonth = monthOfYear;
        startDay = dayOfMonth;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        // Use the current date as the default date in the picker
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, startYear, startMonth, startDay);
        return dialog;

    }

    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        listener.onDateSet(year, monthOfYear, dayOfMonth);

    }
}