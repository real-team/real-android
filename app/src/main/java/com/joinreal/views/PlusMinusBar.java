package com.joinreal.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joinreal.R;

public class PlusMinusBar extends LinearLayout implements View.OnClickListener {
    protected ImageView plusButton;
    private ImageView minusButton;
    protected String label;
    protected int defaultValue;
    protected TypedArray typedArray;
    private TextView labelTextView;
    private TextView valueTextView;

    public PlusMinusBar(Context context) {
        super(context);
    }

    public PlusMinusBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PlusMinusBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(final Context context, AttributeSet attrs) {
        initAttributes(context, attrs);
        if (isInEditMode()) return;
        setViewsContent(context);

        typedArray.recycle();
    }

    protected void initAttributes(Context context, AttributeSet attrs) {
        final View rootView = View.inflate(context, R.layout.view_plus_minus_bar, this);
        plusButton = (ImageView) rootView.findViewById(R.id.plus);
        minusButton = (ImageView) rootView.findViewById(R.id.minus);
        labelTextView = (TextView) rootView.findViewById(R.id.label_textview);
        valueTextView = (TextView) rootView.findViewById(R.id.value_textview);

        typedArray = context.obtainStyledAttributes(attrs, R.styleable.PlusMinusBar);
        label = typedArray.getString(R.styleable.PlusMinusBar_label);
        defaultValue = typedArray.getInt(R.styleable.PlusMinusBar_defaultValue, 0);

    }

    protected void setViewsContent(Context context) {
        labelTextView.setText(label);
        valueTextView.setText(Integer.toString(defaultValue));

        plusButton.setOnClickListener(this);
        minusButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int currentValue = Integer.parseInt(valueTextView.getText().toString());
        switch(v.getId()){
            case R.id.minus :
                currentValue--;
                break;
            case R.id.plus :
                currentValue++;
                break;
        }
        valueTextView.setText(Integer.toString(currentValue));
    }

    public void setLabel(String label) {
        this.label = label;
        labelTextView.setText(label);
    }

    public void setDefaultValue(int defaultValue) {
        this.defaultValue = defaultValue;
    }
}