package com.joinreal.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.adapters.FiltersAdapter;
import com.joinreal.app.common.Helper;
import com.joinreal.app.interfaces.ViewHolderAbstract;
import com.joinreal.model.view.ExpandableItemFiltersVM;

/**
 * Created by britt on 6/2/15.
 */
public class ExpandableFilterLayout extends ExpandableLayout {

    private ImageView arrow;

    public ExpandableFilterLayout(Context context) {
        super(context);
        initAttributes(context, null);
        this.contentID = R.layout.recyclerview_filter_content;
    }

    public ExpandableFilterLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandableFilterLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void initAttributes(Context context, AttributeSet attrs) {
        super.initAttributes(context, attrs);
        if (headerID <= 0) {
            headerID = R.layout.filter_item_header;
        }
    }

    @Override
    protected void setViewsContent(Context context) {
        super.setViewsContent(context);
        this.arrow = (ImageView) headerLayout.findViewById(R.id.header_arrow);
        TextView headerTitleTextView = (TextView) headerLayout.findViewById(R.id.header_title);
        String headerTitle = typedArray.getString(R.styleable.ExpandableLayout_el_title);
        headerTitleTextView.setText(headerTitle);
    }

    public void setTitle(String title){
        TextView headerTitleTextView = (TextView) headerLayout.findViewById(R.id.header_title);
        headerTitleTextView.setText(title);
    }

    public void setInnerItems(RecyclerView.Adapter<ViewHolderAbstract> adapter) {
        //setToolbar(viewModel.getTitle());
        RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams)contentLayout.getLayoutParams();
        p.height = ViewGroup.LayoutParams.MATCH_PARENT;
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        contentLayout.setLayoutParams(p);
        RecyclerView recyclerView = (RecyclerView) contentLayout.findViewById(R.id.recyclerview);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) recyclerView.getLayoutParams();
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        recyclerView.setLayoutParams(params);
        recyclerView.setLayoutManager(new DynamicLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
//        recyclerView.setAdapter(new FiltersAdapter(getContext(), viewModel.getItems(), false));
    }

    public void setInnerItemsAdapter(){
    }
    @Override
    protected void expand(View v) {
        super.expand(v);
        arrow.setImageDrawable(Helper.getDrawable(getContext(), R.drawable.arrow_d_bluedark));
    }

    @Override
    protected void collapse(View v) {
        super.collapse(v);
        arrow.setImageDrawable(Helper.getDrawable(getContext(), R.drawable.arrow_r_bluedark));
    }
}
