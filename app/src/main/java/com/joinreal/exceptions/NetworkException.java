package com.joinreal.exceptions;

/**
 * Created by brittbarak on 8/16/15.
 */
public class NetworkException extends Exception {

    private int code;
    private String requestBody;

    public NetworkException(int code, String detailMessage, String requestBody) {
        super(detailMessage);
        this.code = code;
        this.requestBody = requestBody;
    }

    public NetworkException(int code, String detailMessage, Throwable throwable, String bodyJson) {
        super(detailMessage, throwable);
        this.code = code;
        this.requestBody = bodyJson;
    }

    public NetworkException(int code, String detailMessage, Throwable throwable) {
        this(code, detailMessage, throwable, "");
    }

    public NetworkException(String detailMessage) {
        this(-1, detailMessage, "");
    }

    public int getCode() {
        return code;
    }

    public String getRequestBody() {
        return requestBody;
    }
}
