package com.joinreal.tasks;

import android.os.AsyncTask;

import com.joinreal.app.Application;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.events.CustomerSearchFilterEvent;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.SearchFilter;

/**
 * Created by brittbarak on 7/10/15.
 */
public class GetContactLastSearchTask extends AsyncTask{
    private String customerId;

    public GetContactLastSearchTask(String customerId) {
        this.customerId = customerId;
    }

    @Override
    protected Void doInBackground(Object[] params) {
        SearchFilter result = null;
        try {
            result = Application.getClient().getContactLastSearchFilters(customerId);
        } catch (NetworkException e) {
            BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.CANT_GET_CONTACT_SEARCH));
        }
        BusProvider.getBus().post(new CustomerSearchFilterEvent(result));
        return null;

    }


}
