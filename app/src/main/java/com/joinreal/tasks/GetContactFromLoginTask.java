package com.joinreal.tasks;

import android.os.AsyncTask;

import com.joinreal.app.Application;
import com.joinreal.app.common.BusProvider;
import com.joinreal.events.GetContactFromLoginEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.Contact;

import java.util.Objects;

/**
 * Created by brittbarak on 9/2/15.
 */
public class GetContactFromLoginTask extends AsyncTask<Void, Void, Contact>{
    private final String userLogin;
    private final int dialogVMIndex;

    public GetContactFromLoginTask(String userLogin, int dialogVMIndex) {
        this.userLogin = userLogin;
        this.dialogVMIndex = dialogVMIndex;
    }

    @Override
    protected Contact doInBackground(Void[] params) {
        Contact contact = null;
        try {
            contact = Application.getClient().getContactFromQBId(userLogin);
        } catch (NetworkException e) {
            System.out.println(e.getMessage());
        }
        return contact;
    }

    @Override
    protected void onPostExecute(Contact contact) {
        super.onPostExecute(contact);
        BusProvider.getBus().post(new GetContactFromLoginEvent(contact, dialogVMIndex));

    }
}
