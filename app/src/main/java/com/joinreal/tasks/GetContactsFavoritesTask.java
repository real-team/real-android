package com.joinreal.tasks;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import com.joinreal.app.Application;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.ReceivedPropertiesEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.PropertyThin;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by brittbarak on 7/11/15.
 */
public class GetContactsFavoritesTask extends AsyncTask<Object, Void, ArrayList<PropertyThin>>{
    private String contactId;

    public GetContactsFavoritesTask(String contactId) {
        this.contactId = contactId;
    }


    @Override
    protected ArrayList<PropertyThin> doInBackground(Object[] params) {
        try {
            return Application.getClient().getContactFavorites(contactId);
        } catch (NetworkException e) {
            BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.CANT_GET_CONTACT_FAVORITES));
            return null;
        }
    }

    @Override
    protected void onPostExecute(ArrayList<PropertyThin> propertyList) {
        BusProvider.getBus().post(new ReceivedPropertiesEvent(propertyList));
    }
}
