package com.joinreal.tasks;

import android.os.AsyncTask;

import com.joinreal.app.Application;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.app.common.StaticParams;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.exceptions.NetworkException;

/**
 * Created by brittbarak on 7/13/15.
 */
public class SendMessageTask extends AsyncTask {
    private String[] contactPhones;
    String message;

    public SendMessageTask(String[] contactPhones, String message) {
        this.contactPhones = contactPhones;
        this.message = message;
    }

    public SendMessageTask(String contactPhone, String message) {
        this.contactPhones = new String[] {contactPhone};
        this.message = message;
    }

    @Override
    protected Void doInBackground(Object[] params) {
        try {
            Application.getClient().sendInvitation(contactPhones, message);
        } catch (NetworkException e) {
            BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.ERROR_SEND_INVITE));
        }
        return null;
    }
}
