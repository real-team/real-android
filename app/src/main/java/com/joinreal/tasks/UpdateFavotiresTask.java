package com.joinreal.tasks;

import android.content.Context;

import com.joinreal.app.Application;
import com.joinreal.app.api.RealClient;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.PropertyIds;

import java.util.List;

/**
 * Created by britt on 6/14/15.
 */
public class UpdateFavotiresTask{
    private final List<PropertyIds> propertiesToBeAddedToFavorites;
    private final List<PropertyIds> propertiesToBeRemovedFromFavorites;

    public UpdateFavotiresTask(List<PropertyIds> propertiesToBeAddedToFavorites, List<PropertyIds> propertiesToBeRemovedFromFavorites) {
        this.propertiesToBeAddedToFavorites = propertiesToBeAddedToFavorites;
        this.propertiesToBeRemovedFromFavorites = propertiesToBeRemovedFromFavorites;
    }

    public void execute() {
        final RealClient client = Application.getClient();

        for (final PropertyIds p : propertiesToBeAddedToFavorites) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        client.addToFavorites(p.getMlsId(), p.getMlsnum());
                    } catch (NetworkException e) {
                        BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.CANT_UPDATE_FAVORITES));
                    }

                }
            }).start();
        }

        for (final PropertyIds p : propertiesToBeRemovedFromFavorites) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        client.removeFromFavorites(p.getMlsId(), p.getMlsnum());
                    } catch (NetworkException e) {
                        BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.CANT_UPDATE_FAVORITES));
                    }

                }
            }).start();
        }
    }
}
