package com.joinreal.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;

import com.google.gson.JsonObject;
import com.joinreal.app.Application;
import com.joinreal.app.api.RealClient;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.ReceivedPropertiesEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.PropertyThin;
import com.squareup.otto.Produce;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/14/15.
 */
public class FindPropertiesTask extends AsyncTask<JsonObject, Void, ArrayList<PropertyThin>> {
    JsonObject jsonObject;
    Context context;
    private int resultsPageIndex;
    private String contactId;

    public FindPropertiesTask(Context context, JsonObject jsonObject, int resultsPageIndex, String contactId) {
        this.jsonObject = jsonObject;
        this.context = context;
        this.resultsPageIndex = resultsPageIndex;
        this.contactId = contactId;
    }

    @Override
    protected ArrayList<PropertyThin> doInBackground(JsonObject... params) {
        try {
            return Application.getClient().findProperties(jsonObject, resultsPageIndex, contactId);
        } catch (NetworkException e) {
            BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.CANT_GET_PROPERTIES));
            return null;
        }
    }

    @Override
    protected void onPostExecute(ArrayList<PropertyThin> propertyList) {
        if (propertyList != null) {
            BusProvider.getBus().post(new ReceivedPropertiesEvent(propertyList, jsonObject));
        }
    }
}
