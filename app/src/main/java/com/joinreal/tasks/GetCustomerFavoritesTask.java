package com.joinreal.tasks;

import android.os.AsyncTask;

import com.joinreal.app.Application;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.NetworkCodes;
import com.joinreal.events.NetworkExceptionEvent;
import com.joinreal.events.ReceivedPropertiesEvent;
import com.joinreal.exceptions.NetworkException;
import com.joinreal.model.data.PropertyThin;

import java.util.ArrayList;

/**
 * Created by brittbarak on 7/11/15.
 */
public class GetCustomerFavoritesTask extends AsyncTask<Object, Void, ArrayList<PropertyThin>> {
    private String contactId;

    @Override
    protected ArrayList<PropertyThin> doInBackground(Object[] params) {
        try {
            return Application.getClient().getCustomerFavorites();
        } catch (NetworkException e) {
            BusProvider.getRestBus().post(new NetworkExceptionEvent(e, NetworkCodes.CANT_GET_FAVORITES));
            return null;
        }
    }

    @Override
    protected void onPostExecute(ArrayList<PropertyThin> propertyList) {
        BusProvider.getBus().post(new ReceivedPropertiesEvent(propertyList));
    }
}
