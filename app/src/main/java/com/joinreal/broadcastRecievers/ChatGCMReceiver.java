package com.joinreal.broadcastRecievers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.SplashActivity;
import com.joinreal.app.activities.chat.PrivateChatActivity;
import com.joinreal.app.common.BusProvider;
import com.joinreal.app.common.Prefs;
import com.joinreal.app.common.StaticParams;
import com.joinreal.events.PNActionEvent;
import com.joinreal.events.ShouldFetchDialogsEvent;

/**
 * Created by Nick on 29.03.2015.
 */
public class ChatGCMReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ((intent != null) && (intent.getExtras() != null)) {
            if ((intent.hasExtra("isFromChat")) && (intent.getStringExtra("isFromChat").equals("true") || intent.getStringExtra("isFromChat").equals("1"))) {
                if (!intent.getStringExtra("dialogId").equals(PrivateChatActivity.getActiveDialogID())) {
                    createNotification(intent.getStringExtra("message"), intent.getStringExtra("dialogId"));
                }
            } else {
                //** On iOS there's no notificationType, but they check if "dialogId" != null;
                Prefs.getInstance().saveDialogInfo(intent.getStringExtra("dialogId"), true);
                updateDialogs();
            }

            String actionStr = intent.getStringExtra("action");
            if (actionStr != null) {
               BusProvider.getRestBus().post(new PNActionEvent(actionStr));
            }
        }

    }

    private void updateDialogs() {
        BusProvider.getBus().post(new ShouldFetchDialogsEvent());
    }

    private void createNotification(String msgText, String dialogId) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(Application.getAppContext());

        Intent resultIntent = new Intent(Application.getAppContext(), SplashActivity.class);
        resultIntent.putExtra(StaticParams.KEY_INTENT_SOURCE, StaticParams.SOURCE_PN_CHAT);
        resultIntent.putExtra(StaticParams.KEY_DIALOG_ID, dialogId);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(Application.getAppContext(), 0,
                        resultIntent, PendingIntent.FLAG_UPDATE_CURRENT
                );

        Notification notification = builder.setLargeIcon(BitmapFactory.decodeResource(Application.getAppContext().getResources(), R.drawable.app_icon))
                .setContentTitle("Real Agent")
                .setSmallIcon(R.drawable.logo_notification)
                .setContentText(msgText)
                .setContentIntent(resultPendingIntent)
                .setTicker("Real Agent - new message received")
                .setAutoCancel(true)
                .build();

        NotificationManager mNotificationManager = (NotificationManager) Application.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(Application.NOTIFICATION_ID, notification);

        try {
            Uri notifSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(Application.getAppContext(), notifSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
