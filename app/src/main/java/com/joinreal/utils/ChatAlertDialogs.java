package com.joinreal.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by Nick on 03/11/2014.
 */
public class ChatAlertDialogs {

    public static AlertDialog chooseDialog(Activity activity,String button1,DialogInterface.OnClickListener listener1,
                                           String button2,DialogInterface.OnClickListener listener2,
                                           String button3,DialogInterface.OnClickListener listener3){
        return new AlertDialog.Builder(activity)
                .setPositiveButton(button1, listener1)
                .setNegativeButton(button2,listener2)
                .setNeutralButton(button3, listener3)
                .show();
    }

    public static AlertDialog chooseDialog(Activity activity,String title,String button1,DialogInterface.OnClickListener listener1,
                                           String button2,DialogInterface.OnClickListener listener2){
        return new AlertDialog.Builder(activity)
                .setTitle(title)
                .setPositiveButton(button1, listener1)
                .setNegativeButton(button2, listener2)
                .show();
    }

    public static AlertDialog okDialog(Activity activity,String title,DialogInterface.OnClickListener listener1){
        return new AlertDialog.Builder(activity)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton("Ok", listener1)
                .show();
    }

}
