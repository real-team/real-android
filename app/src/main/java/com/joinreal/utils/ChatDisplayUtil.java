package com.joinreal.utils;

import android.content.res.Resources;
import android.graphics.Point;
import android.util.DisplayMetrics;

/**
 * Created by Yuriy Mysochenko on 06.05.2014.
 */
public final class ChatDisplayUtil {

    public static final float CONST_WIDTH = 720f;
    public static final float CONST_DPI = 2f;
    private static Point point;

    public static Point calculateHeykuViewSize() {
        if (point != null) {
            return point;
        }
        DisplayMetrics outMetrics = Resources.getSystem().getDisplayMetrics();
        int pxWidth = (int) (outMetrics.widthPixels * 0.9375);
        int pxHeight = (int) (outMetrics.widthPixels / 1.26);
        point = new Point(pxWidth, pxHeight);
        return point;
    }

    public static FloatPoint calculateHeykuViewSizeFloat() {
        DisplayMetrics outMetrics = Resources.getSystem().getDisplayMetrics();
        float pxWidth = outMetrics.widthPixels * 0.9375f;
        float pxHeight = outMetrics.widthPixels / 1.26f;
        return new FloatPoint(pxWidth, pxHeight);
    }

    public static float calculateEditTextWidth() {
        DisplayMetrics outMetrics = Resources.getSystem().getDisplayMetrics();
        int pxWidth = outMetrics.widthPixels;
        return (float)((pxWidth*0.9375) - (pxWidth / 18) - (pxWidth / 45));
    }

    public static float calculateTextSize() {
        DisplayMetrics outMetrics = Resources.getSystem().getDisplayMetrics();
        int pxWidth = outMetrics.widthPixels;
        return 36 * pxWidth/CONST_WIDTH;
    }

    public static float getScreenWidth() {
        DisplayMetrics outMetrics = Resources.getSystem().getDisplayMetrics();
        return outMetrics.widthPixels;
    }

    public static float getScreenHeight() {
        DisplayMetrics outMetrics = Resources.getSystem().getDisplayMetrics();
        return outMetrics.heightPixels;
    }

    public static float calculateDpi() {
        DisplayMetrics outMetrics = Resources.getSystem().getDisplayMetrics();
        return outMetrics.densityDpi/160;
    }

    public static float getStrokeWidth() {
        DisplayMetrics outMetrics = Resources.getSystem().getDisplayMetrics();
        return outMetrics.widthPixels * ((float) 11 / 601);
    }

    public static DisplayMetrics getDisplayMetrics(){
        return Resources.getSystem().getDisplayMetrics();
    }

    public static float dp(){
        DisplayMetrics metrics = getDisplayMetrics();
        return (float) metrics.densityDpi / 160f;
    }

    public static final class FloatPoint {

        public float x;
        public float y;

        public FloatPoint(float pxWidth, float pxHeight) {
            x = pxWidth;
            y = pxHeight;
        }
    }

}
