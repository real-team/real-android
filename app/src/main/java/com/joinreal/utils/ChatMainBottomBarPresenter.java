package com.joinreal.utils;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.activities.chat.PrivateChatActivity;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.Prefs;
import com.joinreal.app.common.StaticParams;
import com.joinreal.app.common.TypefaceManager;
import com.joinreal.app.interfaces.OpenChatListener;
import com.joinreal.events.ChatDialogsReceivedEvent;
import com.joinreal.model.data.Agent;
import com.joinreal.model.data.chat.ChatDialogDataVM;
import com.quickblox.core.helper.StringifyArrayList;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/24/15.
 */
public class ChatMainBottomBarPresenter {

    private ChatDialogDataVM dialog;
    private View newAgentMessageLayout;
    private View inputMessage;
    private ImageView agentImage;
    private TextView newMessageFrom;
    private TextView newMessage;
    private boolean shouldGetDialogs;
    private TextView unreadCount;
    private boolean firstInstall = false;
    private OpenChatListener listener;
    private TextView messageText;
    private boolean isAnimating;
    private boolean isAfterOnResume;

    public ChatMainBottomBarPresenter(OpenChatListener listener) {
        this.listener = listener;
    }

    public void init(View contentView) {
        contentView.setVisibility(View.VISIBLE);
        ChatConnect.initialize(getChatConnectListener());
        findViewsByIds(contentView);

        newAgentMessageLayout.setOnClickListener(getOpenChatOnClickListener());
        inputMessage.setOnClickListener(getOpenChatOnClickListener());
        inputMessage.setVisibility(View.GONE);
        messageText.setOnClickListener(getOpenChatOnClickListener());

        TypefaceManager.getInstance(contentView.getContext()).assignTypeface(contentView);
        setBottomBarContentFromPrefs();
    }

    private void setBottomBarContentFromPrefs() {
        List<ChatDialogDataVM> dialogsFromPrefs = Prefs.getDialogs();
        if (isPreviousChatExists(dialogsFromPrefs)) {
            dialog = dialogsFromPrefs.get(0);
            setBottomBarView();
        }
    }

    private boolean isPreviousChatExists(List<ChatDialogDataVM> dialogsFromPrefs) {
        return (dialogsFromPrefs != null) && (dialogsFromPrefs.size() > 0);
    }

    private View.OnClickListener getOpenChatOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setUnreadMessageCount(0);
                openPrivateChat();
            }
        };
    }

    private void findViewsByIds(View contentView) {
        newAgentMessageLayout = contentView.findViewById(R.id.new_agent_message_layout);
        inputMessage = contentView.findViewById(R.id.input_message);
        agentImage = (ImageView) contentView.findViewById(R.id.image);
        newMessage = (TextView) contentView.findViewById(R.id.message_text_from_agent);
        newMessageFrom = (TextView) contentView.findViewById(R.id.new_message_from);
        unreadCount = (TextView) contentView.findViewById(R.id.unread_agent_count);
        messageText = (TextView) contentView.findViewById(R.id.message_text);
    }

    private ChatConnectListener getChatConnectListener() {
        return new ChatConnectListener() {
            @Override
            public void connected() {
                if (!Prefs.isSubscribed()) {
                    Application.subscribeToPushNotifications();
                    firstInstall = true;
                }
                System.out.println("connected");
                listener.getDialogsFromNetwork();
            }

            @Override
            public void onError(List<String> errors) {

            }
        };
    }

    private void setBottomBarView() {
        if (dialog.getUnreadMessageCount() > 0) {
            handleUnreadMessages(dialog);
        } else {
//            if ((newAgentMessageLayout.getVisibility() == View.VISIBLE) && (!isAnimating)) {
//                slideOutNewAgentMsg();
//            }
            newAgentMessageLayout.setVisibility(View.GONE);
            inputMessage.setVisibility(View.VISIBLE);
            messageText.setVisibility(View.VISIBLE);

            messageText.setHint("Message " + Application.getClient().getAssignedAgent().getFirstName());
            messageText.setFocusable(false);
            messageText.setClickable(true);
        }
    }

    private void handleUnreadMessages(final ChatDialogDataVM dialog) {
        if (newAgentMessageLayout.getVisibility() != View.VISIBLE) {
            if (! isAnimating) {
                slideInNewAgentMsg(dialog);
            }
        } else {
            if ((unreadCount.getText() != null) && (! unreadCount.getText().equals(dialog.getUnreadMessageCount().toString()))) {
                updateUnreadMsgsCountAnim(dialog);
            }
        }

        if ((dialog.getLastMessage() != null) && (! dialog.getLastMessage().isEmpty())) {
            newMessage.setText(dialog.getLastMessage());
            newMessage.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else {
            newMessage.setText(Application.getAppContext().getResources().getString(R.string.property));
            newMessage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sent_property, 0, 0, 0);
        }

        Agent agent = Application.getClient().getAssignedAgent();
        newMessageFrom.setText(Application.getAppContext().getResources().getString(R.string.new_message_from) + " " + agent.getFirstname());
        Picasso.with(agentImage.getContext()).load(agent.getImage()).transform(new CircleTransform()).into(agentImage);
    }

    private void updateUnreadMsgsCountAnim(final ChatDialogDataVM dialog) {
        Animation fabOut = AnimationUtils.loadAnimation(unreadCount.getContext(), R.anim.fab_out);
        fabOut.setDuration(400);
        fabOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startFabInAnim(dialog);
            }

            @Override

            public void onAnimationRepeat(Animation animation) {

            }
        });
        unreadCount.startAnimation(fabOut);
    }

    private void startFabInAnim(final ChatDialogDataVM dialog) {
        Animation fabIn = AnimationUtils.loadAnimation(unreadCount.getContext(), R.anim.fab_in);
        fabIn.setDuration(400);
        fabIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                unreadCount.setText(dialog.getUnreadMessageCount().toString());
                unreadCount.setBackground(Helper.getTintedDrawable(R.drawable.circle, unreadCount.getContext().getResources().getColor(R.color.pink)));
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        unreadCount.startAnimation(fabIn);
    }

    private void slideInNewAgentMsg(final ChatDialogDataVM dialog) {
        Animation agentNewMsgAnim = AnimationUtils.loadAnimation(newAgentMessageLayout.getContext(), R.anim.in_from_bottom);
        agentNewMsgAnim.setInterpolator(new AccelerateInterpolator(1.0f));
        agentNewMsgAnim.setDuration(650);
        agentNewMsgAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isAnimating = true;
                newAgentMessageLayout.setVisibility(View.VISIBLE);
                unreadCount.setText(dialog.getUnreadMessageCount().toString());
                unreadCount.setBackground(Helper.getTintedDrawable(R.drawable.circle, unreadCount.getContext().getResources().getColor(R.color.pink)));
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                inputMessage.setVisibility(View.GONE);
                isAnimating = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        newAgentMessageLayout.startAnimation(agentNewMsgAnim);
    }

    private void slideOutNewAgentMsg() {
        isAnimating = true;
        Animation agentNewMsgAnim = AnimationUtils.loadAnimation(newAgentMessageLayout.getContext(), R.anim.out_to_bottom);
        agentNewMsgAnim.setInterpolator(new AccelerateInterpolator(1.0f));
        agentNewMsgAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                inputMessage.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                newAgentMessageLayout.setVisibility(View.GONE);
                isAnimating = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        newAgentMessageLayout.startAnimation(agentNewMsgAnim);
    }


    private void openPrivateChat() {
        if (dialog != null) {
            Intent intent = new Intent(Application.getAppContext(), PrivateChatActivity.class);
            intent.putExtras(getExtrasForPrivateChat());
            listener.openPrivateChat(intent);
        }

    }


    @Subscribe
    public void onNewDialogsRecievedEvent(ChatDialogsReceivedEvent e) {
        for (ChatDialogDataVM agentDialog : e.getDialogs()) {
            if ((ChatDialogsManager.getOpponentId(agentDialog.getQBDialog()) == Application.AGENT_ID) || (Application.FILTER_AGENT_DIALOG == false)) {
                handleAgentMessage(agentDialog);
                break;
            }
        }
        savePerviouslySentMessages();
    }

    private void savePerviouslySentMessages() {
        if (dialog != null) {
            ArrayList<ChatDialogDataVM> list = new ArrayList<>();
            list.add(dialog);
            Prefs.saveDialogs(list);
        }
    }

    private void handleAgentMessage(ChatDialogDataVM agentDialog) {
        dialog = agentDialog;
        setBottomBarView();
        onCustomerFirstMessageView();
    }

    private void onCustomerFirstMessageView() {
        if (firstInstall) {
            firstInstall = false;
            StringifyArrayList<Integer> ar = new StringifyArrayList<Integer>();
            ar.add(ChatDialogsManager.getOpponentId(dialog.getQBDialog()));
            ChatPushNotification.sendFirstInstallPush(ar, dialog.getDialogId());
        }
    }

    public void onNewMessageReceived() {
        listener.getDialogsFromNetwork();
    }

    public void onActivityResumed() {
        System.out.println("onActivityResumed, shouldGetDialogs ? " + shouldGetDialogs);
        isAfterOnResume = true;
        if (shouldGetDialogs) {
            listener.getDialogsFromNetwork();
        }
        shouldGetDialogs = true;
    }

    public Bundle getExtrasForPrivateChat() {
        Bundle bd = new Bundle();
        bd.putSerializable(PrivateChatActivity.DIALOG, dialog.getQBDialog());
        bd.putBoolean(StaticParams.KEY_SHOULD_ANIMATE, true);
        return bd;
    }
}
