package com.joinreal.utils;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.joinreal.app.Application;
import com.joinreal.app.common.Prefs;
import com.joinreal.model.data.Contact;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.customobjects.model.QBPermissions;
import com.quickblox.customobjects.model.QBPermissionsLevel;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nick on 29.04.2015.
 */
public class ChatConnect {

    public static final int AUTO_PRESENCE_INTERVAL_IN_SECONDS = 60;
    private static final String USER_LOGIN = "UserLogin";
    private static final java.lang.String USER_CLASSNAME = "UserInfo";
    private static final String LAST_ENTRANCE_TIME = "lastEntranceTime";
    private static boolean connected = false;

    public interface GetContactListener {
        void onContactFound(Contact contact);
        void onContactNotFound();
    }

    public interface GetUserListener {
        void onUserFound(String userLogin);
        void onUserNotFound();
    }


    public static void initialize(final ChatConnectListener callback) {
        //TODO: is this if needed?
//        if (!ContactManager.isContactsLoaded() && !ContactManager.isContactsLoading()) {
//            ContactManager.loadContactsInBackground(null);
//        }
        if (! QBChatService.isInitialized()) {
            initChatService(callback);
            createSession(callback);
        } else {
            callback.connected();
        }
    }

    public static void initChatService(ChatConnectListener callback) {
        QBChatService.setDebugEnabled(! Application.isProductionEnv());
        QBSettings.getInstance().fastConfigInit(Application.APP_ID, Application.AUTH_KEY, Application.AUTH_SECRET)
                .setServerApiDomain("apirealchatserver.quickblox.com")
                .setChatServerDomain("chatrealchatserver.quickblox.com")
                .setTurnServerDomain("turnserver.quickblox.com")
                .setContentBucketName("qb-realchatserver-s3");
        QBChatService.init(Application.getAppContext());
        QBChatService.getInstance().addConnectionListener(callback);
    }

    public static void createSession(ChatConnectListener callback) {
        if (Prefs.isUserRegistered()) {
            createSessionForRegisteredUser(callback);
        } else {
            createSessionForUnRegisteredUser(callback);
        }
    }

    public static void createSessionForUnRegisteredUser(ChatConnectListener callback) {
        if (! Application.getUserPhone().equals("")) {
            QBAuth.createSession(getCreateSessionCallbackForUnregisteredUser(callback));
        }
    }

    public static void createSessionForRegisteredUser(final ChatConnectListener callback) {
        //TODO: should create new user??
        QBUser user = new QBUser();
        user.setLogin(Application.getClient().getQBUserName());
        user.setPassword(Application.USER_PASSWORD);
        createSession(user, false, callback);
    }

    public static QBEntityCallbackImpl<QBSession> getCreateSessionCallbackForUnregisteredUser(final ChatConnectListener callback) {
        return new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle params) {
                createNewUser(callback);
            }

            @Override
            public void onError(List<String> errors) {
                callback.onError(errors);
            }
        };
    }


    private static void createNewUser(final ChatConnectListener callback) {
        // create QB user
        final QBUser newUser = new QBUser();
        System.out.println("createNewUser() login : " + Application.getClient().getQBUserName());
        newUser.setLogin(Application.getClient().getQBUserName());
        newUser.setPassword(Application.USER_PASSWORD);
        StringifyArrayList<String> tags = new StringifyArrayList<>();
        tags.add("mobile");
        newUser.setTags(tags);
        signUpUserToQB(callback, newUser);

        Prefs.setUserIsRegistered(true);
    }

    private static void signUpUserToQB(final ChatConnectListener callback, final QBUser newUser) {
        QBUsers.signUp(newUser, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
                // success
                Prefs.userCreated();
                createSession(newUser, false, callback);
            }

            @Override
            public void onError(List<String> errors) {
                // user already been signed up
                Log.w(Application.CHAT_TAG, "Contact can not be signed up" + errors);
                createSession(newUser, true, callback);
            }
        });
    }

    public static void createSession(final QBUser chatUser, final boolean shouldUpdateUser, final ChatConnectListener callback) {
        QBAuth.createSession(chatUser, new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle args) {
                // save current chatUser
                chatUser.setId(session.getUserId());

                if (shouldUpdateUser) {
                    QBUsers.getUserByLogin(chatUser.getLogin(), getUserByLoginCallback());
                }
                Application.setCurrentChatUser(chatUser);
                // login to Chat
                loginToChat(chatUser, callback);
            }

            @Override
            public void onError(List<String> errors) {
                callback.onError(errors);
            }
        });
    }

    public static QBEntityCallbackImpl<QBUser> getUserByLoginCallback() {
        return new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(final QBUser result, Bundle params) {
                super.onSuccess(result, params);
                if (result.getTags() != null && result.getTags().contains("sms")) {
                    Prefs.userCreated();
                }
                StringifyArrayList<String> tags = new StringifyArrayList<String>();
                tags.add("created");
                result.setTags(tags);
                QBUsers.updateUser(result, new QBEntityCallbackImpl<QBUser>() {
                    @Override
                    public void onSuccess(QBUser result, Bundle params) {
                        super.onSuccess(result, params);
                    }
                });
            }
        };
    }

    public static void loginToChat(final QBUser user, final ChatConnectListener callback) {

        if (! QBChatService.getInstance().isLoggedIn()) {
            System.out.println("login to chat : ");
            QBChatService.getInstance().login(user, new QBEntityCallbackImpl() {
                @Override
                public void onSuccess() {
                    // Start sending presences
                    try {
                        QBChatService.getInstance().startAutoSendPresence(AUTO_PRESENCE_INTERVAL_IN_SECONDS);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    connected = true;
                    callback.connected();
                }

                @Override
                public void onError(final List errors) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(errors);
                        }
                    });

                }
            });
        } else {
            callback.connected();
        }
    }

    public static boolean isConnected() {
        return connected;
    }


    public interface CreateChatListener {

        void dialogCreated();

        void onError(List<String> errors);

    }


    public interface UserResponseListener {

        void onSuccess(QBUser user, boolean created);

        void onError(List<String> errors);

    }

    public static void getUserOrCreate(final String login, final UserResponseListener listener) {
        QBUsers.getUserByLogin(login, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle params) {
                super.onSuccess(qbUser, params);
                listener.onSuccess(qbUser, false);
            }

            @Override
            public void onError(List<String> errors) {
                super.onError(errors);
                if (errors.contains("Entity you are looking for was not found.")) {
                    createNewQBUser(login, listener);
                } else {
                    listener.onError(errors);
                }
            }
        });
    }

    public static void createNewQBUser(String login, final UserResponseListener listener) {
        final QBUser qbUser = new QBUser();
        qbUser.setLogin(login);
        qbUser.setPassword(Application.USER_PASSWORD);
        StringifyArrayList<String> tags = new StringifyArrayList<String>();
        tags.add("sms");
        tags.add("mobile");
        qbUser.setTags(tags);
        QBUsers.signUp(qbUser, getCallbackForCreateNewUser(listener));
    }


    public static QBEntityCallbackImpl<QBUser> getCallbackForCreateNewUser(final UserResponseListener listener) {
        return new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
                // success
                Log.i(Application.CHAT_TAG, "Contact successfully signed up");
                listener.onSuccess(user, true);
            }

            @Override
            public void onError(List<String> errors) {
                // error
                Log.w(Application.CHAT_TAG, "Contact can not be signed up" + errors);
                listener.onError(errors);
            }
        };
    }



    public static boolean getNameAndPhotoFromLogin(String login, GetContactListener listener) {
//        final Contact result = new Contact();
        if ((Application.getCurrentChatUser() != null) && (login != null)) {
            triggerOnUserFound(login, listener);
                return true;
        }
        return false;
    }

    public static void getNameAndPhotoFromQBId(Integer id, final GetContactListener listener) {
        if (! getNameAndPhotoFromLogin(Application.getUsersPhones().getMap().get(id), listener)) { //TODO: make sure 1st arg is login and not phone to display
            QBUsers.getUser(id, new QBEntityCallbackImpl<QBUser>() {
                @Override
                public void onSuccess(final QBUser networkResult, Bundle params) {
                    super.onSuccess(networkResult, params);
                    Application.addUserPhone(networkResult.getId(), networkResult.getLogin());

                    triggerOnUserFound(networkResult.getLogin(), listener);
                }
            });
        }
    }

    public static void getUserById(int qbUserId, final GetUserListener listener) {
        QBUsers.getUser(qbUserId, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
                if (user != null) {
                    listener.onUserFound(user.getLogin());
                } else {
                    listener.onUserNotFound();
                }
            }

            @Override
            public void onError(List<String> errors) {
                listener.onUserNotFound();
            }
        });
    }

    public static void triggerOnUserFound(String login, GetContactListener listener) {
// TODO: check if OK that deleted!
// final Contact contact = new Contact(null);
//        contact.addPhone(phone);
//        String nameFromContacts = ContactManager.getNameByNumber(phone);
//        if (nameFromContacts != null) {
//            contact.setDisplayName(nameFromContacts);
//            contact.setPhoto(ContactManager.getImage(ContactManager.getContactByLogin(phone)));
//        }
        Contact contact = ContactManager.getContactByLogin(login);
        if (contact == null) {
            listener.onContactNotFound();
        } else {
            listener.onContactFound(contact);
        }
    }

    public static void getNameAndPhotoFromDialog(QBDialog dialog, final GetContactListener listener) {
        getNameAndPhotoFromQBId(ChatDialogsManager.getOpponentId(dialog), listener);
    }

    public interface LastEntranceCallback{

        void lastEntrance(Integer time);

    }

    public static void getLastEntranceTime(String login, final LastEntranceCallback callback){
        QBRequestGetBuilder builder = new QBRequestGetBuilder();
        builder.eq(USER_LOGIN, login);
        QBCustomObjects.getObjects(USER_CLASSNAME, builder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> result, Bundle params) {
                //     Log.d(Application.CHAT_TAG, result.toString());
                if (result != null && result.size() > 0) {
                    QBCustomObject field = result.get(0);
                    callback.lastEntrance(Integer.valueOf((String) field.getFields().get(LAST_ENTRANCE_TIME)));
                }
            }

            @Override
            public void onError(List<String> errors) {
                Log.d(Application.CHAT_TAG, errors.toString());
            }
        });
    }

    public static void updateEntranceTime(String login){
        QBRequestGetBuilder builder = new QBRequestGetBuilder();
        builder.eq(USER_LOGIN, login);
        QBCustomObjects.getObjects(USER_CLASSNAME, builder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> result, Bundle params) {
                //  Log.d(Application.CHAT_TAG, result.toString());
                if (result != null && result.size() > 0) {
                    QBCustomObject field = result.get(0);
                    field.putInteger(LAST_ENTRANCE_TIME, (int) (System.currentTimeMillis() / 1000));
                    QBCustomObjects.updateObject(field, new QBEntityCallbackImpl<QBCustomObject>() {
                        @Override
                        public void onSuccess(QBCustomObject result, Bundle params) {
                            super.onSuccess(result, params);
                            Log.d(Application.CHAT_TAG, result.toString());
                        }

                        @Override
                        public void onError(List<String> errors) {
                            Log.d(Application.CHAT_TAG, errors.toString());
                        }
                    });
                } else {
                    createUserInfo(Application.getCurrentChatUser().getLogin());
                }
            }

            @Override
            public void onError(List<String> errors) {
                Log.d(Application.CHAT_TAG, errors.toString());
            }
        });

    }

    public static void createUserInfo(String phone){
        QBCustomObject object = new QBCustomObject();
        object.setClassName(USER_CLASSNAME);
        object.putString(USER_LOGIN, phone);
        object.putInteger(LAST_ENTRANCE_TIME, (int) (System.currentTimeMillis()/1000));
        QBPermissions permissions = new QBPermissions();
        permissions.setReadPermission(QBPermissionsLevel.OPEN);
        permissions.setUpdatePermission(QBPermissionsLevel.OPEN);
        permissions.setDeletePermission(QBPermissionsLevel.OPEN);
        object.setPermission(permissions);
        QBCustomObjects.createObject(object, new QBEntityCallbackImpl<QBCustomObject>() {
            @Override
            public void onSuccess(QBCustomObject result, Bundle params) {
                super.onSuccess(result, params);
                Log.d(Application.CHAT_TAG, result.toString());
            }

            @Override
            public void onError(List<String> errors) {
                Log.d(Application.CHAT_TAG, errors.toString());
            }
        });
    }


}
