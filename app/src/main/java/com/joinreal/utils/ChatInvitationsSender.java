package com.joinreal.utils;

import com.joinreal.app.Application;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBPrivateChat;
import com.quickblox.chat.QBPrivateChatManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBMessageListener;
import com.quickblox.chat.listeners.QBPrivateChatManagerListener;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.util.List;

/**
 * Created by brittbarak on 7/13/15.
 */
public class ChatInvitationsSender {

    private QBPrivateChatManager privateChatManager;

    public ChatInvitationsSender() {

    }


    public void sendInvite(final String phoneNumberToInvite[], final String msg) {

        final QBMessageListener<QBPrivateChat> privateChatMessageListener = new QBMessageListener<QBPrivateChat>() {
            @Override
            public void processMessage(QBPrivateChat privateChat, final QBChatMessage chatMessage) {

            }

            @Override
            public void processError(QBPrivateChat privateChat, QBChatException error, QBChatMessage originMessage) {

            }

            @Override
            public void processMessageDelivered(QBPrivateChat privateChat, String messageID) {

            }

            @Override
            public void processMessageRead(QBPrivateChat privateChat, String messageID) {
            }
        };

        QBPrivateChatManagerListener privateChatManagerListener = new QBPrivateChatManagerListener() {
            @Override
            public void chatCreated(final QBPrivateChat privateChat, final boolean createdLocally) {
                if (!createdLocally) {
                    privateChat.addMessageListener(privateChatMessageListener);
                }
            }
        };
        QBChatService.getInstance().getPrivateChatManager().addPrivateChatManagerListener(privateChatManagerListener);


        final QBChatMessage chatMessage = new QBChatMessage();
        chatMessage.setBody(msg);
        chatMessage.setProperty("save_to_history", "1"); // Save a message to history
        privateChatManager = QBChatService.getInstance().getPrivateChatManager();
        for (String phoneNum : phoneNumberToInvite) {
            ChatConnect.getUserOrCreate(phoneNum, new ChatConnect.UserResponseListener() {
                @Override
                public void onSuccess(QBUser user, boolean created) {
                    try {
                        int opponentId = user.getId();

                        QBPrivateChat privateChat = privateChatManager.getChat(opponentId);
                        if (privateChat == null) {
                            privateChat = privateChatManager.createChat(opponentId, privateChatMessageListener);
                        }
                        privateChat.sendMessage(chatMessage);
                    } catch (
                            XMPPException e
                            )

                    {

                    } catch (
                            SmackException.NotConnectedException e
                            )

                    {

                    }

                }

                @Override
                public void onError(List<String> errors) {

                }
            });

        }
    }


    public ChatConnect.UserResponseListener getUserResponseListener(final String msg) {
        return new ChatConnect.UserResponseListener() {
            @Override
            public void onSuccess(QBUser user, boolean created) {
                System.out.println("UserResponseListener onSuccess, created ? " + created);
                Application.addUserPhone(user.getId(), user.getLogin());

                QBPrivateChat privateChat = privateChatManager.getChat(user.getId());
                if (privateChat == null) {
                    privateChat = privateChatManager.createChat(user.getId(), null);
                }
                try {
                    privateChat.sendMessage(msg);
                } catch (XMPPException e) {
                    e.printStackTrace();
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(List<String> errors) {
                System.out.println("UserResponseListener onError : " + errors.get(0));
            }
        };
    }


}
