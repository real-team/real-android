package com.joinreal.utils;


import com.joinreal.R;
import com.joinreal.app.Application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by igorkhomenko on 1/13/15.
 */
public class TimeStringUtils {
    public final static long ONE_SECOND = 1000;
    public final static long SECONDS = 60;

    public final static long ONE_MINUTE = ONE_SECOND * 60;
    public final static long MINUTES = 60;

    public final static long ONE_HOUR = ONE_MINUTE * 60;
    public final static long HOURS = 24;

    public final static long ONE_DAY = ONE_HOUR * 24;

    private TimeStringUtils() {
    }

    /**
     * converts time (in milliseconds) to human-readable format
     * "<w> days, <x> hours, <y> minutes and (z) seconds"
     */
    public static String millisToLongDHMS(long duration) {
        if (duration > 0) {
            duration = new Date().getTime() - duration;
        }
        if (duration < 0) {
            duration = 0;
        }

        StringBuffer res = new StringBuffer();
        long temp = 0;
        if (duration >= ONE_SECOND) {
            temp = duration / ONE_DAY;
            if (temp > 0) {
                duration -= temp * ONE_DAY;
                res.append(temp).append(" day").append(temp > 1 ? "s" : "")
                        .append(duration >= ONE_MINUTE ? ", " : "");
            }

            temp = duration / ONE_HOUR;
            if (temp > 0) {
                duration -= temp * ONE_HOUR;
                res.append(temp).append(" hour").append(temp > 1 ? "s" : "")
                        .append(duration >= ONE_MINUTE ? ", " : "");
            }

            temp = duration / ONE_MINUTE;
            if (temp > 0) {
                duration -= temp * ONE_MINUTE;
                res.append(temp).append(" minute").append(temp > 1 ? "s" : "");
            }

            if (!res.toString().equals("") && duration >= ONE_SECOND) {
                res.append(" and ");
            }

            temp = duration / ONE_SECOND;
            if (temp > 0) {
                res.append(temp).append(" second").append(temp > 1 ? "s" : "");
            }
            res.append(" ago");
            return res.toString();
        } else {
            return "0 second ago";
        }
    }

    public static void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String secondsToTime(long dateSent) {
        Calendar current = Calendar.getInstance();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateSent * 1000);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);

        if (current.get(Calendar.YEAR) > year || current.get(Calendar.DAY_OF_YEAR) > dayOfYear + 2) {
            SimpleDateFormat date = new SimpleDateFormat("M/d/yy");
            return date.format(dateSent * 1000);
        } else if (current.get(Calendar.DAY_OF_YEAR) == dayOfYear + 1) {
            return Application.getAppContext().getResources().getString(R.string.yesterday);
        } else {
            SimpleDateFormat date = new SimpleDateFormat("h:mm");
            return date.format(dateSent * 1000) + " " + ampm(calendar.get(Calendar.AM_PM));
        }
    }

    private static String ampm(int i) {
        if (i == 0) {
            return "AM";
        } else if (i == 1) {
            return "PM";
        }
        return "";
    }

    public static String secondsToTimeAgo(long dateSent) {
        long curTime = (System.currentTimeMillis() / 1000);
        long timeDif = curTime - dateSent;

        if (timeDif < 60)
            return "Now";
        else if (timeDif < 60 * 60)
            return (timeDif / 60) + "m ago";
        else if (timeDif < 60 * 60 * 24)
            return (timeDif / (60 * 60)) + "h ago";
        else if (timeDif < 60 * 60 * 24 * 7)
            return (timeDif / (60 * 60 * 24)) + "d ago";
        else if (timeDif < 60 * 60 * 24 * 30)
            return (timeDif / (60 * 60 * 24 * 7)) + "w ago";
        else if (timeDif < 60 * 60 * 24 * 365)
            return (timeDif / (60 * 60 * 24 * 30)) + "m ago";
        else
            return (timeDif / (60 * 60 * 24 * 365)) + "y ago";
    }

    public static String getDate(long time) {
        Calendar currentTime = Calendar.getInstance();

        Calendar messageTime = Calendar.getInstance();
        messageTime.setTimeInMillis(time);
        if (currentTime.get(Calendar.YEAR) == messageTime.get(Calendar.YEAR)) {
            if (currentTime.get(Calendar.DAY_OF_YEAR) == messageTime.get(Calendar.DAY_OF_YEAR)) {
                return "Today";
            }
            if (currentTime.get(Calendar.DAY_OF_YEAR) == messageTime.get(Calendar.DAY_OF_YEAR) + 1) {
                return "Yesterday";
            }
        }
        SimpleDateFormat month_date = new SimpleDateFormat("dd MMMM");
        return month_date.format(messageTime.getTime());
    }

    public static String secondsToTimeMessage(long dateSent) {
        SimpleDateFormat date = new SimpleDateFormat("h:mm a");
        return date.format(dateSent * 1000);
    }

    public static String secondsToDateHeader(long dateSent) {
        Calendar currentTime = Calendar.getInstance();

        Calendar messageTime = Calendar.getInstance();
        messageTime.setTimeInMillis(dateSent * 1000);
        if (currentTime.get(Calendar.DAY_OF_YEAR) == messageTime.get(Calendar.DAY_OF_YEAR)) {
            return "Today";
        }
        SimpleDateFormat date = new SimpleDateFormat("MMMM d, yyyy");
        return date.format(dateSent * 1000);
    }

    static SimpleDateFormat jsonDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
    static Calendar today = Calendar.getInstance();
    static Calendar parsedDate = Calendar.getInstance();
    static SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm");
    static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm");
    static SimpleDateFormat birthdayFormat = new SimpleDateFormat("MM/dd/yyyy");

    public static String parseDateFromJsonFormat(String toParse) {
        try {
            parsedDate.setTime(jsonDateFormat.parse(toParse.substring(0,23)));
            if (today.get(Calendar.YEAR) == parsedDate.get(Calendar.YEAR)
                    && today.get(Calendar.DAY_OF_YEAR) == parsedDate.get(Calendar.DAY_OF_YEAR)) {
                return "Today " + hourFormat.format(parsedDate.getTime());
            } else {
                return dateFormat.format(parsedDate.getTime());

            }
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public static String parseBirthdayFormatToDisplay(Calendar calendar) {
        return birthdayFormat.format(calendar.getTime());
    }

    public static String parseCalendarToJson(Calendar calendar) {
        return jsonDateFormat.format(calendar.getTime());

    }

    public static Calendar getCalendarFromJsonDate(String toParse) {
        try {
            parsedDate.setTime(jsonDateFormat.parse(toParse.substring(0,23)));
            return parsedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Calendar.getInstance();
    }

    public static String getDateForPropertyPage(String dateFromJson) {
        return (new SimpleDateFormat("MMMM d, yyyy")).format(TimeStringUtils.getCalendarFromJsonDate(dateFromJson).getTime());
    }

    public static String getDaysDifferenceFromToday(String dateFromJson) {
        long diff = today.getTimeInMillis() - TimeStringUtils.getCalendarFromJsonDate(dateFromJson).getTimeInMillis();
        return String.valueOf(diff / (24 * 60 * 60 * 1000));
    }

    public static String getCurrentTimeForJson(){
        return jsonDateFormat.format(new Date());
    }
}