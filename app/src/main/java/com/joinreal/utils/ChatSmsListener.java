package com.joinreal.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.joinreal.R;
import com.joinreal.app.Application;


/**
 * Listens to incoming Sms - for verification purposes
 */
public class ChatSmsListener extends BroadcastReceiver {
    private static BroadcastReceiver receiver = null;
    private static VerificationEvent verEvent = null;
    private SharedPreferences preferences;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            //---retrieve the SMS message received---
            if (bundle != null) try {
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                for (int i = 0; i < msgs.length; i++) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    String msgFrom = msgs[i].getOriginatingAddress();
                    String msgBody = msgs[i].getMessageBody();

                    Log.i(Application.CHAT_TAG, msgBody);

                    // send verification event
                    if (msgBody.startsWith(Application.getAppContext().getResources().getString(R.string.verification_code))) {
                        Log.i(Application.CHAT_TAG, "Received verification Sms");

                        if (verEvent != null)
                            verEvent.onVerified();
                    }
                }
            }
            catch (Exception e) {
                Log.e(Application.CHAT_TAG, "Exception caught" + e.getMessage());
            }
        }
    }

    public interface VerificationEvent{
        void onVerified();
    }

    public static void activate(VerificationEvent event) {
        if (receiver == null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.provider.Telephony.SMS_RECEIVED");

            receiver = new ChatSmsListener();
            Application.getAppContext().registerReceiver(receiver, filter);
            verEvent = event;
        }
    }

    public static void deactivate() {
        if (receiver != null) {
            Application.getAppContext().unregisterReceiver(receiver);
            receiver = null;
            verEvent = null;
        }
    }
}
