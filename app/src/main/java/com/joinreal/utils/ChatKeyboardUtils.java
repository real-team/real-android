package com.joinreal.utils;

import android.accessibilityservice.AccessibilityService;
import android.content.Context;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.joinreal.model.data.Contact;


public class ChatKeyboardUtils {

    public static void showKeyboard(View theView) {
        Context context = theView.getContext();
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.showSoftInput(theView, 0);
    }

    public static void showKeyboardDelayed(final View theView) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ChatKeyboardUtils.showKeyboard(theView);
            }
        }, 200);
    }

    public static void hideKeyboard(View theView) {
        Context context = theView.getContext();
        Object service = context.getSystemService(Context.INPUT_METHOD_SERVICE);

        InputMethodManager imm = (InputMethodManager) service;
        if (imm != null)
            imm.hideSoftInputFromWindow(theView.getWindowToken(), 0);
    }

    public static void hideKeyboardDelayed(final View theView) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ChatKeyboardUtils.hideKeyboard(theView);
            }
        }, 200);
    }

    public static boolean isKeyboardShown(Context context) {
        return context.getSystemService(Context.INPUT_METHOD_SERVICE) != null;


    }
}
