package com.joinreal.utils;

import android.net.Uri;

/**
 * Created by brittbarak on 6/30/15.
 */
public class MapUtils {
    private static final String GET_STATIC_MAP_PREFIX = "https://maps.googleapis.com/maps/api/staticmap?center=";
//    private static final String ICON_MARKER = "icon:http://d.joinreal.com/assets/markers/dot_default.png%7C";
    private static final String ICON_MARKER = "color:red%7C";
    private static final String STATIC_MAP_PARAMS = "&zoom=14&size=400x300&markers=" + ICON_MARKER;

    public static String getStaticMapUrl(Double lat, Double lng, String address) {
        String locationStrForMap;
        if ((lat != null) && (lng != null)) {
            locationStrForMap = lat + "," + lng;
        } else {
            locationStrForMap = address;
        }
        return GET_STATIC_MAP_PREFIX + Uri.encode(locationStrForMap) + STATIC_MAP_PARAMS + Uri.encode(locationStrForMap);
    }

}
