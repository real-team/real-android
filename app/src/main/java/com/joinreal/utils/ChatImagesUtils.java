package com.joinreal.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinreal.R;
import com.joinreal.app.Application;
import com.joinreal.app.common.Helper;
import com.joinreal.app.common.StaticParams;
import com.joinreal.events.StartActivityEvent;
import com.joinreal.model.data.Agent;
import com.joinreal.model.view.ContactVM;
import com.squareup.picasso.Picasso;

/**
 * Created by brittbarak on 7/23/15.
 */
public class ChatImagesUtils {

    private static CircleTransform circleTransform = new CircleTransform();

    public interface IImageObject {

        boolean hasImage();

        String getProfileImageFileName();

        int getColorResource();

        String getInitials();

        String getPhone();

        Bitmap getBitmap();

        String getDisplayName();

        String getId();

        String getChatLogin();
    }


    public static Bitmap getCircleBitmap(Bitmap bitmap, int imageSize) {
        Bitmap circleBitmap = Bitmap.createBitmap(imageSize, imageSize, Bitmap.Config.ARGB_8888);
        BitmapShader shader = new BitmapShader(bitmap,  Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setShader(shader);
        paint.setAntiAlias(true);
        Canvas c = new Canvas(circleBitmap);

        int radius = imageSize / 2;
//        circlePaint.setAntiAlias(true);
        c.drawCircle(radius, radius, radius, paint);
        return circleBitmap;
    }

    public interface ISelectImageListener {
        Context getContext();
        void onStartActivityForResult(StartActivityEvent event);

    }
    //TODO: unite with chat contacts adapter
    public static void setContactProfileThumb(IImageObject contactVM, ImageView thumb, TextView initials) {
        if (contactVM.hasImage()) {
            thumb.setVisibility(View.VISIBLE);
            initials.setVisibility(View.GONE);
            Picasso.with(thumb.getContext()).load(contactVM.getProfileImageFileName()).transform(circleTransform).into(thumb);
        } else {
            thumb.setVisibility(View.GONE);
            initials.setVisibility(View.VISIBLE);
            initials.setBackground(Helper.getTintedDrawable(R.drawable.circle, contactVM.getColorResource()));
            initials.setText(contactVM.getInitials());
        }
    }

    public static void setAgentProfileImage(Agent agent, ImageView thumb) {
        thumb.setVisibility(View.VISIBLE);
        if (agent.getNewProfileImage() != null) {
            thumb.setImageBitmap(agent.getNewProfileImage());
        } else if (agent.getImage() != null) {
            Picasso.with(thumb.getContext()).load(agent.getImage()).transform(circleTransform).into(thumb);
        }
    }

    private static int getColorId(String phone) {
        int index = (phone != null) ? Character.getNumericValue(phone.charAt(phone.length() - 1)) : 0;
        return Application.getAppContext().getResources().getIntArray(R.array.initials_colors)[index];
    }


    private static final int MAX_IMAGE_DIMENSION = 512;

    public static CircleTransform getCircleTransform() {
        return circleTransform;
    }

    public static void selectImage(final ISelectImageListener listener) {
        final CharSequence[] items = { "Take Photo", "Choose from Library"};
        AlertDialog.Builder builder = new AlertDialog.Builder(listener.getContext());
        builder.setTitle("Choose Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    listener.onStartActivityForResult(new StartActivityEvent(intent, StaticParams.REQUEST_CAMERA));
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    listener.onStartActivityForResult(new StartActivityEvent(Intent.createChooser(intent, "Select File"),
                            StaticParams.REQUEST_SELECT_FILE));
                }
            }
        });
        builder.show();
    }



//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            if (requestCode == REQUEST_CAMERA) {
//                File f = new File(Environment.getExternalStorageDirectory()
//                        .toString());
//                for (File temp : f.listFiles()) {
//                    if (temp.getName().equals("temp.jpg")) {
//                        f = temp;
//                        break;
//                    }
//                }
//                try {
//                    Bitmap bm;
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inSampleSize = 8;
//                    bm = BitmapFactory.decodeFile(f.getAbsolutePath(), options);
//                    bm = rotateImageIfRequired(this, bm, Uri.fromFile(f));
//                    capturedBitmap(bm);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else if (requestCode == SELECT_FILE) {
//                Uri selectedImageUri = data.getData();
//
//                Bitmap bm = null;
//
//                // from sd card
//                try {
//                    bm = getCorrectlyOrientedImage(this, selectedImageUri);
//                    if (bm == null) {
//                        tryGetFromInternet(selectedImageUri);
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    tryGetFromInternet(selectedImageUri);
//                }
//                if (bm != null) {
//                    capturedBitmap(bm);
//                }
//            }
//        }
//    }
//
//
//    public Bitmap tryGetFromInternet(Uri selectedImageUri) {
//        try {
//            InputStream imageStream = getContentResolver().openInputStream(selectedImageUri);
//            return BitmapFactory.decodeStream(imageStream);
//        } catch (Exception e) {
//            Log.e(Application.CHAT_TAG, "Can not load image from Internet");
//        }
//        return null;
//    }
//
//    public String getPath(Uri uri, Activity activity) {
//        String[] projection = {MediaStore.MediaColumns.DATA};
//        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        cursor.moveToFirst();
//        return cursor.getString(column_index);
//    }
//
//    /**
//     * Select image dialog - from camera or phone library
//     */
//    public void selectImage() {
//        final CharSequence[] items = {"Take a Photo", "Choose Existing Photo", "Cancel"};
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Choose an action");
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                if (item == 0) {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
//                    startActivityForResult(intent, REQUEST_CAMERA);
//                } else if (item == 1) {
//                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
//                    intent.setType("image/*");
//                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
////                    startActivityForResult(intent, SELECT_FILE);
//
//                } else if (items[item].equals("Cancel")) {
//                    dialog.dismiss();
//
//                }
//            }
//        });
//        builder.show();
//    }
//
//    public static Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
//        InputStream is = context.getContentResolver().openInputStream(photoUri);
//        BitmapFactory.Options dbo = new BitmapFactory.Options();
//        dbo.inJustDecodeBounds = true;
//        BitmapFactory.decodeStream(is, null, dbo);
//        is.close();
//
//        int rotatedWidth, rotatedHeight;
//        int orientation = getOrientation(context, photoUri);
//
//        if (orientation == 90 || orientation == 270) {
//            rotatedWidth = dbo.outHeight;
//            rotatedHeight = dbo.outWidth;
//        } else {
//            rotatedWidth = dbo.outWidth;
//            rotatedHeight = dbo.outHeight;
//        }
//
//        Bitmap srcBitmap;
//        is = context.getContentResolver().openInputStream(photoUri);
//        if (rotatedWidth > MAX_IMAGE_DIMENSION || rotatedHeight > MAX_IMAGE_DIMENSION) {
//            float widthRatio = ((float) rotatedWidth) / ((float) MAX_IMAGE_DIMENSION);
//            float heightRatio = ((float) rotatedHeight) / ((float) MAX_IMAGE_DIMENSION);
//            float maxRatio = Math.max(widthRatio, heightRatio);
//
//            // Create the bitmap from file
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inSampleSize = (int) maxRatio;
//            srcBitmap = BitmapFactory.decodeStream(is, null, options);
//        } else {
//            srcBitmap = BitmapFactory.decodeStream(is);
//        }
//        is.close();
//
//    /*
//     * if the orientation is not 0 (or -1, which means we don't know), we
//     * have to do a rotation.
//     */
//        if (orientation > 0) {
//            Matrix matrix = new Matrix();
//            matrix.postRotate(orientation);
//
//            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
//                    srcBitmap.getHeight(), matrix, true);
//        }
//
//        return srcBitmap;
//    }
//
//    /**
//     * Rotate an image if required.
//     *
//     * @param img
//     * @param selectedImage
//     * @return
//     */
//    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) {
//
//        // Detect rotation
//        int rotation = getRotation(context, selectedImage);
//        if (rotation != 0) {
//            Matrix matrix = new Matrix();
//            matrix.postRotate(rotation);
//            Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
//            img.recycle();
//            return rotatedImg;
//        } else {
//            return img;
//        }
//    }
//
//    /**
//     * Get the rotation of the last image added.
//     *
//     * @param context
//     * @param selectedImage
//     * @return
//     */
//    private static int getRotation(Context context, Uri selectedImage) {
//        ExifInterface ei = null;
//        try {
//            ei = new ExifInterface(selectedImage.getPath());
//            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//
//            switch (orientation) {
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    return 90;
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    return 180;
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    return 270;
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return 0;
//    }
//
//    public static int getOrientation(Context context, Uri photoUri) {
//    /* it's on the external media. */
//        Cursor cursor = context.getContentResolver().query(photoUri,
//                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
//
//        if (cursor == null || cursor.getCount() != 1) {
//            return -1;
//        }
//
//        cursor.moveToFirst();
//        return cursor.getInt(0);
//    }
//
//    public void capturedBitmap(Bitmap bitmap) {
//
//    }
}
