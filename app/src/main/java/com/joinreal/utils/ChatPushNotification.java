package com.joinreal.utils;

import android.os.Bundle;
import android.util.Log;

import com.joinreal.app.Application;
import com.joinreal.app.common.StaticParams;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.QBMessages;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBNotificationType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Nick on 30/03/2015.
 */
public class ChatPushNotification {

    public static void sendMessagePush(StringifyArrayList<Integer> userIds, String messageText, String dialogId, String actionStr){
        QBEvent event = new QBEvent();
        event.setUserIds(userIds);
        event.setEnvironment(Application.getQbEnv());
        event.setNotificationType(QBNotificationType.PUSH);
        try {
            JSONObject dialogData = new JSONObject();
            dialogData.put("dialogId", dialogId);
            dialogData.put("messageText", messageText);
            dialogData.put("messageSender", Application.getClient().getNameForPN());
            dialogData.put("message", Application.getClient().getNameForPN() + ": " + messageText);
            dialogData.put("isFromChat", true);
            if (actionStr != null) {
                dialogData.put("action", actionStr);
            }
            event.setMessage(dialogData.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        QBMessages.createEvent(event, new QBEntityCallbackImpl<QBEvent>() {
            @Override
            public void onSuccess(QBEvent qbEvent, Bundle args) {
                Log.d(Application.CHAT_TAG, "push success: " + qbEvent.toString());
            }

            @Override
            public void onError(List<String> errors) {
                Log.e(Application.CHAT_TAG, "push failure: " + errors.toString());
            }
        });
    }

    public static void sendMessagePush(StringifyArrayList<Integer> userIds, String messageText, String dialogId) {
        sendMessagePush(userIds, messageText, dialogId, null);
    }

    public static void sendFirstInstallPush(StringifyArrayList<Integer> ar, String dialogId) {
        sendMessagePush(ar, "One of your customers installed the app", dialogId, StaticParams.PN_ACTION_SYNC_CONTACTS);
    }

    public static void sendMessageRead(StringifyArrayList<Integer> userIds,String dialogId){
        QBEvent event = new QBEvent();
        event.setUserIds(userIds);
        event.setEnvironment(Application.getQbEnv());
        event.setNotificationType(QBNotificationType.PUSH);

        if (dialogId!=null) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("notificationType", "read message");
//                JSONObject dialogData = new JSONObject();
                obj.put("dialogId", dialogId);
                obj.put("isFromChat", false);
//                obj.put("data",dialogData.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            event.setMessage(obj.toString());
            QBMessages.createEvent(event, new QBEntityCallbackImpl<QBEvent>() {
                @Override
                public void onSuccess(QBEvent qbEvent, Bundle args) {
                    Log.d(Application.CHAT_TAG, "push success");
                }

                @Override
                public void onError(List<String> errors) {
                    Log.e(Application.CHAT_TAG, "push failure: " + errors.toString());
                }
            });
        }
    }


}
