package com.joinreal.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.util.Log;


import com.crashlytics.android.Crashlytics;
import com.joinreal.app.Application;
import com.joinreal.model.data.Contact;
import com.joinreal.model.view.ContactVM;
import com.joinreal.model.view.ViewModelItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nick on 17/11/2014.
 */
public class ContactManager {
    private static final String TAG = "ContactManager";
    private static final String LABEL_HOME = "Home";
    private static final String LABEL_MOBILE = "Mobile";
    private static final String LABEL_WORK = "Work";
    public static final String LABEL_OTHER = "Other";
    private static Map<String, Contact> loginToContactMap = new HashMap<>();
    private static List<Contact> contactList = new ArrayList<Contact>();
    //    private static Map<String, Contact> contactsMap = new HashMap<>();
    private static boolean contactsLoaded = false;
    private static boolean contactsLoading = false;
    private static final String[] PROJECTION = new String[]{
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.Data.CONTACT_ID,
            ContactsContract.Data.DATA1,
            ContactsContract.Data.DATA2,
            ContactsContract.Data.MIMETYPE,
            ContactsContract.Contacts.PHOTO_ID,
            ContactsContract.CommonDataKinds.Photo.PHOTO,
            ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
            ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
            ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME,
//            ContactsContract.CommonDataKinds.StructuredPostal.CITY,
//            ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY,
//            ContactsContract.CommonDataKinds.Str ucturedPostal.REGION,
            ContactsContract.RawContacts.VERSION
    };
    private static ArrayList<Contact> contactFromDevice;
//    private static HashMap<String, Contact> phoneToContactMapTemp;
    private static boolean didMergeContactOnce;
    private static ArrayList<Contact> contactsToSyncWithServer;
    private static Map<String, Integer> androidContactIdToVersion;

    public static void init(Context context) {
        Log.i("CONTACTS_TAG", " in init");
        setContactsLoading(true);
        contactFromDevice = new ArrayList<>();
        contactList = Application.getPrefs().getContactList();
        if (contactList != null) {
            for (Contact c : contactList) {
                if (c == null) {
                    Crashlytics.log("ContactManager.init() , Contact == null");
                    return;
                }
//                c.setChatLogin(ChatConnect.getChatLoginNumberFronNumber(c.getPhone()));
                loginToContactMap.put(c.getChatLogin(), c);
            }
        }
        Log.i("CONTACTS_TAG", " in init, done creating map");
        setContactsLoading(false);
        Application.getClient().syncContacts(context);
    }

    public static ArrayList<ViewModelItem> getViewModelsList() {
        ArrayList<ViewModelItem> list = new ArrayList<>();
        for (Contact contact : contactList) {
            if (contact.getContactId() != null) {
                list.add(new ContactVM(contact));
            }
        }
        return list;
    }

    public static ArrayList<ViewModelItem> getContactsWithAppViewModelsList() {
        ArrayList<ViewModelItem> list = new ArrayList<>();
        for (Contact contact : contactList) {
            if ((contact.getContactId() != null) && (contact.getLastSignInAt() != null)) {
                list.add(new ContactVM(contact));
            }
        }
        return list;
    }

    public static void setContacts(Contact[] contacts) {
        contactList = Arrays.asList(contacts);

    }

    public static void addContactsFromServer(Contact[] contactsFromServer) {
        Crashlytics.log(TAG + " addContactsFromServer contactsFromServer : " + (contactsFromServer == null ? "null" : contactsFromServer.length));
        Log.i("CONTACTS_TAG", " addContactsFromServer contactsFromServer : " + (contactsFromServer == null ? "null" : contactsFromServer.length));
        //we merge the data for a contact from server, only if the contact appears on the device contact list.
        //old contact's data is save on server and returns, but we won't display the contact on our list.
        for (Contact c : contactsFromServer) {
            loginToContactMap.put(c.getChatLogin(), c);
        }
//        contactList.addAll(Arrays.asList(contactsFromServer));
        contactList = new ArrayList<>(loginToContactMap.values());
        Collections.sort(contactList, new Comparator<Contact>() {
                    @Override
                    public int compare(Contact contact1, Contact contact2) {
                        return contact1.getDisplayName().compareTo(contact2.getDisplayName());
                    }
                });
        Crashlytics.log("Contacts after merge : " + contactList.size());
        Log.i("CONTACTS_TAG", "Contacts after merge : " + contactList.size());
        Application.getPrefs().setContactList(contactList);
        Application.getPrefs().setContactIdToVersionMap(androidContactIdToVersion);
        didMergeContactOnce = true;
    }

    private static Contact mergeContactWithServer(Contact contactToSave, Contact contactFromServer) {
        if (contactToSave == null) {
            contactToSave = contactFromServer;
        } else {
            contactToSave.setContactId(contactFromServer.getContactId());
            contactToSave.setLastSignInAt(contactFromServer.getLastSignInAt());
            contactToSave.setAgentNotes(contactFromServer.getAgentNotes());
            contactToSave.setAgentId(contactFromServer.getAgentId());
            contactToSave.setVersion(contactFromServer.getVersion());
        }
        return contactToSave;
    }

    public interface ContactsCallback {

        void loaded(ArrayList<Contact> contactsToSyncWithServer);

    }

    public static void loadContactsInBackground(final ContactsCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(Application.CHAT_TAG, "Loading chat_contacts");
                getModifiedContactsFromDevice(callback);
                Log.i(Application.CHAT_TAG, "Contacts are loaded");
            }
        }).start();
    }

    /**
     * Load user chat_contacts from the phone
     *
     * @param callback
     */
    public static void getModifiedContactsFromDevice(ContactsCallback callback) {
        androidContactIdToVersion = Application.getPrefs().getContactIdToVersionMap();
//        phoneToContactMapTemp = new HashMap<>();
        contactsLoaded = false;
        contactsLoading = true;
        Cursor cursor = getCursor();
        if (cursor != null) {
            setModifiedContactsFromQuery(cursor);
            cursor.close();
            contactsLoaded = true;
        }
        if (contactsToSyncWithServer.isEmpty()) {
            didMergeContactOnce = true;
        }
        if (callback != null) {
            callback.loaded(contactsToSyncWithServer);
        }
    }

    private static void setModifiedContactsFromQuery(Cursor cursor) {
        Crashlytics.log(TAG + " in setModifiedContactsFromQuery");
        Log.i("CONTACTS_TAG", "setModifiedContactsFromQuery : contactList.size() " + (contactList == null ? 0 : contactList.size()));

        contactsToSyncWithServer = new ArrayList<>();
        contactFromDevice = new ArrayList<>();

        final int nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        final int dataMimeTypeIndex = cursor.getColumnIndex(ContactsContract.Data.MIMETYPE);
        final int photoIndex = cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID);
        final int versionIndex = cursor.getColumnIndex(ContactsContract.RawContacts.VERSION);

        Contact contact = null;
        String androidContactId, prevAndroidContactId = null;
int prevContactVersion = 0;

        while (cursor.moveToNext()) {
            String name = cursor.getString(nameIndex);
            String dataMimeType = cursor.getString(dataMimeTypeIndex);
            int photoId = cursor.getInt(photoIndex);
            androidContactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
            int currVersion = cursor.getInt(versionIndex);

                if ((contact != null) && (contact.getDisplayName().equals(name))) {
                    editExistingContact(contact, dataMimeType, cursor);
                } else {
                    contact = addNewContact(cursor, versionIndex, name, dataMimeType, photoId);
                    prevAndroidContactId = androidContactId;
                    prevContactVersion = currVersion;
                    if ((androidContactIdToVersion.get(androidContactId) == null) || (currVersion > androidContactIdToVersion.get(androidContactId))) {
                        androidContactIdToVersion.put(prevAndroidContactId, prevContactVersion);
                        contactsToSyncWithServer.add(contact);
                    }

            }
        }
        Crashlytics.log(TAG + " after setModifiedContactsFromQuery");

    }

    @NonNull
    private static Contact addNewContact(Cursor cursor, int versionIndex, String name, String dataMimeType, int photoId) {
        Contact contact;
        contact = new Contact(name);
        contact.setAndroidContactId(cursor.getLong(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID)));
        initContact(contact, photoId, cursor.getInt(versionIndex));
        editExistingContact(contact, dataMimeType, cursor);
        contactFromDevice.add(contact);
        ifContactUpdatedPutInListToSendToServer(contact);
        return contact;
    }

    private static void ifContactUpdatedPutInListToSendToServer(Contact contactFromDevice) {
        if (contactFromDevice.getChatLogin() != null) {
            Contact contactFromCache = loginToContactMap.get(contactFromDevice.getChatLogin());
            if ((contactFromCache == null) || (contactFromDevice.getVersion() > contactFromCache.getVersion())) {
                contactsToSyncWithServer.add(contactFromDevice);
            }
        }
    }

    private static void initContact(Contact contact, int photoId, int version) {
        contact.setImageId(photoId);
        contact.setProfileImageFileName(getThumbUri(photoId).toString());
        contact.setPhoto(ContactManager.getImage(contact));
        contact.setVersion(version);


    }

    private static void editExistingContact(Contact contact, String dataMimeType, Cursor cursor) {
        final int dataValueIndex = cursor.getColumnIndex(ContactsContract.Data.DATA1);
        String dataValue = cursor.getString(dataValueIndex);
        if (dataMimeType.equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
            handlePhoneMimeType(contact, cursor, dataValue);
        } else if (dataMimeType.equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE) && (contact.getEmail() == null)) {
            contact.setEmail(dataValue);
        } else if (dataMimeType.equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)) {
            handleNameMimeType(contact, cursor);
        } else if (dataMimeType.equals(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)) {
            handleLocationMimeType(contact, cursor);
        }
    }

    private static void handleLocationMimeType(Contact contact, Cursor cursor) {
        String city = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
        contact.setCity(city);

        String country = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
        contact.setState(country);

        String zip = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
        contact.setZip(zip);
    }

    private static void handleNameMimeType(Contact contact, Cursor cursor) {
        String name;
        name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
        contact.setLastname(name);
        name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
        contact.setFirstname(name);
        name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME));
        contact.setMiddlename(name);
    }

    private static void handlePhoneMimeType(Contact contact, Cursor cursor, String dataValue) {
        int dataType = cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.DATA2));
//        if ((dataType == ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME) ||
//                (dataType == ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK) ||
//                (dataType == ContactsContract.CommonDataKinds.Phone.TYPE_OTHER_FAX)) {
//            contact.setFax(dataValue);
//        } else {
//            String normalizedPhone = normalizePhoneNumber(dataValue);
            contact.addPhone(dataValue, getLabelFromDataType(dataType));
//            contact.setChatLogin(ChatConnect.getChatLoginNumberFronNumber(normalizedPhone));
//            phoneToContactMapTemp.put(contact.getChatLogin(), contact);

//        }
    }

    private static String getLabelFromDataType(int dataType) {
        switch (dataType) {
            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                return LABEL_HOME;
            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                return LABEL_MOBILE;
            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                return LABEL_WORK;
            default:
                return LABEL_OTHER;
        }
    }


    private static Cursor getCursor() {
        ContentResolver resolver = Application.getAppContext().getContentResolver();
        Cursor cursor = resolver.query(
                ContactsContract.Data.CONTENT_URI,
                PROJECTION,
                ContactsContract.Data.HAS_PHONE_NUMBER + "!=0 AND (" + ContactsContract.Contacts.Data.MIMETYPE + "=? OR " + ContactsContract.Contacts.Data.MIMETYPE + "=? OR " + ContactsContract.Contacts.Data.MIMETYPE + "=?)",
                new String[]{ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE},
                ContactsContract.Contacts.DISPLAY_NAME + " ASC");
        return cursor;
    }


    public static void reloadContacts(Context context) {
        contactsLoaded = false;
        loginToContactMap.clear();
        contactList.clear();
        loadContactsInBackground(null);
    }


    public static Contact getContactByLogin(String login) {
        Contact contact = loginToContactMap.get(login);
        return contact;
    }

    public static boolean isContactsLoaded() {
//        return contactsLoaded;
        return false;
    }

    public static List<Contact> getContacts() {
        Crashlytics.log("ContactManager.getContacts : " + ((contactList == null) ? "null" : contactList.size()));
        return contactList;
    }

    private static final String[] PHOTO_BITMAP_PROJECTION = new String[]{
            ContactsContract.CommonDataKinds.Photo.PHOTO
    };

    private static final String[] PHOTO_ID_PROJECTION = new String[]{
            ContactsContract.Contacts.PHOTO_ID
    };

    private static Bitmap fetchThumbnail(final int thumbnailId) {
        if (thumbnailId <= 0) {
            return null;
        }
        final Uri uri = getThumbUri(thumbnailId);
        final Cursor cursor = Application.getAppContext().getContentResolver().query(uri, PHOTO_BITMAP_PROJECTION, null, null, null);

        try {
            Bitmap thumbnail = null;
            if (cursor.moveToFirst()) {
                final byte[] thumbnailBytes = cursor.getBlob(0);
                if (thumbnailBytes != null)
                    thumbnail = BitmapFactory.decodeByteArray(thumbnailBytes, 0, thumbnailBytes.length);
            }

            return thumbnail;
        } finally {
            cursor.close();
        }
    }

    public static Uri getThumbUri(int thumbnailId) {
        return ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, thumbnailId);
    }

    /**
     * Get contact image
     */
    public static Bitmap getImage(Contact contact) {
        try {
            if (contact != null) {
                return getImage(contact.getImageId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap getImage(int imageId) {
        Bitmap image = fetchThumbnail(imageId);

        if (image != null)
            return ChatPictureUtils.getRoundedBitmap(image);

        return null;
    }

    public static boolean isContactsLoading() {
        return contactsLoading;
    }

    public static void setContactsLoading(boolean contactsLoading) {
        ContactManager.contactsLoading = contactsLoading;
    }

    public static void sortContactsByName() {
        Collections.sort(contactList, new Comparator<Contact>() {

            @Override
            public int compare(Contact contact1, Contact contact2) {
                return contact1.getDisplayName().compareTo(contact2.getDisplayName());
            }
        });
//
    }

    public static ArrayList<Contact> getContactsToSyncWithServer() {
        return contactsToSyncWithServer;
    }

    public static boolean didMergeContactsOnce() {
        return didMergeContactOnce;
    }
}
