package com.joinreal.utils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.joinreal.app.Application;
import com.joinreal.app.common.BusProvider;
import com.joinreal.events.ChatDialogsReceivedEvent;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.squareup.otto.Produce;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brittbarak on 7/23/15.
 */
public class ChatDialogsManager {

    private static final int RECORDS_TO_RETRIEVE = 50;


    private static ChatDialogsReceivedEvent lastRecievedDialogsEvent;
    private static ChatDialogsManager instance;
    private static int triedLoadCount = 0;
    private boolean isBusRegistered;


    public static ChatDialogsManager getInstance() {
        if (instance == null) {
            instance = new ChatDialogsManager();
        }
        return instance;
    }

    protected ChatDialogsManager() {
        lastRecievedDialogsEvent = new ChatDialogsReceivedEvent(Application.getPrefs().getDialogs());
        lastRecievedDialogsEvent.setAreDialogsFromPrefs(true);
    }

    long getDialogsNetworkTime;
    public void getDialogsNetwork() {
        getDialogsNetworkTime = System.currentTimeMillis();
        QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
        customObjectRequestBuilder.setPagesLimit(RECORDS_TO_RETRIEVE);
        QBChatService.getChatDialogs(null, customObjectRequestBuilder, getFetchDialogsCallback());
    }

    private QBEntityCallbackImpl<ArrayList<QBDialog>> getFetchDialogsCallback() {
        return new QBEntityCallbackImpl<ArrayList<QBDialog>>() {


            @Override
            public void onSuccess(final ArrayList<QBDialog> dialogs, Bundle args) {
                System.out.println("-------getFetchDialogsCallback.onSuccess time : " + (System.currentTimeMillis() - getDialogsNetworkTime) + " size : " + dialogs.size());
                Crashlytics.logException(new Throwable("getFetchDialogsCallback.onSuccess time: " + (System.currentTimeMillis() - getDialogsNetworkTime) + " size : " + dialogs.size()));
                triedLoadCount = 0;
                lastRecievedDialogsEvent.setRawDialogs(dialogs);
                lastRecievedDialogsEvent.setAreDialogsFromPrefs(false);
                BusProvider.getBus().post(lastRecievedDialogsEvent);
            }

            @Override
            public void onError(final List<String> errors) {
                Crashlytics.logException(new Throwable("getFetchDialogsCallback.errors time: " + (System.currentTimeMillis() - getDialogsNetworkTime) + " error : " + (((errors != null) && (! errors.isEmpty())) ? errors.get(0) : "non")));
                onErrorFetchingDialogsFromNetwork(errors);
            }
        };
    }

    @Produce
    public ChatDialogsReceivedEvent produceAnswer() {
        System.out.println("ChatDialogsReceivedEvent produceAnswer from prefs ? " + lastRecievedDialogsEvent.areDialogsFromPrefs() + " , size : " + lastRecievedDialogsEvent.getDialogs().size());
        return lastRecievedDialogsEvent;
    }

    private boolean onErrorFetchingDialogsFromNetwork(final List<String> errors) {
        return new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Log.e(Application.CHAT_TAG, "load dialogs error: " + errors.toString());
                triedLoadCount++;
                if (triedLoadCount < 6) {
                    getDialogsNetwork();
                }
            }
        });
    }

    public static int getOpponentId(QBDialog dialog){
        System.out.println("Application.getCurrentChatUser() == null ? " +  (Application.getCurrentChatUser() == null));
        if (Application.getCurrentChatUser().getId() != null) {
            int currentId = Application.getCurrentChatUser().getId();
            for (int id : dialog.getOccupants()) {
                if (id != currentId) {
                    return id;
                }
            }
        }
        return -1;
    }

    public void registerBus() {
        if (! isBusRegistered) {
            BusProvider.getBus().register(this);
            isBusRegistered = true;
        }
    }

    public void unregisterBus() {
        if (isBusRegistered) {
            BusProvider.getBus().unregister(this);
            isBusRegistered = false;
        }
    }
}
