package com.joinreal.events;

/**
 * Created by britt on 6/25/15.
 */
public class MorePropertiessAddedEvent {
    private int itemsCount;

    public MorePropertiessAddedEvent(int itemsCount) {
        this.itemsCount = itemsCount;
    }

    public int getItemsCount() {
        return itemsCount;
    }
}
