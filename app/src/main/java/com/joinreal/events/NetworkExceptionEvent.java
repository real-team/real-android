package com.joinreal.events;

import com.joinreal.exceptions.NetworkException;

/**
 * Created by brittbarak on 8/16/15.
 */
public class NetworkExceptionEvent {
    private final NetworkException exception;
    private final int apiCallId;
    private boolean shouldReportToUser = true;
    private String requestBodyJson;

    public NetworkExceptionEvent(NetworkException exception, int apiCallId, String requestBodyJson) {
        this.exception = exception;
        this.apiCallId = apiCallId;
        this.requestBodyJson = requestBodyJson;
    }

    public NetworkExceptionEvent(NetworkException exception, int apiCallId) {
        this(exception, apiCallId, "");
    }

    public NetworkExceptionEvent(NetworkException exception, int apiCallId, boolean shouldReportToUser) {
        this.exception = exception;
        this.apiCallId = apiCallId;
        this.shouldReportToUser = shouldReportToUser;
        this.requestBodyJson = "";

    }

    public NetworkException getException() {
        return exception;
    }

    public int getApiCallId() {
        return apiCallId;
    }

    public String getBodyJson() {
        return requestBodyJson;
    }

    public boolean shouldReportToUser() {
        return shouldReportToUser;
    }

    public String getLocalizedMessage() {
        return (exception != null) ? exception.getMessage() : "";
    }
}
