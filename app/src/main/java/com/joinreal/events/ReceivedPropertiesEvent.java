package com.joinreal.events;

import com.google.gson.JsonObject;
import com.joinreal.model.data.PropertyThin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/17/15.
 */
public class ReceivedPropertiesEvent {
    private final JsonObject jsonOnRequestBody;
    ArrayList<PropertyThin> properties;

    public ReceivedPropertiesEvent(ArrayList<PropertyThin> propertyList, JsonObject jsonOnRequestBody) {
        this.properties = propertyList;
        this.jsonOnRequestBody = jsonOnRequestBody;
    }

    public ReceivedPropertiesEvent(ArrayList<PropertyThin> propertyList) {
        this(propertyList, null);
    }

    public ArrayList<PropertyThin> getProperties() {
        return properties;
    }

    public JsonObject getJsonOnRequestBody() {
        return jsonOnRequestBody;
    }
}
