package com.joinreal.events;

import android.text.Editable;

/**
 * Created by brittbarak on 7/7/15.
 */
public class SaveContactNotesEvent {
    private String notes;

    public SaveContactNotesEvent(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }
}
