package com.joinreal.events;

/**
 * Created by brittbarak on 8/17/15.
 */
public class SearchButtonVisibilityEvent {
    boolean shouldShowButton;

    public SearchButtonVisibilityEvent(boolean shouldShowButton) {
        this.shouldShowButton = shouldShowButton;
    }

    public boolean shouldShowButton() {
        return shouldShowButton;
    }

    public void setShouldShowButton(boolean shouldShowButton) {
        this.shouldShowButton = shouldShowButton;
    }
}