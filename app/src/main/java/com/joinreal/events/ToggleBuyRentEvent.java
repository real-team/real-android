package com.joinreal.events;

/**
 * Created by britt on 6/13/15.
 */
public class ToggleBuyRentEvent {
    private boolean isBuy;

    public ToggleBuyRentEvent(boolean isBuy) {
        this.isBuy = isBuy;
    }

    public boolean isBuySelected() {
        return isBuy;
    }
}
