package com.joinreal.events;

import com.joinreal.exceptions.NetworkException;

/**
 * Created by brittbarak on 7/19/15.
 */
public class TaskBackFromNetworkEvent {
    int taskCode;
    private NetworkException exception;

    public TaskBackFromNetworkEvent(int taskCode) {
        this.taskCode = taskCode;
    }

    public int getTaskCode() {
        return taskCode;
    }

    public void setException(NetworkException exception) {
        this.exception = exception;
    }

    public NetworkException getException() {
        return exception;
    }
}
