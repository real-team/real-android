package com.joinreal.events;

import com.joinreal.model.data.Contact;

/**
 * Created by brittbarak on 9/2/15.
 */
public class GetContactFromLoginEvent {
    private final Contact contact;
    private final int dialogVMIndex;

    public GetContactFromLoginEvent(Contact contact, int dialogVMIndex) {
        this.contact = contact;
        this.dialogVMIndex = dialogVMIndex;
    }

    public Contact getContact() {
        return contact;
    }

    public int getDialogVMIndex() {
        return dialogVMIndex;
    }
}
