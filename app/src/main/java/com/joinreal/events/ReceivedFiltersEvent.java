package com.joinreal.events;

import com.joinreal.model.data.SearchFilter;

/**
 * Created by brittbarak on 7/16/15.
 */
public class ReceivedFiltersEvent {
    private SearchFilter filter;

    public ReceivedFiltersEvent(SearchFilter filter) {
        this.filter = filter;
    }

    public SearchFilter getFilter() {
        return filter;
    }
}
