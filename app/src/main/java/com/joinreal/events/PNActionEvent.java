package com.joinreal.events;

/**
 * Created by brittbarak on 9/8/15.
 */
public class PNActionEvent {
    private String actionStr;

    public PNActionEvent(String actionStr) {
        this.actionStr = actionStr;
    }

    public String getActionStr() {
        return actionStr;
    }
}
