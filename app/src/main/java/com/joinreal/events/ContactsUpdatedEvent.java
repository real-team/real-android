package com.joinreal.events;

/**
 * Created by brittbarak on 7/14/15.
 */
public class ContactsUpdatedEvent {
    private boolean isUpdateComplete;

    public ContactsUpdatedEvent(boolean isUpdateComplete) {
        this.isUpdateComplete = isUpdateComplete;
    }

    public boolean isUpdateComplete() {
        return isUpdateComplete;
    }
}
