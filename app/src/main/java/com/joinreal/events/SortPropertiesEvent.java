package com.joinreal.events;

import com.google.gson.JsonElement;

/**
 * Created by brittbarak on 6/29/15.
 */
public class SortPropertiesEvent {
    public static final String SORT_BY_NEWEST = "newest";
    public static final String SORT_BY_CHEAPEST = "cheapest";
    public static final String SORT_BY_MOST_EXPENSIVE = "most_expensive";

    private String sortByValue;

    public SortPropertiesEvent(String sortByValue) {
        this.sortByValue = sortByValue;
    }

    public String getSortByValue() {
        return sortByValue;
    }
}
