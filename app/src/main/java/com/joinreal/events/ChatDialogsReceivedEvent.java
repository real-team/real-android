package com.joinreal.events;

import android.app.Dialog;

import com.joinreal.model.data.chat.ChatDialogDataVM;
import com.joinreal.utils.ChatConnect;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brittbarak on 7/14/15.
 */
public class ChatDialogsReceivedEvent {
    private ArrayList<QBDialog> rawDialogs;
    List<ChatDialogDataVM> dialogVMs;
    private int totalUnreadMessages = -1;
    private boolean areDialogsFromPrefs;

    public ChatDialogsReceivedEvent(List<ChatDialogDataVM> dialogVMs) {
        this.dialogVMs = (dialogVMs == null) ? new ArrayList<ChatDialogDataVM>() : dialogVMs;
    }

    public int getTotalUnreadMessages() {
        if (totalUnreadMessages < 0) {
            totalUnreadMessages = 0;
            if (rawDialogs != null) {
                for (QBDialog d : rawDialogs) {
                    totalUnreadMessages += d.getUnreadMessageCount();
                }
            } else if (dialogVMs != null) {
                for (ChatDialogDataVM d : dialogVMs) {
                    totalUnreadMessages += d.getUnreadMessageCount();
                }

            }
        }
        return totalUnreadMessages;
    }

    public void setRawDialogs(ArrayList<QBDialog> rawDialogs) {
        resetAllMembers();
        this.rawDialogs = rawDialogs;
    }

    private void resetAllMembers() {
        rawDialogs = null;
        dialogVMs = null;
        totalUnreadMessages = -1;
    }

    public List<ChatDialogDataVM> getDialogs() {
        if (dialogVMs == null) {
            if (rawDialogs != null) {
                dialogVMs = new ArrayList<>();
                for (QBDialog d : rawDialogs) {
                    dialogVMs.add(new ChatDialogDataVM(d));
                }
            }
        }
        return dialogVMs;
    }

    public void setDialogVMs(List<ChatDialogDataVM> dialogVMs) {
        resetAllMembers();
        this.dialogVMs = dialogVMs;
    }

    public boolean areDialogsFromPrefs() {
        return areDialogsFromPrefs;
    }

    public void setAreDialogsFromPrefs(boolean areDialogsFromPrefs) {
        this.areDialogsFromPrefs = areDialogsFromPrefs;
    }
}
