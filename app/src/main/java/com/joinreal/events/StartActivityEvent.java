package com.joinreal.events;

import android.content.Intent;

/**
 * Created by britt on 6/25/15.
 */
public class StartActivityEvent {
    private Intent intent;
    private boolean shouldStartActivityForResult;
    private int requestCode = -1;

    public StartActivityEvent(Intent intent) {
        this.intent = intent;
        this.shouldStartActivityForResult = false;
        this.requestCode = -1;
    }

    public StartActivityEvent(Intent intent, int requestCode) {
        this.intent = intent;
        this.requestCode = requestCode;
        this.shouldStartActivityForResult = true;
    }

    public Intent getIntent() {
        return intent;
    }

    public boolean shouldStartActivityForResult() {
        return shouldStartActivityForResult;
    }

    public int getRequestCode() {
        return requestCode;
    }
}
