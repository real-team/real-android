package com.joinreal.events;

import com.joinreal.model.data.SearchFilter;

/**
 * Created by brittbarak on 7/10/15.
 */
public class CustomerSearchFilterEvent {
    private SearchFilter filter;

    public CustomerSearchFilterEvent(SearchFilter filter) {

        this.filter = filter;
    }

    public SearchFilter getFilter() {
        return filter;
    }
}
