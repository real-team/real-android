package com.joinreal.events;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by britt on 6/24/15.
 */
public class OpenPropertyEvent {
    String mlsId;
    String mlsNum;
    private ArrayList<String> photos;

    public OpenPropertyEvent(String mlsId, String mlsNum, ArrayList<String> photos) {
        this.mlsId = mlsId;
        this.mlsNum = mlsNum;
        this.photos = photos;

    }

    public String getMlsId() {
        return mlsId;
    }

    public String getMlsNum() {
        return mlsNum;
    }

    public ArrayList<String> getPhotoList() {
        return photos;
    }
}
